#!/usr/bin/env ruby
#
# Config.rb -- Configure the Makefile
#
# Copyright (C) 2012-2014 trap15 <trap15@raidenii.net>
# All Rights Reserved.
#

require 'getoptlong'

# Initial setup
$debug = 0
$profiler = 0

$mobjs = Array.new
$mnobjs = Array.new
$maobjs = Array.new
$blobjs = Array.new
$hdrs = Array.new
$dirs = Array.new
$tgtname = Array.new
$tgtsrc = Array.new
$modules = Array.new
$devs = Array.new
$basedir = ""
$tgtobjs = ""

$defines = Array.new

def addDefine(name, val)
  $cflags += " -D" + name + "=" + val
  $tccflags += " -D" + name + "=" + val
end

if (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
  $platform = "win32"
  $posix = false
elsif (/darwin/ =~ RUBY_PLATFORM) != nil
  $platform = "macosx"
  $posix = true
elsif (/linux/ =~ RUBY_PLATFORM) != nil
  $platform = "linux"
  $posix = true
elsif (/bsd/ =~ RUBY_PLATFORM) != nil
  $platform = "bsd"
  $posix = true
end

$extrafiles = "src/modules.h src/buildconf.h src/devlist.h src/devlist.c"
$mkdir      = "mkdir -p"
$libs       = "-lm"
$cflags     = "-std=c99 -Wall -Wextra -Werror"
$cxxflags   = "-std=gnu++11"
$objcflags  = "-std=gnu99"
$objcxxflags= "-std=gnu++11"
$srcflags   = ""
$tccflags   = "-rdynamic -DTCC_COMPILE=1"
$archflags  = ""
$ldflags    = "-rdynamic"

# Platform handling
# All platforms support the default UI and OSD-SND
$ui_list = "cli"
$osdsnd_list = "sdl sdl2 file"

$ui = "cli"
$osd_snd = "sdl"

if $posix
  addDefine("POSIX", "1")
else
  addDefine("POSIX", "0")
end

if $platform == "win32"
  addDefine("WIN32", "1")
  $cc = "gcc"
  $ui_list += " phoenix"
else
  addDefine("WIN32", "0")
end

if $platform == "macosx"
  addDefine("MACOSX", "1")
  $cc = "gcc"
  $ui_list += " phoenix"

  $srcflags += " -I/usr/local/include" # WHY ISN'T THIS A DEFAULT DAMN IT
  $ldflags += " -L/usr/local/lib" # HURR
else
  addDefine("MACOSX", "0")
end

if $platform == "linux"
  addDefine("LINUX", "1")
  $cc = "gcc"
  $ui_list += " phoenix-gtk phoenix-qt"

  $osd_snd = "alsa"
  $osdsnd_list += " alsa"
else
  addDefine("LINUX", "0")
end

if $platform == "bsd"
  addDefine("BSD", "1")
  $cc = "gcc"
  $ui_list += " phoenix-gtk phoenix-qt"
else
  addDefine("BSD", "0")
end

# Option handling
opts = GetoptLong.new(
  [ '--help', '-h', GetoptLong::NO_ARGUMENT ],
  [ '--debug', GetoptLong::NO_ARGUMENT ],
  [ '--profiler', GetoptLong::NO_ARGUMENT ],
  [ '--osd-snd', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--ui', GetoptLong::REQUIRED_ARGUMENT ]
)

def print_help()
      printf "
config.rb [OPTION]
"
      printf "
-h, --help:
    show help.
"
      printf "
--debug:
    create a debug build.
"

      printf "
--profiler:
    enable the emulated CPU profiler.
"

      printf "
--osd-snd osdname:
    specify the OSD to use for sound.
    valid osdnames are:
        %s

    defaults to:
        %s
", $osdsnd_list, $osd_snd

      printf "
--ui uiname:
    specify the UI to use.
    valid uinames are:
        %s

    defaults to:
        %s
", $ui_list, $ui

      printf "\n"
end

begin
opts.each do |opt, arg|
  case opt
    when '--help'
      exit 0
    when '--debug'
      $debug = 1
    when '--profiler'
      $profiler = 1
    when '--osd-snd'
      $osd_snd = arg
    when '--ui'
      $ui = arg
  end
end
rescue Exception
  print_help()
  exit 1
end

def addDev(dev)
  $devs.push(dev)
end

def addObj(file)
  $mobjs.push($basedir + file)
end

def addMainObj(file)
  $mnobjs.push($basedir + file)
end

def addAltObj(file)
  $maobjs.push($basedir + file)
end

def addBuildObj(file)
  $blobjs.push($basedir + file)
end

def addFullDir(dir)
  $dirs.push(dir)
end

def addDir(dir)
  addFullDir("obj/" + dir)
end

def addSdkDir(dir)
  addFullDir("sdk/" + dir)
end

def addHdr(file)
  $hdrs.push($basedir + file)
end

def addModule(dir, name)
  addSdkDir(dir + "/" + name)
  addDir(dir + "/" + name)
  olddir = $basedir
  $basedir = dir + "/" + name + "/"
  $modules.push($basedir + name + ".h")
  addHdr(name + ".h")
  require './src/' + $basedir + name + '.rb'
  $basedir = olddir
end

def addModuleNoHdr(dir, name)
  addDir(dir + "/" + name)
  olddir = $basedir
  $basedir = dir + "/" + name + "/"
  require './src/' + $basedir + name + '.rb'
  $basedir = olddir
end

def addMainTarget(name, src="")
  addTarget(name, "$(MAINOBJS) "+src)
end

def addAltTarget(name, src="")
  addTarget(name, "$(ALTOBJS) "+src)
end

def addTarget(name, src="")
  $tgtname.push(name)
  $tgtsrc.push(src)
  $tgtobjs += " " + src
end

def emitObjs
  $f.write("OBJECTS  = \\\n")
  i = 0
  for obj in $mobjs
    $f.write(" obj/"+obj);
    i += 1
    if i == 4
      $f.write(" \\\n")
      i = 0
    end
  end
  $f.write(" \\\n")
  $f.write("\n")
end

def emitMainObjs
  $f.write("MAINOBJS  = \\\n")
  i = 0
  for obj in $mnobjs
    $f.write(" obj/"+obj);
    i += 1
    if i == 4
      $f.write(" \\\n")
      i = 0
    end
  end
  $f.write(" \\\n")
  $f.write("\n")
end

def emitAltObjs
  $f.write("ALTOBJS  = \\\n")
  i = 0
  for obj in $maobjs
    $f.write(" obj/"+obj);
    i += 1
    if i == 4
      $f.write(" \\\n")
      i = 0
    end
  end
  $f.write(" \\\n")
  $f.write("\n")
end

def emitBuildObjs
  $f.write("BUILDOBJS  = $(ALTOBJS) $(MAINOBJS) $(OBJECTS) \\\n")
  i = 0
  for obj in $blobjs
    $f.write(" obj/"+obj);
    i += 1
    if i == 4
      $f.write(" \\\n")
      i = 0
    end
  end
  $f.write(" \\\n")
  $f.write("\n")
end

def emitDirs
  $f.write("dirs:")
  for dir in $dirs
    $f.write(" "+dir)
  end
  $f.write("\n")
  for dir in $dirs
    $f.write(dir+":\n")
    $f.write("\t"+$mkdir+" "+dir+"\n")
  end
  $f.write("\n")
end

def emitOutput
  $f.write("OUTPUT    = \\\n")
  i = 0
  for out in $tgtname
    $f.write(" "+out);
    i += 1
    if i == 4
      $f.write(" \\\n")
      i = 0
    end
  end
  $f.write(" \\\n")
  $f.write("\n")
end

def emitTargets
  i = 0
  for out in $tgtname
    $f.write(out+": "+$tgtsrc[i]+" $(OBJECTS)\n")
    $f.write("\t$(CC) "+$tgtsrc[i]+" $(OBJECTS) $(LDFLAGS) -o $@\n")
    i += 1
  end
  $f.write("\n")
end

def emitModules
  for mod in $modules
    $f.write("#include \"" + mod + "\"\n")
  end
end

def emitDevs
  for dev in $devs
    $f.write("  &"+dev+"_def,\n")
  end
end

def emitHdrs
  for hdr in $hdrs
    $f.write("\tcp src/"+hdr+" sdk/"+hdr+"\n")
  end
end

# Interpret OSD setting
addModuleNoHdr("osd", "snd")

# Interpret UI setting
addModuleNoHdr(".", "ui")

# Interpret debug setting
if $debug == 1
  addDefine("DEBUG", "1")
  $cflags  += " -g -O0"
  $ldflags += " -g"
else
  $cflags  += " -O2"
end

$cflags += $archflags
$tccflags += $archflags

require './mklist.rb'

printf "Configuration:
DEBUG=%d
UI=%s
OSD_SND=%s
CFLAGS=%s
CXXFLAGS=%s
OBJCFLAGS=%s
OBJCXXFLAGS=%s
SRCFLAGS=%s
LDFLAGS=%s
LIBS=%s
", $debug, $ui, $osd_snd, $cflags, $cxxflags, $objcflags, $objcxxflags, $srcflags, $ldflags, $libs

######### Create buildconf
$f = File.open("src/buildconf.h", "w")
$f.write("#ifndef _BUILDCONF_H_\n")
$f.write("#define _BUILDCONF_H_\n\n")

$f.write("#define BUILD_CFLAGS \""+$cflags+"\"\n")
$f.write("#define TCC_CFLAGS \""+$tccflags+"\"\n\n")
$f.write("#define ENDIAN_BE 0\n")
$f.write("#define ENDIAN_LE 1\n\n")

$f.write("#define CPU_PROFILER "+$profiler.to_s+"\n\n")

$f.write("#endif\n\n")
$f.close

addHdr("buildconf.h")

######### Create modules
$f = File.open("src/modules.h", "w")
$f.write("#ifndef _MODULES_H_\n")
$f.write("#define _MODULES_H_\n\n")

emitModules

$f.write("\n#endif\n\n")
$f.close

addHdr("modules.h")

######### Create devlist
$f = File.open("src/devlist.h", "w")
$f.write("#ifndef _DEVLIST_H_\n")
$f.write("#define _DEVLIST_H_\n\n")

$f.write("extern const struct hien_dev_info_t *devlist[];\n")
$f.write("extern const size_t devlistcnt;\n")

$f.write("\n#endif\n\n")
$f.close

$f = File.open("src/devlist.c", "w")
$f.write("#include \"hien.h\"\n\n")

$f.write("const hien_dev_info_t *devlist[] = {\n")
emitDevs
$f.write("  NULL,\n")
$f.write("};\n\n")
$f.write("const size_t devlistcnt = "+$devs.size.to_s+";\n\n")

$f.close

addHdr("devlist.h")
addObj("devlist.o")

######### Create Makescript
$f = File.open("Makescript", "w")
$f.write("CC           = $(PREFIX)"+$cc+"\n")
$f.write("COPY         = cp\n")
emitObjs
emitMainObjs
emitAltObjs
emitBuildObjs
$f.write("\n")
emitOutput
$f.write("INCLUDE     += -Isrc/\n")
$f.write("LIBS        += "+$libs+"\n")
$f.write("CFLAGS      += "+$cflags+" "+$srcflags+" $(INCLUDE)\n")
$f.write("CXXFLAGS    += "+$cxxflags+" "+$srcflags+" $(INCLUDE)\n")
$f.write("OBJCFLAGS   += "+$objcflags+" "+$srcflags+" $(INCLUDE)\n")
$f.write("OBJCXXFLAGS += "+$objcxxflags+" "+$srcflags+" $(INCLUDE)\n")
$f.write("LDFLAGS     += "+$ldflags+" $(LIBS)\n")
$f.write("\n");

$f.write(".PHONY: all sdkhdr clean release\n\n")

$f.write("all: dirs sdkhdr $(BUILDOBJS) $(OUTPUT)")
if $platform == "macosx"
  $f.write(" osxbundle")
end
$f.write("\n\n")

emitDirs
$f.write("obj/%.o: src/%.c\n")
$f.write("\t$(CC) $(CFLAGS) -c -o $@ $<\n")
$f.write("obj/%.o: src/%.cpp\n")
$f.write("\t$(CC) $(CXXFLAGS) -c -o $@ $<\n")
$f.write("obj/%.o: src/%.m\n")
$f.write("\t$(CC) $(OBJCFLAGS) -c -o $@ $<\n")
$f.write("obj/%.o: src/%.mm\n")
$f.write("\t$(CC) $(OBJCXXFLAGS) -c -o $@ $<\n")
emitTargets

$f.write("sdkhdr:\n")
emitHdrs
$f.write("osxbundle:\n")
$f.write("\t$(COPY) hien hien.app/Contents/MacOS/hien\n")
$f.write("\t$(COPY) -r sdk hien.app/Contents/Resources/\n")
$f.write("clean:\n")
$f.write("\t$(RM) $(OUTPUT) $(BUILDOBJS) hien.app/Contents/MacOS/hien\n")
$f.write("release: clean\n")
$f.write("\t$(RM) -r obj sdk "+$extrafiles+"\n")

$f.write("\n")
$f.close
