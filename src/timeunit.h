/**********************************************************
*
* timeunit.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
***********************************************************
*
* 1 timetick = 100   attosecond
*              0.1   femtosecond
*              1e-14 second
*
* Not suitable for counting longer than 15 minutes
*
* Timeticks are stored in a friendly manner, allowing
* normal math functions to work fine.
*
* Do not assume the length of a timetick, instead use the
* helpful macros.
*
* Always use timetick_t for holding timetick values.
*
* hz_t can be used for holding frequency values.
*
**********************************************************/

#ifndef _TIMEUNIT_H_
#define _TIMEUNIT_H_

/* Defined conversion units */
#define TIMETICK_PER_FEMTOSECOND    ((timetick_t)10)
#define TIMETICK_PER_PICOSECOND     (TIMETICK_PER_FEMTOSECOND * 1000)
#define TIMETICK_PER_NANOSECOND     (TIMETICK_PER_PICOSECOND * 1000)
#define TIMETICK_PER_MICROSECOND    (TIMETICK_PER_NANOSECOND * 1000)
#define TIMETICK_PER_MILLISECOND    (TIMETICK_PER_MICROSECOND * 1000)
#define TIMETICK_PER_SECOND         (TIMETICK_PER_MILLISECOND * 1000)

#define TIMETICK_MAX                ((timetick_t)0x7FFFFFFFFFFFFFFF)
#define TIMETICK_MIN                (-TIMETICK_MAX)

#define timetick_never              ((timetick_t)-0x8000000000000000)
#define timetick_zero               ((timetick_t)0)

/* Conversions */
#define TIMETICK_TO_HZ(x)           (TIMETICK_PER_SECOND / (x))
#define HZ_TO_TIMETICK(x)           ((timetick_t)(TIMETICK_PER_SECOND / (x)))

typedef int64_t timetick_t;
typedef uint64_t hz_t;

#endif

