/**********************************************************
*
* timer.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

#define TIMER_MGR_BASE_COUNT (4)

/******************************* Single Timer ********************************/
hien_timer_t* tmr_new(void)
{
  hien_timer_t* tmr;
  tmr = mem_alloc_type(hien_timer_t);

  tmr->mgr = NULL;
  tmr->running = FALSE;
  tmr->interval = timetick_never;
  tmr->value = timetick_zero;
  tmr->oneshot = FALSE;
  tmr->callback = NULL;
  tmr->cb_data = NULL;
  tmr->hits = 0;

  return tmr;
}

void tmr_delete(hien_timer_t* tmr)
{
  mem_free(tmr);
}

void tmr_update(hien_timer_t* tmr, timetick_t ticks)
{
  if(!tmr->running)
    return;

  tmr->value += ticks;

  if(tmr->interval == timetick_never)
    return;

  while(tmr->value >= tmr->interval && tmr->running) { /* Timer has fired */
    if(tmr->oneshot) {
      tmr->running = FALSE;
    }
    tmr->value -= tmr->interval;
    tmr->callback(tmr, tmr->cb_data);
    tmr->hits++;
  }
}

void tmr_enable(hien_timer_t* tmr, int enable)
{
  tmr->running = enable;
}

int tmr_enabled(hien_timer_t* tmr)
{
  return tmr->running;
}

void tmr_set_callback(hien_timer_t* tmr, hien_timer_cb_t cb, void* data)
{
  tmr->callback = cb;
  tmr->cb_data = data;
}

hien_timer_cb_t tmr_get_callback(hien_timer_t* tmr)
{
  return tmr->callback;
}

void* tmr_get_callback_data(hien_timer_t* tmr)
{
  return tmr->cb_data;
}

void tmr_reset(hien_timer_t* tmr, timetick_t value)
{
  tmr->value = value;
}

void tmr_set_oneshot(hien_timer_t* tmr, timetick_t interval)
{
  tmr->oneshot = TRUE;
  tmr->interval = interval;
}

void tmr_set_periodic(hien_timer_t* tmr, timetick_t interval)
{
  tmr->oneshot = FALSE;
  tmr->interval = interval;
}

/******************************* Timer Manager *******************************/
hien_timer_mgr_t* tmr_mgr_new(hien_vm_t* vm)
{
  hien_timer_mgr_t* mgr;
  mgr = mem_alloc_type(hien_timer_mgr_t);
  mgr->vm = vm;

  mgr->timers = kuso_pvec_new(TIMER_MGR_BASE_COUNT);

  return mgr;
}

void tmr_mgr_delete(hien_timer_mgr_t* mgr)
{
  hien_timer_t* tmr;
  while((tmr = kuso_pvec_pop(mgr->timers))) {
    tmr_delete(tmr);
  }
  kuso_pvec_delete(mgr->timers);
  mem_free(mgr);
}

void tmr_mgr_add(hien_timer_mgr_t* mgr, hien_timer_t* tmr)
{
  kuso_pvec_append(mgr->timers, tmr);
  tmr->mgr = mgr;
}

void tmr_mgr_remove(hien_timer_mgr_t* mgr, hien_timer_t* tmr)
{
  size_t i;

  hien_timer_t** iter = kuso_pvec_iter(mgr->timers, hien_timer_t*);
  for(i = 0; *iter != NULL; iter++, i++) {
    if(*iter == tmr) {
      kuso_pvec_remove(mgr->timers, i);
      break;
    }
  }
}

void tmr_mgr_update(hien_timer_mgr_t* mgr, timetick_t ticks)
{
  hien_timer_t** iter = kuso_pvec_iter(mgr->timers, hien_timer_t*);
  for(; *iter != NULL; iter++) {
    tmr_update(*iter, ticks);
  }
}
