/**********************************************************
*
* dev/nmk901/nmk901.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

typedef struct dev_nmk901_t dev_nmk901_t;

struct dev_nmk901_t {
  int foo;
};

static void dev_nmk901_delete(hien_dev_t* dev)
{
  dev_nmk901_t* nmk901;
  nmk901 = dev->data;

  mem_free(nmk901);
}

hien_dev_t* dev_nmk901_new(hien_vm_t* vm, char* name, hz_t clock)
{
  hien_dev_t* dev;
  dev_nmk901_t* nmk901;

  nmk901 = mem_alloc_type(dev_nmk901_t);

  dev = dev_new(vm, name, clock);
  dev->delete = dev_nmk901_delete;
  dev->data   = nmk901;

  return dev;
}

hien_dev_info_t dev_nmk901_def = {
  .type = "NMK901",
  .ctor = "dev_nmk901_new",
  .use_clk = TRUE,
};
