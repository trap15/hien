/**********************************************************
*
* dev/m68000/m68000.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

typedef struct dev_m68000_t dev_m68000_t;

struct dev_m68000_t {
  int foo;
};

static void dev_m68000_delete(hien_dev_t* dev)
{
  dev_m68000_t* m68000;
  m68000 = dev->data;

  mem_free(m68000);
}

hien_dev_t* dev_m68000_new(hien_vm_t* vm, char* name, hz_t clock)
{
  hien_dev_t* dev;
  dev_m68000_t* m68000;

  m68000 = mem_alloc_type(dev_m68000_t);

  dev = dev_new(vm, name, clock);
  dev->delete = dev_m68000_delete;
  dev->data   = m68000;

  return dev;
}

hien_dev_info_t dev_m68000_def = {
  .type = "68000",
  .ctor = "dev_m68000_new",
  .use_clk = TRUE,
};
