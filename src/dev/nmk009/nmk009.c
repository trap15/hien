/**********************************************************
*
* dev/nmk009/nmk009.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

typedef struct dev_nmk009_t dev_nmk009_t;

struct dev_nmk009_t {
  int foo;
};

static void dev_nmk009_delete(hien_dev_t* dev)
{
  dev_nmk009_t* nmk009;
  nmk009 = dev->data;

  mem_free(nmk009);
}

hien_dev_t* dev_nmk009_new(hien_vm_t* vm, char* name, hz_t clock)
{
  hien_dev_t* dev;
  dev_nmk009_t* nmk009;

  nmk009 = mem_alloc_type(dev_nmk009_t);

  dev = dev_new(vm, name, clock);
  dev->delete = dev_nmk009_delete;
  dev->data   = nmk009;

  return dev;
}

hien_dev_info_t dev_nmk009_def = {
  .type = "NMK009",
  .ctor = "dev_nmk009_new",
  .use_clk = TRUE,
};
