/**********************************************************
*
* dev/printer/printer.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

static void dev_printer_reset(hien_dev_t* dev)
{
  (void)dev;
}
static void dev_printer_delete(hien_dev_t* dev)
{
  (void)dev;
}
static void dev_printer_data(hien_dev_t* dev, uintmax_t data, uintmax_t mask)
{
  (void)dev;
  printf("%c", (char)(data & mask));
  fflush(stdout);
}

hien_dev_t* dev_printer_new(hien_vm_t* vm, char* name)
{
  hien_dev_t* dev;

  dev = dev_new(vm, name, 0);
  dev->delete = dev_printer_delete;
  dev->reset = dev_printer_reset;

  dev_port_setup(dev, PRINTER_PORT_DATA, 8, PORT_INPUT);
  dev_port_set_callbacks(dev, PRINTER_PORT_DATA, NULL, dev_printer_data);

  dev_reset(dev);

  return dev;
}

hien_dev_info_t dev_printer_def = {
  .type = "PRINTER",
  .ctor = "dev_printer_new",
  .use_clk = FALSE,
};
