/**********************************************************
*
* dev/printer/printer.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _DEV_PRINTER_H_
#define _DEV_PRINTER_H_

#define PRINTER_PORT_DATA  (PORT_USER+0)
#define PRINTER_PORT_COUNT (PORT_USER+1)

PORT_COUNT_CHECK(PRINTER);

hien_dev_t* dev_printer_new(hien_vm_t* vm, char* name);

extern hien_dev_info_t dev_printer_def;

#endif
