/**********************************************************
*
* dev/clkport/clkport.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _DEV_CLKPORT_H_
#define _DEV_CLKPORT_H_

#define CLKPORT_PORT_CLK   (PORT_USER+0)
#define CLKPORT_PORT_COUNT (PORT_USER+1)

PORT_COUNT_CHECK(CLKPORT);

hien_dev_t* dev_clkport_new(hien_vm_t* vm, char* name, hz_t clock);

extern hien_dev_info_t dev_clkport_def;

#endif
