/**********************************************************
*
* dev/clkport/clkport.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

typedef struct dev_clkport_t dev_clkport_t;
struct dev_clkport_t {
  int side;
};

static void dev_clkport_update(hien_dev_t* dev, uintmax_t data, uintmax_t mask)
{
  dev_clkport_t* dat;
  (void)mask;
  (void)data;
  dat = dev->data;

  if(dat->side == 0) {
    dat->side = 3125;
    dev_port_deassert(dev, CLKPORT_PORT_CLK);
  }
  dat->side--;

  if(dat->side == 0)
    dev_port_assert(dev, CLKPORT_PORT_CLK);
}
static void dev_clkport_reset(hien_dev_t* dev)
{
  dev_clkport_t* dat;
  dat = dev->data;

  dat->side = 0;
}
static void dev_clkport_delete(hien_dev_t* dev)
{
  dev_clkport_t* dat;
  dat = dev->data;

  mem_free(dat);
}

hien_dev_t* dev_clkport_new(hien_vm_t* vm, char* name, hz_t clk)
{
  hien_dev_t* dev;
  dev_clkport_t* dat;

  dev = dev_new(vm, name, clk);
  dat = mem_alloc_type(dev_clkport_t);
  dev->data = dat;
  dev->delete = dev_clkport_delete;
  dev->reset = dev_clkport_reset;

  dev_port_set_callbacks(dev, PORT_CLOCK, NULL, dev_clkport_update);

  dev_port_setup(dev, CLKPORT_PORT_CLK, 1, PORT_OUTPUT);

  vm_state_add_int(vm, &dat->side, name, "side", NULL);

  dev_reset(dev);

  return dev;
}

hien_dev_info_t dev_clkport_def = {
  .type = "CLKPORT",
  .ctor = "dev_clkport_new",
  .use_clk = TRUE,
};
