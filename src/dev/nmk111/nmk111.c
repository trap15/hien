/**********************************************************
*
* dev/nmk111/nmk111.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

typedef struct dev_nmk111_t dev_nmk111_t;

struct dev_nmk111_t {
  int foo;
};

static void dev_nmk111_delete(hien_dev_t* dev)
{
  dev_nmk111_t* nmk111;
  nmk111 = dev->data;

  mem_free(nmk111);
}

hien_dev_t* dev_nmk111_new(hien_vm_t* vm, char* name, hz_t clock)
{
  hien_dev_t* dev;
  dev_nmk111_t* nmk111;

  nmk111 = mem_alloc_type(dev_nmk111_t);

  dev = dev_new(vm, name, clock);
  dev->delete = dev_nmk111_delete;
  dev->data   = nmk111;

  return dev;
}

hien_dev_info_t dev_nmk111_def = {
  .type = "NMK111",
  .ctor = "dev_nmk111_new",
  .use_clk = TRUE,
};
