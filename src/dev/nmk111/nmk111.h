/**********************************************************
*
* dev/nmk111/nmk111.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _DEV_NMK111_H_
#define _DEV_NMK111_H_

hien_dev_t* dev_nmk111_new(hien_vm_t* vm, char* name, hz_t clock);

extern hien_dev_info_t dev_nmk111_def;

#endif
