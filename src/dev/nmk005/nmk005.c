/**********************************************************
*
* dev/nmk005/nmk005.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

typedef struct dev_nmk005_t dev_nmk005_t;

struct dev_nmk005_t {
  int foo;
};

static void dev_nmk005_delete(hien_dev_t* dev)
{
  dev_nmk005_t* nmk005;
  nmk005 = dev->data;

  mem_free(nmk005);
}

hien_dev_t* dev_nmk005_new(hien_vm_t* vm, char* name, hz_t clock)
{
  hien_dev_t* dev;
  dev_nmk005_t* nmk005;

  nmk005 = mem_alloc_type(dev_nmk005_t);

  dev = dev_new(vm, name, clock);
  dev->delete = dev_nmk005_delete;
  dev->data   = nmk005;

  return dev;
}

hien_dev_info_t dev_nmk005_def = {
  .type = "NMK005",
  .ctor = "dev_nmk005_new",
  .use_clk = TRUE,
};
