/**********************************************************
*
* dev/nmk112/nmk112.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _DEV_NMK112_H_
#define _DEV_NMK112_H_

#define NMK112_SPACE_OKI1    (SPACE_USER+0)
#define NMK112_SPACE_OKI2    (SPACE_USER+1)
#define NMK112_SPACE_OKI1ROM (SPACE_USER+2)
#define NMK112_SPACE_OKI2ROM (SPACE_USER+3)
#define NMK112_SPACE_COUNT   (SPACE_USER+4)

SPACE_COUNT_CHECK(NMK112);

hien_dev_t* dev_nmk112_new(hien_vm_t* vm, char* name);

extern hien_dev_info_t dev_nmk112_def;

#endif

