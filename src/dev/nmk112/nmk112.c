/**********************************************************
*
* dev/nmk112/nmk112.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

typedef struct dev_nmk112_t dev_nmk112_t;

struct dev_nmk112_t {
  uint8_t slot[2][4];
};

static void dev_nmk112_delete(hien_dev_t* dev)
{
  dev_nmk112_t* nmk112;
  nmk112 = dev->data;

  mem_free(nmk112);
}

static void dev_nmk112_write(hien_dev_t* dev, addr_t addr, uintmax_t data, uintmax_t mask)
{
  dev_nmk112_t* nmk112;
  nmk112 = dev->data;
  addr &= 0x7;

  COMBINE_DATA(nmk112->slot[addr>>2][addr&3], data, mask);
}

static addr_t _nmk112_transform_addr(dev_nmk112_t* nmk112, int oki, addr_t addr)
{
  int slot, bank;

  if(addr < 0x400) { /* Table */
    slot = addr >> 8;
  }else{
    slot = addr >> 16;
  }
  addr &= 0xFFFF;
  bank = nmk112->slot[oki][slot];
  addr |= (bank << 16);

  return addr;
}

static uintmax_t dev_nmk112_oki1_read(hien_dev_t* dev, addr_t addr, uintmax_t mask)
{
  dev_nmk112_t* nmk112;
  nmk112 = dev->data;

  addr = _nmk112_transform_addr(nmk112, 0, addr);

  return dev_read(dev, NMK112_SPACE_OKI1ROM, addr, mask);
}

static uintmax_t dev_nmk112_oki2_read(hien_dev_t* dev, addr_t addr, uintmax_t mask)
{
  dev_nmk112_t* nmk112;
  nmk112 = dev->data;

  addr = _nmk112_transform_addr(nmk112, 1, addr);

  return dev_read(dev, NMK112_SPACE_OKI2ROM, addr, mask);
}

hien_dev_t* dev_nmk112_new(hien_vm_t* vm, char* name)
{
  hien_dev_t* dev;
  dev_nmk112_t* nmk112;

  nmk112 = mem_alloc_type(dev_nmk112_t);
  nmk112->slot[0][0] = 0;
  nmk112->slot[0][1] = 0;
  nmk112->slot[0][2] = 0;
  nmk112->slot[0][3] = 0;
  nmk112->slot[1][0] = 0;
  nmk112->slot[1][1] = 0;
  nmk112->slot[1][2] = 0;
  nmk112->slot[1][3] = 0;

  dev = dev_new(vm, name, 0);
  dev->delete = dev_nmk112_delete;
  dev->data   = nmk112;

  dev_space_setup(dev, SPACE_MAIN,           8);
  dev_space_setup(dev, NMK112_SPACE_OKI1,    8);
  dev_space_setup(dev, NMK112_SPACE_OKI2,    8);
  dev_space_setup(dev, NMK112_SPACE_OKI1ROM, 8);
  dev_space_setup(dev, NMK112_SPACE_OKI2ROM, 8);
  dev_space_set_callbacks(dev, SPACE_MAIN,           NULL,                 dev_nmk112_write);
  dev_space_set_callbacks(dev, NMK112_SPACE_OKI1,    dev_nmk112_oki1_read, NULL);
  dev_space_set_callbacks(dev, NMK112_SPACE_OKI2,    dev_nmk112_oki2_read, NULL);
  dev_space_set_callbacks(dev, NMK112_SPACE_OKI1ROM, NULL,                 NULL);
  dev_space_set_callbacks(dev, NMK112_SPACE_OKI2ROM, NULL,                 NULL);

  vm_state_add_uint8(vm, &nmk112->slot[0][0], name, "slot0", "chan0", NULL);
  vm_state_add_uint8(vm, &nmk112->slot[0][1], name, "slot0", "chan1", NULL);
  vm_state_add_uint8(vm, &nmk112->slot[0][2], name, "slot0", "chan2", NULL);
  vm_state_add_uint8(vm, &nmk112->slot[0][3], name, "slot0", "chan3", NULL);
  vm_state_add_uint8(vm, &nmk112->slot[1][0], name, "slot1", "chan0", NULL);
  vm_state_add_uint8(vm, &nmk112->slot[1][1], name, "slot1", "chan1", NULL);
  vm_state_add_uint8(vm, &nmk112->slot[1][2], name, "slot1", "chan2", NULL);
  vm_state_add_uint8(vm, &nmk112->slot[1][3], name, "slot1", "chan3", NULL);

  return dev;
}

hien_dev_info_t dev_nmk112_def = {
  .type = "NMK112",
  .ctor = "dev_nmk112_new",
  .use_clk = FALSE,
  .has_save = 1,
};
