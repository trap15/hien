/**********************************************************
*
* dev/nmk903/nmk903.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _DEV_NMK903_H_
#define _DEV_NMK903_H_

hien_dev_t* dev_nmk903_new(hien_vm_t* vm, char* name, hz_t clock);

extern hien_dev_info_t dev_nmk903_def;

#endif
