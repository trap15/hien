/**********************************************************
*
* dev/nmk903/nmk903.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

typedef struct dev_nmk903_t dev_nmk903_t;

struct dev_nmk903_t {
  int foo;
};

static void dev_nmk903_delete(hien_dev_t* dev)
{
  dev_nmk903_t* nmk903;
  nmk903 = dev->data;

  mem_free(nmk903);
}

hien_dev_t* dev_nmk903_new(hien_vm_t* vm, char* name, hz_t clock)
{
  hien_dev_t* dev;
  dev_nmk903_t* nmk903;

  nmk903 = mem_alloc_type(dev_nmk903_t);

  dev = dev_new(vm, name, clock);
  dev->delete = dev_nmk903_delete;
  dev->data   = nmk903;

  return dev;
}

hien_dev_info_t dev_nmk903_def = {
  .type = "NMK903",
  .ctor = "dev_nmk903_new",
  .use_clk = TRUE,
};
