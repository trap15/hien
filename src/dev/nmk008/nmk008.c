/**********************************************************
*
* dev/nmk008/nmk008.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

typedef struct dev_nmk008_t dev_nmk008_t;

struct dev_nmk008_t {
  int foo;
};

static void dev_nmk008_delete(hien_dev_t* dev)
{
  dev_nmk008_t* nmk008;
  nmk008 = dev->data;

  mem_free(nmk008);
}

hien_dev_t* dev_nmk008_new(hien_vm_t* vm, char* name, hz_t clock)
{
  hien_dev_t* dev;
  dev_nmk008_t* nmk008;

  nmk008 = mem_alloc_type(dev_nmk008_t);

  dev = dev_new(vm, name, clock);
  dev->delete = dev_nmk008_delete;
  dev->data   = nmk008;

  return dev;
}

hien_dev_info_t dev_nmk008_def = {
  .type = "NMK008",
  .ctor = "dev_nmk008_new",
  .use_clk = TRUE,
};
