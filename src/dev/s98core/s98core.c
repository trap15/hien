/**********************************************************
*
* dev/s98core/s98core.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

enum {
  S98TYPE_NONE = 0,
  S98TYPE_YM2149 = 1,
  S98TYPE_YM2203 = 2,
  S98TYPE_YM2612 = 3,
  S98TYPE_YM2608 = 4,
  S98TYPE_YM2151 = 5,
  S98TYPE_YM2413 = 6,
  S98TYPE_YM3526 = 7,
  S98TYPE_YM3812 = 8,
  S98TYPE_YMF262 = 9,
  S98TYPE_AY_3_8910 = 15,
  S98TYPE_SN76489 = 16,
};

typedef struct dev_s98core_t dev_s98core_t;
typedef struct s98core_entry_t s98core_entry_t;

struct s98core_entry_t {
  hien_dev_t* dev;

  kuso_pvec_t* snds; /* hien_sound_t* */

  uint32_t type;
  uint32_t clk;
  uint32_t pan;
};

struct dev_s98core_t {
  uint32_t sync_wait;

  int32_t waits;

  /* Data pointers */
  uint32_t loop_ptr;
  uint32_t dump_ptr;
  uint32_t tag_ptr;

  uint32_t cur_ptr;

  uint32_t size;

  /* Device hookups */
  s98core_entry_t* devs;
  size_t devcnt;
  size_t devptr;

  /* Device counter */
  int ym2149_cnt;
  int ym2203_cnt;
  int ym2612_cnt;
  int ym2608_cnt;
  int ym2151_cnt;
  int ym2413_cnt;
  int ym3526_cnt;
  int ym3812_cnt;
  int ymf262_cnt;
  int ay8910_cnt;
  int sn76489_cnt;

  /* Sound routing */
  hien_sound_out_t* spkr_l;
  hien_sound_out_t* spkr_r;

};

static uint8_t s98core_r8(hien_dev_t* dev, addr_t addr)
{
  uint8_t ret;
  ret = dev_read(dev, SPACE_MAIN, addr, 0xFF);
  return ret;
}

static uint16_t s98core_r16(hien_dev_t* dev, addr_t addr)
{
  uint16_t ret;
  ret  = s98core_r8(dev, addr+0);
  ret |= s98core_r8(dev, addr+1) << 8;
  return ret;
}

static uint32_t s98core_r32(hien_dev_t* dev, addr_t addr)
{
  uint32_t ret;
  ret  = s98core_r16(dev, addr+0);
  ret |= s98core_r16(dev, addr+2) << 16;
  return ret;
}

static uint32_t s98core_size(hien_dev_t* dev)
{
  uint32_t ret;
#if 0
  ret  = dev_read(dev, S98CORE_SPACE_IO, 0xFF00+0, 0xFF);
  ret |= dev_read(dev, S98CORE_SPACE_IO, 0xFF00+1, 0xFF) << 8;
  ret |= dev_read(dev, S98CORE_SPACE_IO, 0xFF00+2, 0xFF) << 16;
  ret |= dev_read(dev, S98CORE_SPACE_IO, 0xFF00+3, 0xFF) << 24;
#else
  (void)dev;
  ret = -1;
#endif
  return ret;
}

static uint8_t s98core_rd(hien_dev_t* dev)
{
  dev_s98core_t* s98;
  s98 = dev->data;

  if(s98->cur_ptr == s98->size)
    return 0xFD; /* End */

  return s98core_r8(dev, s98->cur_ptr++);
}

static int s98core_ready(hien_dev_t* dev, int d)
{
  s98core_entry_t* ent;
  dev_s98core_t* s98;
  s98 = dev->data;
  ent = s98->devs + d;

  switch(ent->type) {
    case S98TYPE_YM2149:
    default:
      return TRUE;
    case S98TYPE_YM2203:
    case S98TYPE_YM2608:
      return (dev_read(ent->dev, SPACE_MAIN, 0, 0xFF) & 0x80) ? FALSE : TRUE;
  }
}

/* S98 player core */
static void dev_s98core_cmd(hien_dev_t* dev, int d, int ext)
{
  int base = ext * 2;

  uint8_t addr, data;
  s98core_entry_t* ent;
  dev_s98core_t* s98;
  s98 = dev->data;
  ent = s98->devs + d;

  addr = s98core_rd(dev);
  data = s98core_rd(dev);

  switch(ent->type) {
    case S98TYPE_YM2149:
    case S98TYPE_YM2203:
    case S98TYPE_YM2608:
      dev_write(ent->dev, SPACE_MAIN, base+0, addr, 0xFF);
      dev_write(ent->dev, SPACE_MAIN, base+1, data, 0xFF);
      break;
  }
}

static void dev_s98core_update(hien_dev_t* dev, uintmax_t data, uintmax_t mask)
{
  dev_s98core_t* s98;
  uint8_t val;
  int done = FALSE;
  int i;
  int32_t waits = 0;
  (void)mask;
  s98 = dev->data;

  if(data != 1) /* do nothing on falling edge */
    return;

  if(s98->waits < 0) {
    s98->waits++;
    done = TRUE;
  }

  while(!done) {
    val = s98core_rd(dev);
    switch(val) {
      default:
        if(!s98core_ready(dev, val >> 1)) {
          s98->cur_ptr--;
          waits = -1;
          done = TRUE;
          break;
        }
        dev_s98core_cmd(dev, val >> 1, val & 1);
        done = TRUE;
        break;
      case 0xFD: /* End */
        s98->cur_ptr = s98->loop_ptr;
        if(s98->loop_ptr == 0) {
          s98->cur_ptr = s98->dump_ptr;
        }
        done = TRUE;
        break;
      case 0xFE: /* nSYNC */
        waits = 0;
        i = 0;
        for(i = 0; val & 0x80; i++) {
          val = s98core_rd(dev);
          waits |= (val & 0x7F) << (i*7);
        }
        waits += 2;
        waits *= s98->sync_wait;
        done = TRUE;
        break;
      case 0xFF: /* 1SYNC */
        waits = 1 * s98->sync_wait;
        done = TRUE;
        break;
    }
  }
  s98->waits -= waits;
}

/* Sound out port callbacks */
static void s98core_ym2149_write_psga(hien_dev_t* dev, analog_t data)
{
  s98core_entry_t* ent = dev->drv_data;
  sound_add_sample(kuso_pvec_get_type(ent->snds, 0, hien_sound_t), A_AMP(data, A_5V, A_12V));
}
static void s98core_ym2149_write_psgb(hien_dev_t* dev, analog_t data)
{
  s98core_entry_t* ent = dev->drv_data;
  sound_add_sample(kuso_pvec_get_type(ent->snds, 1, hien_sound_t), A_AMP(data, A_5V, A_12V));
}
static void s98core_ym2149_write_psgc(hien_dev_t* dev, analog_t data)
{
  s98core_entry_t* ent = dev->drv_data;
  sound_add_sample(kuso_pvec_get_type(ent->snds, 2, hien_sound_t), A_AMP(data, A_5V, A_12V));
}

static void s98core_ym2203_write_psga(hien_dev_t* dev, analog_t data)
{
  s98core_entry_t* ent = dev->drv_data;
  sound_add_sample(kuso_pvec_get_type(ent->snds, 0, hien_sound_t), A_AMP(data, A_5V, A_12V));
}
static void s98core_ym2203_write_psgb(hien_dev_t* dev, analog_t data)
{
  s98core_entry_t* ent = dev->drv_data;
  sound_add_sample(kuso_pvec_get_type(ent->snds, 1, hien_sound_t), A_AMP(data, A_5V, A_12V));
}
static void s98core_ym2203_write_psgc(hien_dev_t* dev, analog_t data)
{
  s98core_entry_t* ent = dev->drv_data;
  sound_add_sample(kuso_pvec_get_type(ent->snds, 2, hien_sound_t), A_AMP(data, A_5V, A_12V));
}
static void s98core_ym2203_write_fm(hien_dev_t* dev, analog_t data)
{
  s98core_entry_t* ent = dev->drv_data;
  sound_add_sample(kuso_pvec_get_type(ent->snds, 3, hien_sound_t), A_AMP(data, A_5V, A_12V));
}

static void s98core_ym2608_write_psg(hien_dev_t* dev, analog_t data)
{
  s98core_entry_t* ent = dev->drv_data;
  sound_add_sample(kuso_pvec_get_type(ent->snds, 0, hien_sound_t), A_AMP(data, A_5V, A_12V));
}
static void s98core_ym2608_write_fm_l(hien_dev_t* dev, analog_t data)
{
  s98core_entry_t* ent = dev->drv_data;
  sound_add_sample(kuso_pvec_get_type(ent->snds, 1, hien_sound_t), A_AMP(data, A_5V, A_12V));
}
static void s98core_ym2608_write_fm_r(hien_dev_t* dev, analog_t data)
{
  s98core_entry_t* ent = dev->drv_data;
  sound_add_sample(kuso_pvec_get_type(ent->snds, 2, hien_sound_t), A_AMP(data, A_5V, A_12V));
}

static char* s98core_make_path(hien_dev_t* dev, char* type, int cnt)
{
  char* name = NULL;
  char* cname = NULL;
  kuso_asprintf(&cname, "%s.%d", type, cnt);
  name = state_build_path(dev->name, cname, NULL);
  mem_free(cname);

  return name;
}

/* Initialize S98 simulator */
static uint32_t dev_s98core_add_ym2149(hien_dev_t* dev, dev_s98core_t* s98, s98core_entry_t* ent)
{
  uint32_t pan = ent->pan;

  char* name = s98core_make_path(dev, "ym2149", s98->ym2149_cnt++);
  ent->dev = apu_ym2149_new(dev->vm, name, ent->clk);
  mem_free(name);
  ent->dev->drv_data = ent;
  dev_port_set_analog_callbacks(ent->dev, YM2149_PORT_AO_A, NULL, s98core_ym2149_write_psga);
  dev_port_set_analog_callbacks(ent->dev, YM2149_PORT_AO_B, NULL, s98core_ym2149_write_psgb);
  dev_port_set_analog_callbacks(ent->dev, YM2149_PORT_AO_C, NULL, s98core_ym2149_write_psgc);

  kuso_pvec_append(ent->snds, sound_new()); /* PSG-A */
  kuso_pvec_append(ent->snds, sound_new()); /* PSG-B */
  kuso_pvec_append(ent->snds, sound_new()); /* PSG-C */
  sound_set_volume(kuso_pvec_get_type(ent->snds, 0, hien_sound_t), 1.0);
  sound_set_volume(kuso_pvec_get_type(ent->snds, 1, hien_sound_t), 1.0);
  sound_set_volume(kuso_pvec_get_type(ent->snds, 2, hien_sound_t), 1.0);

  dev_reset(ent->dev);

  return pan;
}

static uint32_t dev_s98core_add_ym2203(hien_dev_t* dev, dev_s98core_t* s98, s98core_entry_t* ent)
{
  uint32_t pan = ent->pan;

  char* name = s98core_make_path(dev, "ym2203", s98->ym2203_cnt++);
  ent->dev = apu_ym2203_new(dev->vm, name, ent->clk);
  mem_free(name);
  ent->dev->drv_data = ent;
  dev_port_set_analog_callbacks(ent->dev, YM2203_PORT_AO_A, NULL, s98core_ym2203_write_psga);
  dev_port_set_analog_callbacks(ent->dev, YM2203_PORT_AO_B, NULL, s98core_ym2203_write_psgb);
  dev_port_set_analog_callbacks(ent->dev, YM2203_PORT_AO_C, NULL, s98core_ym2203_write_psgc);
  dev_port_set_analog_callbacks(ent->dev, YM2203_PORT_OP_O, NULL, s98core_ym2203_write_fm);

  kuso_pvec_append(ent->snds, sound_new()); /* PSG-A */
  kuso_pvec_append(ent->snds, sound_new()); /* PSG-B */
  kuso_pvec_append(ent->snds, sound_new()); /* PSG-C */
  kuso_pvec_append(ent->snds, sound_new()); /* FM */
  sound_set_volume(kuso_pvec_get_type(ent->snds, 0, hien_sound_t), 1.0);
  sound_set_volume(kuso_pvec_get_type(ent->snds, 1, hien_sound_t), 1.0);
  sound_set_volume(kuso_pvec_get_type(ent->snds, 2, hien_sound_t), 1.0);
  sound_set_volume(kuso_pvec_get_type(ent->snds, 3, hien_sound_t), 1.0);

  dev_reset(ent->dev);

  return pan;
}

static uint32_t dev_s98core_add_ym2608(hien_dev_t* dev, dev_s98core_t* s98, s98core_entry_t* ent)
{
  uint32_t pan = ent->pan;

  char* name = s98core_make_path(dev, "ym2608", s98->ym2608_cnt++);
  ent->dev = apu_ym2608_new(dev->vm, name, ent->clk);
  mem_free(name);
  ent->dev->drv_data = ent;
  dev_port_set_analog_callbacks(ent->dev, YM2608_PORT_AO,     NULL, s98core_ym2608_write_psg);
  dev_port_set_analog_callbacks(ent->dev, YM2608_PORT_OP_O_L, NULL, s98core_ym2608_write_fm_l);
  dev_port_set_analog_callbacks(ent->dev, YM2608_PORT_OP_O_R, NULL, s98core_ym2608_write_fm_r);

  kuso_pvec_append(ent->snds, sound_new()); /* PSG */
  kuso_pvec_append(ent->snds, sound_new()); /* FM-L */
  kuso_pvec_append(ent->snds, sound_new()); /* FM-R */
  sound_set_volume(kuso_pvec_get_type(ent->snds, 0, hien_sound_t), 0.5);
  sound_set_volume(kuso_pvec_get_type(ent->snds, 1, hien_sound_t), 1.0);
  sound_set_volume(kuso_pvec_get_type(ent->snds, 2, hien_sound_t), 1.0);

  dev_reset(ent->dev);

  /* Panning is ignored for OPNA */
  /* Rewrite it to make FM-L/R actually stereo */
  pan = Ob8(0010,0111);

  return pan;
}

static void dev_s98core_add_entry(hien_dev_t* dev, uint32_t type, uint32_t clk, uint32_t pan)
{
  int i;
  dev_s98core_t* s98;
  s98core_entry_t* ent;
  s98 = dev->data;

  if(type == S98TYPE_NONE)
    return;

  ent = s98->devs + s98->devptr;
  s98->devptr++;

  ent->snds = kuso_pvec_new(1);
  ent->type = type;
  ent->clk = clk;
  ent->pan = pan;

  switch(type) {
    case S98TYPE_YM2149:
      pan = dev_s98core_add_ym2149(dev, s98, ent);
      break;
    case S98TYPE_YM2203:
      pan = dev_s98core_add_ym2203(dev, s98, ent);
      break;
    case S98TYPE_YM2608:
      pan = dev_s98core_add_ym2608(dev, s98, ent);
      break;
  }

  hien_sound_t* snd;
  hien_sound_t** iter = kuso_pvec_iter(ent->snds, hien_sound_t*);
  for(i = 0; *iter != NULL; iter++, i++) {
    snd = *iter;
    if(!((ent->pan >> (i*2)) & 1))
      sound_out_add(s98->spkr_l, snd);
    if(!((ent->pan >> (i*2)) & 2))
      sound_out_add(s98->spkr_r, snd);
  }
}

static void dev_s98core_delete_entry(dev_s98core_t* s98, s98core_entry_t* ent)
{
  int i;
  hien_sound_t* snd;
  for(i = 0; (snd = kuso_pvec_remove(ent->snds, 0)); i++) {
    if(!((ent->pan >> (i*2)) & 1))
      sound_out_remove(s98->spkr_l, snd);
    if(!((ent->pan >> (i*2)) & 2))
      sound_out_remove(s98->spkr_r, snd);
  }

  dev_delete(ent->dev);
}

static void dev_s98core_delete_entries(dev_s98core_t* s98)
{
  size_t i;
  for(i = 0; i < s98->devcnt; i++) {
    dev_s98core_delete_entry(s98, &(s98->devs[i]));
  }
  mem_free(s98->devs);
  s98->devs = NULL;
}

static void dev_s98core_reset(hien_dev_t* dev)
{
  size_t i;
  dev_s98core_t* s98;
  s98 = dev->data;
  uint32_t devtype, clk, pan;
  uint32_t tmr_num, tmr_den;

  s98->devptr = 0;
  s98->waits = 0;
  if(s98->devs != NULL) {
    dev_s98core_delete_entries(s98);
  }

  s98->ym2149_cnt = 0;
  s98->ym2203_cnt = 0;
  s98->ym2612_cnt = 0;
  s98->ym2608_cnt = 0;
  s98->ym2151_cnt = 0;
  s98->ym2413_cnt = 0;
  s98->ym3526_cnt = 0;
  s98->ym3812_cnt = 0;
  s98->ymf262_cnt = 0;
  s98->ay8910_cnt = 0;
  s98->sn76489_cnt = 0;

  /* Check S98 header */
  ASSERT(s98core_r8(dev, 0) == 'S', "Wrong format");
  ASSERT(s98core_r8(dev, 1) == '9', "Wrong format");
  ASSERT(s98core_r8(dev, 2) == '8', "Wrong format");

  s98->size = s98core_size(dev);

  /* TIMER INFO */
  tmr_num = s98core_r32(dev, 4);
  tmr_den = s98core_r32(dev, 8);
  if(tmr_num == 0) /* Default */
    tmr_num = 10;
  if(tmr_den == 0) /* Default */
    tmr_den = 1000;

  s98->sync_wait = (dev->clock * tmr_num) / tmr_den;

  /* FILE OFFSETs */
  s98->tag_ptr  = s98core_r32(dev, 0x10);
  s98->dump_ptr = s98core_r32(dev, 0x14);
  s98->loop_ptr = s98core_r32(dev, 0x18);

  s98->cur_ptr = s98->dump_ptr;

  /* DEVICE data */
  s98->devcnt = s98core_r32(dev, 0x1C);
  if(s98->devcnt == 0) { /* Default */
    s98->devcnt = 1;
    s98->devs = mem_alloc_cnt_type(s98->devcnt, s98core_entry_t);
    dev_s98core_add_entry(dev, S98TYPE_YM2608, 7987200, 0);
  }else{
    s98->devs = mem_alloc_cnt_type(s98->devcnt, s98core_entry_t);
    for(i = 0; i < s98->devcnt; i++) {
      devtype = s98core_r32(dev, 0x20 + (0x10*i) + 0x0);
      clk     = s98core_r32(dev, 0x20 + (0x10*i) + 0x4);
      pan     = s98core_r32(dev, 0x20 + (0x10*i) + 0x8);
      dev_s98core_add_entry(dev, devtype, clk, pan);
    }
  }
}

static void dev_s98core_delete(hien_dev_t* dev)
{
  dev_s98core_t* s98;
  s98 = dev->data;

  sound_mgr_remove(dev->vm->sndmgr, s98->spkr_l);
  sound_mgr_remove(dev->vm->sndmgr, s98->spkr_r);

  if(s98->devs != NULL) {
    dev_s98core_delete_entries(s98);
  }

  sound_out_delete(s98->spkr_l);
  sound_out_delete(s98->spkr_r);

  mem_free(s98);
}

hien_dev_t* dev_s98core_new(hien_vm_t* vm, char* name, hz_t clk)
{
  hien_dev_t* dev;
  dev_s98core_t* s98;

  s98 = mem_alloc_type(dev_s98core_t);
  s98->devs = NULL;
  s98->spkr_l = sound_out_new();
  s98->spkr_r = sound_out_new();

  dev = dev_new(vm, name, clk);
  dev->reset  = dev_s98core_reset;
  dev->delete = dev_s98core_delete;
  dev->data   = s98;

  dev_port_set_callbacks(dev, PORT_CLOCK, NULL, dev_s98core_update);

  dev_space_setup(dev, SPACE_MAIN,         8);
  dev_space_setup(dev, S98CORE_SPACE_IO,   8);

  sound_mgr_add(vm->sndmgr, s98->spkr_l);
  sound_mgr_add(vm->sndmgr, s98->spkr_r);

  return dev;
}

hien_dev_info_t dev_s98core_def = {
  .type = "S98CORE",
  .ctor = "dev_s98core_new",
  .use_clk = TRUE,
};
