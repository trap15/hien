/**********************************************************
*
* dev/s98core/s98core.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _DEV_S98CORE_H_
#define _DEV_S98CORE_H_

#define S98CORE_SPACE_IO     (SPACE_USER+0)
#define S98CORE_SPACE_COUNT  (SPACE_USER+1)

SPACE_COUNT_CHECK(S98CORE);

hien_dev_t* dev_s98core_new(hien_vm_t* vm, char* name, hz_t clk);

extern hien_dev_info_t dev_s98core_def;

#endif
