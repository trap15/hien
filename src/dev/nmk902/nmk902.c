/**********************************************************
*
* dev/nmk902/nmk902.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

typedef struct dev_nmk902_t dev_nmk902_t;

struct dev_nmk902_t {
  int foo;
};

static void dev_nmk902_delete(hien_dev_t* dev)
{
  dev_nmk902_t* nmk902;
  nmk902 = dev->data;

  mem_free(nmk902);
}

hien_dev_t* dev_nmk902_new(hien_vm_t* vm, char* name, hz_t clock)
{
  hien_dev_t* dev;
  dev_nmk902_t* nmk902;

  nmk902 = mem_alloc_type(dev_nmk902_t);

  dev = dev_new(vm, name, clock);
  dev->delete = dev_nmk902_delete;
  dev->data   = nmk902;

  return dev;
}

hien_dev_info_t dev_nmk902_def = {
  .type = "NMK902",
  .ctor = "dev_nmk902_new",
  .use_clk = TRUE,
};
