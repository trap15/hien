/**********************************************************
*
* timer.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _TIMER_H_
#define _TIMER_H_

/* Typedefs */
typedef struct hien_timer_t hien_timer_t;
typedef void (*hien_timer_cb_t)(hien_timer_t* timer, void* data);
typedef struct hien_timer_mgr_t hien_timer_mgr_t;

/* Individual timer */
struct hien_timer_t {
  hien_timer_mgr_t*   mgr;
  int                 running;
  timetick_t          interval;
  timetick_t          value;
  unsigned int        hits;
  int                 oneshot;
  hien_timer_cb_t     callback;
  void*               cb_data;
};

hien_timer_t* tmr_new(void);
void tmr_delete(hien_timer_t* tmr);
void tmr_update(hien_timer_t* tmr, timetick_t ticks);

/* Timer enable status */
void tmr_enable(hien_timer_t* tmr, int enable);
int tmr_enabled(hien_timer_t* tmr);

/* Timer callback and callback data */
void tmr_set_callback(hien_timer_t* tmr, hien_timer_cb_t cb, void* data);
hien_timer_cb_t timer_get_callback(hien_timer_t* tmr);
void* tmr_get_callback_data(hien_timer_t* tmr);

/* Set timer progress */
void tmr_reset(hien_timer_t* tmr, timetick_t value);
/* Delay the timer a set number of intervals */
#define tmr_delay(t,c) tmr_reset(t,-c*(t->interval))

/* Set interval on timer (does not reset progress) */
void tmr_set_oneshot(hien_timer_t* tmr, timetick_t interval);
#define tmr_set_oneshot_hz(t,h) tmr_set_oneshot(t, HZ_TO_TIMETICK(h))
void tmr_set_periodic(hien_timer_t* tmr, timetick_t interval);
#define tmr_set_periodic_hz(t,h) tmr_set_periodic(t, HZ_TO_TIMETICK(h))


/* Timer manager */
struct hien_timer_mgr_t {
  hien_vm_t* vm;

  kuso_pvec_t* timers; /* hien_timer_t* */
};

hien_timer_mgr_t* tmr_mgr_new(hien_vm_t* vm);
void tmr_mgr_delete(hien_timer_mgr_t* mgr);
void tmr_mgr_update(hien_timer_mgr_t* mgr, timetick_t ticks);
void tmr_mgr_add(hien_timer_mgr_t* mgr, hien_timer_t* tmr);
void tmr_mgr_remove(hien_timer_mgr_t* mgr, hien_timer_t* tmr);

#endif

