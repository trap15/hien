/**********************************************************
*
* log.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include <stdarg.h>
#include "hien.h"

static const char* warntxt[] = {
  "CRITICAL", "ERROR", "WARNING",
  "DEBUG", "INFO",
};

void hienlog_va(hien_vm_t* vm, int lvl, char* str, va_list vl)
{
  char *fstr = NULL;

  kuso_catsprintf(&fstr, "[");
  if(vm != NULL) {
    if((lvl != LOG_INFO) && (lvl > vm->config->loglvl))
      return;
    kuso_catsprintf(&fstr, "%s:%f:", vm->name, VM_TIMESTAMP(vm));
  }

  kuso_catsprintf(&fstr, "%s] ", warntxt[lvl+1]);
  kuso_vcatsprintf(&fstr, str, vl);

  fprintf(stderr, "%s", fstr);

  mem_free(fstr);
}

void hienlog(hien_vm_t* vm, int lvl, char* str, ...)
{
  va_list vl;
  va_start(vl, str);

  hienlog_va(vm,lvl, str,vl);

  va_end(vl);
}
