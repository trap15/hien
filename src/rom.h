/**********************************************************
*
* rom.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _ROM_H_
#define _ROM_H_

typedef struct hien_rom_status_t hien_rom_status_t;

enum {
  ROMSTATUS_OK,
  ROMSTATUS_BAD,
  ROMSTATUS_MISSING,
  ROMSTATUS_NOGOOD, /* No known good ROM */
};

struct hien_rom_status_t {
  int status;
  int fail;

  char* loc;
  char* label;

  size_t req_size;
  uint32_t req_crc;

  size_t file_size, read_size;
  uint32_t file_crc;
};

void rom_load(kuso_pvec_t* romvalid, hien_board_t* brd, void* dst, int nvram,
                int abus, int dbus, uint32_t crc,
                char* loc, char* label);
void rom_save(kuso_pvec_t* romvalid, hien_board_t* brd, void* dst,
                int abus, int dbus,
                char* loc, char* label);

#endif
