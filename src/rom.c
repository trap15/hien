/**********************************************************
*
* rom.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

void rom_load(kuso_pvec_t* romvalid, hien_board_t* brd, void* dst, int nvram,
                int abus, int dbus, uint32_t crc,
                char* loc, char* label)
{
  hien_file_t* fp;
  char* path;
  hien_rom_status_t* rom;

  path = config_make_rom_path(brd->vm->config, loc, label);
  fp = file_open(path, FILE_READ);

  rom = mem_alloc_type(hien_rom_status_t);
  rom->req_size = ((dbus << abus) + 7) >> 3;
  rom->req_crc = crc;
  rom->loc = loc;
  rom->label = label;

  memset(dst, 0, rom->req_size);

  if(fp == NULL) {
    rom->file_size = 0;
    rom->read_size = rom->file_size;
  }else{
    rom->file_size = file_size(fp);
    rom->read_size = rom->file_size;
    if(rom->read_size > rom->req_size)
      rom->read_size = rom->req_size;

    file_read(dst, rom->read_size, fp);
    file_close(fp);

    rom->file_crc = crc32(dst, rom->read_size);
  }

  mem_free(path);

  rom->status = ROMSTATUS_OK;
  if(rom->read_size == 0) {
    hienlog(brd->vm, LOG_ERR, "ROM %s.%s missing!\n", loc, label);
    rom->status = ROMSTATUS_MISSING;
  }else{
    if(rom->req_size != rom->file_size) {
      hienlog(brd->vm, LOG_WARN, "ROM %s.%s has bad size: expected 0x%lX, found 0x%lX\n", loc,label, rom->req_size, rom->file_size);
      rom->status = ROMSTATUS_BAD;
    }
    if(rom->req_crc != rom->file_crc) {
      hienlog(brd->vm, LOG_WARN, "ROM %s.%s has bad CRC: expected 0x%X, found 0x%X\n", loc,label, rom->req_crc, rom->file_crc);
      rom->status = ROMSTATUS_BAD;
    }
  }

  switch(rom->status) {
    case ROMSTATUS_MISSING:
      if(nvram) {
        rom->fail = FALSE;
      }else{
        rom->fail = TRUE;
      }
      break;
    case ROMSTATUS_OK:
    case ROMSTATUS_BAD:
    case ROMSTATUS_NOGOOD:
      rom->fail = FALSE;
      break;
    default:
      rom->fail = TRUE;
  }

  kuso_pvec_append(romvalid, rom);

  return;
}

void rom_save(kuso_pvec_t* romvalid, hien_board_t* brd, void* dst,
                int abus, int dbus,
                char* loc, char* label)
{
  (void)romvalid;
  (void)brd;
  (void)dst;
  (void)abus;
  (void)dbus;
  (void)loc;
  (void)label;
}
