/**********************************************************
*
* analog.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
***********************************************************
*
* Analog values can range from -12V to 12V.
* The values are linear, and as such can be operated upon
* with standard math operations.
*
* Do not assume any values for specific voltages, as they
* may change at some point. There are convenient macros
* that define the values safely.
*
**********************************************************/

#ifndef _ANALOG_H_
#define _ANALOG_H_

/* Analog values can range from -12V to 12V */
/* The values are in a linear scaling */
/* Do not assume any particular value for how much 1V is; there are constants to help with this. */

typedef int32_t analog_t;

#define A_0V   ((analog_t)0)
#define A_1V   ((analog_t)0x0AAAAAAA)
#define A_V(x) (A_1V * (x))
#define A_2V   A_V(2)
#define A_3V   A_V(3)
#define A_4V   A_V(4)
#define A_5V   A_V(5)
#define A_6V   A_V(6)
#define A_7V   A_V(7)
#define A_8V   A_V(8)
#define A_9V   A_V(9)
#define A_10V  A_V(10)
#define A_11V  A_V(11)
#define A_12V  A_V(12)

/* Examples:
 *   A_FIX2V(0x7FFFFFFF, A_1V) = 1V
 *   A_FIX2V(0x3FFFFFFF, A_1V) = 0.5V
 *   A_FIX2V(0x5FFFFFFF, A_2V) = 1.5V
 */
#define A_FIX2V(v,t) ((int64_t)(v) * (int64_t)(t) / (int64_t)(0x7FFFFFFF))

/* Examples:
 *   A_V2FIX(A_1V, A_2V) = 0x3FFFFFFF
 *   A_V2FIX(A_1V, A_4V) = 0x1FFFFFFF
 *   A_V2FIX(A_2V, A_4V) = 0x3FFFFFFF
 */
#define A_V2FIX(v,t) ((int64_t)(v) * (int64_t)(0x7FFFFFFF) / (int64_t)(t))

/* Examples:
 *   A_AMP(A_1V, A_2V, A_12V) = A_6V
 */
#define A_AMP(v,s,t) ((((int64_t)(v)) * ((int64_t)(t))) / ((int64_t)(s)))

#define ANALOG_WIDTH 32
#define ANALOG_MASK  0xFFFFFFFF

#endif

