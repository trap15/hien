/**********************************************************
*
* log.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _LOG_H_
#define _LOG_H_

#define LOG_CRIT -1
#define LOG_ERR   0
#define LOG_WARN  1
#define LOG_DEBUG 2
#define LOG_INFO  3

void hienlog(hien_vm_t* vm, int lvl, char* str, ...);
void hienlog_va(hien_vm_t* vm, int lvl, char* str, va_list vl);

#endif
