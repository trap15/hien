/**********************************************************
*
* state.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

hien_state_t* state_new(int type, char* path, void* data, size_t count)
{
  hien_state_t* sta;

  sta = mem_alloc_type(hien_state_t);
  sta->mgr = NULL;
  sta->type = type;
  sta->path = path;
  sta->data = data;
  sta->count = count;
  switch(sta->type) {
    case STATE_TYPE_NONE:
      sta->size = 0;
      break;
    case STATE_TYPE_INT:
      sta->size = sizeof(int64_t);
      break;
    case STATE_TYPE_UINT:
      sta->size = sizeof(uint64_t);
      break;
    case STATE_TYPE_FLOAT:
      sta->size = sizeof(float);
      break;
    case STATE_TYPE_DOUBLE:
      sta->size = sizeof(double);
      break;
    case STATE_TYPE_INT64:
      sta->size = sizeof(int64_t);
      break;
    case STATE_TYPE_INT32:
      sta->size = sizeof(int32_t);
      break;
    case STATE_TYPE_INT16:
      sta->size = sizeof(int16_t);
      break;
    case STATE_TYPE_INT8:
      sta->size = sizeof(int8_t);
      break;
    case STATE_TYPE_UINT64:
      sta->size = sizeof(uint64_t);
      break;
    case STATE_TYPE_UINT32:
      sta->size = sizeof(uint32_t);
      break;
    case STATE_TYPE_UINT16:
      sta->size = sizeof(uint16_t);
      break;
    case STATE_TYPE_UINT8:
      sta->size = sizeof(uint8_t);
      break;
    case STATE_TYPE_TIMETICK:
      sta->size = sizeof(timetick_t);
      break;
    case STATE_TYPE_ADDR:
      sta->size = sizeof(addr_t);
      break;
    case STATE_TYPE_ANALOG:
      sta->size = sizeof(analog_t);
      break;
  }

  return sta;
}
void state_delete(hien_state_t* sta)
{
  mem_free(sta->path);
  mem_free(sta);
}

int state_save(hien_vm_t* vm, char* name, hien_state_t* sta)
{
  char* path;
  hien_file_t* fp;
  int ret = FALSE;
  size_t i;
  uint32_t tmp;

  path = config_make_state_path(vm->config, name, sta->path);
  if(path == NULL)
    goto _done;

  fp = file_open(path, FILE_WRITE|FILE_MKDIR);
  if(fp == NULL)
    goto _done;

  tmp = sta->type;
  file_write(&tmp, sizeof(uint32_t), fp);

  for(i = 0; i < sta->count; i++) {
    uint8_t* dsrc = sta->data;
    switch(sta->type) {
      case STATE_TYPE_INT: {
        int64_t data = ((int*)dsrc)[i];
        file_write(&data, sta->size, fp);
        break;
      }
      case STATE_TYPE_UINT: {
        uint64_t data = ((unsigned int*)dsrc)[i];
        file_write(&data, sta->size, fp);
        break;
      }
      default:
        dsrc += i * sta->size;
        file_write(dsrc, sta->size, fp);
        break;
    }
  }
  ret = TRUE;
  file_close(fp);

_done:
  return ret;
}
int state_load(hien_vm_t* vm, char* name, hien_state_t* sta)
{
  char* path;
  hien_file_t* fp;
  int ret = FALSE;
  size_t i;
  uint32_t tmp;

  path = config_make_state_path(vm->config, name, sta->path);
  if(path == NULL)
    goto _done;

  fp = file_open(path, FILE_READ);
  if(fp == NULL)
    goto _done;

  file_read(&tmp, sizeof(uint32_t), fp);
  if(tmp != sta->type) {
    hienlog(vm,LOG_ERR, "Incorrect type for %s (found %d expected %d)\n", sta->path, tmp, sta->type);
  }

  for(i = 0; i < sta->count; i++) {
    uint8_t* dtgt = sta->data;
    switch(sta->type) {
      case STATE_TYPE_INT: {
        int64_t data;
        file_read(&data, sta->size, fp);
        ((int*)dtgt)[i] = data;
        break;
      }
      case STATE_TYPE_UINT: {
        uint64_t data;
        file_read(&data, sta->size, fp);
        ((int*)dtgt)[i] = data;
        break;
      }
      default:
        dtgt += i * sta->size;
        file_read(dtgt, sta->size, fp);
        break;
    }
  }
  ret = TRUE;
  file_close(fp);

_done:
  return ret;
}



hien_state_mgr_t* state_mgr_new(hien_vm_t* vm)
{
  hien_state_mgr_t* mgr;

  mgr = mem_alloc_type(hien_state_mgr_t);
  mgr->vm = vm;
  mgr->states = kuso_pvec_new(0);

  return mgr;
}

void state_mgr_delete(hien_state_mgr_t* mgr)
{
  hien_state_t* sta;
  while((sta = kuso_pvec_pop(mgr->states))) {
    state_delete(sta);
  }
  kuso_pvec_delete(mgr->states);
  mem_free(mgr);
}

void state_mgr_add(hien_state_mgr_t* mgr, hien_state_t* sta)
{
  kuso_pvec_append(mgr->states, sta);
  sta->mgr = mgr;
}

void state_mgr_remove(hien_state_mgr_t* mgr, hien_state_t* sta)
{
  size_t i;
  hien_state_t** iter = kuso_pvec_iter(mgr->states, hien_state_t*);
  for(i = 0; *iter != NULL; iter++, i++) {
    if(*iter == sta) {
      kuso_pvec_remove(mgr->states, i);
      (*iter)->mgr = NULL;
      break;
    }
  }
}

int state_mgr_save(hien_vm_t* vm, hien_state_mgr_t* mgr, char* name)
{
  int ret = TRUE;
  size_t i;

  if(name == NULL)
    name = "(undef)";

  hien_state_t** iter = kuso_pvec_iter(mgr->states, hien_state_t*);
  for(i = 0; *iter != NULL; iter++, i++) {
    if(!state_save(vm, name, *iter)) {
      ret = FALSE;
      break;
    }
  }

  if(ret)
    hienlog(mgr->vm,LOG_INFO, "Saved state %s...\n", name);
  else
    hienlog(mgr->vm,LOG_INFO, "Failed to save state %s...\n", name);

  return ret;
}

int state_mgr_load(hien_vm_t* vm, hien_state_mgr_t* mgr, char* name)
{
  int ret = TRUE;
  size_t i;

  if(name == NULL)
    name = "(undef)";

  hien_state_t** iter = kuso_pvec_iter(mgr->states, hien_state_t*);
  for(i = 0; *iter != NULL; iter++, i++) {
    if(!state_load(vm, name, *iter)) {
      ret = FALSE;
      break;
    }
  }

  if(ret)
    hienlog(mgr->vm,LOG_INFO, "Loaded state %s...\n", name);
  else
    hienlog(mgr->vm,LOG_INFO, "Failed to load state %s...\n", name);

  return ret;
}


char* state_build_path_va(char* pathf, va_list path)
{
  va_list tmpl;
  char* str;
  char* tmp;
  int cnt;

  va_copy(tmpl, path);

  cnt = 0;
  tmp = pathf;
  while(tmp != NULL) {
    cnt++;
    tmp = va_arg(tmpl, char*);
  }
  if(cnt == 0)
    return NULL;

  str = mem_strdup(pathf);
  while(--cnt) {
    kuso_asprintf(&str, "%s/%s", str, va_arg(path, char*));
  }
  return str;
}
char* state_build_path(char* path, ...)
{
  char* str;
  va_list v;
  va_start(v, path);
  str = state_build_path_va(path, v);
  va_end(v);
  return str;
}

#define STATE_ADD_DEF(name, ctype, rtype) \
void state_add_##name##_arr_va(hien_state_mgr_t* mgr, size_t cnt, ctype* val, char* pathf, va_list path) \
{ \
  hien_state_t* sta; \
  char* fpath; \
\
  fpath = state_build_path_va(pathf, path); \
  if(fpath == NULL) \
    return; \
  sta = state_new(rtype, fpath, val, cnt); \
  state_mgr_add(mgr, sta); \
}

STATE_ADD_DEF(int, int, STATE_TYPE_INT)
STATE_ADD_DEF(uint, unsigned int, STATE_TYPE_UINT)
STATE_ADD_DEF(float, float, STATE_TYPE_FLOAT)
STATE_ADD_DEF(double, double, STATE_TYPE_DOUBLE)
STATE_ADD_DEF(int64, int64_t, STATE_TYPE_INT64)
STATE_ADD_DEF(int32, int32_t, STATE_TYPE_INT32)
STATE_ADD_DEF(int16, int16_t, STATE_TYPE_INT16)
STATE_ADD_DEF(int8, int8_t, STATE_TYPE_INT8)
STATE_ADD_DEF(uint64, uint64_t, STATE_TYPE_UINT64)
STATE_ADD_DEF(uint32, uint32_t, STATE_TYPE_UINT32)
STATE_ADD_DEF(uint16, uint16_t, STATE_TYPE_UINT16)
STATE_ADD_DEF(uint8, uint8_t, STATE_TYPE_UINT8)
STATE_ADD_DEF(timetick, timetick_t, STATE_TYPE_TIMETICK)
STATE_ADD_DEF(addr, addr_t, STATE_TYPE_ADDR)
STATE_ADD_DEF(analog, analog_t, STATE_TYPE_ANALOG)
