/**********************************************************
*
* vm.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

int _save_argc;
char** _save_argv;

hien_vm_t* vm_new(char* name, int type)
{
  hien_vm_t* vm;

  vm = mem_alloc_type(hien_vm_t);
  vm->name = name;

  vm->running = 1;
  vm->type = type;

  vm->uptime_lo = vm->uptime_hi = 0;
  vm->board = NULL;
  vm->statemgr = NULL;
  vm->sndmgr = NULL;
  vm->tmrmgr = NULL;
  vm->config = config_new(vm, "hien.cfg");

  config_set_default(vm->config);
  config_handle_options(vm->config, _save_argc, _save_argv);
  config_handle_cmdline(vm->config, _save_argc, _save_argv);

  ui_init(vm);
  osd_snd_init(vm);

  vm->tmrmgr = tmr_mgr_new(vm);
  vm->sndmgr = sound_mgr_new(vm, vm->config->snd.rate);
  vm->statemgr = state_mgr_new(vm);
  vm->board = board_new(vm);

  if(vm->type == VMTYPE_SNDPLAY)
    vm->sndplay = sndplay_new(vm);

  vm_state_add_int(vm, &vm->type, "vm", "type", NULL);
  vm_state_add_timetick(vm, &vm->uptime_lo, "vm", "uptime_lo", NULL);
  vm_state_add_uint(vm, &vm->uptime_hi, "vm", "uptime_hi", NULL);

  return vm;
}

void vm_delete(hien_vm_t* vm)
{
  if(vm->type == VMTYPE_SNDPLAY)
    sndplay_delete(vm->sndplay);

  board_delete(vm->board);
  state_mgr_delete(vm->statemgr);
  tmr_mgr_delete(vm->tmrmgr);
  sound_mgr_delete(vm->sndmgr);

  config_delete(vm->config);

  osd_snd_finish(vm);
  ui_finish(vm);

  mem_free(vm);
}

void vm_reset(hien_vm_t* vm)
{
  if(vm->type == VMTYPE_SNDPLAY)
    sndplay_delete(vm->sndplay);

  board_delete(vm->board);
  state_mgr_delete(vm->statemgr);
  tmr_mgr_delete(vm->tmrmgr);
  sound_mgr_delete(vm->sndmgr);

  vm->tmrmgr = tmr_mgr_new(vm);
  vm->sndmgr = sound_mgr_new(vm, vm->config->snd.rate);
  vm->statemgr = state_mgr_new(vm);
  vm->board = board_new(vm);

  if(vm->type == VMTYPE_SNDPLAY)
    vm->sndplay = sndplay_new(vm);

  board_reset(vm->board);
}

void vm_run(hien_vm_t* vm, timetick_t ticks)
{
  tmr_mgr_update(vm->tmrmgr, ticks);

  vm->uptime_lo += ticks;
  while(vm->uptime_lo >= TIMETICK_PER_MILLISECOND) {
    vm->uptime_lo -= TIMETICK_PER_MILLISECOND;
    vm->uptime_hi++;
  }
}

void vm_stop(hien_vm_t* vm)
{
  vm->running = 0;
}


int vm_state_save(hien_vm_t* vm, char* name)
{
  if(vm->statemgr == NULL)
    return FALSE;

  if(!state_mgr_save(vm, vm->statemgr, name))
    return FALSE;

  return TRUE;
}
int vm_state_load(hien_vm_t* vm, char* name)
{
  if(vm->statemgr == NULL)
    return FALSE;

  if(!state_mgr_load(vm, vm->statemgr, name))
    return FALSE;

  return TRUE;
}

void vm_add_timer(hien_vm_t* vm, hien_timer_t* tmr)
{
  tmr_mgr_add(vm->tmrmgr, tmr);
}
void vm_add_device(hien_vm_t* vm, hien_dev_t* dev)
{
  kuso_pvec_append(vm->board->devs, dev);
}
void vm_add_sound_out(hien_vm_t* vm, hien_sound_out_t* out)
{
  sound_mgr_add(vm->sndmgr, out);
}

void vm_remove_timer(hien_vm_t* vm, hien_timer_t* tmr)
{
  tmr_mgr_remove(vm->tmrmgr, tmr);
}
void vm_remove_device(hien_vm_t* vm, hien_dev_t* dev)
{
  size_t i;

  hien_dev_t** iter = kuso_pvec_iter(vm->board->devs, hien_dev_t*);
  for(i = 0; *iter != NULL; iter++, i++) {
    if(*iter == dev) {
      kuso_pvec_remove(vm->board->devs, i);
      break;
    }
  }
}
void vm_remove_sound_out(hien_vm_t* vm, hien_sound_out_t* out)
{
  sound_mgr_remove(vm->sndmgr, out);
}

#define VM_STATE_ADD_DEF(name, type) \
void vm_##name(hien_vm_t* vm, type* val, char* path, ...) \
{ \
  va_list v; \
  va_start(v, path); \
  vm_##name##_va(vm, val, path, v); \
  va_end(v); \
} \
void vm_##name##_arr(hien_vm_t* vm, size_t cnt, type* val, char* path, ...) \
{ \
  va_list v; \
  va_start(v, path); \
  vm_##name##_arr_va(vm, cnt, val, path, v); \
  va_end(v); \
} \
void vm_##name##_va(hien_vm_t* vm, type* val, char* pathf, va_list path) \
{ \
  vm_##name##_arr_va(vm, 1, val, pathf, path); \
} \
void vm_##name##_arr_va(hien_vm_t* vm, size_t cnt, type* val, char* pathf, va_list path) \
{ \
  name##_arr_va(vm->statemgr, cnt, val, pathf, path); \
}

VM_STATE_ADD_DEF(state_add_int, int)
VM_STATE_ADD_DEF(state_add_uint, unsigned int)
VM_STATE_ADD_DEF(state_add_float, float)
VM_STATE_ADD_DEF(state_add_double, double)
VM_STATE_ADD_DEF(state_add_int64, int64_t)
VM_STATE_ADD_DEF(state_add_int32, int32_t)
VM_STATE_ADD_DEF(state_add_int16, int16_t)
VM_STATE_ADD_DEF(state_add_int8, int8_t)
VM_STATE_ADD_DEF(state_add_uint64, uint64_t)
VM_STATE_ADD_DEF(state_add_uint32, uint32_t)
VM_STATE_ADD_DEF(state_add_uint16, uint16_t)
VM_STATE_ADD_DEF(state_add_uint8, uint8_t)
VM_STATE_ADD_DEF(state_add_timetick, timetick_t)
VM_STATE_ADD_DEF(state_add_addr, addr_t)
VM_STATE_ADD_DEF(state_add_analog, analog_t)
