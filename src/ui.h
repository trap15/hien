/**********************************************************
*
* ui.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _UI_H_
#define _UI_H_

/* UI handles main itself */
int main(int argc, char* argv[]);
void ui_run(hien_vm_t* vm);

void ui_init(hien_vm_t* vm);
void ui_finish(hien_vm_t* vm);

#endif

