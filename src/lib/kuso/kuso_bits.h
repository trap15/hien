/******************************************************************************
*
* libkuso
* Bit manipulation support header
*
* Copyright (C) 2013, Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met: 
* 
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution. 
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*******************************************************************************
*
* For getting a mask for a set amount of bits:
*     BITMASK(size)
* Where size is how many bits the mask covers (1-based)
*
* For extracting bits out of a value:
*     BITEXT(val, bit, size)
* Where val is the value to extract from, and bit is the
* lowest bit of the range to extract (0 = LSb)
* size is the number of bits extracted (1-based)
*
* For getting a single bit out of a value:
*     BIT(val, bit)
* This is essentially BITEXT(val, bit, 1)
*
* Helpers for specifying binary exist:
*     Ob4(a)                - 4bit binary
*     Ob8(a,b)              - 8bit binary
*     Ob16(a,b,c,d)         - 16bit binary
*     Ob32(a,b,c,d,e,f,g,h) - 32bit binary
* Ob is an alias to Ob8
*
******************************************************************************/

#ifndef LIBKUSO_BITS_H_
#define LIBKUSO_BITS_H_

#define BITMASK(sz) ((1<<(sz))-1)
#define BITEXT(x,n,l) (((x) >> (n)) & BITMASK(l))
#define BIT(x,n) BITEXT(x,n,1)

#define NMASK(l,ml,mr) (ml >= mr) ? \
  ROTL( BITMASK((ml - mr)+1), l, mr) : \
  ROTL(~BITMASK((mr - ml)+1), l, mr)

#define Ob4(x) (((0##x & 01)) | \
                ((0##x & 010) >> 2) | \
                ((0##x & 0100) >> 4) | \
                ((0##x & 01000) >> 6))

#define Ob8(x1,x2) ((Ob4(x1) <<  4) | (Ob4(x2) <<  0))

#define Ob16(x1,x2,x3,x4) ((Ob4(x1) << 12) | (Ob4(x2) <<  8) | \
                           (Ob4(x3) <<  4) | (Ob4(x4) <<  0))

#define Ob32(x1,x2,x3,x4,x5,x6,x7,x8) ((Ob4(x1) << 28) | (Ob4(x2) << 24) | \
                                       (Ob4(x3) << 20) | (Ob4(x4) << 16) | \
                                       (Ob4(x5) << 12) | (Ob4(x6) <<  8) | \
                                       (Ob4(x7) <<  4) | (Ob4(x8) <<  0))

#define Ob(x1,x2) (Ob8(x1,x2))

#define SHR(x,n) (((n) < 0) ? (x) << -(n) : (x) >> (n))
#define SHL(x,n) (((n) < 0) ? (x) >> -(n) : (x) << (n))

#define ROTL(x,l,n) (SHL(x,n) | (SHR(x,(l)-(n)) & BITMASK(n)))
#define ROTR(x,l,n) (SHR(x,n) | (SHL(((x) & BITMASK(n)), (l)-(n))))

#define RLIMM(x,l,n,m) (ROTL(x,l,n) & (m))
#define RRIMM(x,l,n,m) (ROTR(x,l,n) & (m))
#define SLIMM(x,  n,m) (SHL(x,n) & (m))
#define SRIMM(x,  n,m) (SHR(x,n) & (m))

#define RLIM(x,l,n, ms,ml) RLIMM(x,l,n, BITMASK(ml) << ms)
#define RRIM(x,l,n, ms,ml) RRIMM(x,l,n, BITMASK(ml) << ms)
#define SLIM(x,  n, ms,ml) SLIMM(x,  n, BITMASK(ml) << ms)
#define SRIM(x,  n, ms,ml) SRIMM(x,  n, BITMASK(ml) << ms)

#define RLINM(x,l,n, mb,me) RLIMM(x,l,n, NMASK(mb,me))
#define RRINM(x,l,n, mb,me) RRIMM(x,l,n, NMASK(mb,me))
#define SLINM(x,  n, mb,me) SLIMM(x,  n, NMASK(mb,me))
#define SRINM(x,  n, mb,me) SRIMM(x,  n, NMASK(mb,me))

#endif
