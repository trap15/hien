/******************************************************************************
*
* libkuso
* Tokenizer code
*
* Copyright (C) 2013, Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met: 
* 
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution. 
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************/

#include "kuso.h"

size_t kuso_tkncpy(char* dst, char* str, char* split, char* nocopy, size_t* sz, int* last)
{
  int done = 0;
  size_t i = 0;
  size_t l;
  char c;
  size_t splen;
  size_t nclen;

  splen = strlen(split);
  nclen = strlen(nocopy);

  if(last) *last = FALSE;
  if(sz) *sz = 0;
  while(!done) {
    c = str[i++];
    switch(c) {
      case '\0': case '\r': case '\n':
        if(last) *last = TRUE;
        done = 1;
        c = '\0';
        break;
    }
    for(l = 0; l < nclen; l++) {
      if(c == nocopy[l])
        break;
    }
    if(l != nclen)
      continue;
    for(l = 0; l < splen; l++) {
      if(c == split[l]) {
        done = 1;
        c = '\0';
        break;
      }
    }
    if(dst) *dst++ = c;
    if(sz) (*sz)++;
  }
  return i;
}

kuso_tkn_t* kuso_nexttkn(char** str, char* split, char* nocopy, int allow_empty)
{
  size_t sz;
  kuso_tkn_t* tkn;

  kuso_tkncpy(NULL, *str, split, nocopy, &sz, NULL);
  if(sz == 1 && !allow_empty) {
    return NULL;
  }

  tkn = mem_alloc_type(kuso_tkn_t);
  tkn->next = NULL;
  tkn->prev = NULL;
  tkn->text = mem_alloc(sz);
  tkn->len = sz;
  *str += kuso_tkncpy(tkn->text, *str, split, nocopy, NULL, &tkn->last);

  return tkn;
}

kuso_tkn_t* kuso_tokenize(char* str, char* split, char* nocopy)
{
  kuso_tkn_t* ftkn;
  kuso_tkn_t* tkns;
  kuso_tkn_t* newtkn;

  tkns = kuso_nexttkn(&str, split, nocopy, FALSE);
  if(tkns == NULL) {
    ftkn = NULL;
    goto _kuso_tokenize_end;
  }

  ftkn = tkns;
  ftkn->cnt = 0;
  newtkn = tkns;
  while(!newtkn->last) {
    newtkn = kuso_nexttkn(&str, split, nocopy, TRUE);
    tkns->next = newtkn;
    newtkn->prev = tkns;
    tkns = newtkn;
    ftkn->cnt++;
  }
  tkns->next = NULL;
  ftkn->prev = NULL;

_kuso_tokenize_end:
  return ftkn;
}

kuso_tkn_t* kuso_tknlast(kuso_tkn_t* tkn)
{
  while(tkn->next) {
    tkn = tkn->next;
  }
  return tkn;
}

void kuso_tknfree(kuso_tkn_t* tkn)
{
  kuso_tkn_t* ntkn;
  kuso_tkn_t* ptkn;

  ptkn = tkn->prev;
  ntkn = tkn->next;
  if(ptkn)
    ptkn->next = ntkn;
  if(ntkn)
    ntkn->prev = ptkn;

  mem_free(tkn->text);
  mem_free(tkn);
}

void kuso_tknfree_nexts(kuso_tkn_t* tkn)
{
  kuso_tkn_t* ntkn;
  ntkn = tkn->next;
  tkn->next = NULL;
  tkn = ntkn;
  while(tkn) {
    ntkn = tkn->next;
    mem_free(tkn->text);
    mem_free(tkn);
    tkn = ntkn;
  }
}

void kuso_tknfree_prevs(kuso_tkn_t* tkn)
{
  kuso_tkn_t* ntkn;
  ntkn = tkn->prev;
  tkn->prev = NULL;
  tkn = ntkn;
  while(tkn) {
    ntkn = tkn->prev;
    mem_free(tkn->text);
    mem_free(tkn);
    tkn = ntkn;
  }
}

void kuso_tknfree_all(kuso_tkn_t* tkn)
{
  kuso_tknfree_nexts(tkn);
  kuso_tknfree_prevs(tkn);
  kuso_tknfree(tkn);
}

int kuso_cmptkn(kuso_tkn_t* tkn, char* str)
{
  if(strlen(tkn->text) != strlen(str)) return FALSE;

  if(strcmp(tkn->text, str) == 0)
    return TRUE;
  else
    return FALSE;
}

char* kuso_nextline(char* str)
{
  char c;
  int nl_hit = 0;
  int done = 0;
  while(!done) {
    c = *str++;
    switch(c) {
      case '\r': case '\n':
        if(nl_hit)
          done = 1;
        nl_hit++;
        break;
      case '\0':
        done = 1;
        break;
      default:
        if(nl_hit)
          done = 1;
        break;
    }
  }
  str--;
  return str;
}

static size_t kuso_stripcomment_core(char* str, char* comments, char* out)
{
  int done = 0;
  char c;
  size_t cmtlen;
  size_t i, l;
  size_t sz;

  cmtlen = strlen(comments);

  sz = 0;
  i = 0;
  while(!done) {
    c = str[i++];
    switch(c) {
      case '\0': case '\r': case '\n':
        done = 1;
        c = '\0';
        break;
    }
    for(l = 0; l < cmtlen; l++) {
      if(c == comments[l]) {
        done = 1;
        c = '\0';
        break;
      }
    }
    if(out) *out++ = c;
    sz++;
  }

  return sz;
}

char* kuso_stripcomment(char* str, char* comments)
{
  size_t sz;
  char* out;

  sz = kuso_stripcomment_core(str, comments, NULL);
  out = mem_alloc(sz);
  if(out == NULL)
    return NULL;

  kuso_stripcomment_core(str, comments, out);
  return out;
}

static size_t kuso_stripspace_core(char* str, char* out)
{
  int done = 0;
  char c;
  size_t i;
  size_t sz;

  sz = 0;
  i = 0;
  while(!done) {
    c = str[i++];
    switch(c) {
      case '\0': case '\r': case '\n':
        done = 1;
        c = '\0';
        /* fall-thru */
      default:
        if(out) *out++ = c;
        sz++;
        break;
      case ' ': case '\t':
        break;
    }
  }

  return sz;
}

char* kuso_stripspace(char* str)
{
  size_t sz;
  char* out;

  sz = kuso_stripspace_core(str, NULL);

  out = mem_alloc(sz);
  if(out == NULL)
    return NULL;

  kuso_stripspace_core(str, out);
  return out;
}
