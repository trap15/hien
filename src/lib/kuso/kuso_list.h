/******************************************************************************
*
* libkuso
* List-likes support header
*
* Copyright (C) 2013, Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*******************************************************************************
*
* kuso_pvec_t (and the kuso_pvec_* functions) are for pointer vectors. They act
* very similarly to C++ vectors, but can only hold pointers.
* Very useful for lists of objects.
*
* kuso_fifo_t (and the kuso_fifo_* functions) are for FIFOs. They act like any
* other FIFO in existence. The elements are typed 'uintmax_t'.
*
* kuso_pfifo_t (and the kuso_pfifo_* functions) are for pointer FIFOs.
* They act like just like kuso_fifo_t, but the elements are typed 'void*'.
*
* kuso_stack_t (and the kuso_stack_* functions) are for stacks. They act like
* any other stack in existence. The elements are typed 'uintmax_t'.
*
* kuso_pstack_t (and the kuso_pstack_* functions) are for pointer stacks.
* They act like just like kuso_stack_t, but the elements are typed 'void*'.
*
******************************************************************************/

#ifndef LIBKUSO_LIST_H_
#define LIBKUSO_LIST_H_

/* Typedefs */
typedef struct kuso_pvec_t kuso_pvec_t;
typedef struct kuso_fifo_t kuso_fifo_t;
typedef struct kuso_pfifo_t kuso_pfifo_t;
typedef struct kuso_stack_t kuso_stack_t;
typedef struct kuso_pstack_t kuso_pstack_t;

/* Pointer vectors */
kuso_pvec_t* kuso_pvec_new(size_t num);
void kuso_pvec_delete(kuso_pvec_t* vec);
size_t kuso_pvec_size(kuso_pvec_t* vec);
void* kuso_pvec_get(kuso_pvec_t* vec, size_t idx);
void** kuso_pvec_iter_void(kuso_pvec_t* vec);
void kuso_pvec_expand(kuso_pvec_t* vec, size_t cnt);
void kuso_pvec_append(kuso_pvec_t* vec, void* elem);
void kuso_pvec_concat(kuso_pvec_t* tgt, kuso_pvec_t* src);
void kuso_pvec_insert(kuso_pvec_t* vec, size_t idx, void* elem);
void kuso_pvec_prepend(kuso_pvec_t* vec, void* elem);
void* kuso_pvec_remove(kuso_pvec_t* vec, size_t idx);
void* kuso_pvec_pop(kuso_pvec_t* vec);

#define kuso_pvec_get_type(v,i,t) ((t*)kuso_pvec_get(v,i))
#define kuso_pvec_remove_type(v,i,t) ((t*)kuso_pvec_remove(v,i))
#define kuso_pvec_pop_type(v,t) ((t*)kuso_pvec_pop(v))
#define kuso_pvec_iter(v,t) ((t*)kuso_pvec_iter_void(v))

/* FIFOs */
kuso_fifo_t* kuso_fifo_new(size_t size);
void kuso_fifo_delete(kuso_fifo_t* fifo);
void kuso_fifo_push(kuso_fifo_t* fifo, uintmax_t data);
uintmax_t kuso_fifo_pull(kuso_fifo_t* fifo);
uintmax_t kuso_fifo_peek(kuso_fifo_t* fifo);
int kuso_fifo_empty(kuso_fifo_t* fifo);
int kuso_fifo_has_data(kuso_fifo_t* fifo);
size_t kuso_fifo_count(kuso_fifo_t* fifo);

/* Pointer FIFOs */
kuso_pfifo_t* kuso_pfifo_new(size_t size);
void kuso_pfifo_delete(kuso_pfifo_t* fifo);
void kuso_pfifo_push(kuso_pfifo_t* fifo, void* data);
void* kuso_pfifo_pull(kuso_pfifo_t* fifo);
void* kuso_pfifo_peek(kuso_pfifo_t* fifo);
int kuso_pfifo_empty(kuso_pfifo_t* fifo);
int kuso_pfifo_has_data(kuso_pfifo_t* fifo);
size_t kuso_pfifo_count(kuso_pfifo_t* fifo);

#define kuso_pfifo_pull_type(t,f) ((t*)kuso_pfifo_pull(f))
#define kuso_pfifo_peek_type(t,f) ((t*)kuso_pfifo_peek(f))

/* Stacks */
kuso_stack_t* kuso_stack_new(size_t num);
void kuso_stack_delete(kuso_stack_t* stk);
void kuso_stack_push(kuso_stack_t* stk, uintmax_t data);
uintmax_t kuso_stack_pop(kuso_stack_t* stk);
uintmax_t kuso_stack_peek(kuso_stack_t* stk);
uintmax_t kuso_stack_get(kuso_stack_t* stk, ssize_t idx);
int kuso_stack_empty(kuso_stack_t* stk);
int kuso_stack_has_data(kuso_stack_t* stk);
size_t kuso_stack_count(kuso_stack_t* stk);

/* Pointer stacks */
kuso_pstack_t* kuso_pstack_new(size_t num);
void kuso_pstack_delete(kuso_pstack_t* stk);
void kuso_pstack_push(kuso_pstack_t* stk, void* data);
void* kuso_pstack_pop(kuso_pstack_t* stk);
void* kuso_pstack_peek(kuso_pstack_t* stk);
void* kuso_pstack_get(kuso_pstack_t* stk, ssize_t idx);
int kuso_pstack_empty(kuso_pstack_t* stk);
int kuso_pstack_has_data(kuso_pstack_t* stk);
size_t kuso_pstack_count(kuso_pstack_t* stk);

#define kuso_pstack_pop_type(t,f) ((t*)kuso_pstack_pop(f))
#define kuso_pstack_peek_type(t,f) ((t*)kuso_pstack_peek(f))
#define kuso_pstack_get_type(t,f,i) ((t*)kuso_pstack_peek(f,i))

#endif
