#include "../kiban_int.h"

char* cdg_gen_space_read_single(kiban_board_t* brd, kiban_memmapper_t* mapr)
{
  char* out;
  char* core;
  char* atrans;
  char* dtrans;

  out = NULL;
  if((mapr->mask == 0) && (mapr->val == 0)) { /* Always */
    kuso_catsprintf(&out,
"  {\n");
  }else{
    kuso_catsprintf(&out,
"  if((addr & 0x%X) == 0x%X) {\n"
, mapr->mask, mapr->val);
  }

  if(mapr->abmap != NULL) {
    atrans = cdg_gen_busmap(brd, mapr->abmap, "addr");
    kuso_strcat(&out, atrans);
    mem_free(atrans);
  }

  core = cdg_gen_devid_read(brd, mapr->dev, "addr");
  kuso_catsprintf(&out, "    val = %s;\n", core);
  mem_free(core);

  if(mapr->dbmap != NULL) {
    dtrans = cdg_gen_busmap(brd, mapr->dbmap, "val");
    kuso_strcat(&out, dtrans);
    mem_free(dtrans);
  }

  kuso_catsprintf(&out,
"    goto _hndlr_done;\n"
"  }\n");

  return out;
}

char* cdg_gen_space_read(kiban_board_t* brd, kiban_memmap_t* map)
{
  char* out;
  char* ref;
  int has_content;

  kiban_memmapper_t* mapr;

  out = NULL;

  ref = cdg_ref_spcread(brd, map->tag);
  kuso_catsprintf(&out,
"static uintmax_t %s(hien_dev_t* dev, addr_t addr, uintmax_t mask)\n"
"{\n"
"  %s* drv = dev->drv_data;\n"
"  uintmax_t val = 0;\n"
"  (void)mask;\n"
"  addr &= 0x%X;\n"
"\n"
, ref, brd->strname, BITMASK(map->abus));
  mem_free(ref);

  has_content = FALSE;
  kiban_memmapper_t** iter = kuso_pvec_iter(map->map, kiban_memmapper_t*);
  for(; *iter != NULL; iter++) {
    mapr = *iter;
    if(!(mapr->acc & 1)) /* Only do ones with Read capability */
      continue;
    ref = cdg_gen_space_read_single(brd, mapr);
    kuso_strcat(&out, ref);
    mem_free(ref);
    has_content = TRUE;
  }

  if(has_content) {
    kuso_catsprintf(&out, "_hndlr_done:\n");
  }else{
    kuso_catsprintf(&out, "  (void)drv;\n");
  }

  kuso_catsprintf(&out,
"  return val & 0x%X;\n"
"}\n"
, BITMASK(map->dbus));

  return out;
}
