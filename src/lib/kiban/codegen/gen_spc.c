#include "../kiban_int.h"

char* cdg_gen_space(kiban_board_t* brd, kiban_memmap_t* map)
{
  char* out;
  char* rd;
  char* wr;

  out = NULL;
  rd = cdg_gen_space_read(brd, map);
  wr = cdg_gen_space_write(brd, map);
  kuso_asprintf(&out, "%s%s", rd, wr);
  mem_free(rd);
  mem_free(wr);
  return out;
}

char* cdg_gen_all_spaces(kiban_board_t* brd)
{
  char* out;
  char* spc;

  out = NULL;
  kiban_memmap_t** iter = kuso_pvec_iter(brd->memmaps, kiban_memmap_t*);
  for(; *iter != NULL; iter++) {
    spc = cdg_gen_space(brd, *iter);
    kuso_strcat(&out, spc);
    mem_free(spc);
  }

  return out;
}
