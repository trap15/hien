#include "../kiban_int.h"

/*
       d8888 888      888       .d88888b.   .d8888b.  8888888b.  Y88b   d88P 888b     d888
      d88888 888      888      d88P" "Y88b d88P  Y88b 888   Y88b  Y88b d88P  8888b   d8888
     d88P888 888      888      888     888 888    888 888    888   Y88o88P   88888b.d88888
    d88P 888 888      888      888     888 888        888   d88P    Y888P    888Y88888P888
   d88P  888 888      888      888     888 888        8888888P"     d888b    888 Y888P 888
  d88P   888 888      888      888     888 888    888 888 T88b     d88888b   888  Y8P  888
 d8888888888 888      888      Y88b. .d88P Y88b  d88P 888  T88b   d88P Y88b  888   "   888
d88P     888 88888888 88888888  "Y88888P"   "Y8888P"  888   T88b d88P   Y88b 888       888
*/
static char* cdg_gen_driver_load_allocrXm(kiban_board_t* brd, void* elem, int indent, int vtype, int dtype)
{
  char* out;
  char* ref;
  kiban_dev_t* dev = elem;

  out = NULL;
  if(dev->stype != dtype)
    return NULL;

  if((dev->vmtype & vtype) != vtype)
    return NULL;

  ref = cdg_ref_rawdev(brd, dev);
  kuso_catsprintf(&out, "%*s%s = mem_alloc(0x%X);\n", indent, "", ref, dev->size);
  mem_free(ref);

  return out;
}

static char* cdg_gen_driver_load_allocram(kiban_board_t* brd, void* elem, int indent, int vtype)
{
  return cdg_gen_driver_load_allocrXm(brd, elem, indent, vtype, DEVTYPE_RAM);
}

static char* cdg_gen_driver_load_allocrom(kiban_board_t* brd, void* elem, int indent, int vtype)
{
  return cdg_gen_driver_load_allocrXm(brd, elem, indent, vtype, DEVTYPE_ROM);
}

/*
888       .d88888b.         d8888 8888888b.  8888888b.   .d88888b.  888b     d888
888      d88P" "Y88b       d88888 888  "Y88b 888   Y88b d88P" "Y88b 8888b   d8888
888      888     888      d88P888 888    888 888    888 888     888 88888b.d88888
888      888     888     d88P 888 888    888 888   d88P 888     888 888Y88888P888
888      888     888    d88P  888 888    888 8888888P"  888     888 888 Y888P 888
888      888     888   d88P   888 888    888 888 T88b   888     888 888  Y8P  888
888      Y88b. .d88P  d8888888888 888  .d88P 888  T88b  Y88b. .d88P 888   "   888
88888888  "Y88888P"  d88P     888 8888888P"  888   T88b  "Y88888P"  888       888
*/
static char* cdg_gen_driver_load_loadrom(kiban_board_t* brd, void* elem, int indent, int vtype)
{
  char* out;
  char* ref;
  int hash_noenforce;
  kiban_dev_t* dev = elem;

  out = NULL;
  if(dev->stype != DEVTYPE_ROM)
    return NULL;

  if((dev->vmtype & vtype) != vtype)
    return NULL;

  if(dev->type == DEVTYPE_EEPROM)
    hash_noenforce = TRUE;
  else
    hash_noenforce = FALSE;

  ref = cdg_ref_rawdev(brd, dev);
  kuso_catsprintf(&out,
"%*srom_load(romvalid, brd, %s, %s,\n"
"%*s             %d, %d, 0x%X,\n"
"%*s             \"%s\", \"%s\");\n"
, indent, "", ref, hash_noenforce? "TRUE" : "FALSE"
, indent, "", dev->abus, dev->dbus, dev->crc
, indent, "", dev->loc, dev->label);
  mem_free(ref);

  return out;
}

/*
 .d8888b.   .d88888b.  8888888b.  8888888888
d88P  Y88b d88P" "Y88b 888   Y88b 888
888    888 888     888 888    888 888
888        888     888 888   d88P 8888888
888        888     888 8888888P"  888
888    888 888     888 888 T88b   888
Y88b  d88P Y88b. .d88P 888  T88b  888
 "Y8888P"   "Y88888P"  888   T88b 8888888888
*/
char* cdg_gen_driver_load(kiban_board_t* brd)
{
  char* out;
  char* allocram;
  char* allocrom;
  char* loadrom;

  out = NULL;
  allocram = cdg_gen_driver_typed(brd, brd->devs, cdg_gen_driver_load_allocram);
  allocrom = cdg_gen_driver_typed(brd, brd->devs, cdg_gen_driver_load_allocrom);
  loadrom = cdg_gen_driver_typed(brd, brd->devs, cdg_gen_driver_load_loadrom);

  kuso_catsprintf(&out,
"static kuso_pvec_t* %s_load(hien_board_t* brd)\n"
"{\n"
"  kuso_pvec_t* romvalid; /* hien_rom_valid_t */\n"
"  %s* drv = brd->data;\n"
"\n"
"  romvalid = kuso_pvec_new(0);\n"
"\n"
, brd->prefix, brd->strname);

  if(allocram) {
    kuso_catsprintf(&out,
"  /* Allocate RAM space */\n"
"%s\n", allocram);
    mem_free(allocram);
  }
  if(allocrom) {
    kuso_catsprintf(&out,
"  /* Allocate ROM space */\n"
"%s\n", allocrom);
    mem_free(allocrom);
  }
  if(loadrom) {
    kuso_catsprintf(&out,
"  /* Load ROM data */\n"
"%s\n", loadrom);
    mem_free(loadrom);
  }

  kuso_catsprintf(&out,
"  return romvalid;\n"
"}\n");

  return out;
}
