#include "../kiban_int.h"

int cdg_gen_get_device_abus(kiban_board_t* brd, int idx)
{
  kiban_dev_t* dev;
  dev = kuso_pvec_get(brd->devs, idx);
  return dev->abus;
}

int cdg_gen_get_device_dbus(kiban_board_t* brd, int idx)
{
  kiban_dev_t* dev;
  dev = kuso_pvec_get(brd->devs, idx);
  return dev->dbus;
}

uintmax_t cdg_gen_get_device_addrmask(kiban_board_t* brd, int idx)
{
  return BITMASK(cdg_gen_get_device_abus(brd, idx));
}

int cdg_gen_get_clk_rate(kiban_board_t* brd, int idx)
{
  kiban_clock_t* clk;
  clk = kuso_pvec_get(brd->clks, idx);
  return clk->spd;
}

char* cdg_gen_get_device_maker(kiban_board_t* brd, kiban_dev_t* dev)
{
  char* out;
  char* ref;
  const hien_dev_info_t* def;

  out = NULL;
  def = devinfo_find(dev->typestr);

  if(def != NULL) {
    kuso_strcat(&out, def->ctor);
  }else{
    kuso_catsprintf(&out, "dev_%s_new", dev->typestr);
  }
  kuso_catsprintf(&out, "(brd->vm, \"%s\"", dev->tag);

  if(dev->clk_id != -1) {
    ref = cdg_id2ref_clock(brd, dev->clk_id);
    kuso_catsprintf(&out, ", %s / %d", ref, dev->dvr);
    mem_free(ref);
  }else{
    if(def != NULL && def->use_clk) {
      kuso_strcat(&out, ", 0");
    }
  }
  kuso_strcat(&out, ")");

  return out;
}

kiban_sndsrc_desc_t* cdg_gen_sndidx2sndsrc(kiban_board_t* brd, int dev, int spc)
{
  kiban_snd_desc_t* snd;
  kiban_sndsrc_desc_t* sndsrc;

  snd = kuso_pvec_get(brd->snds, dev);
  sndsrc = kuso_pvec_get(snd->srcs, spc);

  return sndsrc;
}
