#include "../kiban_int.h"

/*
8888888b.  8888888888 888     888        d8888 8888888b.  8888888b.
888  "Y88b 888        888     888       d88888 888  "Y88b 888  "Y88b
888    888 888        888     888      d88P888 888    888 888    888
888    888 8888888    Y88b   d88P     d88P 888 888    888 888    888
888    888 888         Y88b d88P     d88P  888 888    888 888    888
888    888 888          Y88o88P     d88P   888 888    888 888    888
888  .d88P 888           Y888P     d8888888888 888  .d88P 888  .d88P
8888888P"  8888888888     Y8P     d88P     888 8888888P"  8888888P"
*/
static char* cdg_gen_driver_new_devadd(kiban_board_t* brd, void* elem, int indent, int vtype)
{
  char* out;
  char* ndev;
  char* ref;
  kiban_dev_t* dev = elem;

  out = NULL;
  if(dev->stype != DEVTYPE_DEV)
    return NULL;

  if((dev->vmtype & vtype) != vtype)
    return NULL;

  ndev = cdg_gen_get_device_maker(brd, dev);
  ref = cdg_ref_rawdev(brd, dev);
  kuso_catsprintf(&out, "%*svm_add_device(brd->vm, %s = %s);\n", indent, "", ref, ndev);
  mem_free(ref);
  mem_free(ndev);

  return out;
}

/*
8888888b.  8888888888 888     888  .d8888b.  8888888888  .d8888b.
888  "Y88b 888        888     888 d88P  Y88b 888        d88P  Y88b
888    888 888        888     888 888    888 888        888    888
888    888 8888888    Y88b   d88P 888        8888888    888
888    888 888         Y88b d88P  888        888        888  88888
888    888 888          Y88o88P   888    888 888        888    888
888  .d88P 888           Y888P    Y88b  d88P 888        Y88b  d88P
8888888P"  8888888888     Y8P      "Y8888P"  888         "Y8888P88
*/
static char* cdg_gen_driver_new_devcfg(kiban_board_t* brd, void* elem, int indent, int vtype)
{
  char* out;
  char* ref;
  char* rd;
  char* wr;
  kiban_memmap_ref_t* smap;
  kiban_portmap_ref_t* pmap;
  kiban_dev_t* dev = elem;

  out = NULL;
  if(dev->stype != DEVTYPE_DEV)
    return NULL;

  if((dev->vmtype & vtype) != vtype)
    return NULL;

  ref = cdg_ref_rawdev(brd, dev);

  kuso_catsprintf(&out,
"%*s/* Configure %s [%s] */\n"
, indent, "", dev->tag, dev->typestr);

  kuso_catsprintf(&out,
"%*s%s->drv_data = drv;\n"
, indent, "", ref);

  kiban_memmap_ref_t** smiter = kuso_pvec_iter(dev->spacemap, kiban_memmap_ref_t*);
  for(; *smiter != NULL; smiter++) {
    smap = *smiter;

    rd = cdg_ref_spcread(brd, smap->map->tag);
    wr = cdg_ref_spcwrite(brd, smap->map->tag);
    kuso_catsprintf(&out, "%*sdev_space_set_callbacks(%s, %s, %s, %s);\n",
                indent, "", ref, smap->name, rd, wr);
    mem_free(rd);
    mem_free(wr);
  }

  kiban_portmap_ref_t** priter = kuso_pvec_iter(dev->portmap, kiban_portmap_ref_t*);
  for(; *priter != NULL; priter++) {
    pmap = *priter;

    if(pmap->map == NULL) {
      kuso_catsprintf(&out, "%*sdev_port_%sassert(%s, %s);\n",
                indent, "", pmap->key ? "" : "de", ref, pmap->name);
    }else{
      rd = cdg_ref_portread(brd, pmap->map->tag);
      wr = cdg_ref_portwrite(brd, pmap->map->tag);
      if(pmap->map->bus == 0) { /* Analog */
        kuso_catsprintf(&out, "%*sdev_port_set_analog_callbacks(%s, %s, %s, %s);\n",
                  indent, "", ref, pmap->name, rd, wr);
      }else{
        kuso_catsprintf(&out, "%*sdev_port_set_callbacks(%s, %s, %s, %s);\n",
                  indent, "", ref, pmap->name, rd, wr);
      }
      mem_free(rd);
      mem_free(wr);
    }
  }
  kuso_catsprintf(&out, "\n");

  mem_free(ref);

  return out;
}

/*
 .d8888b.  888b    888 8888888b.  888b     d888 888    d8P
d88P  Y88b 8888b   888 888  "Y88b 8888b   d8888 888   d8P
Y88b.      88888b  888 888    888 88888b.d88888 888  d8P
 "Y888b.   888Y88b 888 888    888 888Y88888P888 888d88K
    "Y88b. 888 Y88b888 888    888 888 Y888P 888 8888888b
      "888 888  Y88888 888    888 888  Y8P  888 888  Y88b
Y88b  d88P 888   Y8888 888  .d88P 888   "   888 888   Y88b
 "Y8888P"  888    Y888 8888888P"  888       888 888    Y88b
*/
static char* cdg_gen_driver_new_sndmk(kiban_board_t* brd)
{
  char* out;
  char* ref;
  kiban_snd_desc_t* snd;
  kiban_sndsrc_desc_t* sndsrc;

  out = NULL;
  kiban_snd_desc_t** snditer = kuso_pvec_iter(brd->snds, kiban_snd_desc_t*);
  for(; *snditer != NULL; snditer++) {
    snd = *snditer;

    ref = cdg_ref_snd(brd, snd);
    kuso_catsprintf(&out, "  %s = sound_out_new();\n", ref);
    mem_free(ref);

    kiban_sndsrc_desc_t** srciter = kuso_pvec_iter(snd->srcs, kiban_sndsrc_desc_t*);
    for(; *srciter != NULL; srciter++) {
      sndsrc = *srciter;

      ref = cdg_ref_sndsrc(brd, snd, sndsrc);
      kuso_catsprintf(&out, "  %s = sound_new();\n", ref);
      mem_free(ref);
    }
  }

  return out;
}

/*
 .d8888b.  888b    888 8888888b.   .d8888b.   .d88888b.  888b    888
d88P  Y88b 8888b   888 888  "Y88b d88P  Y88b d88P" "Y88b 8888b   888
Y88b.      88888b  888 888    888 888    888 888     888 88888b  888
 "Y888b.   888Y88b 888 888    888 888        888     888 888Y88b 888
    "Y88b. 888 Y88b888 888    888 888        888     888 888 Y88b888
      "888 888  Y88888 888    888 888    888 888     888 888  Y88888
Y88b  d88P 888   Y8888 888  .d88P Y88b  d88P Y88b. .d88P 888   Y8888
 "Y8888P"  888    Y888 8888888P"   "Y8888P"   "Y88888P"  888    Y888
*/
static char* cdg_gen_driver_new_sndcon(kiban_board_t* brd)
{
  char* out;
  char* tref;
  char* ref;
  kiban_snd_desc_t* snd;
  kiban_sndsrc_desc_t* sndsrc;

  out = NULL;
  kiban_snd_desc_t** snditer = kuso_pvec_iter(brd->snds, kiban_snd_desc_t*);
  for(; *snditer != NULL; snditer++) {
    snd = *snditer;

    tref = cdg_ref_snd(brd, snd);

    kiban_sndsrc_desc_t** srciter = kuso_pvec_iter(snd->srcs, kiban_sndsrc_desc_t*);
    for(; *srciter != NULL; srciter++) {
      sndsrc = *srciter;

      ref = cdg_ref_sndsrc(brd, snd, sndsrc);
      kuso_catsprintf(&out, "  sound_out_add(%s, %s);\n", tref, ref);
      mem_free(ref);
    }

    kuso_catsprintf(&out, "  vm_add_sound_out(brd->vm, %s);\n", tref);
    mem_free(tref);
  }

  return out;
}

/*
 .d8888b.  888b    888 8888888b.  888     888  .d88888b.  888
d88P  Y88b 8888b   888 888  "Y88b 888     888 d88P" "Y88b 888
Y88b.      88888b  888 888    888 888     888 888     888 888
 "Y888b.   888Y88b 888 888    888 Y88b   d88P 888     888 888
    "Y88b. 888 Y88b888 888    888  Y88b d88P  888     888 888
      "888 888  Y88888 888    888   Y88o88P   888     888 888
Y88b  d88P 888   Y8888 888  .d88P    Y888P    Y88b. .d88P 888
 "Y8888P"  888    Y888 8888888P"      Y8P      "Y88888P"  88888888
*/
static char* cdg_gen_driver_new_sndvol(kiban_board_t* brd)
{
  char* out;
  char* ref;
  kiban_snd_desc_t* snd;
  kiban_sndsrc_desc_t* sndsrc;

  out = NULL;
  kiban_snd_desc_t** snditer = kuso_pvec_iter(brd->snds, kiban_snd_desc_t*);
  for(; *snditer != NULL; snditer++) {
    snd = *snditer;
    kiban_sndsrc_desc_t** srciter = kuso_pvec_iter(snd->srcs, kiban_sndsrc_desc_t*);
    for(; *srciter != NULL; srciter++) {
      sndsrc = *srciter;

      ref = cdg_ref_sndsrc(brd, snd, sndsrc);
      kuso_catsprintf(&out, "  sound_set_volume(%s, %f);\n", ref, sndsrc->vol);
      mem_free(ref);
    }
  }
  return out;
}

/*
 .d8888b.   .d88888b.  8888888b.  8888888888
d88P  Y88b d88P" "Y88b 888   Y88b 888
888    888 888     888 888    888 888
888        888     888 888   d88P 8888888
888        888     888 8888888P"  888
888    888 888     888 888 T88b   888
Y88b  d88P Y88b. .d88P 888  T88b  888
 "Y8888P"   "Y88888P"  888   T88b 8888888888
*/
char* cdg_gen_driver_new(kiban_board_t* brd)
{
  char* out;
  char* devadd;
  char* devcfg;
  char* sndmk;
  char* sndcon;
  char* sndvol;

  out = NULL;
  devadd = cdg_gen_driver_typed(brd, brd->devs, cdg_gen_driver_new_devadd);
  devcfg = cdg_gen_driver_typed(brd, brd->devs, cdg_gen_driver_new_devcfg);
  sndmk = cdg_gen_driver_new_sndmk(brd);
  sndcon = cdg_gen_driver_new_sndcon(brd);
  sndvol = cdg_gen_driver_new_sndvol(brd);

  kuso_catsprintf(&out,
"static void* %s_new(hien_board_t* brd)\n"
"{\n"
"  %s* drv;\n"
"  drv = mem_alloc_type(%s);\n"
"  drv->brd = brd;\n"
"\n"
, brd->prefix
, brd->strname, brd->strname);

  if(devadd) {
    kuso_catsprintf(&out,
"  /* Add devices */\n"
"%s\n", devadd);
    mem_free(devadd);
  }
  if(devcfg) {
    kuso_catsprintf(&out,
"  /* Configure devices */\n"
"%s\n", devcfg);
    mem_free(devcfg);
  }
  if(sndmk) {
    kuso_catsprintf(&out,
"  /* Create output sound streams */\n"
"%s\n", sndmk);
    mem_free(sndmk);
  }
  if(sndcon) {
    kuso_catsprintf(&out,
"  /* Connect output sound streams */\n"
"%s\n", sndcon);
    mem_free(sndcon);
  }
  if(sndvol) {
    kuso_catsprintf(&out,
"  /* Set volume settings per stream */\n"
"%s\n", sndvol);
    mem_free(sndvol);
  }

  kuso_catsprintf(&out,
"  return drv;\n"
"}\n");

  return out;
}
