#include "../kiban_int.h"

char* cdg_gen_busmap(kiban_board_t* brd, kiban_busmap_t* map, char* var)
{
  char* out;
  int tablen;
  int firsthit = 1;

  char* shiftdir = NULL;
  int shiftcnt;
  char* acc = NULL;
  addr_t mask;

  kiban_busmapper_t* mapr;

  out = NULL;
  kuso_catsprintf(&out, "    %s = ", var);
  tablen = strlen(var) + 7;

  kiban_busmapper_t** iter = kuso_pvec_iter(map->map, kiban_busmapper_t*);
  for(; *iter != NULL; iter++) {
    mapr = *iter;
    if(mapr->key == 1) { /* hard-set pin */
      if(mapr->spin == -1) /* Floating */
        continue;

      kuso_asprintf(&acc, "0x%X", ((mapr->spin == 0) ? 0 : BITMASK(mapr->len)) << mapr->dpin);
    }else{
      /* i is destination pin, map->map[i].pin is source pin */
      if(mapr->dev == NULL) {
        acc = mem_strdup(var);
      }else{
        acc = cdg_gen_devid_read(brd, mapr->dev, NULL);
        kuso_asprintf(&acc, "(%s)", acc);
      }
      shiftdir = "<<";
      shiftcnt = mapr->dpin - mapr->spin;
      if(shiftcnt < 0) {
        shiftdir = ">>";
        shiftcnt = -shiftcnt;
      }

      if(shiftcnt != 0)
        kuso_asprintf(&acc, "(%s %s %d)", acc, shiftdir, shiftcnt);
    }

    mask = BITMASK(mapr->len) << mapr->dpin;
    if(!firsthit)
      kuso_catsprintf(&out, " |\n%*s", tablen, "");
    kuso_catsprintf(&out, "(%s & 0x%X)",
                      acc, mask);
    mem_free(acc);
    firsthit = 0;
  }
  if(firsthit)
    kuso_strcat(&out, "0");
  kuso_strcat(&out, ";\n");

  return out;
}
