#include "../kiban_int.h"

/*
 .d8888b.   .d88888b.  8888888b.  8888888888
d88P  Y88b d88P" "Y88b 888   Y88b 888
888    888 888     888 888    888 888
888        888     888 888   d88P 8888888
888        888     888 8888888P"  888
888    888 888     888 888 T88b   888
Y88b  d88P Y88b. .d88P 888  T88b  888
 "Y8888P"   "Y88888P"  888   T88b 8888888888
*/
char* cdg_gen_driver_reset(kiban_board_t* brd)
{
  char* out;

  out = NULL;
  kuso_catsprintf(&out,
"static void %s_reset(hien_board_t* brd)\n"
"{\n"
"  %s* drv = brd->data;\n"
"  (void)drv;\n"
"}\n"
, brd->prefix, brd->strname);

  return out;
}
