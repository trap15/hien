#include "../kiban_int.h"

static char* cdg_ref_rom(kiban_board_t* brd, kiban_dev_t* dev)
{
  char* out;
  (void)brd;
  out = NULL;
  kuso_asprintf(&out, "drv->roms.%s", dev->tag);
  return out;
}

static char* cdg_ref_ram(kiban_board_t* brd, kiban_dev_t* dev)
{
  char* out;
  (void)brd;
  out = NULL;
  kuso_asprintf(&out, "drv->rams.%s", dev->tag);
  return out;
}

static char* cdg_ref_dev(kiban_board_t* brd, kiban_dev_t* dev)
{
  char* out;
  (void)brd;
  out = NULL;
  kuso_asprintf(&out, "drv->devs.%s", dev->tag);
  return out;
}

char* cdg_ref_rawdev(kiban_board_t* brd, kiban_dev_t* dev)
{
  char* out;
  switch(dev->stype) {
    case DEVTYPE_ROM:
      out = cdg_ref_rom(brd, dev);
      break;
    case DEVTYPE_RAM:
      out = cdg_ref_ram(brd, dev);
      break;
    case DEVTYPE_DEV:
      out = cdg_ref_dev(brd, dev);
      break;
    default:
      out = mem_strdup("");
      break;
  }
  return out;
}
char* cdg_ref_snd(kiban_board_t* brd, kiban_snd_desc_t* snd)
{
  char* out;
  (void)brd;
  out = NULL;
  kuso_asprintf(&out, "drv->snds.%s.output", snd->tag);
  return out;
}
char* cdg_ref_sndsrc(kiban_board_t* brd, kiban_snd_desc_t* snd, kiban_sndsrc_desc_t* sndsrc)
{
  char* out;
  (void)brd;
  out = NULL;
  kuso_asprintf(&out, "drv->snds.%s.%s", snd->tag, sndsrc->tag);
  return out;
}
char* cdg_ref_clock(kiban_board_t* brd, kiban_clock_t* clk)
{
  char* out;
  (void)brd;
  out = NULL;
  kuso_asprintf(&out, "XTAL_%s", clk->tag);
  return out;
}


char* cdg_id2ref_rawdev(kiban_board_t* brd, int idx)
{
  return cdg_ref_rawdev(brd, kuso_pvec_get(brd->devs, idx));
}
char* cdg_id2ref_snd(kiban_board_t* brd, int dev)
{
  return cdg_ref_snd(brd, kuso_pvec_get(brd->snds, dev));
}
char* cdg_id2ref_sndsrc(kiban_board_t* brd, int dev, int spc)
{
  kiban_snd_desc_t* snd = kuso_pvec_get(brd->snds, dev);
  return cdg_ref_sndsrc(brd, snd, kuso_pvec_get(snd->srcs, spc));
}
char* cdg_id2ref_clock(kiban_board_t* brd, int idx)
{
  return cdg_ref_clock(brd, kuso_pvec_get(brd->clks, idx));
}

char* cdg_ref_spcwrite(kiban_board_t* brd, char* tag)
{
  char* out;
  (void)brd;
  out = NULL;
  kuso_asprintf(&out, "drv_spc_write_%s", tag);
  return out;
}

char* cdg_ref_spcread(kiban_board_t* brd, char* tag)
{
  char* out;
  (void)brd;
  out = NULL;
  kuso_asprintf(&out, "drv_spc_read_%s", tag);
  return out;
}

char* cdg_ref_portwrite(kiban_board_t* brd, char* tag)
{
  char* out;
  (void)brd;
  out = NULL;
  kuso_asprintf(&out, "drv_prt_write_%s", tag);
  return out;
}

char* cdg_ref_portread(kiban_board_t* brd, char* tag)
{
  char* out;
  (void)brd;
  out = NULL;
  kuso_asprintf(&out, "drv_prt_read_%s", tag);
  return out;
}

char* cdg_ref_subwrite(kiban_board_t* brd, char* tag)
{
  char* out;
  (void)brd;
  out = NULL;
  kuso_asprintf(&out, "drv_sub_write_%s", tag);
  return out;
}

char* cdg_ref_subread(kiban_board_t* brd, char* tag)
{
  char* out;
  (void)brd;
  out = NULL;
  kuso_asprintf(&out, "drv_sub_read_%s", tag);
  return out;
}

