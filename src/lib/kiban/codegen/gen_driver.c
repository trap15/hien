#include "../kiban_int.h"

char* cdg_gen_driver_typed(kiban_board_t* brd, kuso_pvec_t* vec, cdg_vmtype_dep_emit_cb cb)
{
  char* out;
  char* ref;

  char* refnml;
  char* refsnd;

  out = NULL;
  refnml = NULL;
  refsnd = NULL;
  void** iter = kuso_pvec_iter(vec, void*);
  for(; *iter != NULL; iter++) {
    ref = cb(brd, *iter, 6, VMTYPE_NORMAL);
    if(ref != NULL) {
      kuso_strcat(&refnml, ref);
      mem_free(ref);
    }
    ref = cb(brd, *iter, 6, VMTYPE_SNDPLAY);
    if(ref != NULL) {
      kuso_strcat(&refsnd, ref);
      mem_free(ref);
    }
  }

  if((refnml == NULL) && (refsnd == NULL))
    return NULL;

  out = NULL;
  kuso_catsprintf(&out,
"  switch(brd->vm->type) {\n");

  kuso_catsprintf(&out,
"    case VMTYPE_SNDPLAY:\n"
"%s"
"      break;\n\n", refsnd?:"");
  mem_free(refsnd);

  kuso_catsprintf(&out,
"    case VMTYPE_NORMAL:\n"
"%s"
"      break;\n", refnml?:"");
  mem_free(refnml);

  kuso_catsprintf(&out,
"  }\n");

  return out;
}

/** Driver "core" structure **/
static char* cdg_gen_driver_core(kiban_board_t* brd)
{
  char* out;

  out = NULL;
  kuso_catsprintf(&out,
"hien_board_desc_t %s_desc = {\n"
"  .name = \"%s\",\n"
"  .year = \"%s\",\n"
"  .mfg  = \"%s\",\n"
"\n"
"  .new    = %s_new,\n"
"  .delete = %s_delete,\n"
"  .reset  = %s_reset,\n"
"  .load   = %s_load,\n"
"  .unload = %s_unload,\n"
"\n"
"  .sndplay = &%s_sndplay,\n"
"};\n"
, brd->prefix, brd->name, brd->year, brd->mfg
, brd->prefix, brd->prefix, brd->prefix, brd->prefix, brd->prefix, brd->prefix);

  return out;
}

char* cdg_gen_driver(kiban_board_t* brd)
{
  char* out;
  char* dnew;
  char* ddel;
  char* drst;
  char* dload;
  char* dunload;
  char* dsndp;
  char* dcore;

  out = NULL;
  dnew = cdg_gen_driver_new(brd);
  ddel = cdg_gen_driver_delete(brd);
  drst = cdg_gen_driver_reset(brd);
  dload = cdg_gen_driver_load(brd);
  dunload = cdg_gen_driver_unload(brd);
  dsndp = cdg_gen_sndplay(brd);
  dcore = cdg_gen_driver_core(brd);

  if(dnew) {
    kuso_catsprintf(&out, "%s\n", dnew);
    mem_free(dnew);
  }
  if(ddel) {
    kuso_catsprintf(&out, "%s\n", ddel);
    mem_free(ddel);
  }
  if(drst) {
    kuso_catsprintf(&out, "%s\n", drst);
    mem_free(drst);
  }
  if(dload) {
    kuso_catsprintf(&out, "%s\n", dload);
    mem_free(dload);
  }
  if(dunload) {
    kuso_catsprintf(&out, "%s\n", dunload);
    mem_free(dunload);
  }
  if(dsndp) {
    kuso_catsprintf(&out, "%s\n", dsndp);
    mem_free(dsndp);
  }
  if(dcore) {
    kuso_catsprintf(&out, "%s\n", dcore);
    mem_free(dcore);
  }

  return out;
}
