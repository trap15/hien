#include "../kiban_int.h"

static char* cdg_gen_clock(kiban_board_t* brd, kiban_clock_t* clk)
{
  char* out;
  char* ref;

  out = NULL;
  ref = cdg_ref_clock(brd, clk);
  kuso_catsprintf(&out, "#define %s %d\n", ref, clk->spd);
  mem_free(ref);

  return out;
}

char* cdg_gen_clocks(kiban_board_t* brd)
{
  char* out;
  char* ref;

  out = NULL;
  kiban_clock_t** iter = kuso_pvec_iter(brd->clks, kiban_clock_t*);
  for(; *iter != NULL; iter++) {
    ref = cdg_gen_clock(brd, *iter);
    kuso_strcat(&out, ref);
    mem_free(ref);
  }

  return out;
}
