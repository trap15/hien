#include "../kiban_int.h"

char* cdg_gen_space_write_single(kiban_board_t* brd, kiban_memmapper_t* mapr)
{
  char* out;
  char* core;
  char* atrans;
  char* dtrans;

  out = NULL;
  if((mapr->mask == 0) && (mapr->val == 0)) { /* Always */
    kuso_catsprintf(&out,
"  {\n");
  }else{
    kuso_catsprintf(&out,
"  if((addr & 0x%X) == 0x%X) {\n"
, mapr->mask, mapr->val);
  }

  if(mapr->abmap != NULL) {
    atrans = cdg_gen_busmap(brd, mapr->abmap, "addr");
    kuso_strcat(&out, atrans);
    mem_free(atrans);
  }

  if(mapr->dbmap != NULL) {
    dtrans = cdg_gen_busmap(brd, mapr->dbmap, "data");
    kuso_strcat(&out, dtrans);
    mem_free(dtrans);
  }

  core = cdg_gen_devid_write(brd, mapr->dev, "addr", "data");
  kuso_catsprintf(&out, "    %s;\n", core);
  mem_free(core);

  kuso_catsprintf(&out,
"    goto _hndlr_done;\n"
"  }\n"
);

  return out;
}

char* cdg_gen_space_write(kiban_board_t* brd, kiban_memmap_t* map)
{
  int has_content;
  char* out;
  char* ref;

  kiban_memmapper_t* mapr;

  out = NULL;

  ref = cdg_ref_spcwrite(brd, map->tag);
  kuso_catsprintf(&out,
"static void %s(hien_dev_t* dev, addr_t addr, uintmax_t data, uintmax_t mask)\n"
"{\n"
"  %s* drv = dev->drv_data;\n"
"  addr &= 0x%X;\n"
"  data &= 0x%X;\n"
"  (void)mask;\n"
"\n"
, ref, brd->strname, BITMASK(map->abus), BITMASK(map->dbus));
  mem_free(ref);

  has_content = FALSE;
  kiban_memmapper_t** iter = kuso_pvec_iter(map->map, kiban_memmapper_t*);
  for(; *iter != NULL; iter++) {
    mapr = *iter;
    if(!(mapr->acc & 2)) /* Only do ones with Write capability */
      continue;

    ref = cdg_gen_space_write_single(brd, mapr);
    kuso_strcat(&out, ref);
    mem_free(ref);
    has_content = TRUE;
  }

  if(has_content) {
    kuso_catsprintf(&out,
"_hndlr_done:\n"
"  return;\n");
  }else{
    kuso_catsprintf(&out, "  (void)drv;\n");
  }

  kuso_catsprintf(&out,
"}\n");

  return out;
}
