#ifndef GEN_INT_H_
#define GEN_INT_H_

/*~ Reference Generation (from ID) ~*/
char* cdg_id2ref_rawdev(kiban_board_t* brd, int idx);
char* cdg_id2ref_snd(kiban_board_t* brd, int dev);
char* cdg_id2ref_sndsrc(kiban_board_t* brd, int dev, int spc);
char* cdg_id2ref_clock(kiban_board_t* brd, int idx);

/*~ Reference Generation (from ptr) ~*/
char* cdg_ref_rawdev(kiban_board_t* brd, kiban_dev_t* dev);
char* cdg_ref_snd(kiban_board_t* brd, kiban_snd_desc_t* snd);
char* cdg_ref_sndsrc(kiban_board_t* brd, kiban_snd_desc_t* snd, kiban_sndsrc_desc_t* sndsrc);
char* cdg_ref_clock(kiban_board_t* brd, kiban_clock_t* clk);

char* cdg_ref_spcwrite(kiban_board_t* brd, char* tag);
char* cdg_ref_spcread(kiban_board_t* brd, char* tag);
char* cdg_ref_portwrite(kiban_board_t* brd, char* tag);
char* cdg_ref_portread(kiban_board_t* brd, char* tag);
char* cdg_ref_subwrite(kiban_board_t* brd, char* tag);
char* cdg_ref_subread(kiban_board_t* brd, char* tag);

/*~ Device interfacing ~*/
kiban_sndsrc_desc_t* cdg_gen_sndidx2sndsrc(kiban_board_t* brd, int dev, int spc);
uintmax_t cdg_gen_get_device_addrmask(kiban_board_t* brd, int idx);
int cdg_gen_get_device_abus(kiban_board_t* brd, int idx);
int cdg_gen_get_device_dbus(kiban_board_t* brd, int idx);
char* cdg_gen_get_device_maker(kiban_board_t* brd, kiban_dev_t* dev);
int cdg_gen_get_clk_rate(kiban_board_t* brd, int idx);

/*~ Devid ~*/
char* cdg_gen_devid_read(kiban_board_t* brd, kiban_dev_id_t* dev, char* addr);
char* cdg_gen_devid_write(kiban_board_t* brd, kiban_dev_id_t* dev, char* addr, char* data);

/*~ Busmap ~*/
char* cdg_gen_busmap(kiban_board_t* brd, kiban_busmap_t* map, char* var);

/*~ Busmap ~*/
char* cdg_gen_all_submaps(kiban_board_t* brd);
char* cdg_gen_submap(kiban_board_t* brd, kiban_submap_t* map);
char* cdg_gen_submap_read(kiban_board_t* brd, kiban_submap_t* map);
char* cdg_gen_submap_write(kiban_board_t* brd, kiban_submap_t* map);

/*~ Space ~*/
char* cdg_gen_all_spaces(kiban_board_t* brd);
char* cdg_gen_space(kiban_board_t* brd, kiban_memmap_t* map);

char* cdg_gen_space_read_single(kiban_board_t* brd, kiban_memmapper_t* mapr);
char* cdg_gen_space_read(kiban_board_t* brd, kiban_memmap_t* map);

char* cdg_gen_space_write_single(kiban_board_t* brd, kiban_memmapper_t* mapr);
char* cdg_gen_space_write(kiban_board_t* brd, kiban_memmap_t* map);

/*~ Portmap ~*/
char* cdg_gen_all_ports(kiban_board_t* brd);
char* cdg_gen_port(kiban_board_t* brd, kiban_portmap_t* map);

char* cdg_gen_portmapr_read(kiban_board_t* brd, kiban_portmapper_t* mapr);
char* cdg_gen_port_read_single(kiban_board_t* brd, kiban_portmapper_t* mapr);
char* cdg_gen_port_read(kiban_board_t* brd, kiban_portmap_t* map);
char* cdg_gen_dev_pread(kiban_board_t* brd, int devidx, char* space);

char* cdg_gen_portmapr_write(kiban_board_t* brd, kiban_portmapper_t* mapr, char* data);
char* cdg_gen_port_write_single(kiban_board_t* brd, kiban_portmapper_t* mapr);
char* cdg_gen_port_write(kiban_board_t* brd, kiban_portmap_t* map);
char* cdg_gen_dev_pwrite(kiban_board_t* brd, int devidx, char* space, char* data);
char* cdg_gen_snd_pwrite(kiban_board_t* brd, int sndidx, int srcidx, char* data);

/*~ Driver ~*/
typedef char* (*cdg_vmtype_dep_emit_cb)(kiban_board_t* brd, void* elem, int indent, int vtype);
char* cdg_gen_driver_typed(kiban_board_t* brd, kuso_pvec_t* vec, cdg_vmtype_dep_emit_cb cb);
char* cdg_gen_driver(kiban_board_t* brd);
char* cdg_gen_drvstruct(kiban_board_t* brd);
char* cdg_gen_driver_new(kiban_board_t* brd);
char* cdg_gen_driver_delete(kiban_board_t* brd);
char* cdg_gen_driver_reset(kiban_board_t* brd);
char* cdg_gen_driver_load(kiban_board_t* brd);
char* cdg_gen_driver_unload(kiban_board_t* brd);

/*~ Clock ~*/
char* cdg_gen_clocks(kiban_board_t* brd);

/*~ sndplay ~*/
char* cdg_gen_sndplay(kiban_board_t* brd);


#endif
