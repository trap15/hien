#include "../kiban_int.h"

char* kiban_codegen(kiban_board_t* brd)
{
  char* out;
  char* clks;
  char* drvstr;
  char* space;
  char* port;
  char* sub;
  char* driver;

  out = NULL;
  clks = cdg_gen_clocks(brd);
  drvstr = cdg_gen_drvstruct(brd);
  space = cdg_gen_all_spaces(brd);
  port = cdg_gen_all_ports(brd);
  sub = cdg_gen_all_submaps(brd);
  driver = cdg_gen_driver(brd);

  kuso_catsprintf(&out,
"/******************************************************************************\n"
"*\n"
"* All rights reserved.\n"
"*\n"
"******************************************************************************/\n"
"\n"
"#include \"hien.h\"\n"
"\n");

  if(clks) {
    kuso_catsprintf(&out,
"/******************************************************************************\n"
"*                                    CLOCKS                                   *\n"
"******************************************************************************/\n\n"
"%s\n\n", clks);
    mem_free(clks);
  }
  if(drvstr) {
    kuso_catsprintf(&out,
"/******************************************************************************\n"
"*                               DRIVER STRUCTURE                              *\n"
"******************************************************************************/\n\n"
"%s\n\n", drvstr);
    mem_free(drvstr);
  }
  if(sub) {
    kuso_catsprintf(&out,
"/******************************************************************************\n"
"*                             SUB ACCESS HANDLERS                             *\n"
"******************************************************************************/\n\n"
"%s\n\n", sub);
    mem_free(sub);
  }
  if(space) {
    kuso_catsprintf(&out,
"/******************************************************************************\n"
"*                            SPACE ACCESS HANDLERS                            *\n"
"******************************************************************************/\n\n"
"%s\n\n", space);
    mem_free(space);
  }
  if(port) {
    kuso_catsprintf(&out,
"/******************************************************************************\n"
"*                             PORT ACCESS HANDLERS                            *\n"
"******************************************************************************/\n\n"
"%s\n\n", port);
    mem_free(port);
  }
  if(driver) {
    kuso_catsprintf(&out,
"/******************************************************************************\n"
"*                                 DRIVER SETUP                                *\n"
"******************************************************************************/\n\n"
"%s\n\n", driver);
    mem_free(driver);
  }

  return out;
}
