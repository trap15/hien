#include "../kiban_int.h"

/*
888     888 888b    888        d8888 888      888       .d88888b.   .d8888b.  8888888b.  Y88b   d88P 888b     d888
888     888 8888b   888       d88888 888      888      d88P" "Y88b d88P  Y88b 888   Y88b  Y88b d88P  8888b   d8888
888     888 88888b  888      d88P888 888      888      888     888 888    888 888    888   Y88o88P   88888b.d88888
888     888 888Y88b 888     d88P 888 888      888      888     888 888        888   d88P    Y888P    888Y88888P888
888     888 888 Y88b888    d88P  888 888      888      888     888 888        8888888P"     d888b    888 Y888P 888
888     888 888  Y88888   d88P   888 888      888      888     888 888    888 888 T88b     d88888b   888  Y8P  888
Y88b. .d88P 888   Y8888  d8888888888 888      888      Y88b. .d88P Y88b  d88P 888  T88b   d88P Y88b  888   "   888
 "Y88888P"  888    Y888 d88P     888 88888888 88888888  "Y88888P"   "Y8888P"  888   T88b d88P   Y88b 888       888
*/
static char* cdg_gen_driver_unload_unallocrXm(kiban_board_t* brd, void* elem, int indent, int vtype, int dtype)
{
  char* out;
  char* ref;
  kiban_dev_t* dev = elem;

  out = NULL;
  if(dev->stype != dtype)
    return NULL;

  if((dev->vmtype & vtype) != vtype)
    return NULL;

  ref = cdg_ref_rawdev(brd, dev);
  kuso_catsprintf(&out, "%*smem_free(%s);\n", indent, "", ref);
  mem_free(ref);

  return out;
}

static char* cdg_gen_driver_unload_unallocram(kiban_board_t* brd, void* elem, int indent, int vtype)
{
  return cdg_gen_driver_unload_unallocrXm(brd, elem, indent, vtype, DEVTYPE_RAM);
}

static char* cdg_gen_driver_unload_unallocrom(kiban_board_t* brd, void* elem, int indent, int vtype)
{
  return cdg_gen_driver_unload_unallocrXm(brd, elem, indent, vtype, DEVTYPE_ROM);
}

/*
 .d8888b.         d8888 888     888 8888888888 8888888b.   .d88888b.  888b     d888
d88P  Y88b       d88888 888     888 888        888   Y88b d88P" "Y88b 8888b   d8888
Y88b.           d88P888 888     888 888        888    888 888     888 88888b.d88888
 "Y888b.       d88P 888 Y88b   d88P 8888888    888   d88P 888     888 888Y88888P888
    "Y88b.    d88P  888  Y88b d88P  888        8888888P"  888     888 888 Y888P 888
      "888   d88P   888   Y88o88P   888        888 T88b   888     888 888  Y8P  888
Y88b  d88P  d8888888888    Y888P    888        888  T88b  Y88b. .d88P 888   "   888
 "Y8888P"  d88P     888     Y8P     8888888888 888   T88b  "Y88888P"  888       888
*/
static char* cdg_gen_driver_unload_saverom(kiban_board_t* brd, void* elem, int indent, int vtype)
{
  char* out;
  char* ref;
  kiban_dev_t* dev = elem;

  out = NULL;
  if(dev->type != DEVTYPE_EEPROM)
    return NULL;

  if((dev->vmtype & vtype) != vtype)
    return NULL;

  ref = cdg_ref_rawdev(brd, dev);
  kuso_catsprintf(&out,
"%*srom_save(romvalid, brd, %s, %s\n"
"%*s             %d, %d,\n"
"%*s             \"%s\", \"%s\");\n"
, indent, "", ref
, indent, "", dev->abus, dev->dbus
, indent, "", dev->loc, dev->label);
  mem_free(ref);

  return out;
}

/*
 .d8888b.   .d88888b.  8888888b.  8888888888
d88P  Y88b d88P" "Y88b 888   Y88b 888
888    888 888     888 888    888 888
888        888     888 888   d88P 8888888
888        888     888 8888888P"  888
888    888 888     888 888 T88b   888
Y88b  d88P Y88b. .d88P 888  T88b  888
 "Y8888P"   "Y88888P"  888   T88b 8888888888
*/
char* cdg_gen_driver_unload(kiban_board_t* brd)
{
  char* out;
  char* svrom;
  char* rom;
  char* ram;

  out = NULL;
  svrom = cdg_gen_driver_typed(brd, brd->devs, cdg_gen_driver_unload_saverom);
  rom = cdg_gen_driver_typed(brd, brd->devs, cdg_gen_driver_unload_unallocrom);
  ram = cdg_gen_driver_typed(brd, brd->devs, cdg_gen_driver_unload_unallocram);

  kuso_catsprintf(&out,
"static kuso_pvec_t* %s_unload(hien_board_t* brd)\n"
"{\n"
"  kuso_pvec_t* romvalid; /* hien_rom_valid_t */\n"
"  %s* drv = brd->data;\n"
"\n"
"  romvalid = kuso_pvec_new(0);\n"
"\n"
, brd->prefix, brd->strname);

  if(svrom) {
    kuso_catsprintf(&out,
"  /* Save \"changable\" ROM space */\n"
"%s\n", svrom);
    mem_free(svrom);
  }
  if(rom) {
    kuso_catsprintf(&out,
"  /* Unallocate ROM space */\n"
"%s\n", rom);
    mem_free(rom);
  }
  if(ram) {
    kuso_catsprintf(&out,
"  /* Unallocate RAM space */\n"
"%s\n", ram);
    mem_free(ram);
  }

  kuso_catsprintf(&out,
"  return romvalid;\n"
"}\n");

  return out;
}

