#include "../kiban_int.h"

char* cdg_gen_snd_pwrite(kiban_board_t* brd, int sndidx, int srcidx, char* data)
{
  char* out;
  char* ref;
  kiban_sndsrc_desc_t* snd;

  out = NULL;
  ref = cdg_id2ref_sndsrc(brd, sndidx, srcidx);
  snd = cdg_gen_sndidx2sndsrc(brd, sndidx, srcidx);

  kuso_catsprintf(&out,
"sound_add_sample(%s, A_AMP(%s, A_V(%f), A_12V))"
, ref, data, snd->srcamp);
  mem_free(ref);

  return out;
}

char* cdg_gen_portmapr_read(kiban_board_t* brd, kiban_portmapper_t* mapr)
{
  char* out;

  switch(mapr->type) {
    case DEVTYPE_DEV:
      out = cdg_gen_devid_read(brd, mapr->dev, NULL);
      break;
    case DEVTYPE_SND:
      out = NULL;
      break;
    default:
      out = NULL;
      break;
  }

  return out;
}

char* cdg_gen_portmapr_write(kiban_board_t* brd, kiban_portmapper_t* mapr, char* data)
{
  char* out;

  switch(mapr->type) {
    case DEVTYPE_DEV:
      out = cdg_gen_devid_write(brd, mapr->dev, NULL, data);
      break;
    case DEVTYPE_SND:
      out = cdg_gen_snd_pwrite(brd, mapr->sndidx, mapr->srcidx, data);
      break;
    default:
      out = NULL;
      break;
  }

  return out;
}

char* cdg_gen_port_read_single(kiban_board_t* brd, kiban_portmapper_t* mapr)
{
  char* out;
  char* ref;

  out = NULL;
  ref = cdg_gen_portmapr_read(brd, mapr);
  if(ref != NULL) {
    kuso_catsprintf(&out, "  val = %s;\n", ref);
    mem_free(ref);
  }

  if(mapr->bmap != NULL) {
    ref = cdg_gen_busmap(brd, mapr->bmap, "val");
    if(ref != NULL) {
        kuso_strcat(&out, ref);
        mem_free(ref);
    }
  }

  return out;
}

char* cdg_gen_port_write_single(kiban_board_t* brd, kiban_portmapper_t* mapr)
{
  char* out;
  char* ref;

  out = NULL;
  if(mapr->bmap != NULL) {
    ref = cdg_gen_busmap(brd, mapr->bmap, "data");
    if(ref != NULL) {
        kuso_strcat(&out, ref);
        mem_free(ref);
    }
  }

  ref = cdg_gen_portmapr_write(brd, mapr, "data");
  if(ref != NULL) {
    kuso_catsprintf(&out, "  %s;\n", ref);
    mem_free(ref);
  }

  return out;
}

char* cdg_gen_port_read(kiban_board_t* brd, kiban_portmap_t* map)
{
  char* out;
  char* ref;

  out = NULL;

  ref = cdg_ref_portread(brd, map->tag);
  if(map->bus == 0) { /* Analog */
    kuso_catsprintf(&out,
"static analog_t %s(hien_dev_t* dev)\n"
"{\n"
"  %s* drv = dev->drv_data;\n"
"  analog_t val = 0;\n"
"  (void)drv;\n"
, ref, brd->strname);
  }else{
    kuso_catsprintf(&out,
"static uintmax_t %s(hien_dev_t* dev, uintmax_t mask)\n"
"{\n"
"  %s* drv = dev->drv_data;\n"
"  uintmax_t val = 0;\n"
"  (void)drv;\n"
, ref, brd->strname);
  }
  mem_free(ref);

  kiban_portmapper_t** iter = kuso_pvec_iter(map->map, kiban_portmapper_t*);
  for(; *iter != NULL; iter++) {
    ref = cdg_gen_port_read_single(brd, *iter);
    if(ref != NULL) {
      kuso_strcat(&out, ref);
      mem_free(ref);
    }
  }

  if(map->bus == 0) { /* Analog */
    kuso_catsprintf(&out,
"  return val;\n"
"}\n"
);
  }else{
    kuso_catsprintf(&out,
"  return val & 0x%X;\n"
"}\n"
, BITMASK(map->bus));
  }

  return out;
}

char* cdg_gen_port_write(kiban_board_t* brd, kiban_portmap_t* map)
{
  char* out;
  char* ref;

  out = NULL;

  ref = cdg_ref_portwrite(brd, map->tag);
  if(map->bus == 0) { /* Analog */
    kuso_catsprintf(&out,
"static void %s(hien_dev_t* dev, analog_t data)\n"
"{\n"
"  %s* drv = dev->drv_data;\n"
"  (void)drv;\n"
, ref, brd->strname);
  }else{
    kuso_catsprintf(&out,
"static void %s(hien_dev_t* dev, uintmax_t data, uintmax_t mask)\n"
"{\n"
"  %s* drv = dev->drv_data;\n"
"  (void)drv;\n"
, ref, brd->strname);
  }
  mem_free(ref);

  kiban_portmapper_t** iter = kuso_pvec_iter(map->map, kiban_portmapper_t*);
  for(; *iter != NULL; iter++) {
    ref = cdg_gen_port_write_single(brd, *iter);
    if(ref != NULL) {
      kuso_strcat(&out, ref);
      mem_free(ref);
    }
  }

  kuso_catsprintf(&out, "}\n");

  return out;
}

char* cdg_gen_port(kiban_board_t* brd, kiban_portmap_t* map)
{
  char* out;
  char* rd;
  char* wr;

  out = NULL;
  rd = cdg_gen_port_read(brd, map);
  wr = cdg_gen_port_write(brd, map);
  kuso_asprintf(&out, "%s%s", rd, wr);
  mem_free(rd);
  mem_free(wr);
  return out;
}

char* cdg_gen_all_ports(kiban_board_t* brd)
{
  char* out;
  char* ref;

  out = NULL;
  kiban_portmap_t** iter = kuso_pvec_iter(brd->portmaps, kiban_portmap_t*);
  for(; *iter != NULL; iter++) {
    ref = cdg_gen_port(brd, *iter);
    kuso_strcat(&out, ref);
    mem_free(ref);
  }

  return out;
}
