#include "../kiban_int.h"

char* cdg_gen_submap_read_single(kiban_board_t* brd, kiban_submapper_t* mapr)
{
  char* out;
  char* core;
  char* atrans;
  char* dtrans;

  out = NULL;

  kuso_catsprintf(&out,
"  { /* Device [%s] */ \n"
"    taddr = addr;\n"
, mapr->dev->desc->tag);

  if(mapr->abmap != NULL) {
    atrans = cdg_gen_busmap(brd, mapr->abmap, "taddr");
    kuso_strcat(&out, atrans);
    mem_free(atrans);
  }

  core = cdg_gen_devid_read(brd, mapr->dev, "taddr");
  kuso_catsprintf(&out, "    tval = %s;\n", core);
  mem_free(core);

  if(mapr->dbmap != NULL) {
    dtrans = cdg_gen_busmap(brd, mapr->dbmap, "tval");
    kuso_strcat(&out, dtrans);
    mem_free(dtrans);
  }

  kuso_catsprintf(&out,
"    val |= tval;\n"
"  }\n"
"\n");

  return out;
}

char* cdg_gen_submap_read(kiban_board_t* brd, kiban_submap_t* map)
{
  char* out;
  char* ref;
  int has_content;

  kiban_submapper_t* mapr;

  out = NULL;

  ref = cdg_ref_subread(brd, map->tag);
  kuso_catsprintf(&out,
"static uintmax_t %s(hien_dev_t* dev, addr_t addr)\n"
"{\n"
"  %s* drv = dev->drv_data;\n"
"  uintmax_t val = 0;\n"
"  uintmax_t tval;\n"
"  addr_t taddr;\n"
"  addr &= 0x%X;\n"
"\n"
, ref, brd->strname, BITMASK(map->abus));
  mem_free(ref);

  has_content = FALSE;
  kiban_submapper_t** iter = kuso_pvec_iter(map->map, kiban_submapper_t*);
  for(; *iter != NULL; iter++) {
    mapr = *iter;
    ref = cdg_gen_submap_read_single(brd, mapr);
    kuso_strcat(&out, ref);
    mem_free(ref);
    has_content = TRUE;
  }

  if(!has_content) {
    kuso_strcat(&out, "  (void)taddr;\n");
    kuso_strcat(&out, "  (void)tval;\n");
  }

  kuso_catsprintf(&out,
"  return val & 0x%X;\n"
"}\n"
, BITMASK(map->dbus));

  return out;
}

char* cdg_gen_submap_write_single(kiban_board_t* brd, kiban_submapper_t* mapr)
{
  char* out;
  char* core;
  char* atrans;
  char* dtrans;

  out = NULL;

  kuso_catsprintf(&out,
"  { /* Device [%s] */ \n"
"    taddr = addr;\n"
"    tval = val;\n"
, mapr->dev->desc->tag);

  if(mapr->abmap != NULL) {
    atrans = cdg_gen_busmap(brd, mapr->abmap, "taddr");
    kuso_strcat(&out, atrans);
    mem_free(atrans);
  }

  if(mapr->dbmap != NULL) {
    dtrans = cdg_gen_busmap(brd, mapr->dbmap, "tval");
    kuso_strcat(&out, dtrans);
    mem_free(dtrans);
  }

  core = cdg_gen_devid_write(brd, mapr->dev, "taddr", "tval");
  kuso_catsprintf(&out, "    %s;\n", core);
  mem_free(core);

  kuso_catsprintf(&out,
"  }\n"
"\n");

  return out;
}

char* cdg_gen_submap_write(kiban_board_t* brd, kiban_submap_t* map)
{
  char* out;
  char* ref;
  int has_content;

  kiban_submapper_t* mapr;

  out = NULL;

  ref = cdg_ref_subwrite(brd, map->tag);
  kuso_catsprintf(&out,
"static void %s(hien_dev_t* dev, addr_t addr, uintmax_t val)\n"
"{\n"
"  %s* drv = dev->drv_data;\n"
"  uintmax_t tval;\n"
"  addr_t taddr;\n"
"  addr &= 0x%X;\n"
"\n"
, ref, brd->strname, BITMASK(map->abus));
  mem_free(ref);

  has_content = FALSE;
  kiban_submapper_t** iter = kuso_pvec_iter(map->map, kiban_submapper_t*);
  for(; *iter != NULL; iter++) {
    mapr = *iter;
    ref = cdg_gen_submap_write_single(brd, mapr);
    kuso_strcat(&out, ref);
    mem_free(ref);
    has_content = TRUE;
  }

  if(!has_content) {
    kuso_strcat(&out, "  (void)taddr;\n");
    kuso_strcat(&out, "  (void)tval;\n");
  }

  kuso_catsprintf(&out,
"}\n"
, BITMASK(map->dbus));

  return out;
}

char* cdg_gen_submap(kiban_board_t* brd, kiban_submap_t* map)
{
  char* out;
  char* rd;
  char* wr;

  out = NULL;
  rd = cdg_gen_submap_read(brd, map);
  wr = cdg_gen_submap_write(brd, map);
  kuso_asprintf(&out, "%s%s", rd, wr);
  mem_free(rd);
  mem_free(wr);
  return out;
}

char* cdg_gen_all_submaps(kiban_board_t* brd)
{
  char* out;
  char* sub;

  out = NULL;
  kiban_submap_t** iter = kuso_pvec_iter(brd->submaps, kiban_submap_t*);
  for(; *iter != NULL; iter++) {
    sub = cdg_gen_submap(brd, *iter);
    kuso_strcat(&out, sub);
    mem_free(sub);
  }

  return out;
}
