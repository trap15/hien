#include "../kiban_int.h"

static char* cdg_gen_sndplay_cmd_one(kiban_board_t* brd, kiban_music_cmd_t* cmd)
{
  char* out;
  char* datastr;
  char* ref;

  out = NULL;
  datastr = NULL;

  if(cmd->key) {
    kuso_asprintf(&datastr, "sndid");
  }else{
    kuso_asprintf(&datastr, "0x%X", cmd->data);
  }

  ref = cdg_gen_devid_write(brd, cmd->dev, NULL, datastr);
  mem_free(datastr);

  kuso_asprintf(&out, "  %s;\n", ref);

  mem_free(ref);

  return out;
}

static char* cdg_gen_sndplay_cmds(kiban_board_t* brd, kuso_pvec_t* cmds)
{
  char* out;
  char* ref;
  out = NULL;

  kiban_music_cmd_t** iter = kuso_pvec_iter(cmds, kiban_music_cmd_t*);
  for(; *iter != NULL; iter++) {
    ref = cdg_gen_sndplay_cmd_one(brd, *iter);
    kuso_strcat(&out, ref);
    mem_free(ref);
  }
  return out;
}

static char* cdg_gen_sndplay_play(kiban_board_t* brd)
{
  return cdg_gen_sndplay_cmds(brd, brd->music->playcmds);
}

static char* cdg_gen_sndplay_stop(kiban_board_t* brd)
{
  return cdg_gen_sndplay_cmds(brd, brd->music->stopcmds);
}

static char* cdg_gen_sndplay_reset(kiban_board_t* brd)
{
  return cdg_gen_sndplay_cmds(brd, brd->music->rstcmds);
}

char* cdg_gen_sndplay(kiban_board_t* brd)
{
  char* out;
  char* ref;

  char* playstr;
  char* stopstr;
  char* resetstr;

  out = NULL;
  playstr = mem_strdup("NULL");
  stopstr = mem_strdup("NULL");
  resetstr = mem_strdup("NULL");

  if(brd->music->has_play) {
    kuso_asprintf(&playstr, "%s_sndplay_play", brd->prefix);
    kuso_catsprintf(&out,
"static void %s(hien_sndplay_t* sndp, int sndid)\n"
"{\n"
"  %s* drv = sndp->vm->board->data;\n"
"  (void)drv;\n"
"  (void)sndid;\n"
, playstr, brd->strname);
    ref = cdg_gen_sndplay_play(brd);
    kuso_strcat(&out, ref);
    mem_free(ref);
    kuso_catsprintf(&out, "}\n\n");
  }
  if(brd->music->has_stop) {
    kuso_asprintf(&stopstr, "%s_sndplay_stop", brd->prefix);
    kuso_catsprintf(&out,
"static void %s(hien_sndplay_t* sndp, int sndid)\n"
"{\n"
"  %s* drv = sndp->vm->board->data;\n"
"  (void)drv;\n"
"  (void)sndid;\n"
, stopstr, brd->strname);
    ref = cdg_gen_sndplay_stop(brd);
    kuso_strcat(&out, ref);
    mem_free(ref);
    kuso_catsprintf(&out, "}\n\n");
  }
  if(brd->music->has_reset) {
    kuso_asprintf(&resetstr, "%s_sndplay_reset", brd->prefix);
    kuso_catsprintf(&out,
"static void %s(hien_sndplay_t* sndp)\n"
"{\n"
"  %s* drv = sndp->vm->board->data;\n"
"  (void)drv;\n"
, resetstr, brd->strname);
    ref = cdg_gen_sndplay_reset(brd);
    kuso_strcat(&out, ref);
    mem_free(ref);
    kuso_catsprintf(&out, "}\n\n");
  }

  kuso_catsprintf(&out,
"static hien_sndplay_desc_t %s_sndplay = {\n"
"  .play  = %s,\n"
"  .stop  = %s,\n"
"  .reset = %s,\n"
"\n"
"  .default_id = %d,\n"
"  .stop_id    = %d,\n"
"  .min_id     = %d,\n"
"  .max_id     = %d,\n"
"\n"
"  .boot_time = %lld,\n"
"  .chg_time = %lld,\n"
"};\n"
, brd->prefix
, playstr, stopstr, resetstr
, brd->music->default_id, brd->music->stop_id, brd->music->min_id, brd->music->max_id
, brd->music->boot_time, brd->music->chg_time
);

  mem_free(playstr);
  mem_free(stopstr);
  mem_free(resetstr);

  return out;
}
