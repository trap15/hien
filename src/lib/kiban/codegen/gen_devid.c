#include "../kiban_int.h"

static char* cdg_gen_addr_gen(char* addrvar, addr_t base)
{
  char* out = NULL;

  if(addrvar && base)
    kuso_catsprintf(&out, "(%s + 0x%X)", addrvar, base);
  else if(addrvar)
    kuso_catsprintf(&out, "%s", addrvar);
  else if(base)
    kuso_catsprintf(&out, "0x%X", base);
  else
    kuso_catsprintf(&out, "0");

  return out;
}

static char* cdg_gen_devid_rXm_access(kiban_board_t* brd, kiban_dev_id_t* dev, char* addr)
{
  char* out;

  char* ref;
  char* type;
  int shift;
  addr_t amask;
  int dbus;

  out = NULL;
  dbus = dev->desc->dbus;
  amask = BITMASK(dev->desc->abus);

  if(dbus <= 8) {
    type = "uint8_t";
    shift = 0;
  }else if(dbus <= 16) {
    type = "uint16_t";
    shift = 1;
  }else if(dbus <= 32) {
    type = "uint32_t";
    shift = 2;
  }else if(dbus <= 64) {
    type = "uint64_t";
    shift = 3;
  }else{ /* TODO: Uhh */
    type = "uint8_t";
    shift = 0;
  }

  ref = cdg_ref_rawdev(brd, dev->desc);

  kuso_catsprintf(&out,
"((%s*)%s)"
, type, ref);

  mem_free(ref);

  if((amask == 0) || ((dev->addr == 0) && (addr == NULL))) {
    kuso_asprintf(&out, "*%s", out);
  }else{
    ref = cdg_gen_addr_gen(addr, dev->addr);

    if(shift)
      kuso_catsprintf(&out, "[(%s >> %d) & 0x%X]", ref, shift, amask);
    else
      kuso_catsprintf(&out, "[%s & 0x%X]", ref, amask);

    mem_free(ref);
  }

  return out;
}

static char* cdg_gen_devid_rXm_read(kiban_board_t* brd, kiban_dev_id_t* dev, char* addr)
{
  char* out;

  char* ref;
  addr_t dmask;

  out = NULL;
  dmask = BITMASK(dev->desc->dbus);

  ref = cdg_gen_devid_rXm_access(brd, dev, addr);

  kuso_asprintf(&out, "%s & 0x%X", ref, dmask);

  mem_free(ref);

  return out;
}

static char* cdg_gen_devid_rom_read(kiban_board_t* brd, kiban_dev_id_t* dev, char* addr)
{
  return cdg_gen_devid_rXm_read(brd, dev, addr);
}

static char* cdg_gen_devid_ram_read(kiban_board_t* brd, kiban_dev_id_t* dev, char* addr)
{
  return cdg_gen_devid_rXm_read(brd, dev, addr);
}

static char* cdg_gen_devid_dev_space_read(kiban_board_t* brd, kiban_dev_id_t* dev, char* addr)
{
  char* out;
  char* ref;

  out = NULL;

  ref = cdg_ref_rawdev(brd, dev->desc);
  kuso_catsprintf(&out,
"dev_read(%s, %s, "
, ref, dev->spc_prt ?: "SPACE_MAIN");
  mem_free(ref);

  ref = cdg_gen_addr_gen(addr, dev->addr);
  kuso_catsprintf(&out, "%s, mask)", ref);
  mem_free(ref);

  return out;
}

static char* cdg_gen_devid_submap_read(kiban_board_t* brd, kiban_dev_id_t* dev, char* addr)
{
  char* out;
  char* ref;
  kiban_submap_t* map;
  map = dev->desc->exdat;

  out = NULL;

  ref = cdg_ref_subread(brd, map->tag);
  kuso_catsprintf(&out,
"%s(dev, %s)"
, ref, addr ?: "0");
  mem_free(ref);

  return out;
}

static char* cdg_gen_devid_space_read(kiban_board_t* brd, kiban_dev_id_t* dev, char* addr)
{
  char* out = NULL;
  switch(dev->desc->stype) {
    case DEVTYPE_ROM:
      out = cdg_gen_devid_rom_read(brd, dev, addr);
      break;
    case DEVTYPE_RAM:
      out = cdg_gen_devid_ram_read(brd, dev, addr);
      break;
    case DEVTYPE_DEV:
      out = cdg_gen_devid_dev_space_read(brd, dev, addr);
      break;
    case DEVTYPE_SUBMAP:
      out = cdg_gen_devid_submap_read(brd, dev, addr);
      break;
    default:
      kiban_error(brd, "CODEGEN:DEVID:SPACE:READ", NULL, "Unknown stype %d", dev->desc->stype);
      break;
  }

  return out;
}
static char* cdg_gen_devid_port_read(kiban_board_t* brd, kiban_dev_id_t* dev)
{
  char* out;
  char* ref;

  out = NULL;
  ref = cdg_ref_rawdev(brd, dev->desc);

  kuso_catsprintf(&out,
"dev_port_read(%s, %s, mask)"
, ref, dev->spc_prt);
  mem_free(ref);

  return out;
}
char* cdg_gen_devid_read(kiban_board_t* brd, kiban_dev_id_t* dev, char* addr)
{
  char* out = NULL;
  switch(dev->type) {
    case DEVID_PORT:
      out = cdg_gen_devid_port_read(brd, dev);
      break;
    case DEVID_SPACE:
      out = cdg_gen_devid_space_read(brd, dev, addr);
      break;
  }
  return out;
}




static char* cdg_gen_devid_rXm_write(kiban_board_t* brd, kiban_dev_id_t* dev, char* addr, char* data)
{
  char* out;

  char* ref;
  addr_t dmask;

  out = NULL;
  dmask = BITMASK(dev->desc->dbus);

  ref = cdg_gen_devid_rXm_access(brd, dev, addr);

  kuso_asprintf(&out, "%s = %s & 0x%X", ref, data, dmask);

  mem_free(ref);

  return out;
}

static char* cdg_gen_devid_rom_write(kiban_board_t* brd, kiban_dev_id_t* dev, char* addr, char* data)
{
  (void)brd;
  (void)dev;
  (void)addr;
  (void)data;
  /* TODO: Error? */
  return mem_strdup("");
}

static char* cdg_gen_devid_ram_write(kiban_board_t* brd, kiban_dev_id_t* dev, char* addr, char* data)
{
  return cdg_gen_devid_rXm_write(brd, dev, addr, data);
}

static char* cdg_gen_devid_dev_space_write(kiban_board_t* brd, kiban_dev_id_t* dev, char* addr, char* data)
{
  char* out;
  char* ref;

  out = NULL;

  ref = cdg_ref_rawdev(brd, dev->desc);
  kuso_catsprintf(&out,
"dev_write(%s, %s, "
, ref, dev->spc_prt ?: "SPACE_MAIN");
  mem_free(ref);

  ref = cdg_gen_addr_gen(addr, dev->addr);
  kuso_catsprintf(&out, "%s, %s, mask)", ref, data);
  mem_free(ref);

  return out;
}

static char* cdg_gen_devid_submap_write(kiban_board_t* brd, kiban_dev_id_t* dev, char* addr, char* data)
{
  char* out;
  char* ref;
  kiban_submap_t* map;
  map = dev->desc->exdat;

  out = NULL;

  ref = cdg_ref_subwrite(brd, map->tag);
  kuso_catsprintf(&out,
"%s(dev, %s, %s)"
, ref, addr ?: "0", data ?: "0");
  mem_free(ref);

  return out;
}

static char* cdg_gen_devid_space_write(kiban_board_t* brd, kiban_dev_id_t* dev, char* addr, char* data)
{
  char* out = NULL;
  switch(dev->desc->stype) {
    case DEVTYPE_ROM:
      out = cdg_gen_devid_rom_write(brd, dev, addr, data);
      break;
    case DEVTYPE_RAM:
      out = cdg_gen_devid_ram_write(brd, dev, addr, data);
      break;
    case DEVTYPE_DEV:
      out = cdg_gen_devid_dev_space_write(brd, dev, addr, data);
      break;
    case DEVTYPE_SUBMAP:
      out = cdg_gen_devid_submap_write(brd, dev, addr, data);
      break;
    default:
      kiban_error(brd, "CODEGEN:DEVID:SPACE:WRITE", NULL, "Unknown stype %d", dev->desc->stype);
      break;
  }

  return out;
}
static char* cdg_gen_devid_port_write(kiban_board_t* brd, kiban_dev_id_t* dev, char* data)
{
  char* out;
  char* ref;

  out = NULL;
  ref = cdg_ref_rawdev(brd, dev->desc);

  kuso_catsprintf(&out,
"dev_port_write(%s, %s, %s, mask)"
, ref, dev->spc_prt, data);
  mem_free(ref);

  return out;
}
char* cdg_gen_devid_write(kiban_board_t* brd, kiban_dev_id_t* dev, char* addr, char* data)
{
  char* out = NULL;
  switch(dev->type) {
    case DEVID_PORT:
      out = cdg_gen_devid_port_write(brd, dev, data);
      break;
    case DEVID_SPACE:
      out = cdg_gen_devid_space_write(brd, dev, addr, data);
      break;
  }
  return out;
}
