#include "../kiban_int.h"

/*
 .d8888b.  888b    888 8888888b.  8888888b.  8888888  .d8888b.
d88P  Y88b 8888b   888 888  "Y88b 888  "Y88b   888   d88P  Y88b
Y88b.      88888b  888 888    888 888    888   888   Y88b.
 "Y888b.   888Y88b 888 888    888 888    888   888    "Y888b.
    "Y88b. 888 Y88b888 888    888 888    888   888       "Y88b.
      "888 888  Y88888 888    888 888    888   888         "888
Y88b  d88P 888   Y8888 888  .d88P 888  .d88P   888   Y88b  d88P
 "Y8888P"  888    Y888 8888888P"  8888888P"  8888888  "Y8888P"
*/
static char* cdg_gen_driver_delete_snddis(kiban_board_t* brd)
{
  char* out;
  char* tref;
  char* ref;
  kiban_snd_desc_t* snd;
  kiban_sndsrc_desc_t* sndsrc;

  out = NULL;
  kiban_snd_desc_t** snditer = kuso_pvec_iter(brd->snds, kiban_snd_desc_t*);
  for(; *snditer != NULL; snditer++) {
    snd = *snditer;

    tref = cdg_ref_snd(brd, snd);
    kuso_catsprintf(&out, "  vm_remove_sound_out(brd->vm, %s);\n", tref);

    kiban_sndsrc_desc_t** srciter = kuso_pvec_iter(snd->srcs, kiban_sndsrc_desc_t*);
    for(; *srciter != NULL; srciter++) {
      sndsrc = *srciter;

      ref = cdg_ref_sndsrc(brd, snd, sndsrc);
      kuso_catsprintf(&out, "  sound_out_remove(%s, %s);\n", tref, ref);
      mem_free(ref);
    }

    mem_free(tref);
  }
  return out;
}

/*
 .d8888b.  888b    888 8888888b.  8888888b.  8888888888 888
d88P  Y88b 8888b   888 888  "Y88b 888  "Y88b 888        888
Y88b.      88888b  888 888    888 888    888 888        888
 "Y888b.   888Y88b 888 888    888 888    888 8888888    888
    "Y88b. 888 Y88b888 888    888 888    888 888        888
      "888 888  Y88888 888    888 888    888 888        888
Y88b  d88P 888   Y8888 888  .d88P 888  .d88P 888        888
 "Y8888P"  888    Y888 8888888P"  8888888P"  8888888888 88888888
*/
static char* cdg_gen_driver_delete_snddel(kiban_board_t* brd)
{
  char* out;
  char* ref;
  kiban_snd_desc_t* snd;
  kiban_sndsrc_desc_t* sndsrc;

  out = NULL;
  kiban_snd_desc_t** snditer = kuso_pvec_iter(brd->snds, kiban_snd_desc_t*);
  for(; *snditer != NULL; snditer++) {
    snd = *snditer;

    ref = cdg_ref_snd(brd, snd);
    kuso_catsprintf(&out, "  sound_out_delete(%s);\n", ref);
    mem_free(ref);

    kiban_sndsrc_desc_t** srciter = kuso_pvec_iter(snd->srcs, kiban_sndsrc_desc_t*);
    for(; *srciter != NULL; srciter++) {
      sndsrc = *srciter;

      ref = cdg_ref_sndsrc(brd, snd, sndsrc);
      kuso_catsprintf(&out, "  sound_delete(%s);\n", ref);
      mem_free(ref);
    }
  }
  return out;
}

/*
 .d8888b.   .d88888b.  8888888b.  8888888888
d88P  Y88b d88P" "Y88b 888   Y88b 888
888    888 888     888 888    888 888
888        888     888 888   d88P 8888888
888        888     888 8888888P"  888
888    888 888     888 888 T88b   888
Y88b  d88P Y88b. .d88P 888  T88b  888
 "Y8888P"   "Y88888P"  888   T88b 8888888888
*/
char* cdg_gen_driver_delete(kiban_board_t* brd)
{
  char* out;
  char* snddis;
  char* snddel;

  out = NULL;
  snddis = cdg_gen_driver_delete_snddis(brd);
  snddel = cdg_gen_driver_delete_snddel(brd);

  kuso_catsprintf(&out,
"static void %s_delete(hien_board_t* brd)\n"
"{\n"
"  %s* drv = brd->data;\n"
"\n"
, brd->prefix, brd->strname);

  if(snddis) {
    kuso_catsprintf(&out,
"  /* Disconnect sound streams */\n"
"%s\n", snddis);
    mem_free(snddis);
  }
  if(snddel) {
    kuso_catsprintf(&out,
"  /* Delete sound streams */\n"
"%s\n", snddel);
    mem_free(snddel);
  }

  kuso_catsprintf(&out,
"  mem_free(drv);\n"
"}\n");

  return out;
}
