#include "../kiban_int.h"

static char* cdg_gen_drvstruct_rXms(kiban_board_t* brd, int type, char* ctype)
{
  char* out;

  kiban_dev_t* dev;

  out = NULL;
  kiban_dev_t** iter = kuso_pvec_iter(brd->devs, kiban_dev_t*);
  for(; *iter != NULL; iter++) {
    dev = *iter;
    if(dev->stype != type)
      continue;

    kuso_catsprintf(&out, "    %s %s; %s%s%s\n", ctype, dev->tag,
              dev->partstr?"/* Part: ":"", dev->partstr?:"",
              dev->partstr?" */":"");
  }

  return out;
}

static char* cdg_gen_drvstruct_roms(kiban_board_t* brd)
{
  return cdg_gen_drvstruct_rXms(brd, DEVTYPE_ROM, "void*");
}
static char* cdg_gen_drvstruct_rams(kiban_board_t* brd)
{
  return cdg_gen_drvstruct_rXms(brd, DEVTYPE_RAM, "void*");
}
static char* cdg_gen_drvstruct_devs(kiban_board_t* brd)
{
  return cdg_gen_drvstruct_rXms(brd, DEVTYPE_DEV, "hien_dev_t*");
}

static char* cdg_gen_drvstruct_snds(kiban_board_t* brd)
{
  char* out;
  kiban_snd_desc_t* snd;
  kiban_sndsrc_desc_t* sndsrc;

  out = NULL;
  kiban_snd_desc_t** snditer = kuso_pvec_iter(brd->snds, kiban_snd_desc_t*);
  for(; *snditer != NULL; snditer++) {
    snd = *snditer;

    kuso_strcat(&out, "    struct {\n");

    kiban_sndsrc_desc_t** srciter = kuso_pvec_iter(snd->srcs, kiban_sndsrc_desc_t*);
    for(; *srciter != NULL; srciter++) {
      sndsrc = *srciter;

      kuso_catsprintf(&out, "      hien_sound_t* %s;\n", sndsrc->tag);
    }

    kuso_catsprintf(&out,
"      hien_sound_out_t* output;\n"
"    } %s;\n\n", snd->tag);
  }

  return out;
}

char* cdg_gen_drvstruct(kiban_board_t* brd)
{
  char* out;
  char* roms;
  char* rams;
  char* devs;
  char* snds;

  out = NULL;

  roms = cdg_gen_drvstruct_roms(brd);
  rams = cdg_gen_drvstruct_rams(brd);
  devs = cdg_gen_drvstruct_devs(brd);
  snds = cdg_gen_drvstruct_snds(brd);

  kuso_catsprintf(&out,
"typedef struct %s %s;\n"
"struct %s {\n"
"  hien_board_t* brd;\n"
"\n"
, brd->strname, brd->strname, brd->strname);

  if(roms) {
    kuso_catsprintf(&out,
"  struct {\n"
"%s"
"  } roms;\n\n"
, roms);
    mem_free(roms);
  }

  if(rams) {
    kuso_catsprintf(&out,
"  struct {\n"
"%s"
"  } rams;\n\n"
, rams);
    mem_free(rams);
  }

  if(devs) {
    kuso_catsprintf(&out,
"  struct {\n"
"%s"
"  } devs;\n\n"
, devs);
    mem_free(devs);
  }

  if(snds) {
    kuso_catsprintf(&out,
"  struct {\n"
"%s"
"  } snds;\n\n"
, snds);
    mem_free(snds);
  }

  kuso_strcat(&out, "};\n");

  return out;
}
