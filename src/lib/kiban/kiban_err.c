#include "hien.h"
#include "kiban_int.h"

void kiban_error_va(kiban_board_t* brd, char* subsys, char* entity, char* msg, va_list vl)
{
  (void)brd;
  char* fmsg = NULL;

  kuso_catsprintf(&fmsg, "[KIBAN", subsys);

  if(subsys != NULL)
    kuso_catsprintf(&fmsg, ":%s", subsys);

  kuso_catsprintf(&fmsg, "] Error");
  if(entity != NULL)
    kuso_catsprintf(&fmsg, " on entity '%s'", entity);

  kuso_catsprintf(&fmsg, ": ", entity);
  kuso_vcatsprintf(&fmsg, msg, vl);

  fprintf(stderr, "%s\n", fmsg);

  mem_free(fmsg);
}

void kiban_error(kiban_board_t* brd, char* subsys, char* entity, char* msg, ...)
{
  va_list vl;
  va_start(vl, msg);

  kiban_error_va(brd, subsys,entity,msg,vl);

  va_end(vl);
}
