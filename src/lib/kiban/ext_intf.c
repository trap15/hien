/* Interface to non-local things */

#include "kiban_int.h"

char* kiban_ext_make_rom_path(void* intf, char* loc, char* label)
{
  hien_vm_t* vm = intf;
  return config_make_rom_path(vm->config, loc, label);
}
char* kiban_ext_make_def_path(void* intf, char* name, char* ext)
{
  hien_vm_t* vm = intf;
  return config_make_def_path(vm->config, name, ext);
}
char* kiban_ext_make_state_path(void* intf, char* name, char* path)
{
  hien_vm_t* vm = intf;
  return config_make_rom_path(vm->config, name, path);
}
