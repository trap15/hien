/* Interface to non-local things */

#ifndef EXT_INTF_H_
#define EXT_INTF_H_

#include "hien.h"

char* kiban_ext_make_rom_path(void* intf, char* loc, char* label);
char* kiban_ext_make_def_path(void* intf, char* name, char* ext);
char* kiban_ext_make_state_path(void* intf, char* name, char* path);

#endif
