#include "hien.h"
#include "kiban_int.h"

char* kiban_generate(void* intf)
{
  char* path;
  char* code;
  kiban_board_t* brd;

  brd = kiban_board_new(intf);

  brd->base = mem_strdup("drv");
  kuso_asprintf(&brd->prefix, "brd_%s", brd->base);
  kuso_asprintf(&brd->strname, "%s_t", brd->prefix);

  path = kiban_ext_make_def_path(intf, "board", "def");
  if(kiban_gmd(brd, path) < 0) {
    kiban_error(brd, "GMD", path, "Parsing failed");
    mem_free(path);

    code = NULL;
    goto _gen_end;
  }
  mem_free(path);

  char** hdl_iter = kuso_pvec_iter(brd->hdls, char*);
  for(; *hdl_iter != NULL; hdl_iter++) {
    path = kiban_ext_make_def_path(intf, *hdl_iter, NULL);
    if(kiban_hdl(brd, path) < 0) {
      kiban_error(brd, "HDL", path, "Parsing failed");
      mem_free(path);

      code = NULL;
      goto _gen_end;
    }
    mem_free(path);
  }

  if(kiban_board_cook(brd) < 0) {
    kiban_error(brd, "COOKER", path, "Kiban cooking failed");

    code = NULL;
    goto _gen_end;
  }

  code = kiban_codegen(brd);
  if(code == NULL) {
    kiban_error(brd, "CODEGEN", path, "Kiban codegen failed");

    code = NULL;
    goto _gen_end;
  }

_gen_end:
  kiban_board_free(brd);
  return code;
}
