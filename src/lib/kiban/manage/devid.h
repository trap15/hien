#ifndef MANAGE_DEVID_H_
#define MANAGE_DEVID_H_

struct kiban_dev_id_t {
  char* dev;
  char* spc_prt;
  addr_t addr;
  int type;

  kiban_dev_t* desc;
};

kiban_dev_id_t* kiban_devid_new(void);
void kiban_devid_free(kiban_dev_id_t* id);
int kiban_devid_cook(kiban_board_t* brd, kiban_dev_id_t* id);
void kiban_devid_set_devname(kiban_dev_id_t* id, char* text);
void kiban_devid_set_space(kiban_dev_id_t* id, char* text);
void kiban_devid_set_port(kiban_dev_id_t* id, char* text);
void kiban_devid_set_addr(kiban_dev_id_t* id, addr_t addr);

#endif
