#ifndef MANAGE_SUBMAP_H_
#define MANAGE_SUBMAP_H_

struct kiban_submap_t {
  kiban_dev_t* fdev; /* pseudo-dev */

  char* tag;
  int abus;
  int dbus;

  int mpcnt;

  kuso_pvec_t* map; /* kiban_submapper_t* */
};

kiban_submap_t* kiban_submap_new(char* tag);
void kiban_submap_free(kiban_submap_t* map);
int kiban_submap_cook(kiban_board_t* brd, kiban_submap_t* map);
void kiban_submap_add_mapper(kiban_submap_t* map, kiban_submapper_t* mapr);

#endif
