#ifndef MANAGE_PORTMAP_REF_H_
#define MANAGE_PORTMAP_REF_H_

struct kiban_portmap_ref_t {
  char* name;
  kiban_portmap_t* map;
  int key;
};

kiban_portmap_ref_t* kiban_portmap_ref_new(void);
void kiban_portmap_ref_free(kiban_portmap_ref_t* mref);
int kiban_portmap_ref_cook(kiban_board_t* brd, kiban_portmap_ref_t* mref);

#endif
