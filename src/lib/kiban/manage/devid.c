#include "../kiban_int.h"

kiban_dev_id_t* kiban_devid_new(void)
{
  kiban_dev_id_t* id;

  id = mem_alloc_type(kiban_dev_id_t);
  id->dev = mem_strdup("");
  id->spc_prt = mem_strdup("SPACE_MAIN");
  id->addr = 0;
  id->type = DEVID_SPACE;

  id->desc = NULL;

  return id;
}

void kiban_devid_free(kiban_dev_id_t* id)
{
  mem_free(id->dev);
  mem_free(id->spc_prt);
  mem_free(id);
}

void kiban_devid_set_devname(kiban_dev_id_t* id, char* text)
{
  mem_free(id->dev);
  id->dev = mem_strdup(text);
}
void kiban_devid_set_space(kiban_dev_id_t* id, char* text)
{
  mem_free(id->spc_prt);
  id->spc_prt = mem_strdup(text);
  id->type = DEVID_SPACE;
  id->addr = 0;
}
void kiban_devid_set_port(kiban_dev_id_t* id, char* text)
{
  mem_free(id->spc_prt);
  id->spc_prt = mem_strdup(text);
  id->type = DEVID_PORT;
}
void kiban_devid_set_addr(kiban_dev_id_t* id, addr_t addr)
{
  id->addr = addr;
}

int kiban_devid_cook(kiban_board_t* brd, kiban_dev_id_t* id)
{
  id->desc = kiban_str2ptr_device(brd, id->dev);
  if((id->desc == NULL) && (id->dev != NULL)) {
    kiban_error(brd, "COOKER:DEVID", NULL, "DevID cooking failed");
    goto _fail;
  }

  return 0;
_fail:
  return -1;
}
