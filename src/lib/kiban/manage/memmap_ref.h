#ifndef MANAGE_MEMMAP_REF_H_
#define MANAGE_MEMMAP_REF_H_

struct kiban_memmap_ref_t {
  char* name;
  kiban_memmap_t* map;
};

kiban_memmap_ref_t* kiban_memmap_ref_new(void);
void kiban_memmap_ref_free(kiban_memmap_ref_t* mref);
int kiban_memmap_ref_cook(kiban_board_t* brd, kiban_memmap_ref_t* mref);

#endif
