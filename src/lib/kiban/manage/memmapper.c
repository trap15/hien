#include "../kiban_int.h"

kiban_memmapper_t* kiban_memmapper_new(void)
{
  kiban_memmapper_t* mapr;

  mapr = mem_alloc_type(kiban_memmapper_t);
  mapr->acc = 0;
  mapr->mask = 0;
  mapr->val = 0;
  mapr->amap = NULL;
  mapr->dmap = NULL;
  mapr->abmap = NULL;
  mapr->dbmap = NULL;
  mapr->dev = NULL;

  return mapr;
}

void kiban_memmapper_free(kiban_memmapper_t* mapr)
{
  if(mapr->amap != NULL) mem_free(mapr->amap);
  if(mapr->dmap != NULL) mem_free(mapr->dmap);
  if(mapr->dev != NULL) kiban_devid_free(mapr->dev);
  mem_free(mapr);
}

int kiban_memmapper_cook(kiban_board_t* brd, kiban_memmapper_t* mapr)
{
  if(mapr->dev) {
    if(kiban_devid_cook(brd, mapr->dev) < 0) {
      kiban_error(brd, "COOKER:MEMMAPPER", NULL, "DevID cooking failed");
      goto _fail;
    }
  }

  if(mapr->amap) {
    mapr->abmap = kiban_str2ptr_busmap(brd, mapr->amap);
    if(mapr->abmap == NULL) {
      kiban_error(brd, "COOKER:MEMMAPPER", NULL, "Busmap '%s' not found", mapr->amap);
      goto _fail;
    }
  }else{
    mapr->abmap = NULL;
  }

  if(mapr->dmap) {
    mapr->dbmap = kiban_str2ptr_busmap(brd, mapr->dmap);
    if(mapr->dbmap == NULL) {
      kiban_error(brd, "COOKER:MEMMAPPER", NULL, "Busmap '%s' not found", mapr->dmap);
      goto _fail;
    }
  }else{
    mapr->dbmap = NULL;
  }

  return 0;
_fail:
  return -1;
}

void kiban_memmapper_set_access(kiban_memmapper_t* mapr, int acc)
{
  mapr->acc = acc;
}
void kiban_memmapper_set_maskaddr(kiban_memmapper_t* mapr, addr_t mask, addr_t val)
{
  mapr->mask = mask;
  mapr->val = val;
}
void kiban_memmapper_set_amap(kiban_memmapper_t* mapr, char* map)
{
  if(map == NULL)
    mapr->amap = NULL;
  else
    mapr->amap = mem_strdup(map);
}
void kiban_memmapper_set_dmap(kiban_memmapper_t* mapr, char* map)
{
  if(map == NULL)
    mapr->dmap = NULL;
  else
    mapr->dmap = mem_strdup(map);
}
void kiban_memmapper_set_devid(kiban_memmapper_t* mapr, char* devstr)
{
  mapr->dev = hdl_parse_devid(devstr);
}
