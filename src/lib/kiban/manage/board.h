#ifndef MANAGE_BOARD_H_
#define MANAGE_BOARD_H_

struct kiban_board_t {
  void* intf; /* Information for the interface to the world */

  char* name;
  char* year;
  char* mfg; /* Manufacturer/Distributer */
  char* base;
  char* strname;
  char* prefix;

  kiban_music_t* music;

  kuso_pvec_t* clks; /* kiban_clock_t */
  kuso_pvec_t* devs; /* kiban_dev_t */
  kuso_pvec_t* snds; /* kiban_snd_desc_t */

  kuso_pvec_t* hdls; /* char* */

  kuso_pvec_t* memmaps; /* kiban_memmap_t */
  kuso_pvec_t* portmaps; /* kiban_portmap_t */
  kuso_pvec_t* busmaps; /* kiban_busmap_t */
  kuso_pvec_t* submaps; /* kiban_submap_t */
};

kiban_board_t* kiban_board_new(void* intf);
void kiban_board_free(kiban_board_t* brd);
int kiban_board_cook(kiban_board_t* brd);

int kiban_str2id_clock(kiban_board_t* brd, char* clk);
int kiban_str2id_device(kiban_board_t* brd, char* dev);
int kiban_str2id_sound(kiban_board_t* brd, char* snd);
int kiban_str2id_soundsrc(kiban_board_t* brd, char* snd, char* src);
int kiban_str2id_busmap(kiban_board_t* brd, char* map);
int kiban_str2id_submap(kiban_board_t* brd, char* map);

kiban_clock_t* kiban_str2ptr_clock(kiban_board_t* brd, char* clk);
kiban_dev_t* kiban_str2ptr_device(kiban_board_t* brd, char* dev);
kiban_snd_desc_t* kiban_str2ptr_sound(kiban_board_t* brd, char* snd);
kiban_sndsrc_desc_t* kiban_str2ptr_soundsrc(kiban_board_t* brd, char* snd, char* src);
kiban_busmap_t* kiban_str2ptr_busmap(kiban_board_t* brd, char* map);
kiban_submap_t* kiban_str2ptr_submap(kiban_board_t* brd, char* map);

kiban_clock_t* kiban_id2ptr_clock(kiban_board_t* brd, int clk);
kiban_dev_t* kiban_id2ptr_device(kiban_board_t* brd, int dev);
kiban_snd_desc_t* kiban_id2ptr_sound(kiban_board_t* brd, int snd);
kiban_sndsrc_desc_t* kiban_id2ptr_soundsrc(kiban_board_t* brd, int snd, int src);
kiban_busmap_t* kiban_id2ptr_busmap(kiban_board_t* brd, int map);
kiban_submap_t* kiban_id2ptr_submap(kiban_board_t* brd, int map);

#endif
