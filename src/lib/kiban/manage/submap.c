#include "../kiban_int.h"

kiban_submap_t* kiban_submap_new(char* tag)
{
  kiban_submap_t* map;

  map = mem_alloc_type(kiban_submap_t);
  map->tag = mem_strdup(tag);
  map->map = kuso_pvec_new(0);
  map->abus = 16;
  map->dbus = 8;
  map->mpcnt = 0;

  map->fdev = kiban_dev_new();
  map->fdev->loc = mem_strdup("");
  map->fdev->tag = mem_strdup(tag);
  map->fdev->type = DEVTYPE_SUBMAP;
  map->fdev->vmtype = VMTYPE_ALL;
  map->fdev->exdat = map;

  return map;
}

void kiban_submap_free(kiban_submap_t* map)
{
  mem_free(map->tag);

  kiban_submapper_t** iter = kuso_pvec_iter(map->map, kiban_submapper_t*);
  for(; *iter != NULL; iter++) {
    kiban_submapper_free(*iter);
  }
  kuso_pvec_delete(map->map);

  mem_free(map);
}

int kiban_submap_cook(kiban_board_t* brd, kiban_submap_t* map)
{
  map->fdev->abus = map->abus;
  map->fdev->dbus = map->dbus;
  if(kiban_dev_cook(brd, map->fdev) < 0) {
    kiban_error(brd, "COOKER:SUBMAP", NULL, "Faux device cooking failed");
    goto _fail;
  }

  kiban_submapper_t** iter = kuso_pvec_iter(map->map, kiban_submapper_t*);
  for(; *iter != NULL; iter++) {
    if(kiban_submapper_cook(brd, *iter) < 0) {
      kiban_error(brd, "COOKER:SUBMAP", map->tag, "Submapper cooking failed");
      goto _fail;
    }
  }

  return 0;
_fail:
  return -1;
}

void kiban_submap_add_mapper(kiban_submap_t* map, kiban_submapper_t* mapr)
{
  kuso_pvec_append(map->map, mapr);
}
