#include "../kiban_int.h"

kiban_memmap_t* kiban_memmap_new(char* tag)
{
  kiban_memmap_t* map;

  map = mem_alloc_type(kiban_memmap_t);
  map->tag = mem_strdup(tag);
  map->map = kuso_pvec_new(0);
  map->abus = 16;
  map->dbus = 8;
  map->mpcnt = 0;

  return map;
}

void kiban_memmap_free(kiban_memmap_t* map)
{
  mem_free(map->tag);

  kiban_memmapper_t** iter = kuso_pvec_iter(map->map, kiban_memmapper_t*);
  for(; *iter != NULL; iter++) {
    kiban_memmapper_free(*iter);
  }
  kuso_pvec_delete(map->map);

  mem_free(map);
}

int kiban_memmap_cook(kiban_board_t* brd, kiban_memmap_t* map)
{
  kiban_memmapper_t** iter = kuso_pvec_iter(map->map, kiban_memmapper_t*);
  for(; *iter != NULL; iter++) {
    if(kiban_memmapper_cook(brd, *iter) < 0) {
      kiban_error(brd, "COOKER:MEMMAP", map->tag, "Memmapper cooking failed");
      goto _fail;
    }
  }

  return 0;
_fail:
  return -1;
}

void kiban_memmap_add_mapper(kiban_memmap_t* map, kiban_memmapper_t* mapr)
{
  kuso_pvec_append(map->map, mapr);
}
