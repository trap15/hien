#include "../kiban_int.h"

kiban_portmap_t* kiban_portmap_new(char* tag)
{
  kiban_portmap_t* map;

  map = mem_alloc_type(kiban_portmap_t);
  map->tag = mem_strdup(tag);
  map->map = kuso_pvec_new(0);
  map->bus = 1;
  map->mpcnt = 0;

  return map;
}
void kiban_portmap_free(kiban_portmap_t* map)
{
  mem_free(map->tag);

  kiban_portmapper_t** iter = kuso_pvec_iter(map->map, kiban_portmapper_t*);
  for(; *iter != NULL; iter++) {
    kiban_portmapper_free(*iter);
  }
  kuso_pvec_delete(map->map);

  mem_free(map);
}
int kiban_portmap_cook(kiban_board_t* brd, kiban_portmap_t* map)
{
  kiban_portmapper_t** iter = kuso_pvec_iter(map->map, kiban_portmapper_t*);
  for(; *iter != NULL; iter++) {
    if(kiban_portmapper_cook(brd, *iter) < 0) {
      kiban_error(brd, "COOKER:PORTMAP", map->tag, "Portmapper cooking failed");
      goto _fail;
    }
  }

  return 0;
_fail:
  return -1;
}
void kiban_portmap_add_mapper(kiban_portmap_t* map, kiban_portmapper_t* mapr)
{
  kuso_pvec_append(map->map, mapr);
}
void kiban_portmap_set_buslen(kiban_portmap_t* map, int bus)
{
  map->bus = bus;
}
