#ifndef MANAGE_BUSMAPPER_H_
#define MANAGE_BUSMAPPER_H_

struct kiban_busmapper_t {
  kiban_dev_id_t* dev;

  int spin;
  int dpin;
  int len;
  int key;
};

kiban_busmapper_t* kiban_busmapper_new(void);
void kiban_busmapper_free(kiban_busmapper_t* mapr);
int kiban_busmapper_cook(kiban_board_t* brd, kiban_busmapper_t* mapr);
void kiban_busmapper_set_devid(kiban_busmapper_t* mapr, char* devstr);

#endif
