#ifndef MANAGE_MUSIC_H_
#define MANAGE_MUSIC_H_

enum {
  MUSCMD_DATA_NONE=0,
  MUSCMD_DATA_SNDID,
};

struct kiban_music_cmd_t {
  kiban_dev_id_t* dev;

  int key;

  addr_t addr;
  uintmax_t data;
};

struct kiban_music_t {
  int stop_id;
  int default_id;
  int min_id;
  int max_id;

  timetick_t boot_time; /* Time to boot */
  timetick_t chg_time; /* Time to switch songs */

  int has_play;
  int has_stop;
  int has_reset;

  kuso_pvec_t* playcmds; /* kiban_music_cmd_t */
  kuso_pvec_t* stopcmds; /* kiban_music_cmd_t */
  kuso_pvec_t* rstcmds; /* kiban_music_cmd_t */
};

kiban_music_t* kiban_music_new(void);
void kiban_music_free(kiban_music_t* desc);
int kiban_music_cook(kiban_board_t* brd, kiban_music_t* desc);

kiban_music_cmd_t* kiban_music_cmd_new(void);
void kiban_music_cmd_free(kiban_music_cmd_t* cmd);
int kiban_music_cmd_cook(kiban_board_t* brd, kiban_music_cmd_t* cmd);
void kiban_music_cmd_set_devid(kiban_music_cmd_t* cmd, char* devstr);
void kiban_music_cmd_set_fixdata(kiban_music_cmd_t* cmd, int key);
void kiban_music_cmd_set_data(kiban_music_cmd_t* cmd, uintmax_t data);

#endif
