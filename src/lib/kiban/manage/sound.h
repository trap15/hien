#ifndef MANAGE_SOUND_H_
#define MANAGE_SOUND_H_

struct kiban_snd_desc_t {
  char* tag;
  kuso_pvec_t* srcs; /* kiban_sndsrc_desc_t */
};

kiban_snd_desc_t* kiban_sound_new(void);
void kiban_sound_free(kiban_snd_desc_t* snd);
int kiban_sound_cook(kiban_board_t* brd, kiban_snd_desc_t* snd);
void kiban_sound_add_source(kiban_snd_desc_t* snd, kiban_sndsrc_desc_t* src);

#endif
