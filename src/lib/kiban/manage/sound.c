#include "../kiban_int.h"

kiban_snd_desc_t* kiban_sound_new(void)
{
  kiban_snd_desc_t* snd;

  snd = mem_alloc_type(kiban_snd_desc_t);
  snd->srcs = kuso_pvec_new(0);
  snd->tag = NULL;

  return snd;
}

void kiban_sound_free(kiban_snd_desc_t* snd)
{
  kiban_sndsrc_desc_t** iter = kuso_pvec_iter(snd->srcs, kiban_sndsrc_desc_t*);
  for(; *iter != NULL; iter++) {
    kiban_soundsrc_free(*iter);
  }
  kuso_pvec_delete(snd->srcs);

  if(snd->tag != NULL)
    mem_free(snd->tag);
  mem_free(snd);
}

int kiban_sound_cook(kiban_board_t* brd, kiban_snd_desc_t* snd)
{
  kiban_sndsrc_desc_t** iter = kuso_pvec_iter(snd->srcs, kiban_sndsrc_desc_t*);
  for(; *iter != NULL; iter++) {
    if(kiban_soundsrc_cook(brd, *iter) < 0) {
      kiban_error(brd, "COOKER:SOUND", snd->tag, "Sound source cooking failed");
      goto _fail;
    }
  }
  return 0;
_fail:
  return -1;
}

void kiban_sound_add_source(kiban_snd_desc_t* snd, kiban_sndsrc_desc_t* src)
{
  kuso_pvec_append(snd->srcs, src);
}
