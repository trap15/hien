#include "../kiban_int.h"

kiban_board_t* kiban_board_new(void* intf)
{
  kiban_board_t* brd;

  brd = mem_alloc_type(kiban_board_t);
  brd->intf = intf;

  brd->hdls = kuso_pvec_new(0);
  brd->clks = kuso_pvec_new(0);
  brd->devs = kuso_pvec_new(0);
  brd->snds = kuso_pvec_new(0);
  brd->memmaps = kuso_pvec_new(0);
  brd->portmaps = kuso_pvec_new(0);
  brd->busmaps = kuso_pvec_new(0);
  brd->submaps = kuso_pvec_new(0);

  brd->music = kiban_music_new();

  brd->base = NULL;
  brd->prefix = NULL;
  brd->strname = NULL;
  brd->name = NULL;
  brd->year = NULL;
  brd->mfg = NULL;

  return brd;
}

void kiban_board_free(kiban_board_t* brd)
{
  if(brd->name != NULL) mem_free(brd->name);
  if(brd->year != NULL) mem_free(brd->year);
  if(brd->mfg != NULL) mem_free(brd->mfg);
  if(brd->base != NULL) mem_free(brd->base);
  if(brd->prefix != NULL) mem_free(brd->prefix);
  if(brd->strname != NULL) mem_free(brd->strname);

  kiban_clock_t** clkiter = kuso_pvec_iter(brd->clks, kiban_clock_t*);
  for(; *clkiter != NULL; clkiter++) {
    kiban_clock_free(*clkiter);
  }
  kuso_pvec_delete(brd->clks);

  kiban_dev_t** deviter = kuso_pvec_iter(brd->devs, kiban_dev_t*);
  for(; *deviter != NULL; deviter++) {
    kiban_dev_free(*deviter);
  }
  kuso_pvec_delete(brd->devs);

  kiban_snd_desc_t** snditer = kuso_pvec_iter(brd->snds, kiban_snd_desc_t*);
  for(; *snditer != NULL; snditer++) {
    kiban_sound_free(*snditer);
  }
  kuso_pvec_delete(brd->snds);

  char** hdliter = kuso_pvec_iter(brd->hdls, char*);
  for(; *hdliter != NULL; hdliter++) {
    mem_free(*hdliter);
  }
  kuso_pvec_delete(brd->hdls);

  kiban_memmap_t** mmiter = kuso_pvec_iter(brd->memmaps, kiban_memmap_t*);
  for(; *mmiter != NULL; mmiter++) {
    kiban_memmap_free(*mmiter);
  }
  kuso_pvec_delete(brd->memmaps);

  kiban_portmap_t** priter = kuso_pvec_iter(brd->portmaps, kiban_portmap_t*);
  for(; *priter != NULL; priter++) {
    kiban_portmap_free(*priter);
  }
  kuso_pvec_delete(brd->portmaps);

  kiban_busmap_t** bsiter = kuso_pvec_iter(brd->busmaps, kiban_busmap_t*);
  for(; *bsiter != NULL; bsiter++) {
    kiban_busmap_free(*bsiter);
  }
  kuso_pvec_delete(brd->busmaps);

  kiban_submap_t** sbiter = kuso_pvec_iter(brd->submaps, kiban_submap_t*);
  for(; *sbiter != NULL; sbiter++) {
    kiban_submap_free(*sbiter);
  }
  kuso_pvec_delete(brd->submaps);

  kiban_music_free(brd->music);
  mem_free(brd);
}

int kiban_board_cook(kiban_board_t* brd)
{
  kiban_memmap_t** mmiter = kuso_pvec_iter(brd->memmaps, kiban_memmap_t*);
  for(; *mmiter != NULL; mmiter++) {
    if(kiban_memmap_cook(brd, *mmiter) < 0) {
      kiban_error(brd, "COOKER:BOARD", NULL, "Memmap cooking failed");
      goto _fail;
    }
  }
  kiban_portmap_t** priter = kuso_pvec_iter(brd->portmaps, kiban_portmap_t*);
  for(; *priter != NULL; priter++) {
    if(kiban_portmap_cook(brd, *priter) < 0) {
      kiban_error(brd, "COOKER:BOARD", NULL, "Portmap cooking failed");
      goto _fail;
    }
  }
  kiban_busmap_t** bsiter = kuso_pvec_iter(brd->busmaps, kiban_busmap_t*);
  for(; *bsiter != NULL; bsiter++) {
    if(kiban_busmap_cook(brd, *bsiter) < 0) {
      kiban_error(brd, "COOKER:BOARD", NULL, "Busmap cooking failed");
      goto _fail;
    }
  }
  kiban_submap_t** sbiter = kuso_pvec_iter(brd->submaps, kiban_submap_t*);
  for(; *sbiter != NULL; sbiter++) {
    if(kiban_submap_cook(brd, *sbiter) < 0) {
      kiban_error(brd, "COOKER:BOARD", NULL, "Submap cooking failed");
      goto _fail;
    }
  }
  kiban_dev_t** dviter = kuso_pvec_iter(brd->devs, kiban_dev_t*);
  for(; *dviter != NULL; dviter++) {
    if(kiban_dev_cook(brd, *dviter) < 0) {
      kiban_error(brd, "COOKER:BOARD", NULL, "Device cooking failed");
      goto _fail;
    }
  }

  if(kiban_music_cook(brd, brd->music) < 0) {
    kiban_error(brd, "COOKER:BOARD", NULL, "Music cooking failed");
    goto _fail;
  }

  return 0;
_fail:
  return -1;
}



int kiban_str2id_clock(kiban_board_t* brd, char* clk)
{
  int i;
  if(clk == NULL) return -1;
  kiban_clock_t** iter = kuso_pvec_iter(brd->clks, kiban_clock_t*);
  for(i = 0; *iter != NULL; iter++, i++) {
    if(strcmp((*iter)->tag, clk) == 0)
      return i;
  }
  return -1;
}
int kiban_str2id_device(kiban_board_t* brd, char* dev)
{
  int i;
  if(dev == NULL) return -1;
  kiban_dev_t** iter = kuso_pvec_iter(brd->devs, kiban_dev_t*);
  for(i = 0; *iter != NULL; iter++, i++) {
    if(strcmp((*iter)->tag, dev) == 0)
      return i;
  }
  return -1;
}
int kiban_str2id_sound(kiban_board_t* brd, char* snd)
{
  int i;
  if(snd == NULL) return -1;
  kiban_snd_desc_t** iter = kuso_pvec_iter(brd->snds, kiban_snd_desc_t*);
  for(i = 0; *iter != NULL; iter++, i++) {
    if(strcmp((*iter)->tag, snd) == 0)
      return i;
  }
  return -1;
}
int kiban_str2id_soundsrc(kiban_board_t* brd, char* snd, char* src)
{
  int i;
  if(snd == NULL) return -1;
  if(src == NULL) return -1;
  kiban_snd_desc_t* desc = kiban_str2ptr_sound(brd, snd);
  if(desc == NULL) return -1;
  kiban_sndsrc_desc_t** iter = kuso_pvec_iter(desc->srcs, kiban_sndsrc_desc_t*);
  for(i = 0; *iter != NULL; iter++, i++) {
    if(strcmp((*iter)->tag, src) == 0)
      return i;
  }
  return -1;
}
int kiban_str2id_busmap(kiban_board_t* brd, char* map)
{
  int i;
  if(map == NULL) return -1;
  kiban_busmap_t** iter = kuso_pvec_iter(brd->busmaps, kiban_busmap_t*);
  for(i = 0; *iter != NULL; iter++, i++) {
    if(strcmp((*iter)->tag, map) == 0)
      return i;
  }
  return -1;
}
int kiban_str2id_submap(kiban_board_t* brd, char* map)
{
  int i;
  if(map == NULL) return -1;
  kiban_submap_t** iter = kuso_pvec_iter(brd->submaps, kiban_submap_t*);
  for(i = 0; *iter != NULL; iter++, i++) {
    if(strcmp((*iter)->tag, map) == 0)
      return i;
  }
  return -1;
}


kiban_clock_t* kiban_str2ptr_clock(kiban_board_t* brd, char* clk)
{
  int i = kiban_str2id_clock(brd, clk);
  if(i < 0) return NULL;
  return kiban_id2ptr_clock(brd, i);
}
kiban_dev_t* kiban_str2ptr_device(kiban_board_t* brd, char* dev)
{
  int i = kiban_str2id_device(brd, dev);
  if(i < 0) return NULL;
  return kiban_id2ptr_device(brd, i);
}
kiban_snd_desc_t* kiban_str2ptr_sound(kiban_board_t* brd, char* snd)
{
  int i = kiban_str2id_sound(brd, snd);
  if(i < 0) return NULL;
  return kiban_id2ptr_sound(brd, i);
}
kiban_sndsrc_desc_t* kiban_str2ptr_soundsrc(kiban_board_t* brd, char* snd, char* src)
{
  int oi = kiban_str2id_sound(brd, snd);
  if(oi < 0) return NULL;
  int i = kiban_str2id_soundsrc(brd, snd, src);
  if(i < 0) return NULL;
  return kiban_id2ptr_soundsrc(brd, oi, i);
}
kiban_busmap_t* kiban_str2ptr_busmap(kiban_board_t* brd, char* map)
{
  int i = kiban_str2id_busmap(brd, map);
  if(i < 0) return NULL;
  return kiban_id2ptr_busmap(brd, i);
}
kiban_submap_t* kiban_str2ptr_submap(kiban_board_t* brd, char* map)
{
  int i = kiban_str2id_submap(brd, map);
  if(i < 0) return NULL;
  return kiban_id2ptr_submap(brd, i);
}


kiban_clock_t* kiban_id2ptr_clock(kiban_board_t* brd, int clk)
{
  return kuso_pvec_get(brd->clks, clk);
}
kiban_dev_t* kiban_id2ptr_device(kiban_board_t* brd, int dev)
{
  return kuso_pvec_get(brd->devs, dev);
}
kiban_snd_desc_t* kiban_id2ptr_sound(kiban_board_t* brd, int snd)
{
  return kuso_pvec_get(brd->snds, snd);
}
kiban_sndsrc_desc_t* kiban_id2ptr_soundsrc(kiban_board_t* brd, int snd, int src)
{
  kiban_snd_desc_t* desc = kiban_id2ptr_sound(brd, snd);
  if(desc == NULL) return NULL;
  return kuso_pvec_get(desc->srcs, src);
}
kiban_busmap_t* kiban_id2ptr_busmap(kiban_board_t* brd, int map)
{
  return kuso_pvec_get(brd->busmaps, map);
}
kiban_submap_t* kiban_id2ptr_submap(kiban_board_t* brd, int map)
{
  return kuso_pvec_get(brd->submaps, map);
}
