#include "../kiban_int.h"

kiban_busmapper_t* kiban_busmapper_new(void)
{
  kiban_busmapper_t* mapr;

  mapr = mem_alloc_type(kiban_busmapper_t);
  mapr->dev = NULL;
  mapr->spin = 0;
  mapr->dpin = 0;
  mapr->len = 0;
  mapr->key = 0;

  return mapr;
}

void kiban_busmapper_free(kiban_busmapper_t* mapr)
{
  if(mapr->dev)
    kiban_devid_free(mapr->dev);
  mem_free(mapr);
}

int kiban_busmapper_cook(kiban_board_t* brd, kiban_busmapper_t* mapr)
{
  if(mapr->dev) {
    if(kiban_devid_cook(brd, mapr->dev) < 0) {
      kiban_error(brd, "COOKER:BUSMAPPER", NULL, "DevID cooking failed");
      goto _fail;
    }
  }

  return 0;
_fail:
  return -1;
}

void kiban_busmapper_set_devid(kiban_busmapper_t* mapr, char* devstr)
{
  mapr->dev = hdl_parse_devid(devstr);
}
