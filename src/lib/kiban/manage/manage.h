#ifndef MANAGE_H_
#define MANAGE_H_

#include "clock.h"
#include "devid.h"
#include "busmap.h"
#include "busmapper.h"
#include "submap.h"
#include "submapper.h"
#include "memmap.h"
#include "memmapper.h"
#include "memmap_ref.h"
#include "portmap.h"
#include "portmapper.h"
#include "portmap_ref.h"
#include "dev.h"
#include "sound.h"
#include "sound_src.h"
#include "music.h"
#include "board.h"

#endif
