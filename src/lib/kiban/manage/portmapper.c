#include "../kiban_int.h"

kiban_portmapper_t* kiban_portmapper_new(int type)
{
  kiban_portmapper_t* mapr;

  mapr = mem_alloc_type(kiban_portmapper_t);
  mapr->map = NULL;
  mapr->bmap = NULL;
  mapr->dev = NULL;
  mapr->type = type;
  mapr->snd = NULL;
  mapr->sndsrc = NULL;

  return mapr;
}
void kiban_portmapper_free(kiban_portmapper_t* mapr)
{
  if(mapr->map != NULL) mem_free(mapr->map);
  if(mapr->snd != NULL) mem_free(mapr->snd);
  if(mapr->sndsrc != NULL) mem_free(mapr->sndsrc);
  if(mapr->dev != NULL) kiban_devid_free(mapr->dev);
  mem_free(mapr);
}
static int kiban_portmapper_cook_dev(kiban_board_t* brd, kiban_portmapper_t* mapr)
{
  if(mapr->dev) {
    if(kiban_devid_cook(brd, mapr->dev) < 0) {
      kiban_error(brd, "COOKER:PORTMAPPER:DEV", NULL, "DevID cooking failed");
      goto _fail;
    }
  }

  if(mapr->map) {
    mapr->bmap = kiban_str2ptr_busmap(brd, mapr->map);
    if(mapr->bmap == NULL) {
      kiban_error(brd, "COOKER:PORTMAPPER:DEV", NULL, "Busmap '%s' not found", mapr->map);
      goto _fail;
    }
  }else{
    mapr->bmap = NULL;
  }

  return 0;
_fail:
  return -1;
}
static int kiban_portmapper_cook_snd(kiban_board_t* brd, kiban_portmapper_t* mapr)
{
  mapr->sndidx = kiban_str2id_sound(brd, mapr->snd);
  if(mapr->sndidx < 0) {
    kiban_error(brd, "COOKER:PORTMAPPER:SND", NULL, "Sound output '%s' not found", mapr->snd);
    goto _fail;
  }

  mapr->srcidx = kiban_str2id_soundsrc(brd, mapr->snd, mapr->sndsrc);
  if(mapr->srcidx < 0) {
    kiban_error(brd, "COOKER:PORTMAPPER:SND", NULL, "Sound source '%s':'%s' not found", mapr->snd, mapr->sndsrc);
    goto _fail;
  }

  return 0;
_fail:
  return -1;
}
int kiban_portmapper_cook(kiban_board_t* brd, kiban_portmapper_t* mapr)
{
  switch(mapr->type) {
    case DEVTYPE_SND:
      return kiban_portmapper_cook_snd(brd, mapr);
    case DEVTYPE_DEV:
      return kiban_portmapper_cook_dev(brd, mapr);
    default:
      kiban_error(brd, "COOKER:PORTMAPPER", NULL, "Unknown map type %d", mapr->type);
      goto _fail;
  }
  return 0;
_fail:
  return -1;
}
void kiban_portmapper_set_map(kiban_portmapper_t* mapr, char* map)
{
  if(map == NULL)
    mapr->map = NULL;
  else
    mapr->map = mem_strdup(map);
}
void kiban_portmapper_set_devid(kiban_portmapper_t* mapr, char* devstr)
{
  mapr->dev = hdl_parse_devid(devstr);
}
void kiban_portmapper_set_snd(kiban_portmapper_t* mapr, char* snd)
{
  mapr->snd = mem_strdup(snd);
}
void kiban_portmapper_set_sndsrc(kiban_portmapper_t* mapr, char* src)
{
  mapr->sndsrc = mem_strdup(src);
}
