#include "../kiban_int.h"

kiban_dev_t* kiban_dev_new(void)
{
  kiban_dev_t* dev;

  dev = mem_alloc_type(kiban_dev_t);
  dev->type = DEVTYPE_DEV;
  dev->partstr = NULL;
  dev->typestr = NULL;
  dev->label = NULL;
  dev->clkstr = NULL;
  dev->dvr = 0;

  dev->abus = 0;
  dev->dbus = 0;
  dev->access = 0;
  dev->crc = 0;

  dev->spacemap = kuso_pvec_new(0);
  dev->portmap = kuso_pvec_new(0);
  dev->vmtype = VMTYPE_NORMAL;
  dev->exdat = NULL;

  return dev;
}

void kiban_dev_free(kiban_dev_t* dev)
{
  mem_free(dev->loc);
  mem_free(dev->tag);
  if(dev->label)
    mem_free(dev->label);
  if(dev->partstr)
    mem_free(dev->partstr);
  if(dev->typestr)
    mem_free(dev->typestr);
  if(dev->clkstr)
    mem_free(dev->clkstr);

  kiban_memmap_ref_t** mmiter = kuso_pvec_iter(dev->spacemap, kiban_memmap_ref_t*);
  for(; *mmiter != NULL; mmiter++) {
    kiban_memmap_ref_free(*mmiter);
  }
  kuso_pvec_delete(dev->spacemap);

  kiban_portmap_ref_t** priter = kuso_pvec_iter(dev->portmap, kiban_portmap_ref_t*);
  for(; *priter != NULL; priter++) {
    kiban_portmap_ref_free(*priter);
  }
  kuso_pvec_delete(dev->portmap);

  mem_free(dev);
}

int kiban_dev_cook(kiban_board_t* brd, kiban_dev_t* dev)
{
  dev->size = ((dev->dbus << dev->abus) + 7) >> 3; /* Round up */

  switch(dev->type) {
    case DEVTYPE_DEV:
      dev->stype = DEVTYPE_DEV;
      break;
    case DEVTYPE_RAM:
    case DEVTYPE_SRAM:
    case DEVTYPE_DRAM:
    case DEVTYPE_EEPROM:
      dev->stype = DEVTYPE_RAM;
      break;
    case DEVTYPE_ROM:
    case DEVTYPE_PROM:
    case DEVTYPE_EPROM:
    case DEVTYPE_MASKROM:
      dev->stype = DEVTYPE_ROM;
      break;
    default:
      dev->stype = dev->type;
      break;
  }

  if(dev->clkstr == NULL) {
    dev->clk_id = -1;
  }else{
    dev->clk_id = kiban_str2id_clock(brd, dev->clkstr);
    if(dev->clk_id < 0) {
      kiban_error(brd, "COOKER:DEV", dev->tag, "Clock '%s' not found", dev->clkstr);
      goto _fail;
    }
  }

  kiban_memmap_ref_t** spiter = kuso_pvec_iter(dev->spacemap, kiban_memmap_ref_t*);
  for(; *spiter != NULL; spiter++) {
    if(kiban_memmap_ref_cook(brd, *spiter) < 0) {
      kiban_error(brd, "COOKER:DEV", dev->tag, "Memmap cooking failed");
      goto _fail;
    }
  }

  kiban_portmap_ref_t** priter = kuso_pvec_iter(dev->portmap, kiban_portmap_ref_t*);
  for(; *priter != NULL; priter++) {
    if(kiban_portmap_ref_cook(brd, *priter) < 0) {
      kiban_error(brd, "COOKER:DEV", dev->tag, "Portmap cooking failed");
      goto _fail;
    }
  }

  return 0;
_fail:
  return -1;
}
