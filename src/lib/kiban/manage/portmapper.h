#ifndef MANAGE_PORTMAPPER_H_
#define MANAGE_PORTMAPPER_H_

struct kiban_portmapper_t {
  int type;

  char* map;
  kiban_dev_id_t* dev;

  kiban_busmap_t* bmap;

  char* snd;
  char* sndsrc;
  int sndidx, srcidx;
};

kiban_portmapper_t* kiban_portmapper_new(int type);
void kiban_portmapper_free(kiban_portmapper_t* mapr);
int kiban_portmapper_cook(kiban_board_t* brd, kiban_portmapper_t* mapr);
void kiban_portmapper_set_map(kiban_portmapper_t* mapr, char* map);
void kiban_portmapper_set_devid(kiban_portmapper_t* mapr, char* devstr);
void kiban_portmapper_set_snd(kiban_portmapper_t* mapr, char* snd);
void kiban_portmapper_set_sndsrc(kiban_portmapper_t* mapr, char* src);

#endif
