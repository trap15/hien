#ifndef MANAGE_BUSMAP_H_
#define MANAGE_BUSMAP_H_

struct kiban_busmap_t {
  char* tag;
  int size;

  kuso_pvec_t* map; /* kiban_busmapper_t* */
};

kiban_busmap_t* kiban_busmap_new(char* tag);
void kiban_busmap_free(kiban_busmap_t* map);
int kiban_busmap_cook(kiban_board_t* brd, kiban_busmap_t* map);
void kiban_busmap_add_mapper(kiban_busmap_t* map, kiban_busmapper_t* mapr);

#endif
