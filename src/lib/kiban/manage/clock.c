#include "../kiban_int.h"

kiban_clock_t* kiban_clock_new(void)
{
  kiban_clock_t* clk;

  clk = mem_alloc_type(kiban_clock_t);
  clk->loc = NULL;
  clk->tag = NULL;
  clk->spd = 1;

  return clk;
}

void kiban_clock_free(kiban_clock_t* clk)
{
  if(clk->loc)
    mem_free(clk->loc);
  if(clk->tag)
    mem_free(clk->tag);
  mem_free(clk);
}

int kiban_clock_cook(kiban_board_t* brd, kiban_clock_t* clk)
{
  (void)brd;
  (void)clk;
  return 0;
}
