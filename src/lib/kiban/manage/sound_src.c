#include "../kiban_int.h"

kiban_sndsrc_desc_t* kiban_soundsrc_new(void)
{
  kiban_sndsrc_desc_t* src;

  src = mem_alloc_type(kiban_sndsrc_desc_t);

  return src;
}

void kiban_soundsrc_free(kiban_sndsrc_desc_t* src)
{
  mem_free(src->tag);
  mem_free(src);
}

int kiban_soundsrc_cook(kiban_board_t* brd, kiban_sndsrc_desc_t* src)
{
  (void)brd;
  (void)src;
  return 0;
}
