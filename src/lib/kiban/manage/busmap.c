#include "../kiban_int.h"

kiban_busmap_t* kiban_busmap_new(char* tag)
{
  kiban_busmap_t* map;

  map = mem_alloc_type(kiban_busmap_t);
  map->tag = mem_strdup(tag);
  map->size = 8;
  map->map = kuso_pvec_new(0);

  return map;
}

void kiban_busmap_free(kiban_busmap_t* map)
{
  mem_free(map->tag);
  kiban_busmapper_t** iter = kuso_pvec_iter(map->map, kiban_busmapper_t*);
  for(; *iter != NULL; iter++) {
    kiban_busmapper_free(*iter);
  }
  kuso_pvec_delete(map->map);

  mem_free(map);
}

int kiban_busmap_cook(kiban_board_t* brd, kiban_busmap_t* map)
{
  kiban_busmapper_t** iter = kuso_pvec_iter(map->map, kiban_busmapper_t*);
  for(; *iter != NULL; iter++) {
    if(kiban_busmapper_cook(brd, *iter) < 0) {
      kiban_error(brd, "COOKER:BUSMAP", map->tag, "Busmapper cooking failed");
      goto _fail;
    }
  }
  return 0;
_fail:
  return -1;
}

void kiban_busmap_add_mapper(kiban_busmap_t* map, kiban_busmapper_t* mapr)
{
  kuso_pvec_append(map->map, mapr);
}
