#ifndef MANAGE_MEMMAP_H_
#define MANAGE_MEMMAP_H_

struct kiban_memmap_t {
  char* tag;
  int abus;
  int dbus;

  int mpcnt;

  kuso_pvec_t* map; /* kiban_memmapper_t */
};

kiban_memmap_t* kiban_memmap_new(char* tag);
void kiban_memmap_free(kiban_memmap_t* map);
int kiban_memmap_cook(kiban_board_t* brd, kiban_memmap_t* map);
void kiban_memmap_add_mapper(kiban_memmap_t* map, kiban_memmapper_t* mapr);

#endif
