#ifndef MANAGE_DEV_H_
#define MANAGE_DEV_H_

struct kiban_dev_t {
  char* loc;
  char* tag;
  char* label;
  char* partstr;
  char* typestr;
  char* clkstr;

  int type;
  int stype;
  int abus, dbus;
  int access; /* Access speed in ns */

  uint32_t crc; /* ROM only */

  int clk_id;
  int dvr;

  size_t size;

  int vmtype;

  void* exdat;

/* Hookups */
  kuso_pvec_t* spacemap; /* kiban_memmap_ref_t */
  kuso_pvec_t* portmap; /* kiban_portmap_ref_t */
};

kiban_dev_t* kiban_dev_new(void);
void kiban_dev_free(kiban_dev_t* dev);
int kiban_dev_cook(kiban_board_t* brd, kiban_dev_t* dev);

#endif
