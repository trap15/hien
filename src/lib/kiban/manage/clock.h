#ifndef MANAGE_CLOCK_H_
#define MANAGE_CLOCK_H_

struct kiban_clock_t {
  char* loc;
  char* tag;
  int spd;
};

kiban_clock_t* kiban_clock_new(void);
void kiban_clock_free(kiban_clock_t* clk);
int kiban_clock_cook(kiban_board_t* brd, kiban_clock_t* clk);

#endif
