#ifndef MANAGE_MEMMAPPER_H_
#define MANAGE_MEMMAPPER_H_

enum {
  MEMMAPR_AC_RD=1,
  MEMMAPR_AC_WR=2,
  MEMMAPR_AC_RW=MEMMAPR_AC_RD|MEMMAPR_AC_WR,
};

struct kiban_memmapper_t {
  int acc;
  addr_t mask;
  addr_t val;

  char* amap;
  char* dmap;
  kiban_dev_id_t* dev;

  kiban_busmap_t* abmap;
  kiban_busmap_t* dbmap;
};

kiban_memmapper_t* kiban_memmapper_new(void);
void kiban_memmapper_free(kiban_memmapper_t* mapr);
int kiban_memmapper_cook(kiban_board_t* brd, kiban_memmapper_t* mapr);
void kiban_memmapper_set_access(kiban_memmapper_t* mapr, int acc);
void kiban_memmapper_set_maskaddr(kiban_memmapper_t* mapr, addr_t mask, addr_t val);
void kiban_memmapper_set_amap(kiban_memmapper_t* mapr, char* map);
void kiban_memmapper_set_dmap(kiban_memmapper_t* mapr, char* map);
void kiban_memmapper_set_devid(kiban_memmapper_t* mapr, char* devstr);

#endif
