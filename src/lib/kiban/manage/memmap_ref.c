#include "../kiban_int.h"

kiban_memmap_ref_t* kiban_memmap_ref_new(void)
{
  kiban_memmap_ref_t* mref;

  mref = mem_alloc_type(kiban_memmap_ref_t);
  mref->name = NULL;

  return mref;
}

void kiban_memmap_ref_free(kiban_memmap_ref_t* mref)
{
  if(mref->name != NULL)
    mem_free(mref->name);
  mem_free(mref);
}

int kiban_memmap_ref_cook(kiban_board_t* brd, kiban_memmap_ref_t* mref)
{
  (void)brd;
  (void)mref;

  return 0;
}
