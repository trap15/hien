#ifndef MANAGE_PORTMAP_H_
#define MANAGE_PORTMAP_H_

struct kiban_portmap_t {
  char* tag;
  int bus;

  int mpcnt;

  kuso_pvec_t* map; /* kiban_portmapper_t */
};

kiban_portmap_t* kiban_portmap_new(char* tag);
void kiban_portmap_free(kiban_portmap_t* map);
int kiban_portmap_cook(kiban_board_t* brd, kiban_portmap_t* map);
void kiban_portmap_set_buslen(kiban_portmap_t* map, int bus);
void kiban_portmap_add_mapper(kiban_portmap_t* map, kiban_portmapper_t* mapr);

#endif
