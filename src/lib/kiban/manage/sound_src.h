#ifndef MANAGE_SOUND_SRC_H_
#define MANAGE_SOUND_SRC_H_

struct kiban_sndsrc_desc_t {
  char* tag;
  double vol;

  double srcamp;
};

kiban_sndsrc_desc_t* kiban_soundsrc_new(void);
void kiban_soundsrc_free(kiban_sndsrc_desc_t* ssrc);
int kiban_soundsrc_cook(kiban_board_t* brd, kiban_sndsrc_desc_t* src);

#endif
