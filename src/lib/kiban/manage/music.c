#include "../kiban_int.h"

kiban_music_t* kiban_music_new(void)
{
  kiban_music_t* desc;

  desc = mem_alloc_type(kiban_music_t);
  desc->has_play = FALSE;
  desc->has_stop = FALSE;
  desc->has_reset = FALSE;

  desc->playcmds = kuso_pvec_new(0);
  desc->stopcmds = kuso_pvec_new(0);
  desc->rstcmds = kuso_pvec_new(0);

  return desc;
}

static void kiban_music_free_cmds(kuso_pvec_t* vec)
{
  if(vec == NULL)
    return;
  kiban_music_cmd_t** iter = kuso_pvec_iter(vec, kiban_music_cmd_t*);
  for(; *iter != NULL; iter++) {
    kiban_music_cmd_free(*iter);
  }
  kuso_pvec_delete(vec);
}
void kiban_music_free(kiban_music_t* desc)
{
  kiban_music_free_cmds(desc->playcmds);
  kiban_music_free_cmds(desc->stopcmds);
  kiban_music_free_cmds(desc->rstcmds);
  mem_free(desc);
}

static int kiban_music_cook_cmds(kiban_board_t* brd, kuso_pvec_t* vec)
{
  if(vec == NULL)
    return 0;

  kiban_music_cmd_t** iter = kuso_pvec_iter(vec, kiban_music_cmd_t*);
  for(; *iter != NULL; iter++) {
    if(kiban_music_cmd_cook(brd, *iter) < 0) {
      kiban_error(brd, "COOKER:MUSIC", NULL, "Music command cooking failed");
      goto _fail;
    }
  }
  return 0;
_fail:
  return -1;
}
int kiban_music_cook(kiban_board_t* brd, kiban_music_t* desc)
{
  if(kiban_music_cook_cmds(brd, desc->playcmds) < 0) {
    kiban_error(brd, "COOKER:MUSIC", NULL, "Play commands cooking failed");
    goto _fail;
  }
  if(kiban_music_cook_cmds(brd, desc->stopcmds) < 0) {
    kiban_error(brd, "COOKER:MUSIC", NULL, "Stop commands cooking failed");
    goto _fail;
  }
  if(kiban_music_cook_cmds(brd, desc->rstcmds) < 0) {
    kiban_error(brd, "COOKER:MUSIC", NULL, "Reset commands cooking failed");
    goto _fail;
  }
  return 0;
_fail:
  return -1;
}

/* Cmd */
kiban_music_cmd_t* kiban_music_cmd_new(void)
{
  kiban_music_cmd_t* cmd;

  cmd = mem_alloc_type(kiban_music_cmd_t);
  cmd->data = 0;
  cmd->key = 0;
  cmd->dev = NULL;

  return cmd;
}

void kiban_music_cmd_free(kiban_music_cmd_t* cmd)
{
  if(cmd->dev)
    kiban_devid_free(cmd->dev);
  mem_free(cmd);
}

int kiban_music_cmd_cook(kiban_board_t* brd, kiban_music_cmd_t* cmd)
{
  if(kiban_devid_cook(brd, cmd->dev) < 0) {
    kiban_error(brd, "COOKER:MUSIC:CMD", NULL, "DevID cooking failed");
    goto _fail;
  }

  return 0;
_fail:
  return -1;
}

void kiban_music_cmd_set_devid(kiban_music_cmd_t* cmd, char* devstr)
{
  cmd->dev = hdl_parse_devid(devstr);
}
void kiban_music_cmd_set_fixdata(kiban_music_cmd_t* cmd, int key)
{
  cmd->key = key;
}
void kiban_music_cmd_set_data(kiban_music_cmd_t* cmd, uintmax_t data)
{
  cmd->data = data;
}
