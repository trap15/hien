#ifndef MANAGE_SUBMAPPER_H_
#define MANAGE_SUBMAPPER_H_

struct kiban_submapper_t {
  kiban_dev_id_t* dev;

  char* amap;
  char* dmap;

  int tmpsize;

  kiban_busmap_t* abmap;
  kiban_busmap_t* dbmap;
};

kiban_submapper_t* kiban_submapper_new(void);
void kiban_submapper_free(kiban_submapper_t* mapr);
int kiban_submapper_cook(kiban_board_t* brd, kiban_submapper_t* mapr);
void kiban_submapper_set_devid(kiban_submapper_t* mapr, char* devstr);
void kiban_submapper_set_amap(kiban_submapper_t* mapr, char* map);
void kiban_submapper_set_dmap(kiban_submapper_t* mapr, char* map);

#endif
