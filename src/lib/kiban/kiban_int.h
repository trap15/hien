#ifndef KIBAN_INT_H_
#define KIBAN_INT_H_

#include "ext_intf.h"

enum {
  DEVTYPE_DEV=0,
  DEVTYPE_SND,
  DEVTYPE_SRAM,
  DEVTYPE_DRAM,
  DEVTYPE_RAM,
  DEVTYPE_EEPROM,
  DEVTYPE_ROM,
  DEVTYPE_MASKROM,
  DEVTYPE_PROM,
  DEVTYPE_EPROM,
  DEVTYPE_SUBMAP,
  DEVTYPE_FILE,
};

enum {
  DEVID_SPACE=0,
  DEVID_PORT,
};

typedef struct kiban_board_t       kiban_board_t;

typedef struct kiban_portmap_t     kiban_portmap_t;
typedef struct kiban_portmapper_t  kiban_portmapper_t;
typedef struct kiban_portmap_ref_t kiban_portmap_ref_t;

typedef struct kiban_music_cmd_t   kiban_music_cmd_t;
typedef struct kiban_music_t       kiban_music_t;

typedef struct kiban_snd_desc_t    kiban_snd_desc_t;
typedef struct kiban_sndsrc_desc_t kiban_sndsrc_desc_t;

typedef struct kiban_clock_t       kiban_clock_t;
typedef struct kiban_dev_t         kiban_dev_t;

typedef struct kiban_dev_id_t      kiban_dev_id_t;

typedef struct kiban_busmapper_t   kiban_busmapper_t;
typedef struct kiban_busmap_t      kiban_busmap_t;

typedef struct kiban_submapper_t   kiban_submapper_t;
typedef struct kiban_submap_t      kiban_submap_t;

typedef struct kiban_memmapper_t   kiban_memmapper_t;
typedef struct kiban_memmap_t      kiban_memmap_t;
typedef struct kiban_memmap_ref_t  kiban_memmap_ref_t;

#include "kiban_err.h"

#include "manage/manage.h"
#include "hdl/hdl_int.h"
#include "gmd/gmd_int.h"
#include "codegen/gen_int.h"

char* kiban_codegen(kiban_board_t* brd);
int kiban_gmd(kiban_board_t* brd, char* file);
int kiban_hdl(kiban_board_t* brd, char* file);

#endif
