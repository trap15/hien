#include "../kiban_int.h"

int kiban_gmd(kiban_board_t* brd, char *file)
{
  int ret = 0;
  hien_file_t* fp;
  size_t sz;
  char* str;
  yaml_t* yml;

  fp = file_open(file, FILE_READ);
  if(fp == NULL) {
    kiban_error(brd, "GMD", NULL, "Can't open %s", file);
    return -1;
  }
  sz = file_size(fp);
  str = mem_alloc(sz+1);

  file_read(str, sz, fp);
  file_close(fp);

  str[sz] = '\0';

  yml = kuso_yaml_parse(str);
  if(yml == NULL) {
    kiban_error(brd, "GMD", NULL, "Error [%s] in GMD file before:\n%s", kuso_yaml_problem(), kuso_yaml_problem_ptr());
    ret = -1;
    goto _error;
  }

  gmd_parse_root(brd, yml);
  kuso_yaml_delete(yml);

_error:
  mem_free(str);

  return ret;
}
