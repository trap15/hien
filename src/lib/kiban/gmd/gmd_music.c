#include "../kiban_int.h"

static void gmd_parse_music_ids(kiban_board_t* brd, yaml_t* root)
{
  int val;

  brd->music->stop_id = -1;
  brd->music->default_id = -1;
  brd->music->max_id = -1;
  brd->music->min_id = -1;

  if(kuso_yaml_get_map_int(root, "stop", &val))
    brd->music->stop_id = val;
  if(kuso_yaml_get_map_int(root, "default", &val))
    brd->music->default_id = val;
  if(kuso_yaml_get_map_int(root, "max", &val))
    brd->music->max_id = val;
  if(kuso_yaml_get_map_int(root, "min", &val))
    brd->music->min_id = val;
}

static void gmd_parse_music_time(kiban_board_t* brd, yaml_t* root)
{
  double val;

  brd->music->boot_time = 0;
  brd->music->chg_time = 0;

  if(kuso_yaml_get_map_flt(root, "boot", &val))
    brd->music->boot_time = val * TIMETICK_PER_SECOND;
  if(kuso_yaml_get_map_flt(root, "change", &val))
    brd->music->chg_time = val * TIMETICK_PER_SECOND;
}

static void gmd_parse_music_device_one(kiban_board_t* brd, char* dev)
{
  kiban_dev_t** iter = kuso_pvec_iter(brd->devs, kiban_dev_t*);
  for(;* iter != NULL; iter++) {
    if(strcmp((*iter)->tag, dev) == 0) {
      (*iter)->vmtype |= VMTYPE_SNDPLAY;
      break;
    }
  }
}

static void gmd_parse_music_devices(kiban_board_t* brd, yaml_t* root)
{
  int cnt, i;
  char* str;

  cnt = kuso_yaml_count(root);
  for(i = 0; i < cnt; i++) {
    if(!kuso_yaml_get_seq_str(root, i, &str))
      continue;

    gmd_parse_music_device_one(brd, str);
  }
}

void gmd_parse_music(kiban_board_t* brd, yaml_t* root)
{
  yaml_t* yml;

  if(kuso_yaml_get_map_obj(root, "ids", &yml))
    gmd_parse_music_ids(brd, yml);
  if(kuso_yaml_get_map_obj(root, "timing", &yml))
    gmd_parse_music_time(brd, yml);
  if(kuso_yaml_get_map_obj(root, "devices", &yml))
    gmd_parse_music_devices(brd, yml);
}
