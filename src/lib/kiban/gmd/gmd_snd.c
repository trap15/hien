#include "../kiban_int.h"

static kiban_sndsrc_desc_t* gmd_parse_sndsrc_one(yaml_t* root)
{
  double val;
  kiban_sndsrc_desc_t* desc;
  desc = kiban_soundsrc_new();

  desc->tag = mem_strdup(root->name);

  val = 0;
  kuso_yaml_get_map_flt(root, "volume", &val);
  desc->vol = val;

  return desc;
}

static kiban_snd_desc_t* gmd_parse_snd_one(yaml_t* root)
{
  int cnt, i;
  yaml_t* yml;
  kiban_snd_desc_t* desc;
  kiban_sndsrc_desc_t* sndsrc;

  desc = kiban_sound_new();
  desc->tag = mem_strdup(root->name);

  cnt = kuso_yaml_count(root);
  for(i = 0; i < cnt; i++) {
    if(!kuso_yaml_get_seq_obj(root, i, &yml))
      continue;
    if(!kuso_yaml_get_seq_obj(yml, 0, &yml))
      continue;

    sndsrc = gmd_parse_sndsrc_one(yml);
    if(!sndsrc) continue;
    kuso_pvec_append(desc->srcs, sndsrc);
  }

  return desc;
}

void gmd_parse_snd(kiban_board_t* brd, yaml_t* root)
{
  int cnt, i;
  yaml_t* yml;
  kiban_snd_desc_t* snd;

  cnt = kuso_yaml_count(root);
  for(i = 0; i < cnt; i++) {
    if(!kuso_yaml_get_seq_obj(root, i, &yml))
      continue;
    if(!kuso_yaml_get_seq_obj(yml, 0, &yml))
      continue;

    snd = gmd_parse_snd_one(yml);
    if(!snd) continue;
    kuso_pvec_append(brd->snds, snd);
  }
}
