#include "../kiban_int.h"

void gmd_parse_meta(kiban_board_t* brd, yaml_t* root)
{
  char* str;

  brd->name = NULL;
  brd->year = NULL;
  brd->mfg = NULL;

  if(kuso_yaml_get_map_str(root, "name", &str))
    brd->name = mem_strdup(str);
  if(kuso_yaml_get_map_str(root, "year", &str))
    brd->year = mem_strdup(str);
  if(kuso_yaml_get_map_str(root, "mfg", &str))
    brd->mfg = mem_strdup(str);
}
