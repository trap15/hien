#include "../kiban_int.h"

void gmd_parse_hdl(kiban_board_t* brd, yaml_t* root)
{
  int cnt, i;
  char* str;

  cnt = kuso_yaml_count(root);
  for(i = 0; i < cnt; i++) {
    if(!kuso_yaml_get_seq_str(root,i,&str))
      continue;

    str = mem_strdup(str);
    kuso_pvec_append(brd->hdls, str);
  }
}
