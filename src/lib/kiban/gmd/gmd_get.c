#include "../kiban_int.h"

void gmd_get_type_from_devlist(kiban_dev_t* desc, char* str)
{
  const hien_dev_info_t* def;
  def = devinfo_find(str);
  if(def == NULL)
    return;
  desc->type = DEVTYPE_DEV;
}

void gmd_get_type_from_string(kiban_dev_t* desc, char* str)
{
  if(strcmp(str, "ram") == 0) { desc->type = DEVTYPE_RAM; } else
  if(strcmp(str, "sram") == 0) { desc->type = DEVTYPE_SRAM; } else
  if(strcmp(str, "dram") == 0) { desc->type = DEVTYPE_DRAM; } else
  if(strcmp(str, "rom") == 0) { desc->type = DEVTYPE_ROM; } else
  if(strcmp(str, "maskrom") == 0) { desc->type = DEVTYPE_MASKROM; } else
  if(strcmp(str, "prom") == 0) { desc->type = DEVTYPE_PROM; } else
  if(strcmp(str, "eprom") == 0) { desc->type = DEVTYPE_EPROM; } else
  if(strcmp(str, "eeprom") == 0) { desc->type = DEVTYPE_EEPROM; } else
  if(strcmp(str, "file") == 0) { desc->type = DEVTYPE_FILE; } else
  gmd_get_type_from_devlist(desc, str);
}
