#ifndef GMD_INT_H_
#define GMD_INT_H_

/* Parser */
void gmd_parse_root(kiban_board_t* brd, yaml_t* root);

/* Get */
void gmd_get_type_from_devlist(kiban_dev_t* desc, char* str);
void gmd_get_type_from_string(kiban_dev_t* desc, char* str);

void gmd_parse_hdl(kiban_board_t* brd, yaml_t* root);
void gmd_parse_clk(kiban_board_t* brd, yaml_t* root);
void gmd_parse_dev(kiban_board_t* brd, yaml_t* root);
void gmd_parse_snd(kiban_board_t* brd, yaml_t* root);
void gmd_parse_music(kiban_board_t* brd, yaml_t* root);
void gmd_parse_include(kiban_board_t* brd, yaml_t* root);
void gmd_parse_meta(kiban_board_t* brd, yaml_t* root);

#endif
