#include "../kiban_int.h"

static kiban_clock_t* gmd_parse_clk_one(yaml_t* root)
{
  kiban_clock_t* desc;

  desc = kiban_clock_new();
  desc->loc = mem_strdup(root->name);
  desc->tag = desc->loc;
  kuso_yaml_get_map_str(root, "tag", &desc->tag);
  desc->tag = mem_strdup(desc->tag);

  if(!kuso_yaml_get_map_int(root, "rate", &desc->spd))
    desc->spd = 0;

  return desc;
}

void gmd_parse_clk(kiban_board_t* brd, yaml_t* root)
{
  int cnt, i;
  yaml_t* yml;
  kiban_clock_t* clk;

  cnt = kuso_yaml_count(root);
  for(i = 0; i < cnt; i++) {
    if(!kuso_yaml_get_seq_obj(root, i, &yml))
      continue;
    if(!kuso_yaml_get_seq_obj(yml, 0, &yml))
      continue;

    clk = gmd_parse_clk_one(yml);
    kuso_pvec_append(brd->clks, clk);
  }
}
