#include "../kiban_int.h"

void gmd_parse_include(kiban_board_t* brd, yaml_t* root)
{
  int cnt, i;
  char* str;
  char* path;

  cnt = kuso_yaml_count(root);
  for(i = 0; i < cnt; i++) {
    if(!kuso_yaml_get_seq_str(root,i,&str))
      continue;

    path = kiban_ext_make_def_path(brd->intf, str, NULL);
    if(kiban_gmd(brd, path) < 0) {
      mem_free(path);
      return;
    }
    mem_free(path);
  }
}
