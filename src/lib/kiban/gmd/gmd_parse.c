#include "../kiban_int.h"

void gmd_parse_root(kiban_board_t* brd, yaml_t* root)
{
  yaml_t* yml;

  if(kuso_yaml_get_map_obj(root, "metadata", &yml))
    gmd_parse_meta(brd, yml);

  if(kuso_yaml_get_map_obj(root, "clocks", &yml))
    gmd_parse_clk(brd, yml);

  if(kuso_yaml_get_map_obj(root, "hdls", &yml))
    gmd_parse_hdl(brd, yml);
  if(kuso_yaml_get_map_obj(root, "include", &yml))
    gmd_parse_include(brd, yml);

  if(kuso_yaml_get_map_obj(root, "devices", &yml))
    gmd_parse_dev(brd, yml);

  if(kuso_yaml_get_map_obj(root, "sound", &yml))
    gmd_parse_snd(brd, yml);
  if(kuso_yaml_get_map_obj(root, "music", &yml))
    gmd_parse_music(brd, yml);
}
