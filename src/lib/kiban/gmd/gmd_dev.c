#include "../kiban_int.h"

static kiban_dev_t* gmd_parse_dev_one(yaml_t* root)
{
  int val;
  char* str;
  kiban_dev_t* desc;
  desc = kiban_dev_new();

  desc->tag = mem_strdup(root->name);

  if(!kuso_yaml_get_map_str(root, "location", &str))
    str = desc->tag;
  desc->loc = mem_strdup(str);

  if(kuso_yaml_get_map_str(root, "part", &str))
    desc->partstr = mem_strdup(str);
  else
    desc->partstr = NULL;

  if(kuso_yaml_get_map_str(root, "type", &str)) {
    desc->typestr = mem_strdup(str);
    gmd_get_type_from_string(desc, desc->typestr);
  }else{
    desc->typestr = NULL;
  }

  if(kuso_yaml_get_map_str(root, "access", &str))
    sscanf(str, "%dns", &desc->access);
  else
    desc->access = 0;

  if(kuso_yaml_get_map_int(root, "abus", &val))
    desc->abus = val;
  if(kuso_yaml_get_map_int(root, "dbus", &val))
    desc->dbus = val;

  if(kuso_yaml_get_map_str(root, "label", &str))
    desc->label = mem_strdup(str);
  else
    desc->label = NULL;

  if(kuso_yaml_get_map_str(root, "crc", &str))
    sscanf(str, "%08X", &desc->crc);
  else
    desc->crc = 0;

  if(kuso_yaml_get_map_int(root, "xtal_div", &val))
    desc->dvr = val;
  else
    desc->dvr = 1;

  if(kuso_yaml_get_map_str(root, "xtal", &str))
    desc->clkstr = mem_strdup(str);
  else
    desc->clkstr = NULL;

  return desc;
}

void gmd_parse_dev(kiban_board_t* brd, yaml_t* root)
{
  int cnt, i;
  yaml_t* yml;
  kiban_dev_t* dev;

  cnt = kuso_yaml_count(root);
  for(i = 0; i < cnt; i++) {
    if(!kuso_yaml_get_seq_obj(root, i, &yml))
      continue;
    if(!kuso_yaml_get_seq_obj(yml, 0, &yml))
      continue;

    dev = gmd_parse_dev_one(yml);
    kuso_pvec_append(brd->devs, dev);
  }
}
