#ifndef _LIB_KIBAN_ERR_H_
#define _LIB_KIBAN_ERR_H_

void kiban_error(kiban_board_t* brd, char* subsys, char* entity, char* msg, ...);
void kiban_error_va(kiban_board_t* brd, char* subsys, char* entity, char* msg, va_list vl);

#endif
