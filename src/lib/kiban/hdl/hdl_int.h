#ifndef HDL_INT_H_
#define HDL_INT_H_

typedef struct hdl_token_t hdl_token_t;
typedef struct hdl_psrstt_t hdl_psrstt_t;
typedef struct hdl_parser_t hdl_parser_t;

typedef struct hdl_psrlist_entry_t hdl_psrlist_entry_t;
typedef struct hdl_psrlist_t hdl_psrlist_t;

enum {
  HDLPSR_TOP = 0,
    HDLPSR_EXDEV,
    HDLPSR_BUSMAP,
    HDLPSR_MEMMAP,
      HDLPSR_MEMMAP_EXP,
    HDLPSR_DEVMAP,
    HDLPSR_SNDMAP,
    HDLPSR_MUSIC,
    HDLPSR_PORTMAP,
      HDLPSR_PORTMAP_DEV_BUS,
    HDLPSR_SUBMAP,
      HDLPSR_SUBMAP_DEV,
        HDLPSR_SUBMAP_DEV_BUS,
};

struct hdl_psrstt_t {
  int state;

  void* data;
};

struct hdl_parser_t {
  kiban_board_t* board;

  kuso_pstack_t* state; /* hdl_psrstt_t */
  int line;
  char* fn;
  char* ostr;
  char* str;
  kuso_tkn_t* tkn;
  kuso_tkn_t* etkn;
};

struct hdl_psrlist_entry_t {
  char* name;
  int psrid;
  int (*run)(hdl_parser_t* psr);
};

struct hdl_psrlist_t {
  char* name;
  int (*start)(hdl_parser_t* psr);
  int (*def)(hdl_parser_t* psr);
  int (*end)(hdl_parser_t* psr);
  hdl_psrlist_entry_t entry[];
};

enum {
  HDLERR_SYNTAX=0,
  HDLERR_TOOFEWARGS,
  HDLERR_TOOMANYARGS,
  HDLERR_ARGCOUNT,
  HDLERR_BADPARAM,
};

/* Parser */
void hdl_error(hdl_parser_t* psr, int errcode, char* str, ...);
void hdl_warn(hdl_parser_t* psr, int errcode, char* str, ...);

int hdl_parse(kiban_board_t* brd, char* fn, char* str);

#define TKNHNDSTART() if(0) {}
#define TKNHANDLE(t,s) else if(kuso_cmptkn((t), s))

void hdl_parser_pushstate(hdl_parser_t* psr, int stt);
hdl_psrstt_t* hdl_parser_popstate(hdl_parser_t* psr);
hdl_psrstt_t* hdl_psrstate(hdl_parser_t* psr);
hdl_psrstt_t* hdl_psrstate_parent(hdl_parser_t* psr, int level);

int hdl_parser_newstate(hdl_parser_t* psr, int stt);
hdl_psrstt_t* hdl_parser_endstate(hdl_parser_t* psr);

kiban_dev_id_t* hdl_parse_devid(char* str);

/* Each Handler */
extern hdl_psrlist_t hdllist_top;
extern hdl_psrlist_t hdllist_exdev;
extern hdl_psrlist_t hdllist_busmap;
extern hdl_psrlist_t hdllist_memmap;
extern hdl_psrlist_t hdllist_memmap_exp;
extern hdl_psrlist_t hdllist_devmap;
extern hdl_psrlist_t hdllist_sndmap;
extern hdl_psrlist_t hdllist_music;
extern hdl_psrlist_t hdllist_portmap;
extern hdl_psrlist_t hdllist_portmap_dev_bus;
extern hdl_psrlist_t hdllist_submap;
extern hdl_psrlist_t hdllist_submap_dev;
extern hdl_psrlist_t hdllist_busmap_dev_bus;

/* Special handlers */
int hdl_tknparse_busmap(hdl_parser_t* psr);

/* Tokenizer */
int hdl_tknassign(kuso_tkn_t* tkn, char** dst, char** src);
/* Return of this is neat. ret < 0 (src <= dst), ret > 0 (src => dst) */
int hdl_tknmove(char** str, int* dir, int* src,int* slen, int* dst,int* dlen);

int hdl_tknparam_int(hdl_parser_t* psr, char* name, int* data);
int hdl_tknparam_str(hdl_parser_t* psr, char* name, char** data);

#endif
