#include "../kiban_int.h"

int hdl_tknparse_memmap_cmp(hdl_parser_t* psr)
{
  int end = 0;
  char* tdst;
  char* tsrc;

  kuso_tkn_t* tkn;
  kiban_memmap_t* map;
  kiban_memmapper_t* mapr;
  hdl_psrstt_t* stt;

  stt = hdl_psrstate(psr);
  map = stt->data;

  mapr = kiban_memmapper_new();

  /* acc */
  tkn = psr->tkn;
  TKNHNDSTART()
  TKNHANDLE(tkn, "RD") { /* Read handler */
    kiban_memmapper_set_access(mapr, MEMMAPR_AC_RD);
  }
  TKNHANDLE(tkn, "WR") { /* Write handler */
    kiban_memmapper_set_access(mapr, MEMMAPR_AC_WR);
  }
  TKNHANDLE(tkn, "RW") { /* Read/Write handler */
    kiban_memmapper_set_access(mapr, MEMMAPR_AC_RW);
  }else{
    hdl_error(psr, HDLERR_BADPARAM, "MEMMAP entry, access type %s", tkn->text);
  }

  /* Mask=val */
  tkn = tkn->next;
  if(tkn->text[0] == '\0') {
    kiban_memmapper_set_maskaddr(mapr, 0,0);
  }else{
    if(!hdl_tknassign(tkn, &tdst, &tsrc)) {
      hdl_error(psr, HDLERR_SYNTAX, "MEMMAP entry, Mask=val");
      end = -1;
      return end;
    }
    kiban_memmapper_set_maskaddr(mapr, xtoi_bs(tdst, 16),xtoi_bs(tsrc, 16));
    mem_free(tdst);
    mem_free(tsrc);
  }

  /* amap */
  tkn = tkn->next;
  if((strcmp(tkn->text, "FLAT") == 0) || (strcmp(tkn->text, "") == 0))
    kiban_memmapper_set_amap(mapr, NULL);
  else
    kiban_memmapper_set_amap(mapr, tkn->text);

  /* dmap */
  tkn = tkn->next;
  if((strcmp(tkn->text, "FLAT") == 0) || (strcmp(tkn->text, "") == 0))
    kiban_memmapper_set_dmap(mapr, NULL);
  else
    kiban_memmapper_set_dmap(mapr, tkn->text);

  /* devid */
  tkn = tkn->next;
  kiban_memmapper_set_devid(mapr, tkn->text);

  kiban_memmap_add_mapper(map, mapr);

  return end;
}

static int hdl_tknparse_memmap_exp_start(hdl_parser_t* psr)
{
  int ret = 0;
  char* name = NULL;

  hdl_psrstt_t* stt;
  hdl_psrstt_t* mpstt;

  kiban_submap_t* map;
  kiban_memmap_t* mmp;

  stt = hdl_psrstate(psr);
  mpstt = hdl_psrstate_parent(psr, 1);
  mmp = mpstt->data;

  kuso_asprintf(&name, "_autobusmap_%d_%s", mmp->mpcnt, mmp->tag);
  map = kiban_submap_new(name);
  map->abus = mmp->abus;
  map->dbus = mmp->dbus;
  mem_free(name);

  stt->data = map;

  kuso_pvec_append(psr->board->devs, map->fdev);
  kuso_pvec_append(psr->board->submaps, map);

  return ret;
}
hdl_psrlist_t hdllist_memmap_exp = {
  .name = "MEMMAP:ACCESS",
  .start = hdl_tknparse_memmap_exp_start,
  .def = NULL,
  .end = NULL,
  .entry = {
    { "DEVICE", HDLPSR_SUBMAP_DEV, NULL, },
    { NULL,     -1,                NULL, },
  },
};
int hdl_tknparse_memmap_exp(hdl_parser_t* psr)
{
  int end = 0;
  char* tdst;
  char* tsrc;
  char* name = NULL;

  kuso_tkn_t* tkn;
  kiban_memmap_t* map;
  kiban_memmapper_t* mapr;
  hdl_psrstt_t* stt;

  stt = hdl_psrstate(psr);
  map = stt->data;

  mapr = kiban_memmapper_new();

  /* acc */
  tkn = psr->tkn;
  TKNHNDSTART()
  TKNHANDLE(tkn, "RD") { /* Read handler */
    kiban_memmapper_set_access(mapr, MEMMAPR_AC_RD);
  }
  TKNHANDLE(tkn, "WR") { /* Write handler */
    kiban_memmapper_set_access(mapr, MEMMAPR_AC_WR);
  }
  TKNHANDLE(tkn, "RW") { /* Read/Write handler */
    kiban_memmapper_set_access(mapr, MEMMAPR_AC_RW);
  }else{
    hdl_error(psr, HDLERR_BADPARAM, "MEMMAP entry, access type %s", tkn->text);
  }

  /* Mask=val */
  tkn = tkn->next;
  if(tkn->text[0] == '\0') {
    kiban_memmapper_set_maskaddr(mapr, 0,0);
  }else{
    if(!hdl_tknassign(tkn, &tdst, &tsrc)) {
      hdl_error(psr, HDLERR_SYNTAX, "MEMMAP entry, Mask=val");
      end = -1;
      return end;
    }
    kiban_memmapper_set_maskaddr(mapr, xtoi_bs(tdst, 16),xtoi_bs(tsrc, 16));
    mem_free(tdst);
    mem_free(tsrc);
  }

  kuso_asprintf(&name, "_autobusmap_%d_%s", map->mpcnt, map->tag);
  kiban_memmapper_set_devid(mapr, name);
  mem_free(name);

  kiban_memmap_add_mapper(map, mapr);

  hdl_parser_newstate(psr, HDLPSR_MEMMAP_EXP);

  return end;
}

static int hdl_tknparse_memmap(hdl_parser_t* psr)
{
  kuso_tkn_t* tkn;

  tkn = psr->tkn;
  if(tkn->cnt == 4) {
    return hdl_tknparse_memmap_cmp(psr);
  }else if(tkn->cnt == 1) {
    return hdl_tknparse_memmap_exp(psr);
  }else if(tkn->cnt < 1) {
    hdl_error(psr, HDLERR_TOOFEWARGS, "MEMMAP entry");
    return -1;
  }else if(tkn->cnt > 4) {
    hdl_error(psr, HDLERR_TOOMANYARGS, "MEMMAP entry");
    return -1;
  }else{
    hdl_error(psr, HDLERR_ARGCOUNT, "MEMMAP entry");
    return -1;
  }
}

static int hdl_tknparse_memmap_start(hdl_parser_t* psr)
{
  int ret = 0;

  kiban_memmap_t* map;
  hdl_psrstt_t* stt;

  stt = hdl_psrstate(psr);

  map = kiban_memmap_new(psr->etkn->text);

  hdl_tknparam_int(psr, "ABUS", &map->abus);
  hdl_tknparam_int(psr, "DBUS", &map->dbus);

  stt->data = map;

  kuso_pvec_append(psr->board->memmaps, map);

  return ret;
}

hdl_psrlist_t hdllist_memmap = {
  .name = "MEMMAP",
  .start = hdl_tknparse_memmap_start,
  .def = hdl_tknparse_memmap,
  .end = NULL,
  .entry = {
    { NULL, -1, NULL, },
  },
};
