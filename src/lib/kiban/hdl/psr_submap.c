#include "../kiban_int.h"

static int hdl_tknparse_submap_dev_exp_start(hdl_parser_t* psr)
{
  int ret = 0;
  char* name = NULL;

  hdl_psrstt_t* stt;
  hdl_psrstt_t* mpstt;
  hdl_psrstt_t* mprstt;

  kiban_busmap_t* map;
  kiban_submapper_t* smapr;
  kiban_submap_t* smap;

  stt = hdl_psrstate(psr);
  mpstt = hdl_psrstate_parent(psr, 2);
  mprstt = hdl_psrstate_parent(psr, 1);
  smap = mpstt->data;
  smapr = mprstt->data;

  kuso_asprintf(&name, "_submap_autogen_%d_%s", smap->mpcnt, smap->tag);
  map = kiban_busmap_new(name);
  map->size = 0; /*smapr->tmpsize;*/
  mem_free(name);

  stt->data = map;

  kuso_pvec_append(psr->board->busmaps, map);

  return ret;
}

hdl_psrlist_t hdllist_busmap_dev_bus = {
  .name = "SUBMAP:DEVICE:BUS",
  .start = hdl_tknparse_submap_dev_exp_start,
  .def = hdl_tknparse_busmap,
  .end = NULL,
  .entry = {
    { NULL, -1, NULL, },
  },
};

static char* hdl_tknparse_submap_dev_cmp(hdl_parser_t* psr)
{
  kuso_tkn_t* tkn;
  hdl_psrstt_t* stt;
  kiban_submapper_t* mapr;

  stt = hdl_psrstate(psr);
  mapr = stt->data;

  tkn = psr->tkn;
  tkn = tkn->next;

  if(kuso_cmptkn(tkn, "FLAT") || kuso_cmptkn(tkn, ""))
    return NULL;

  return mem_strdup(tkn->text);
}

static char* hdl_tknparse_submap_dev_exp(hdl_parser_t* psr)
{
  hdl_psrstt_t* mpstt;
  kiban_submap_t* map;
  char* name = NULL;

  mpstt = hdl_psrstate_parent(psr, 1);
  map = mpstt->data;

  hdl_parser_newstate(psr, HDLPSR_SUBMAP_DEV_BUS);

  kuso_asprintf(&name, "_submap_autogen_%d_%s", map->mpcnt, map->tag);
  map->mpcnt++;
  return name;
}
static int hdl_tknparse_submap_dev_addr(hdl_parser_t* psr)
{
  char* mapname = NULL;
  kuso_tkn_t* tkn;
  hdl_psrstt_t* stt;
  hdl_psrstt_t* pstt;
  kiban_submap_t* map;
  kiban_submapper_t* mapr;

  stt = hdl_psrstate(psr);
  pstt = hdl_psrstate_parent(psr, 1);
  map = pstt->data;
  mapr = stt->data;

  mapr->tmpsize = map->abus;
  tkn = psr->tkn;
  if(tkn->cnt == 1) {
    mapname = hdl_tknparse_submap_dev_cmp(psr);
  }else if(tkn->cnt == 0) {
    mapname = hdl_tknparse_submap_dev_exp(psr);
  }else if(tkn->cnt > 1) {
    hdl_error(psr, HDLERR_TOOMANYARGS, "SUBMAP:DEVICE:ADDR entry");
    return -1;
  }else{
    hdl_error(psr, HDLERR_SYNTAX, "SUBMAP:DEVICE:ADDR entry");
    return -1;
  }

  kiban_submapper_set_amap(mapr, mapname);
  if(mapname != NULL)
    mem_free(mapname);
  return 0;
}

static int hdl_tknparse_submap_dev_data(hdl_parser_t* psr)
{
  char* mapname = NULL;
  kuso_tkn_t* tkn;
  hdl_psrstt_t* stt;
  hdl_psrstt_t* pstt;
  kiban_submap_t* map;
  kiban_submapper_t* mapr;

  stt = hdl_psrstate(psr);
  pstt = hdl_psrstate_parent(psr, 1);
  map = pstt->data;
  mapr = stt->data;

  mapr->tmpsize = map->dbus;
  tkn = psr->tkn;
  if(tkn->cnt == 1) {
    mapname = hdl_tknparse_submap_dev_cmp(psr);
  }else if(tkn->cnt == 0) {
    mapname = hdl_tknparse_submap_dev_exp(psr);
  }else if(tkn->cnt > 1) {
    hdl_error(psr, HDLERR_TOOMANYARGS, "SUBMAP:DEVICE:DATA entry");
    return -1;
  }else{
    hdl_error(psr, HDLERR_SYNTAX, "SUBMAP:DEVICE:DATA entry");
    return -1;
  }

  kiban_submapper_set_dmap(mapr, mapname);
  if(mapname != NULL)
    mem_free(mapname);
  return 0;
}

static int hdl_tknparse_submap_dev_start(hdl_parser_t* psr)
{
  int ret = 0;

  kiban_submapper_t* mapr;
  hdl_psrstt_t* stt;
  hdl_psrstt_t* pstt;

  stt = hdl_psrstate(psr);
  pstt = hdl_psrstate_parent(psr, 1);

  mapr = kiban_submapper_new();
  kiban_submapper_set_devid(mapr, psr->etkn->text);

  stt->data = mapr;

  kiban_submap_add_mapper(pstt->data, mapr);

  return ret;
}

static int hdl_tknparse_submap_start(hdl_parser_t* psr)
{
  int ret = 0;

  kiban_submap_t* map;
  hdl_psrstt_t* stt;

  stt = hdl_psrstate(psr);

  map = kiban_submap_new(psr->etkn->text);

  hdl_tknparam_int(psr, "ABUS", &map->abus);
  hdl_tknparam_int(psr, "DBUS", &map->dbus);

  stt->data = map;

  kuso_pvec_append(psr->board->devs, map->fdev);
  kuso_pvec_append(psr->board->submaps, map);

  return ret;
}

hdl_psrlist_t hdllist_submap_dev = {
  .name = "SUBMAP:DEVICE",
  .start = hdl_tknparse_submap_dev_start,
  .def = NULL,
  .end = NULL,
  .entry = {
    { "ADDR", -1, hdl_tknparse_submap_dev_addr, },
    { "DATA", -1, hdl_tknparse_submap_dev_data, },
    { NULL,   -1, NULL, },
  },
};

hdl_psrlist_t hdllist_submap = {
  .name = "SUBMAP",
  .start = hdl_tknparse_submap_start,
  .def = NULL,
  .end = NULL,
  .entry = {
    { "DEVICE", HDLPSR_SUBMAP_DEV, NULL, },
    { NULL,     -1,                NULL, },
  },
};
