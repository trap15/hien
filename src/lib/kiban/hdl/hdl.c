#include "../kiban_int.h"

static const char* errstrs[] = {
  "Syntax error",
  "Too few arguments",
  "Too many arguments",
  "Too many or too few arguments",
  "Bad parameter",
};

void hdl_error(hdl_parser_t* psr, int errcode, char* str, ...)
{
  va_list ap;
  char* ostr;

  ostr = NULL;
  va_start(ap, str);
  kuso_vasprintf(&ostr, str, ap);
  va_end(ap);

  hienlog(NULL,LOG_CRIT,
"Kiban HDL ERROR: %s: %s\n"
"Offending line in file %s:\n"
"Line %d: %s\n"
, errstrs[errcode], ostr
, psr->fn
, psr->line
, psr->ostr);

  mem_free(ostr);
}

void hdl_warn(hdl_parser_t* psr, int errcode, char* str, ...)
{
  va_list ap;
  char* ostr;

  ostr = NULL;
  va_start(ap, str);
  kuso_vasprintf(&ostr, str, ap);
  va_end(ap);

  hienlog(NULL,LOG_WARN,
"Kiban HDL WARNING: %s: %s\n"
"Offending line in file %s:\n"
"Line %d: %s\n"
, errstrs[errcode], ostr
, psr->fn
, psr->line
, psr->ostr);

  mem_free(ostr);
}


int kiban_hdl(kiban_board_t* brd, char* file)
{
  int ret;
  hien_file_t* fp;
  size_t sz;
  char* str;

  fp = file_open(file, FILE_READ);
  if(fp == NULL) {
    hienlog(NULL,LOG_CRIT, "Kiban HDL ERROR: Can't open %s\n", file);
    return -1;
  }
  sz = file_size(fp);
  str = mem_alloc(sz+1);

  file_read(str, sz, fp);
  file_close(fp);

  str[sz] = '\0';

  ret = hdl_parse(brd, file, str);

  mem_free(str);
  return ret;
}
