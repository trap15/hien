#include "../kiban_int.h"

static int hdl_tknparse_sndmap_voltage(hdl_parser_t* psr)
{
  int end = 0;

  char* ends;
  double vlt;
  kuso_tkn_t* tkn;
  kiban_sndsrc_desc_t* src;
  hdl_psrstt_t* stt;

  stt = hdl_psrstate(psr);
  src = stt->data;

  tkn = psr->tkn;
  if(tkn->cnt < 1) {
    hdl_error(psr, HDLERR_TOOFEWARGS, "SNDMAP:VOLTAGE");
    end = -1;
    return end;
  }
  if(tkn->cnt > 2) {
    hdl_error(psr, HDLERR_TOOMANYARGS, "SNDMAP:VOLTAGE");
    end = -1;
    return end;
  }

  /* voltage */
  tkn = tkn->next;
  vlt = strtod(tkn->text, &ends);
  if(*ends != 'V') {
    hdl_warn(psr, HDLERR_SYNTAX, "SNDMAP:VOLTAGE, voltage lacks V specifier");
  }

  src->srcamp = vlt;

  return end;
}

static int hdl_tknparse_sndmap_start(hdl_parser_t* psr)
{
  int ret = 0;

  char* tag;

  kiban_snd_desc_t* snd;
  kuso_tkn_t* tkn;
  hdl_psrstt_t* stt;

  stt = hdl_psrstate(psr);
  tkn = psr->tkn;
  if(tkn->cnt < 2) {
    hdl_error(psr, HDLERR_TOOFEWARGS, "SNDMAP");
    ret = -1;
    goto end;
  }
  if(tkn->cnt > 2) {
    hdl_error(psr, HDLERR_TOOMANYARGS, "SNDMAP");
    ret = -1;
    goto end;
  }

  /* output */
  tkn = tkn->next;
  tag = tkn->text;
  kiban_snd_desc_t** snditer = kuso_pvec_iter(psr->board->snds, kiban_snd_desc_t*);
  for(; *snditer != NULL; snditer++) {
    if(strcmp((*snditer)->tag, tag) != 0)
      continue;

    break;
  }
  if(*snditer == NULL) {
    hdl_error(psr, HDLERR_BADPARAM, "SNDMAP, can't find output %s", tag);
    ret = -1;
    goto end;
  }
  snd = *snditer;

  /* source */
  tkn = tkn->next;
  tag = tkn->text;
  kiban_sndsrc_desc_t** srciter = kuso_pvec_iter(snd->srcs, kiban_sndsrc_desc_t*);
  for(; *srciter != NULL; srciter++) {
    if(strcmp((*srciter)->tag, tag) != 0)
      continue;

    break;
  }
  if(*srciter == NULL) {
    hdl_error(psr, HDLERR_BADPARAM, "SNDMAP, can't find source %s", tkn->text);
    ret = -1;
    goto end;
  }
  stt->data = *srciter;

end:
  return ret;
}

hdl_psrlist_t hdllist_sndmap = {
  .name = "SNDMAP",
  .start = hdl_tknparse_sndmap_start,
  .def = NULL,
  .end = NULL,
  .entry = {
    { "VOLTAGE", -1, hdl_tknparse_sndmap_voltage, },
    { NULL,      -1, NULL, },
  },
};
