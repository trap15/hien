#include "../kiban_int.h"

static int hdl_tknparse_exdev_memory(hdl_parser_t* psr)
{
  int end = 0;

  kiban_dev_t* dev;

  dev = kiban_dev_new();
  dev->tag = mem_strdup(psr->etkn->text);
  dev->loc = mem_strdup("");
  dev->typestr = mem_strdup("ram");
  dev->type = DEVTYPE_RAM;
  dev->vmtype = VMTYPE_ALL;

  dev->abus = 0;
  dev->dbus = 1;
  dev->access = 0;

  hdl_tknparam_int(psr, "ABUS", &dev->abus);
  hdl_tknparam_int(psr, "DBUS", &dev->dbus);

  kuso_pvec_append(psr->board->devs, dev);

  return end;
}

static int hdl_tknparse_exdev_clock(hdl_parser_t* psr)
{
  int end = 0;

  kiban_clock_t* clk;

  clk = kiban_clock_new();

  clk->loc = mem_strdup("");
  clk->tag = mem_strdup(psr->etkn->text);

  hdl_tknparam_int(psr, "RATE", &clk->spd);

  kuso_pvec_append(psr->board->clks, clk);

  return end;
}

hdl_psrlist_t hdllist_exdev = {
  .name = "EXDEV",
  .start = NULL,
  .def = NULL,
  .end = NULL,
  .entry = {
    { "MEMORY", -1, hdl_tknparse_exdev_memory, },
    { "CLOCK",  -1, hdl_tknparse_exdev_clock, },
    { NULL,     -1, NULL, },
  },
};
