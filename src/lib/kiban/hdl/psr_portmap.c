#include "../kiban_int.h"

static int hdl_tknparse_portmap_device_cmp(hdl_parser_t* psr)
{
  int end = 0;

  kuso_tkn_t* tkn;
  kiban_portmap_t* map;
  kiban_portmapper_t* mapr;
  hdl_psrstt_t* stt;

  stt = hdl_psrstate(psr);
  map = stt->data;

  mapr = kiban_portmapper_new(DEVTYPE_DEV);

  tkn = psr->tkn;

  /* map */
  tkn = tkn->next;
  if((strcmp(tkn->text, "FLAT") == 0) || (strcmp(tkn->text, "") == 0))
    kiban_portmapper_set_map(mapr, NULL);
  else
    kiban_portmapper_set_map(mapr, tkn->text);

  /* devid */
  tkn = tkn->next;
  kiban_portmapper_set_devid(mapr, tkn->text);

  kiban_portmap_add_mapper(map, mapr);

  return end;
}
static int hdl_tknparse_portmap_dev_exp_start(hdl_parser_t* psr)
{
  int ret = 0;
  char* name = NULL;

  hdl_psrstt_t* stt;
  hdl_psrstt_t* mpstt;

  kiban_busmap_t* map;
  kiban_portmap_t* pmap;

  stt = hdl_psrstate(psr);
  mpstt = hdl_psrstate_parent(psr, 1);
  pmap = mpstt->data;

  kuso_asprintf(&name, "_portmap_autogen_%d_%s", pmap->mpcnt, pmap->tag);
  map = kiban_busmap_new(name);
  map->size = 0;
  mem_free(name);

  stt->data = map;

  kuso_pvec_append(psr->board->busmaps, map);

  return ret;
}

hdl_psrlist_t hdllist_portmap_dev_bus = {
  .name = "PORTMAP:DEVICE:BUS",
  .start = hdl_tknparse_portmap_dev_exp_start,
  .def = hdl_tknparse_busmap,
  .end = NULL,
  .entry = {
    { NULL, -1, NULL, },
  },
};
static int hdl_tknparse_portmap_device_exp(hdl_parser_t* psr)
{
  int end = 0;
  char* name = NULL;

  kuso_tkn_t* tkn;
  kiban_portmap_t* map;
  kiban_portmapper_t* mapr;
  hdl_psrstt_t* stt;

  stt = hdl_psrstate(psr);
  map = stt->data;

  mapr = kiban_portmapper_new(DEVTYPE_DEV);

  tkn = psr->tkn;

  /* devid */
  tkn = tkn->next;
  kiban_portmapper_set_devid(mapr, tkn->text);

  /* map */
  kuso_asprintf(&name, "_portmap_autogen_%d_%s", map->mpcnt, map->tag);
  kiban_portmapper_set_map(mapr, name);
  mem_free(name);

  kiban_portmap_add_mapper(map, mapr);

  hdl_parser_newstate(psr, HDLPSR_PORTMAP_DEV_BUS);
  map->mpcnt++;

  return end;
}
static int hdl_tknparse_portmap_device(hdl_parser_t* psr)
{
  kuso_tkn_t* tkn;

  tkn = psr->tkn;
  if(tkn->cnt == 2) {
    return hdl_tknparse_portmap_device_cmp(psr);
  }else if(tkn->cnt == 1) {
    return hdl_tknparse_portmap_device_exp(psr);
  }else if(tkn->cnt < 1) {
    hdl_error(psr, HDLERR_TOOFEWARGS, "PORTMAP:DEVICE");
    return -1;
  }else if(tkn->cnt > 2) {
    hdl_error(psr, HDLERR_TOOMANYARGS, "PORTMAP:DEVICE");
    return -1;
  }else{
    hdl_error(psr, HDLERR_ARGCOUNT, "PORTMAP:DEVICE");
    return -1;
  }
}

static int hdl_tknparse_portmap_sound(hdl_parser_t* psr)
{
  int end = 0;

  kuso_tkn_t* tkn;
  kiban_portmap_t* map;
  kiban_portmapper_t* mapr;
  hdl_psrstt_t* stt;

  stt = hdl_psrstate(psr);
  map = stt->data;

  mapr = kiban_portmapper_new(DEVTYPE_SND);

  tkn = psr->tkn;
  if(tkn->cnt < 2) {
    hdl_error(psr, HDLERR_TOOFEWARGS, "PORTMAP:SOUND");
    return -1;
  }else if(tkn->cnt > 2) {
    hdl_error(psr, HDLERR_TOOMANYARGS, "PORTMAP:SOUND");
    return -1;
  }

  /* sound output */
  tkn = tkn->next;
  kiban_portmapper_set_snd(mapr, tkn->text);

  /* sound source */
  tkn = tkn->next;
  kiban_portmapper_set_sndsrc(mapr, tkn->text);

  kiban_portmap_add_mapper(map, mapr);

  return end;
}

static int hdl_tknparse_portmap_start(hdl_parser_t* psr)
{
  int ret = 0;

  kiban_portmap_t* map;
  hdl_psrstt_t* stt;
  char* busstr;
  int bus;

  stt = hdl_psrstate(psr);

  map = kiban_portmap_new(psr->etkn->text);
  bus = map->bus;

  hdl_tknparam_str(psr, "BUS", &busstr);

  if(busstr[0] != 'A') {
    hdl_tknparam_int(psr, "BUS", &bus);
    kiban_portmap_set_buslen(map, bus);
  }else{
    kiban_portmap_set_buslen(map, 0);
  }
  mem_free(busstr);

  stt->data = map;

  kuso_pvec_append(psr->board->portmaps, map);

  return ret;
}

hdl_psrlist_t hdllist_portmap = {
  .name = "PORTMAP",
  .start = hdl_tknparse_portmap_start,
  .def = NULL,
  .end = NULL,
  .entry = {
    { "DEVICE", -1, hdl_tknparse_portmap_device, },
    { "SOUND",  -1, hdl_tknparse_portmap_sound, },
    { NULL,     -1, NULL, },
  },
};
