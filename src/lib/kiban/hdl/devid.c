#include "../kiban_int.h"

kiban_dev_id_t* hdl_parse_devid(char* str)
{
  kuso_tkn_t* tkns;
  kuso_tkn_t* tkn;
  kiban_dev_id_t* id;

  id = kiban_devid_new();
  /* If it's neither a space nor a port, the devname is the whole string */
  kiban_devid_set_devname(id, str);

  tkn = tkns = kuso_tokenize(str, "$", "");
  if(!tkn->last) { /* Space ID */
    kiban_devid_set_devname(id, tkn->text);

    tkn = tkn->next;
    kiban_devid_set_space(id, tkn->text);

    /* Check for address token */
    kuso_tknfree_all(tkns);
    tkn = tkns = kuso_tokenize(str, "@", "");
    if(!tkn->last) { /* Has address */
      tkn = tkn->next;
      kiban_devid_set_addr(id, xtoi(tkn->text));
    }
  }else{ /* Port ID or raw devid */
    kuso_tknfree_all(tkns);
    tkn = tkns = kuso_tokenize(str, "!", "");
    if(!tkn->last) { /* Port ID */
      kiban_devid_set_devname(id, tkn->text);

      tkn = tkn->next;
      kiban_devid_set_port(id, tkn->text);
    }
  }
  kuso_tknfree_all(tkns);
  return id;
}
