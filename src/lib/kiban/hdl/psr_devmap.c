#include "../kiban_int.h"

static int hdl_tknparse_devmap_space(hdl_parser_t* psr)
{
  int end = 0;

  kuso_tkn_t* tkn;
  kiban_dev_t* dev;
  hdl_psrstt_t* stt;
  kiban_memmap_ref_t* ref;

  stt = hdl_psrstate(psr);
  dev = stt->data;
  tkn = psr->tkn;
  ref = mem_alloc_type(kiban_memmap_ref_t);

  /* Space name */
  tkn = tkn->next;
  ref->name = mem_strdup(tkn->text);

  /* Space map name */
  tkn = tkn->next;
  kiban_memmap_t** iter = kuso_pvec_iter(psr->board->memmaps, kiban_memmap_t*);
  for(; *iter != NULL; iter++) {
    if(strcmp((*iter)->tag, tkn->text) == 0)
      break;
  }
  if(*iter == NULL) {
    hdl_error(psr, HDLERR_BADPARAM, "DEVMAP:SPACE, can't find MEMMAP %s", tkn->text);
    end = -1;
    goto devmap_space_done;
  }
  ref->map = *iter;

  kuso_pvec_append(dev->spacemap, ref);

devmap_space_done:
  return end;
}

static int hdl_tknparse_devmap_port(hdl_parser_t* psr)
{
  int end = 0;

  kuso_tkn_t* tkn;
  kiban_dev_t* dev;
  hdl_psrstt_t* stt;
  kiban_portmap_ref_t* ref;

  stt = hdl_psrstate(psr);
  dev = stt->data;
  tkn = psr->tkn;
  ref = mem_alloc_type(kiban_portmap_ref_t);

  /* Port name */
  tkn = tkn->next;
  ref->name = mem_strdup(tkn->text);

  /* Port map name */
  tkn = tkn->next;
  if(strcmp("HIGH", tkn->text) == 0) {
    ref->map = NULL;
    ref->key = 1;
  }else if(strcmp("LOW", tkn->text) == 0) {
    ref->map = NULL;
    ref->key = 0;
  }else{
    kiban_portmap_t** iter = kuso_pvec_iter(psr->board->portmaps, kiban_portmap_t*);
    for(; *iter != NULL; iter++) {
      if(strcmp((*iter)->tag, tkn->text) == 0)
        break;
    }
    if(*iter == NULL) {
      hdl_error(psr, HDLERR_BADPARAM, "DEVMAP:PORT, can't find PORTMAP %s", tkn->text);
      end = -1;
      goto devmap_port_done;
    }
    ref->map = *iter;
  }

  kuso_pvec_append(dev->portmap, ref);

devmap_port_done:
  return end;
}

static int hdl_tknparse_devmap_start(hdl_parser_t* psr)
{
  int ret = 0;

  char* tag;
  hdl_psrstt_t* stt;

  stt = hdl_psrstate(psr);

  tag = psr->etkn->text;
  kiban_dev_t** iter = kuso_pvec_iter(psr->board->devs, kiban_dev_t*);
  for(; *iter != NULL; iter++) {
    if(strcmp((*iter)->tag, tag) != 0)
      continue;

    break;
  }
  if(*iter == NULL) {
    hdl_error(psr, HDLERR_BADPARAM, "DEVMAP, can't find device %s", tag);
    ret = -1;
  }
  stt->data = *iter;

  return ret;
}

hdl_psrlist_t hdllist_devmap = {
  .name = "DEVMAP",
  .start = hdl_tknparse_devmap_start,
  .def = NULL,
  .end = NULL,
  .entry = {
    { "SPACE", -1, hdl_tknparse_devmap_space, },
    { "PORT",  -1, hdl_tknparse_devmap_port, },
    { NULL,    -1, NULL, },
  },
};
