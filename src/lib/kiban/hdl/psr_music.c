#include "../kiban_int.h"

static int hdl_tknparse_music_write(hdl_parser_t* psr)
{
  int end = 0;

  char* str;
  kuso_tkn_t* tkn;
  hdl_psrstt_t* stt;
  kuso_pvec_t* cmds;
  kiban_music_cmd_t* cmd;

  stt = hdl_psrstate(psr);
  cmds = stt->data;
  cmd = kiban_music_cmd_new();

  tkn = psr->tkn;

  /* Parameters */
  tkn = tkn->next;
  str = NULL;
  hdl_tknparam_str(psr, "DATA", &str);

  if(str != NULL) {
    if(strcmp(str, "SNDID") == 0)
      kiban_music_cmd_set_fixdata(cmd, MUSCMD_DATA_SNDID);
    else
      kiban_music_cmd_set_data(cmd, xtoi(str));
    mem_free(str);
  }

  /* Device */
  kiban_music_cmd_set_devid(cmd, psr->etkn->text);

  kuso_pvec_append(cmds, cmd);

  return end;
}

static int hdl_tknparse_music_start(hdl_parser_t* psr)
{
  int end = 0;

  kuso_tkn_t* tkn;
  hdl_psrstt_t* stt;

  stt = hdl_psrstate(psr);
  tkn = psr->tkn;
  if(tkn->cnt < 1) {
    hdl_error(psr, HDLERR_TOOFEWARGS, "Music");
    end = -1;
    return end;
  }
  if(tkn->cnt > 1) {
    hdl_error(psr, HDLERR_TOOMANYARGS, "Music");
    end = -1;
    return end;
  }

  /* type */
  tkn = tkn->next;
  TKNHNDSTART()
  TKNHANDLE(tkn, "PLAY") {
    stt->data = psr->board->music->playcmds;
    psr->board->music->has_play = TRUE;
  }
  TKNHANDLE(tkn, "STOP") {
    stt->data = psr->board->music->stopcmds;
    psr->board->music->has_stop = TRUE;
  }
  TKNHANDLE(tkn, "RESET") {
    stt->data = psr->board->music->rstcmds;
    psr->board->music->has_reset = TRUE;
  }else{
    hdl_error(psr, HDLERR_BADPARAM, "Music, command type %s", tkn->text);
    end = -1;
    return end;
  }

  return end;
}

hdl_psrlist_t hdllist_music = {
  .name = "MUSIC",
  .start = hdl_tknparse_music_start,
  .def = NULL,
  .end = NULL,
  .entry = {
    { "WRITE", -1, hdl_tknparse_music_write, },
    { NULL,    -1, NULL, },
  },
};
