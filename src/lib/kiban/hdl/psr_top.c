#include "../kiban_int.h"

hdl_psrlist_t hdllist_top = {
  .name = "TOP",
  .start = NULL,
  .def = NULL,
  .end = NULL,
  .entry = {
    { "EXDEV",   HDLPSR_EXDEV,  NULL, },
    { "BUSMAP",  HDLPSR_BUSMAP, NULL, },
    { "MEMMAP",  HDLPSR_MEMMAP, NULL, },
    { "DEVMAP",  HDLPSR_DEVMAP, NULL, },
    { "SNDMAP",  HDLPSR_SNDMAP, NULL, },
    { "MUSIC",   HDLPSR_MUSIC,  NULL, },
    { "PORTMAP", HDLPSR_PORTMAP,NULL, },
    { "SUBMAP",  HDLPSR_SUBMAP, NULL, },
    { NULL,      -1,            NULL, },
  },
};
