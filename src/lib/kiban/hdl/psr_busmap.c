#include "../kiban_int.h"

int hdl_tknparse_busmap(hdl_parser_t* psr)
{
  int end = 0;

  int src,slen, dst,dlen;
  int dir;

  char* asn;

  hdl_psrstt_t* stt;
  kuso_tkn_t* tkn;
  kiban_busmap_t* map;
  kiban_busmapper_t* mapr;

  stt = hdl_psrstate(psr);
  map = stt->data;
  mapr = kiban_busmapper_new();

  tkn = psr->tkn;
  /* Source */
  if(!tkn->last) { /* Has a source */
    if(strcmp(tkn->text, "SRC") != 0)
      kiban_busmapper_set_devid(mapr, tkn->text);
    tkn = tkn->next;
  }

  /* Mapping */
  asn = tkn->text;

  if(hdl_tknmove(&asn, &dir, &src,&slen, &dst,&dlen) < 0) {
    hdl_error(psr, HDLERR_SYNTAX, "Busmap entry parsing");
    end = -1;
    goto busmap_done;
  }
  if(dir < 0) {
    hdl_error(psr, HDLERR_SYNTAX, "Busmap entry direction");
    end = -1;
    goto busmap_done;
  }

  if(dlen != slen && slen != -1) {
    hdl_error(psr, HDLERR_BADPARAM, "Busmap entry, unbalanced sizes %d => %d", slen, dlen);
    end = -1;
    goto busmap_done;
  }

  if((map->size != 0) && (dst+dlen > map->size)) {
    hdl_error(psr, HDLERR_BADPARAM, "Busmap entry, too big %d+%d > %d", dst, dlen, map->size);
    end = -1;
    goto busmap_done;
  }

  if(slen == -1)
    mapr->key = 1;
  mapr->spin = src;
  mapr->dpin = dst;
  mapr->len = dlen;

  kiban_busmap_add_mapper(map, mapr);

busmap_done:
  if(end < 0)
    kiban_busmapper_free(mapr);
  return end;
}

static int hdl_tknparse_busmap_start(hdl_parser_t* psr)
{
  int ret = 0;

  kiban_busmap_t* map;
  hdl_psrstt_t* stt;

  stt = hdl_psrstate(psr);

  map = kiban_busmap_new(psr->etkn->text);
  hdl_tknparam_int(psr, "SIZE", &map->size);

  stt->data = map;

  kuso_pvec_append(psr->board->busmaps, map);

  return ret;
}

hdl_psrlist_t hdllist_busmap = {
  .name = "BUSMAP",
  .start = hdl_tknparse_busmap_start,
  .def = hdl_tknparse_busmap,
  .end = NULL,
  .entry = {
    { NULL, -1, NULL, },
  },
};
