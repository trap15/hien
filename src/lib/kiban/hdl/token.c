#include "../kiban_int.h"

int hdl_tknassign(kuso_tkn_t* tkn, char** dst, char** src)
{
  size_t d,s;

  if(dst) *dst = NULL;
  if(src) *src = NULL;

  for(d = 0; d < tkn->len; d++) {
    if(tkn->text[d] != '=')
      continue;

    if(dst) {
      if(d) {
        *dst = mem_alloc(d+1);
        memcpy(*dst, tkn->text, d);
        (*dst)[d] = '\0';
      }
    }
    break;
  }
  if(d == tkn->len) {
    return 0;
  }
  if(src) {
    s = tkn->len - (d+1);
    if(s > 0) {
      *src = mem_alloc(s+1);
      memcpy(*src, tkn->text+d+1, s);
      (*src)[s] = '\0';
    }
  }

  return 1;
}

static int hdl_tknrange(char** str, int* val,int* len)
{
  char* asn = *str;
  *val = 0;
  *len = -1;
  switch(asn[0]) {
    case 'X': /* Floating */
      *val = -1;
      asn++;
      break;
    case 'L': /* Pull low */
      *val = 0;
      asn++;
      break;
    case 'H': /* Pull high */
      *val = 1;
      asn++;
      break;
    default:
      *len = 1;
      *val = xtoi_full(asn, -1, &asn);
      if(asn[0] == '.' && asn[1] == '.') { /* Range */
        asn += 2;
        if(asn[0] == '.') { /* Inclusive range */
          *len -= 1;
          asn++;
        }
        *len += xtoi_full(asn, -1, &asn) - *val;
      }
      if(*len < 0)
        return -1;
      break;
  }
  *str = asn;
  return 0;
}

int hdl_tknmove(char** str, int* dir, int* src,int* slen, int* dst,int* dlen)
{
  char* asn = *str;

  if(hdl_tknrange(&asn, src,slen) < 0)
    return -1;

  if((asn[0] == '=') && (asn[1] == '>')) {
    *dir = 1;
  }else if((asn[0] == '<') && (asn[1] == '=')) {
    *dir = -1;
  }else{
    return -1;
  }
  asn += 2;

  if(hdl_tknrange(&asn, dst,dlen) < 0)
    return -1;
  *str = asn;
  return 0;
}

int hdl_tknparam_int(hdl_parser_t* psr, char* name, int* data)
{
  kuso_tkn_t* tkn;
  int cnt, i;
  char* dst;
  char* src;
  int ret = 0;

  cnt = psr->tkn->cnt;
  tkn = psr->tkn->next;

  if(data == NULL)
    return 0;

  for(i = 0; i < cnt && tkn != NULL; i++, tkn = tkn->next) {
    if(!hdl_tknassign(tkn, &dst, &src))
      continue;

    if(strcmp(name, dst) == 0) {
      *data = xtoi(src);
      ret++;
    }

    mem_free(dst);
    mem_free(src);
  }

  return ret;
}

int hdl_tknparam_str(hdl_parser_t* psr, char* name, char** data)
{
  kuso_tkn_t* tkn;
  int cnt, i;
  char* dst;
  char* src;
  int ret = 0;

  cnt = psr->tkn->cnt;
  tkn = psr->tkn->next;

  if(data == NULL)
    return 0;

  for(i = 0; i < cnt && tkn != NULL; i++, tkn = tkn->next) {
    if(!hdl_tknassign(tkn, &dst, &src))
      continue;

    if(strcmp(name, dst) == 0) {
      *data = src;
      mem_free(dst);
      ret++;
      return ret;
    }

    mem_free(dst);
    mem_free(src);
  }

  return ret;
}
