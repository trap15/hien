#include "../kiban_int.h"

static hdl_psrlist_t* hdllists[] = {
  [HDLPSR_TOP] = &hdllist_top,
    [HDLPSR_EXDEV]   = &hdllist_exdev,
    [HDLPSR_BUSMAP]  = &hdllist_busmap,
    [HDLPSR_MEMMAP]  = &hdllist_memmap,
      [HDLPSR_MEMMAP_EXP] = &hdllist_memmap_exp,
    [HDLPSR_DEVMAP]  = &hdllist_devmap,
    [HDLPSR_SNDMAP]  = &hdllist_sndmap,
    [HDLPSR_MUSIC]   = &hdllist_music,
    [HDLPSR_PORTMAP] = &hdllist_portmap,
      [HDLPSR_PORTMAP_DEV_BUS] = &hdllist_portmap_dev_bus,
    [HDLPSR_SUBMAP]  = &hdllist_submap,
      [HDLPSR_SUBMAP_DEV] = &hdllist_submap_dev,
        [HDLPSR_SUBMAP_DEV_BUS] = &hdllist_busmap_dev_bus,
};

void hdl_parser_pushstate(hdl_parser_t* psr, int stt)
{
  hdl_psrstt_t* nst;
  nst = mem_alloc_type(hdl_psrstt_t);
  nst->state = stt;
  kuso_pstack_push(psr->state, nst);
}

hdl_psrstt_t* hdl_parser_popstate(hdl_parser_t* psr)
{
  return kuso_pstack_pop(psr->state);
}

int hdl_parser_newstate(hdl_parser_t* psr, int stt)
{
  int end = 0;
  int st;
  hdl_psrlist_t* list;

  hdl_parser_pushstate(psr, stt);
  st = hdl_psrstate(psr)->state;
  list = hdllists[st];

  if(list->start != NULL)
    end = list->start(psr);

  return end;
}

hdl_psrstt_t* hdl_parser_endstate(hdl_parser_t* psr)
{
  int st;
  hdl_psrlist_t* list;

  st = hdl_psrstate(psr)->state;
  list = hdllists[st];

  if(list->end != NULL)
    list->end(psr);

  return kuso_pstack_pop(psr->state);
}

hdl_psrstt_t* hdl_psrstate(hdl_parser_t* psr)
{
  return kuso_pstack_peek(psr->state);
}

hdl_psrstt_t* hdl_psrstate_parent(hdl_parser_t* psr, int level)
{
  return kuso_pstack_get(psr->state, -1 - level);
}

static int hdl_tknparse(hdl_parser_t* psr)
{
  int done = 0;
  int end;
  int st;
  int hit = FALSE;
  kuso_tkn_t* tkn;
  hdl_psrlist_t* list;
  hdl_psrlist_entry_t* ent;

  tkn = psr->tkn;

  st = hdl_psrstate(psr)->state;
  list = hdllists[st];

  if(kuso_cmptkn(tkn, "END")) {
    end = 1;
    goto _tknparse_end;
  }

  /* Handle each entry */
  for(ent = list->entry; (ent->name != NULL); ent++) {
    if(kuso_cmptkn(tkn, ent->name)) {
      if(ent->psrid != -1)
        end = hdl_parser_newstate(psr, ent->psrid);

      if(ent->run != NULL)
        end = ent->run(psr);

      hit = TRUE;
      break;
    }
  }

  /* Handle an unknown token */
  if(!hit) {
    if(list->def != NULL) {
      end = list->def(psr);
    }else{
      hdl_error(psr, HDLERR_SYNTAX, "Unknown token [%s] in section [%s]", tkn->text, list->name);
      end = -1;
    }
  }

_tknparse_end:
  switch(end) {
    case 1:
      mem_free(hdl_parser_endstate(psr));
      if(kuso_pstack_empty(psr->state))
        done = 1;
      break;
    case -1:
      done = -1;
      break;
    default:
      break;
  }

  return done;
}

int hdl_parse(kiban_board_t* brd, char* fn, char* str)
{
  int ret = 0;
  int done = 0;
  char* tmp;
  char och;
  char* kptr;
  hdl_parser_t psr;

  psr.board = brd;
  psr.state = kuso_pstack_new(1);
  psr.line = 1;
  psr.str = str;
  psr.fn = fn;
  psr.ostr = str;

  hdl_parser_pushstate(&psr, HDLPSR_TOP);

  while((*psr.ostr != '\0') && (done == 0)) {
    tmp = kuso_stripcomment(psr.ostr, "#");
    psr.str = kuso_stripspace(tmp);
    mem_free(tmp);

    kptr = kuso_nextline(psr.ostr);
    och = *kptr;
    *kptr = '\0';

    psr.tkn = kuso_tokenize(psr.str, ":", "");
    if(psr.tkn != NULL) {
      psr.etkn = kuso_tknlast(psr.tkn);
      done = hdl_tknparse(&psr);
      kuso_tknfree_all(psr.tkn);
    }

    *kptr = och;

    mem_free(psr.str);
    psr.ostr = kuso_nextline(psr.ostr);
    psr.line++;
  }

  if(done == -1)
    ret = -1;

  while(!kuso_pstack_empty(psr.state))
    mem_free(hdl_parser_popstate(&psr));

  kuso_pstack_delete(psr.state);

  return ret;
}
