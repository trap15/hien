/**********************************************************
*
* hien.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
***********************************************************
*
* Global include header. Do not include any headers that
* are included here!
*
* TODO: Maybe make a static assert throw if you do?
*
**********************************************************/

#ifndef _HIEN_H_
#define _HIEN_H_

/* System */
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

/* Make Apple's cdefs.h shut up */
#if MACOSX && TCC_COMPILE
#define __GNUC__ 4
typedef void* __builtin_va_list;
#endif

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h> /* Probably want to remove this later */

typedef struct hien_vm_t hien_vm_t;
typedef struct hien_board_t hien_board_t;

/* Build configuration */
#include "buildconf.h"
#include "devlist.h"

#include "lib/kuso/kuso.h"
#if !TCC_COMPILE
#include "lib/kiban/kiban.h"
#endif

/* Util */
#include "helper.h"
#include "file.h"
#include "timeunit.h"
#include "hash.h"

/* Core */
#include "timer.h"
#include "analog.h"
#include "cpuprof.h"
#include "dev.h"
#include "devinfo.h"
#include "sound.h"
#include "sndplay.h"
#include "board_desc.h"
#include "board.h"
#include "rom.h"
#include "log.h"
#include "config.h"
#include "state.h"

#include "vm.h"

/* OSD */
#include "osd.h"
#include "ui.h"

/* Modules */
#include "modules.h"

#endif

