/**********************************************************
*
* dev.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

static void dev_update(hien_timer_t* tmr, void* data)
{
  hien_dev_t* dev = data;
  (void)tmr;
  dev_port_invert(dev, PORT_CLOCK);
}

hien_dev_t* dev_new(hien_vm_t* vm, char* name, hz_t clock)
{
  size_t i;
  hien_dev_t* dev;

  dev = mem_alloc_type(hien_dev_t);

  dev->vm = vm;
  if(name != NULL)
    dev->name = mem_strdup(name);
  else
    dev->name = NULL;

  dev->prof = NULL;

  dev->data = NULL;
  dev->drv_data = NULL;
  dev->delete = NULL;
  dev->reset = NULL;
  for(i = 0; i < SPACE_MAX; i++) {
    dev->space[i].width = 0;
    dev->space[i].read  = NULL;
    dev->space[i].write = NULL;
    dev->space[i].data  = 0;
  }
  for(i = 0; i < PORT_MAX; i++) {
    dev->port[i].width        = 0;
    dev->port[i].flag         = 0;
    dev->port[i].val          = 0;
    dev->port[i].aval         = A_0V;
    dev->port[i].read         = NULL;
    dev->port[i].write        = NULL;
    dev->port[i].analog_read  = NULL;
    dev->port[i].analog_write = NULL;
  }
  dev->ctmr = NULL;
  dev_change_clock(dev, clock);

  dev_port_setup(dev, PORT_CLOCK, 1, PORT_INPUT);
  dev_port_deassert(dev, PORT_CLOCK); /* Start on logic low, first clock has rising edge */

  return dev;
}

void dev_delete(hien_dev_t* dev)
{
  if(dev->prof) {
    cpuprof_delete(dev->prof);
    dev->prof = NULL;
  }

  if(dev->ctmr) {
    tmr_mgr_remove(dev->vm->tmrmgr, dev->ctmr);
    tmr_delete(dev->ctmr);
  }

  if(dev->name)
    mem_free(dev->name);

  if(dev->delete)
    dev->delete(dev);

  mem_free(dev);
}

void dev_reset(hien_dev_t* dev)
{
  if(dev->ctmr) {
    tmr_reset(dev->ctmr, timetick_zero);
  }
  if(dev->reset) {
    dev->reset(dev);
  }
}

hz_t dev_change_clock(hien_dev_t* dev, hz_t clock)
{
  hz_t oclock = dev->clock;
  dev->clock = clock;

  if(dev->ctmr != NULL) {
    tmr_mgr_remove(dev->vm->tmrmgr, dev->ctmr);
    tmr_delete(dev->ctmr);
    dev->ctmr = NULL;
  }

  if(dev->clock != 0) {
    dev->ctmr = tmr_new();
    tmr_enable(dev->ctmr, TRUE);
    tmr_set_callback(dev->ctmr, dev_update, dev);
    tmr_set_periodic_hz(dev->ctmr, dev->clock * dev->vm->config->speed * 2); /* x2 for hitting on high and low */
    tmr_mgr_add(dev->vm->tmrmgr, dev->ctmr);
  }

  return oclock;
}

uintmax_t dev_read(hien_dev_t* dev, int space, addr_t addr, uintmax_t mask)
{
  if(dev->space[space].read != NULL)
    dev->space[space].data = dev->space[space].read(dev, addr, mask);
  return dev_space_get_openbus(dev, space, mask);
}

void dev_write(hien_dev_t* dev, int space, addr_t addr, uintmax_t data, uintmax_t mask)
{
  COMBINE_DATA(dev->space[space].data, data, mask);
  if(dev->space[space].write != NULL)
    dev->space[space].write(dev, addr, data, mask);
}

uintmax_t dev_space_get_openbus(hien_dev_t* dev, int space, uintmax_t mask)
{
  return dev->space[space].data & mask;
}

void dev_space_setup(hien_dev_t* dev, int space, size_t width)
{
  dev->space[space].width = width;
}

void dev_space_set_callbacks(hien_dev_t* dev, int space,
                        uintmax_t (*read)(hien_dev_t* dev, addr_t addr, uintmax_t mask),
                        void (*write)(hien_dev_t* dev, addr_t addr, uintmax_t data, uintmax_t mask))
{
  dev->space[space].read  = read;
  dev->space[space].write = write;
}


void dev_port_setup(hien_dev_t* dev, int port, size_t width, int flag)
{
  dev->port[port].flag       = flag;
  dev->port[port].width      = width;
  dev->port[port].mask       = BITMASK(width);
  dev->port[port].assert_cmp = BITMASK(width) >> 1;
}

uintmax_t dev_port_read(hien_dev_t* dev, int port, uintmax_t mask)
{
  if(dev->port[port].read != NULL)
    dev->port[port].val = dev->port[port].read(dev, mask) & mask;
  return dev_port_get_openbus(dev, port, mask);
}

void dev_port_write(hien_dev_t* dev, int port, uintmax_t data, uintmax_t mask)
{
  if((dev->port[port].val & mask) == (data & mask))
    return;
  COMBINE_DATA(dev->port[port].val, data & mask, mask);
  if(dev->port[port].write != NULL)
    dev->port[port].write(dev, data & mask, mask);
}

uintmax_t dev_port_get_openbus(hien_dev_t* dev, int port, uintmax_t mask)
{
  return dev->port[port].val & mask;
}

void dev_port_assert(hien_dev_t* dev, int port)
{
  dev_port_write(dev, port, ~0, dev->port[port].mask);
}

void dev_port_deassert(hien_dev_t* dev, int port)
{
  dev_port_write(dev, port, 0, dev->port[port].mask);
}

int dev_port_is_asserted(hien_dev_t* dev, int port)
{
  uintmax_t val = dev_port_read(dev, port, dev->port[port].mask);
  uintmax_t cmp = dev->port[port].assert_cmp;
  if(val > cmp)
    return 1;
  else
    return 0;
}

void dev_port_invert(hien_dev_t* dev, int port)
{
  if(dev_port_is_asserted(dev, port))
    dev_port_deassert(dev, port);
  else
    dev_port_assert(dev, port);
}


analog_t dev_port_analog_read(hien_dev_t* dev, int port)
{
  if(dev->port[port].analog_read != NULL)
    dev->port[port].aval = dev->port[port].analog_read(dev);
  return dev_port_analog_get_openbus(dev, port);
}

void dev_port_analog_write(hien_dev_t* dev, int port, analog_t data)
{
  dev->port[port].aval = data;
  if(dev->port[port].analog_write != NULL)
    dev->port[port].analog_write(dev, data);
}

analog_t dev_port_analog_get_openbus(hien_dev_t* dev, int port)
{
  return dev->port[port].aval;
}

void dev_port_set_callbacks(hien_dev_t* dev, int port,
                        uintmax_t (*read)(hien_dev_t* dev, uintmax_t mask),
                        void (*write)(hien_dev_t* dev, uintmax_t data, uintmax_t mask))
{
  dev->port[port].read  = read;
  dev->port[port].write = write;
}

void dev_port_set_analog_callbacks(hien_dev_t* dev, int port,
                        analog_t (*read)(hien_dev_t* dev),
                        void (*write)(hien_dev_t* dev, analog_t data))
{
  dev->port[port].analog_read  = read;
  dev->port[port].analog_write = write;
}



