/**********************************************************
*
* cpu/z80/z80_internal.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _CPU_Z80_INTERNAL_H_
#define _CPU_Z80_INTERNAL_H_

typedef struct cpu_z80_t cpu_z80_t;

struct cpu_z80_t {
  /* State */
  int z80model;

  uint8_t ir; /* Instruction Register */

  int cyc_use;

  /* Z80 */
  hien_dev_t* dev;

  pair16_t prvpc, pc, sp, af, bc, de, hl, ix, iy, wz;
  pair16_t af2,bc2,de2,hl2;
  uint8_t  r,r2,i;
  int      iff1,iff2;
  int      im;

  int      rst_last;
  int      nmi_last;

  uint16_t ea;
  int      after_ei;
  int      after_ldair;

  uint8_t        rtemp;
  const uint8_t* cc_op;
  const uint8_t* cc_cb;
  const uint8_t* cc_ed;
  const uint8_t* cc_xy;
  const uint8_t* cc_xycb;
  const uint8_t* cc_ex;
};

#define CF 0x01
#define NF 0x02
#define PF 0x04
#define VF PF
#define XF 0x08
#define HF 0x10
#define YF 0x20
#define ZF 0x40
#define SF 0x80

#define PC pc.v

#define SP sp.v

#define AF af.v
#define A  af.i.h
#define F  af.i.l

#define BC bc.v
#define B  bc.i.h
#define C  bc.i.l

#define DE de.v
#define D  de.i.h
#define E  de.i.l

#define HL hl.v
#define H  hl.i.h
#define L  hl.i.l

#define IX  ix.v
#define IXh ix.i.h
#define IXl ix.i.l

#define IY  iy.v
#define IYh iy.i.h
#define IYl iy.i.l

#define WZ  wz.v
#define WZh wz.i.h
#define WZl wz.i.l

void _z80_emu_cycle(hien_dev_t* dev);
void _z80_emu_init(hien_dev_t* dev);
void _z80_emu_delete(hien_dev_t* dev);

void _z80_emu_nmi(hien_dev_t* dev, cpu_z80_t* cpu);
void _z80_emu_int(hien_dev_t* dev, cpu_z80_t* cpu);

void _z80_dis_do(hien_dev_t* dev, addr_t pc, char* buf);
void _z80_dis_init(hien_dev_t* dev);
void _z80_dis_delete(hien_dev_t* dev);

#endif
