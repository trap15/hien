/**********************************************************
*
* cpu/z80/z80.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"
#include "z80_internal.h"

#define Z80DEBUG 0

static void cpu_z80_update(hien_dev_t* dev, uintmax_t data, uintmax_t mask)
{
  cpu_z80_t* cpu;
  addr_t pc;
  (void)mask;
  cpu = dev->data;

  if(data != 1) /* do nothing on falling edge */
    return;

  pc = cpu->PC;

  cpu->cyc_use = 0;

  if(dev_port_is_asserted(dev, Z80_PORT_RESET)) {
#if Z80DEBUG
    printf("RESET 0x%04X\n", cpu->PC);
#endif
    dev_reset(dev);
    cpu->ir = 0x00;
  }else if(dev_port_is_asserted(dev, Z80_PORT_NMI) && !cpu->nmi_last) {
#if Z80DEBUG
    printf("NMI 0x%04X\n", cpu->PC);
#endif
    dev_port_deassert(dev, Z80_PORT_HALT);

    if(cpu->z80model == Z80_MODEL_NMOS) { /* LD A,I|R bug */
      if(cpu->after_ldair)
        cpu->F &= ~PF;
    }

    _z80_emu_nmi(dev, cpu);

  }else if(dev_port_is_asserted(dev, Z80_PORT_INT) && cpu->iff1) {
#if Z80DEBUG
    printf("INT 0x%04X\n", cpu->PC);
#endif
    dev_port_deassert(dev, Z80_PORT_HALT);

    if(cpu->z80model == Z80_MODEL_NMOS) { /* LD A,I|R bug */
      if(cpu->after_ldair)
        cpu->F &= ~PF;
    }

    _z80_emu_int(dev, cpu);

  }else if(dev_port_is_asserted(dev, Z80_PORT_HALT)) {
#if Z80DEBUG
    printf("HALT 0x%04X\n", cpu->PC);
#endif
    /* Execute a NOP */
    cpu->ir = 0x00;
  }else{
#if Z80DEBUG
    printf("NORMAL 0x%04X\n", cpu->PC);
#endif
    cpu->ir = dev_read(dev, SPACE_MAIN, cpu->PC++, 0xFF);
  }
  cpu->nmi_last = dev_port_is_asserted(dev, Z80_PORT_NMI);

#if Z80DEBUG
  char tmpbuf[512];
  _z80_dis_do(dev, cpu->PC-1, tmpbuf);
  printf("DIS: %02X %s\n", cpu->ir, tmpbuf);
#endif
  _z80_emu_cycle(dev);

  cpuprof_sub_cycle(dev->prof, pc, cpu->cyc_use);
  cpuprof_sub_insn(dev->prof, pc, 1);
  cpuprof_sub_byte(dev->prof, pc, 1);

  tmr_delay(dev->ctmr, cpu->cyc_use-1);
}

static uintmax_t cpu_z80_read(hien_dev_t* dev, addr_t addr, uintmax_t mask)
{
  dev_port_write(dev, Z80_PORT_DBUS, addr, 0xFFFF);
  return dev_read(dev, SPACE_MAIN, addr, mask);
}

static void cpu_z80_write(hien_dev_t* dev, addr_t addr, uintmax_t data, uintmax_t mask)
{
  dev_port_write(dev, Z80_PORT_DBUS, addr, 0xFFFF);
  dev_write(dev, SPACE_MAIN, addr, data, mask);
}

static uintmax_t cpu_z80_io_read(hien_dev_t* dev, addr_t addr, uintmax_t mask)
{
  dev_port_write(dev, Z80_PORT_DBUS, addr, 0xFFFF);
  return dev_read(dev, Z80_SPACE_IO, addr, mask);
}

static void cpu_z80_io_write(hien_dev_t* dev, addr_t addr, uintmax_t data, uintmax_t mask)
{
  dev_port_write(dev, Z80_PORT_DBUS, addr, 0xFFFF);
  dev_write(dev, Z80_SPACE_IO, addr, data, mask);
}

static void cpu_z80_reset(hien_dev_t* dev)
{
  cpu_z80_t* cpu;
  cpu = dev->data;

  cpu->PC = 0;
  cpu->SP = 0;
  cpu->AF = 0;
  cpu->WZ = cpu->PC;

  cpu->af2.v = 0;

  cpu->r = 0;
  cpu->r2 = 0;
  cpu->iff1 = FALSE;
  cpu->iff2 = FALSE;

  cpu->im = 0;
  cpu->i = 0;

  cpu->after_ei = 0;
  cpu->after_ldair = 0;
  cpu->ea = 0;
  cpu->nmi_last = 0;

  cpu->IX = 0xFFFF;
  cpu->IY = 0xFFFF;
  cpu->F = ZF;
}

static void cpu_z80_delete(hien_dev_t* dev)
{
  cpu_z80_t* cpu;
  cpu = dev->data;

  _z80_emu_delete(dev);

  mem_free(cpu);
}

static hien_dev_t* cpu_z80base_new(hien_vm_t* vm, char* name, hz_t clock)
{
  hien_dev_t* dev;
  cpu_z80_t* cpu;

  cpu = mem_alloc_type(cpu_z80_t);

  dev = dev_new(vm, name, clock);
  dev->delete = cpu_z80_delete;
  dev->reset  = cpu_z80_reset;

  dev_port_set_callbacks(dev, PORT_CLOCK, NULL, cpu_z80_update);

  dev_space_setup(dev, SPACE_MAIN,   8);
  dev_space_setup(dev, Z80_SPACE_IO, 8);
  dev_space_set_callbacks(dev, SPACE_MAIN,   cpu_z80_read,    cpu_z80_write   );
  dev_space_set_callbacks(dev, Z80_SPACE_IO, cpu_z80_io_read, cpu_z80_io_write);

  dev_port_setup(dev, Z80_PORT_BUSACK, 1,  PORT_OUTPUT);
  dev_port_setup(dev, Z80_PORT_BUSREQ, 1,  PORT_INPUT);
  dev_port_setup(dev, Z80_PORT_HALT,   1,  PORT_OUTPUT);
  dev_port_setup(dev, Z80_PORT_INT,    1,  PORT_INPUT);
  dev_port_setup(dev, Z80_PORT_M1,     1,  PORT_OUTPUT);
  dev_port_setup(dev, Z80_PORT_MREQ,   1,  PORT_OUTPUT);
  dev_port_setup(dev, Z80_PORT_NMI,    1,  PORT_INPUT);
  dev_port_setup(dev, Z80_PORT_RESET,  1,  PORT_INPUT);
  dev_port_setup(dev, Z80_PORT_RFSH,   1,  PORT_OUTPUT);
  dev_port_setup(dev, Z80_PORT_WAIT,   1,  PORT_INPUT);
  dev_port_setup(dev, Z80_PORT_DBUS,   8,  PORT_INPUT);
  dev_port_setup(dev, Z80_PORT_ABUS,   16, PORT_OUTPUT);

  dev->data = cpu;
  cpu->dev = dev;

  _z80_emu_init(dev);

  dev_reset(dev);

  vm_state_add_uint8(vm, &cpu->ir, name, "regs", "ir", NULL);
  vm_state_add_uint16(vm, &cpu->prvpc.v, name, "regs", "prvpc", NULL);
  vm_state_add_uint16(vm, &cpu->pc.v, name, "regs", "pc", NULL);
  vm_state_add_uint16(vm, &cpu->sp.v, name, "regs", "sp", NULL);
  vm_state_add_uint16(vm, &cpu->af.v, name, "regs", "af", NULL);
  vm_state_add_uint16(vm, &cpu->bc.v, name, "regs", "bc", NULL);
  vm_state_add_uint16(vm, &cpu->de.v, name, "regs", "de", NULL);
  vm_state_add_uint16(vm, &cpu->hl.v, name, "regs", "hl", NULL);
  vm_state_add_uint16(vm, &cpu->ix.v, name, "regs", "ix", NULL);
  vm_state_add_uint16(vm, &cpu->iy.v, name, "regs", "iy", NULL);
  vm_state_add_uint16(vm, &cpu->ix.v, name, "regs", "wz", NULL);
  vm_state_add_uint16(vm, &cpu->af2.v, name, "regs", "af2", NULL);
  vm_state_add_uint16(vm, &cpu->bc2.v, name, "regs", "bc2", NULL);
  vm_state_add_uint16(vm, &cpu->de2.v, name, "regs", "de2", NULL);
  vm_state_add_uint16(vm, &cpu->hl2.v, name, "regs", "hl2", NULL);
  vm_state_add_uint8(vm, &cpu->r, name, "regs", "r", NULL);
  vm_state_add_uint8(vm, &cpu->r2, name, "regs", "r2", NULL);
  vm_state_add_uint8(vm, &cpu->i, name, "regs", "i", NULL);
  vm_state_add_int(vm, &cpu->iff1, name, "irq", "iff1", NULL);
  vm_state_add_int(vm, &cpu->iff2, name, "irq", "iff2", NULL);
  vm_state_add_int(vm, &cpu->im, name, "irq", "im", NULL);
  vm_state_add_int(vm, &cpu->rst_last, name, "irq", "rst_last", NULL);
  vm_state_add_int(vm, &cpu->nmi_last, name, "irq", "nmi_last", NULL);
  vm_state_add_int(vm, &cpu->after_ei, name, "irq", "after_ei", NULL);
  vm_state_add_int(vm, &cpu->after_ldair, name, "irq", "after_ldair", NULL);
  vm_state_add_uint8(vm, &cpu->rtemp, name, "rtemp", NULL);
  vm_state_add_uint16(vm, &cpu->ea, name, "ea", NULL);
  vm_state_add_int(vm, &cpu->cyc_use, name, "cyc_use", NULL);

  dev->prof = cpuprof_new(vm, dev, 3);

  return dev;
}

hien_dev_t* cpu_z80_new(hien_vm_t* vm, char* name, hz_t clock)
{
  hien_dev_t* dev;
  cpu_z80_t* cpu;

  dev = cpu_z80base_new(vm, name, clock);
  cpu = dev->data;

  cpu->z80model = Z80_MODEL_NMOS;

  return dev;
}

hien_dev_t* cpu_z80c_new(hien_vm_t* vm, char* name, hz_t clock)
{
  hien_dev_t* dev;
  cpu_z80_t* cpu;

  dev = cpu_z80base_new(vm, name, clock);
  cpu = dev->data;

  cpu->z80model = Z80_MODEL_CMOS;

  return dev;
}


hien_dev_info_t cpu_z80_def = {
  .type = "Z80",
  .ctor = "cpu_z80_new",
  .use_clk = TRUE,
  .has_save = 1,
};

hien_dev_info_t cpu_z80c_def = {
  .type = "Z80C",
  .ctor = "cpu_z80c_new",
  .use_clk = TRUE,
  .has_save = 1,
};
