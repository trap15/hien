/**********************************************************
*
* cpu/z80/z80_ca.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

/* Unfinished cycle-accurate Z80 core */

#include "hien.h"

typedef struct cpu_z80_t cpu_z80_t;

#if ENDIAN_BE
#define Z80REG8DEF(h,l) \
  z80##h, z80##l
#elif ENDIAN_LE
#define Z80REG8DEF(h,l) \
  z80##l, z80##h
#endif

enum z80reg8 {
  Z80REG8DEF(A,  F),
  Z80REG8DEF(B,  C),
  Z80REG8DEF(D,  E),
  Z80REG8DEF(H,  L),
  Z80REG8DEF(IXh,IXl),
  Z80REG8DEF(IYh,IYl),
  Z80REG8DEF(SPh,SPl),
  Z80REG8DEF(W,  Z),
  Z80REG8DEF(A2, F2),
  Z80REG8DEF(B2, C2),
  Z80REG8DEF(D2, E2),
  Z80REG8DEF(H2, L2),
  Z80REG8DEF(TMP, ACT),
  z80MAX8,
};

enum z80reg16 {
  z80AF,
  z80BC,
  z80DE,
  z80HL,
  z80IX,
  z80IY,
  z80SP,
  z80WZ,
  z80AF2,
  z80BC2,
  z80DE2,
  z80HL2,
  z80TMPACT,
  z80MAX16,
};

struct cpu_z80_t {
  union {
    uint16_t r16[z80MAX16];
    uint8_t  r8[z80MAX8];
  };
  uint8_t i, r;
  int iff1, iff2;
  int im;
  uint16_t pc;

  /* State */
  int z80model;

  uint8_t ir; /* Instruction Register */

  int xy;
  int cb;

  int cyc;
  int cyc2; /* Fetch cycle progress */

  int next_m[8]; /* Next M (for use with 'global' M cycle things) */
  int tgt_reg;

  /* Decoded instruction */
  int op_x, op_y, op_z, op_p, op_q;
};

#define z80FC 0x01
#define z80FN 0x02
#define z80FV 0x04
#define z80FX 0x08
#define z80FH 0x10
#define z80FY 0x20
#define z80FZ 0x40
#define z80FS 0x80

#define Z80CYC(m,t) (((m) << 4) | (t))

#define Z80M_RHL 0x10
#define Z80M_WHL 0x11
#define Z80M_ALU 0x12
#define Z80M_OD  0x13
#define Z80M_ODL 0x14
#define Z80M_ODH 0x15

#define Z80_ADVM(c) \
  (c)->cyc = Z80CYC((c)->next_m[0],1); \
  (c)->next_m[0] = (c)->next_m[1]; \
  (c)->next_m[1] = (c)->next_m[2]; \
  (c)->next_m[2] = (c)->next_m[3]; \
  (c)->next_m[3] = (c)->next_m[4]; \
  (c)->next_m[4] = (c)->next_m[5]; \
  (c)->next_m[5] = (c)->next_m[6]; \
  (c)->next_m[6] = (c)->next_m[7]; \
  (c)->next_m[7] = 1; \

static int _z80regtbl_r[8] = {
  z80B, z80C, z80D, z80E, z80H, z80L, -1, z80A,
};

static int _z80regtbl_rp[4] = {
  z80BC, z80DE, z80HL, z80SP,
};

static int _z80regtbl_rp2[4] = {
  z80BC, z80DE, z80HL, z80AF,
};

/* pre-calculated flags for 8bit ALU ops: [x][y][z]
 * [x] is carry (0 for non-carry ops)
 * [y] is src
 * [z] is result
 *
 * Bitwise just takes the result
 */
static int _z80_flag_add[2][256][256];
static int _z80_flag_sub[2][256][256];
static int _z80_flag_btw[256];

static void cpu_z80_update(hien_dev_t* dev)
{
  cpu_z80_t* cpu;
  cpu = dev->data;

  if(cpu->cyc == Z80CYC(1,1)) { /* Handle all M1,T1 pre-stuff */
    /* Should sample NMI, and INT here */
    if(dev_is_asserted(dev, Z80_PORT_HALT)) {
      /* Execute a NOP */
      cpu->ir = 0x00;
      cpu->cyc = Z80CYC(1,3);
    }
  }

/* cyc2 is a hack of sorts to properly handle the 'pipelining'
 * We do this because the Z80 can start a second instruction at the end
 *   of an instruction to save some time.
 * This should be a semi-proper way to do it:
 *   Instead of duplicating code, cyc2 is used for M1,T1 and M1,T2.
 *   M1,T3 in cyc2 will then set cyc to M1,T3 and properly do everything
 *
 * The way it's set up even lets you ignore cyc2 outside of starting
 *   the 'pipeline'.
 */
  switch(cpu->cyc2) {
    case -1: /* Waiting for the main exec to finish */
      if(   (cpu->cyc != Z80CYC(1,1)) /* Jump-off point */
         && (cpu->cyc != Z80CYC(Z80M_ALU,1)) /* ALU can always be piped */
        )
        break;
      /* M1,T1 time bitches! */
    case Z80CYC(1,1): /* M1,T1 */
      dev_assert(dev, Z80_PORT_MREQ);
      dev_assert(dev, Z80_PORT_M1);
      cpu->cyc2 = Z80CYC(1,2);
      break;
    case Z80CYC(1,2): /* M1,T2 */
      cpu->ir = dev_read(dev, 0, cpu->pc, 0xFF);
      if(!dev_is_asserted(dev, Z80_PORT_WAIT)) {
        cpu->cyc2 = Z80CYC(1,3);
        cpu->pc++;
      }
      break;
    case Z80CYC(1,3): /* M1,T3 */
      /* load it into cyc */
      cpu->cyc = Z80CYC(1,3);
      cpu->cyc2 = -1;
      break;
  }


  switch(cpu->cyc) {
    case Z80CYC(1,3): /* M1,T3 */
      dev_deassert(dev, Z80_PORT_M1);
      dev->port[Z80_PORT_ABUS].val = cpu->r;
      dev_assert(dev, Z80_PORT_RFSH);

      /* Decoding step */
      cpu->op_x = (cpu->ir >> 6) & 3;
      cpu->op_y = (cpu->ir >> 3) & 7;
      cpu->op_z = (cpu->ir >> 0) & 7;
      cpu->op_p = (cpu->ir >> 4) & 3;
      cpu->op_q = (cpu->ir >> 3) & 1;

      cpu->cyc = Z80CYC(1,4);
      break;
    case Z80CYC(1,4): /* M1,T4 */
      dev_deassert(dev, Z80_PORT_MREQ);
      switch(cpu->op_x) {
        case 01: /* LD r,r */
          if(cpu->op_y == 6 && cpu->op_z == 6) { /* HALT */
            dev_assert(dev, Z80_PORT_HALT);
            cpu->cyc = Z80CYC(1,1);
          }else if(cpu->op_z == 6) { /* SSS is (HL) */
            cpu->r8[z80ACT] = cpu->r8[z80A];
            cpu->cyc = Z80CYC(Z80M_RHL,1);
            cpu->tgt_reg = _z80regtbl_r[op_y];
          }else if(cpu->op_y == 6) { /* DDD is (HL) */
            cpu->r8[z80TMP] = cpu->r8[_z80regtbl_r[op_z]];
            cpu->cyc = Z80CYC(Z80M_WHL,1);
          }else{ /* normal registers */
            cpu->r8[z80TMP] = cpu->r8[_z80regtbl_r[op_z]];
            cpu->cyc = Z80CYC(1,5);
            cpu->cyc2 = Z80CYC(1,1);
          }
          break;
        case 02: /* ALU A,r */
          cpu->r8[z80ACT] = cpu->r8[z80A];
          if(cpu->op_z == 6) { /* SSS is (HL) */
            cpu->cyc = Z80CYC(Z80M_RHL,1);
            cpu->tgt_reg = z80TMP;
            cpu->next_m[0] = Z80M_ALU;
          }else{
            cpu->r8[z80TMP] = cpu->r8[_z80regtbl_r[op_z]];
            cpu->cyc = Z80CYC(Z80M_ALU,1);
          }
          break;
      }
      dev_deassert(dev, Z80_PORT_RFSH);
      break;
    case Z80CYC(1,5): /* M1,T5 */
      switch(cpu->op_x) {
        case 01: /* LD r,r */
          cpu->r8[_z80regtbl_r[op_z]] = cpu->r8[z80TMP];
          cpu->cyc = Z80CYC(1,1);
          break;
      }
      break;
    case Z80CYC(1,6): /* M1,T6 */
      break;

/* 'High-level' M-cycles */
    case Z80CYC(Z80M_ALU,1): /* Mx,T1 */
      cpu->cyc = Z80CYC(Z80M_ALU,2);
      break;
    case Z80CYC(Z80M_ALU,2): /* Mx,T2 */
      switch(cpu->op_y) {
        case 0: /* ADD */
          cpu->r8[z80A] = cpu->r8[z80ACT] + cpu->r8[z80TMP];
          cpu->r8[z80F] = _z80_flag_add[0][cpu->r8[z80ACT]][cpu->r8[z80A]];
          break;
        case 1: /* ADC */
          cpu->r8[z80A] = cpu->r8[z80ACT] + cpu->r8[z80TMP] + (cpu->r8[z80F]&z80FC);
          cpu->r8[z80F] = _z80_flag_add[cpu->r8[z80F]&z80FC][cpu->r8[z80ACT]][cpu->r8[z80A]];
          break;
        case 2: /* SUB */
          cpu->r8[z80A] = cpu->r8[z80ACT] - cpu->r8[z80TMP];
          cpu->r8[z80F] = _z80_flag_sub[0][cpu->r8[z80ACT]][cpu->r8[z80A]];
          break;
        case 3: /* SBC */
          cpu->r8[z80A] = cpu->r8[z80ACT] - cpu->r8[z80TMP] - (cpu->r8[z80F]&z80FC);
          cpu->r8[z80F] = _z80_flag_sub[cpu->r8[z80F]&z80FC][cpu->r8[z80ACT]][cpu->r8[z80A]];
          break;
        case 4: /* AND */
          cpu->r8[z80A] = cpu->r8[z80ACT] & cpu->r8[z80TMP];
          cpu->r8[z80F] = _z80_flag_btw[cpu->r8[z80A]] | z80FH;
          break;
        case 5: /* XOR */
          cpu->r8[z80A] = cpu->r8[z80ACT] ^ cpu->r8[z80TMP];
          cpu->r8[z80F] = _z80_flag_btw[cpu->r8[z80A]];
          break;
        case 6: /* OR */
          cpu->r8[z80A] = cpu->r8[z80ACT] | cpu->r8[z80TMP];
          cpu->r8[z80F] = _z80_flag_btw[cpu->r8[z80A]];
          break;
        case 7: /* CP */
          cpu->r8[z80TMP] = cpu->r8[z80ACT] - cpu->r8[z80TMP];
          cpu->r8[z80F] = _z80_flag_sub[0][cpu->r8[z80ACT]][cpu->r8[z80A]];
          break;
      }
      Z80_ADVM(cpu);
      break;

    case Z80CYC(Z80M_RHL,1): /* Mx,T1 */
      dev_assert(dev, Z80_PORT_MREQ);
      cpu->cyc = Z80CYC(Z80M_RHL,2);
      break;
    case Z80CYC(Z80M_RHL,2): /* Mx,T2 */
      cpu->r8[cpu->tgt_reg] = dev_read(dev, 0, cpu->r16[cpu->xy], 0xFF);
      if(!dev_is_asserted(dev, Z80_PORT_WAIT)) {
        cpu->cyc = Z80CYC(Z80M_RHL,3);
      }
      break;
    case Z80CYC(Z80M_RHL,3): /* Mx,T3 */
      dev_deassert(dev, Z80_PORT_MREQ);
      Z80_ADVM(cpu);
      break;

    case Z80CYC(Z80M_WHL,1): /* Mx,T1 */
      dev_assert(dev, Z80_PORT_MREQ);
      cpu->cyc = Z80CYC(Z80M_WHL,2);
      break;
    case Z80CYC(Z80M_WHL,2): /* Mx,T2 */
      dev_write(dev, 0, cpu->r16[cpu->xy], cpu->r8[z80TMP], 0xFF);
      if(!dev_is_asserted(dev, Z80_PORT_WAIT)) {
        cpu->cyc = Z80CYC(Z80M_WHL,3);
      }
      break;
    case Z80CYC(Z80M_WHL,3): /* Mx,T3 */
      dev_deassert(dev, Z80_PORT_MREQ);
      Z80_ADVM(cpu);
      break;

    case Z80CYC(Z80M_OD,1): /* Mx,T1 */
      dev_assert(dev, Z80_PORT_MREQ);
      cpu->cyc = Z80CYC(Z80M_OD,2);
      break;
    case Z80CYC(Z80M_OD,2): /* Mx,T2 */
      cpu->r8[cpu->tgt_reg] = dev_read(dev, 0, cpu->pc, 0xFF);
      if(!dev_is_asserted(dev, Z80_PORT_WAIT)) {
        cpu->pc++;
        cpu->cyc = Z80CYC(Z80M_OD,3);
      }
      break;
    case Z80CYC(Z80M_OD,3): /* Mx,T3 */
      dev_deassert(dev, Z80_PORT_MREQ);
      Z80_ADVM(cpu);
      break;

    case Z80CYC(Z80M_ODL,1): /* Mx,T1 */
      dev_assert(dev, Z80_PORT_MREQ);
      cpu->cyc = Z80CYC(Z80M_ODL,2);
      break;
    case Z80CYC(Z80M_ODL,2): /* Mx,T2 */
      cpu->r8[z80Z] = dev_read(dev, 0, cpu->pc, 0xFF);
      if(!dev_is_asserted(dev, Z80_PORT_WAIT)) {
        cpu->pc++;
        cpu->cyc = Z80CYC(Z80M_ODL,3);
      }
      break;
    case Z80CYC(Z80M_ODL,3): /* Mx,T3 */
      dev_deassert(dev, Z80_PORT_MREQ);
      Z80_ADVM(cpu);
      break;

    case Z80CYC(Z80M_ODH,1): /* Mx,T1 */
      dev_assert(dev, Z80_PORT_MREQ);
      cpu->cyc = Z80CYC(Z80M_ODL,2);
      break;
    case Z80CYC(Z80M_ODH,2): /* Mx,T2 */
      cpu->r8[z80W] = dev_read(dev, 0, cpu->pc, 0xFF);
      if(!dev_is_asserted(dev, Z80_PORT_WAIT)) {
        cpu->pc++;
        cpu->cyc = Z80CYC(Z80M_ODL,3);
      }
      break;
    case Z80CYC(Z80M_ODH,3): /* Mx,T3 */
      dev_deassert(dev, Z80_PORT_MREQ);
      Z80_ADVM(cpu);
      break;
  }
}

static void cpu_z80_delete(hien_dev_t* dev)
{
  cpu_z80_t* cpu;
  cpu = dev->data;

  mem_free(cpu);
}

static uintmax_t cpu_z80_read(hien_dev_t* dev, addr_t addr, uintmax_t mask)
{
  dev->port[Z80_PORT_ABUS].val = addr;
  return dev_read(dev, 0, addr, mask);
}

static void cpu_z80_write(hien_dev_t* dev, addr_t addr, uintmax_t data, uintmax_t mask)
{
  dev->port[Z80_PORT_ABUS].val = addr;
  dev_write(dev, 0, addr, data, mask);
}

static uintmax_t cpu_z80_io_read(hien_dev_t* dev, addr_t addr, uintmax_t mask)
{
  dev->port[Z80_PORT_ABUS].val = addr;
  return dev_read(dev, Z80_SPACE_IO, addr, mask);
}

static void cpu_z80_io_write(hien_dev_t* dev, addr_t addr, uintmax_t data, uintmax_t mask)
{
  dev->port[Z80_PORT_ABUS].val = addr;
  dev_write(dev, Z80_SPACE_IO, addr, data, mask);
}

static void cpu_z80_reset(hien_dev_t* dev)
{
  cpu_z80_t* cpu;
  cpu = dev->data;

  z80->pc = 0;
  z80->im = 0;
  z80->iff1 = FALSE;
  z80->iff2 = TRUE;
  z80->im = 0;
  z80->i = 0;
  z80->r = 0;

  z80->cyc = -1;
  z80->cyc2 = Z80CYC(1,1);
  z80->xy = z80HL;
  z80->cb = 0;

  for(i = 0; i < 8; i++) {
    z80->next_m[i] = 1;
  }
}

hien_dev_t* cpu_z80_new(hien_vm_t* vm, hz_t clock)
{
  uint8_t tmp;
  int n;
  int y,z;
  hien_dev_t* dev;
  cpu_z80_t* cpu;

  cpu = mem_alloc_type(cpu_z80_t);
  cpu->z80model = Z80_MODEL_NMOS;

  dev = dev_new(vm, clock);
  dev->update = cpu_z80_update;
  dev->delete = cpu_z80_delete;
  dev->reset  = cpu_z80_reset;

  dev->space[0].width = 8;
  dev->space[0].read  = cpu_z80_read;
  dev->space[0].write = cpu_z80_write;
  dev->space[Z80_SPACE_IO].width = 8;
  dev->space[Z80_SPACE_IO].read  = cpu_z80_io_read;
  dev->space[Z80_SPACE_IO].write = cpu_z80_io_write;

  dev->port[Z80_PORT_BUSACK].width = 1;
  dev->port[Z80_PORT_BUSACK].dir   = PORT_DIR_OUTPUT;
  dev->port[Z80_PORT_BUSREQ].width = 1;
  dev->port[Z80_PORT_BUSREQ].dir   = PORT_DIR_INPUT;
  dev->port[Z80_PORT_HALT  ].width = 1;
  dev->port[Z80_PORT_HALT  ].dir   = PORT_DIR_OUTPUT;
  dev->port[Z80_PORT_INT   ].width = 1;
  dev->port[Z80_PORT_INT   ].dir   = PORT_DIR_INPUT;
  dev->port[Z80_PORT_M1    ].width = 1;
  dev->port[Z80_PORT_M1    ].dir   = PORT_DIR_OUTPUT;
  dev->port[Z80_PORT_MREQ  ].width = 1;
  dev->port[Z80_PORT_MREQ  ].dir   = PORT_DIR_OUTPUT;
  dev->port[Z80_PORT_NMI   ].width = 1;
  dev->port[Z80_PORT_NMI   ].dir   = PORT_DIR_INPUT;
  dev->port[Z80_PORT_RESET ].width = 1;
  dev->port[Z80_PORT_RESET ].dir   = PORT_DIR_INPUT;
  dev->port[Z80_PORT_RFSH  ].width = 1;
  dev->port[Z80_PORT_RFSH  ].dir   = PORT_DIR_OUTPUT;
  dev->port[Z80_PORT_WAIT  ].width = 1;
  dev->port[Z80_PORT_WAIT  ].dir   = PORT_DIR_INPUT;
  dev->port[Z80_PORT_DBUS  ].width = 8;
  dev->port[Z80_PORT_DBUS  ].dir   = PORT_DIR_INPUT;
  dev->port[Z80_PORT_ABUS  ].width = 16;
  dev->port[Z80_PORT_ABUS  ].dir   = PORT_DIR_OUTPUT;

  dev->data = cpu;

  for(y = 0; y < 256; y++) {
    for(z = 0; z < 256; z++) {
      /* Add */
      n = z - y;
      tmp = 0;
      if(z == 0) tmp |= z80FZ;
      if((z & 0xF) < (y & 0xF)) tmp |= z80FH;
      if(z < y) tmp |= z80FC;
      if((n^y^0x80) & (n^z) & 0x80) tmp |= z80FV;
      tmp |= z & (z80FY | z80FX | z80FS);
      _z80_flag_add[0][y][z] = tmp;
      /* Add w/ Carry */
      n = z - y - 1;
      tmp = 0;
      if(z == 0) tmp |= z80FZ;
      if((z & 0xF) < (y & 0xF)) tmp |= z80FH;
      if(z < y) tmp |= z80FC;
      if((n^y^0x80) & (n^z) & 0x80) tmp |= z80FV;
      tmp |= z & (z80FY | z80FX | z80FS);
      _z80_flag_add[1][y][z] = tmp;

      /* Sub */
      n = y - z;
      tmp = z80FN;
      if(z == 0) tmp |= z80FZ;
      if((z & 0xF) > (y & 0xF)) tmp |= z80FH;
      if(z > y) tmp |= z80FC;
      if((n^y) & (y^z) & 0x80) tmp |= z80FV;
      tmp |= z & (z80FY | z80FX | z80FS);
      _z80_flag_sub[0][y][z] = tmp;
      /* Sub w/ Carry */
      n = y - z - 1;
      tmp = z80FN;
      if(z == 0) tmp |= z80FZ;
      if((z & 0xF) > (y & 0xF)) tmp |= z80FH;
      if(z > y) tmp |= z80FC;
      if((n^y) & (y^z) & 0x80) tmp |= z80FV;
      tmp |= z & (z80FY | z80FX | z80FS);
      _z80_flag_sub[0][y][z] = tmp;
    }
  }

  /* Some bitwise stuff */
  for(z = 0; z < 256; z++) {
    tmp = 0;
    y = 0;
    for(n = 0; n < 8; n++) if(z & (1<<n)) y++;

    if(z == 0) tmp |= z80FZ;
    if(y & 1) tmp |= z80FV;
    tmp |= z & (z80FY | z80FX | z80FS);
    _z80_flag_btw[z] = tmp;
  }

  dev_reset(dev);

  return dev;
}

hien_dev_t* cpu_z80c_new(hien_vm_t* vm, hz_t clock)
{
  hien_dev_t* dev;
  cpu_z80_t* cpu;

  dev = cpu_z80_new(vm, clock);
  cpu = dev->data;

  cpu->z80model = Z80_MODEL_CMOS;

  return dev;
}
