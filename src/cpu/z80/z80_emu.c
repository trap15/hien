/**********************************************************
*
* cpu/z80/z80_emu.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"
#include "z80_internal.h"

/* TODO:
 * Finish instructions
 * WZ stuff
 * Undocumented XYCB instructions
 * Add profiler support
 */

static int _flag_init = FALSE;

static const uint8_t _z80_cyc_op[0x100] = {
   4,10, 7, 6, 4, 4, 7, 4, 4,11, 7, 6, 4, 4, 7, 4,
   8,10, 7, 6, 4, 4, 7, 4,12,11, 7, 6, 4, 4, 7, 4,
   7,10,16, 6, 4, 4, 7, 4, 7,11,16, 6, 4, 4, 7, 4,
   7,10,13, 6,11,11,10, 4, 7,11,13, 6, 4, 4, 7, 4,
   4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4,
   4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4,
   4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4,
   7, 7, 7, 7, 7, 7, 4, 7, 4, 4, 4, 4, 4, 4, 7, 4,
   4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4,
   4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4,
   4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4,
   4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4,
   5,10,10,10,10,11, 7,11, 5,10,10, 4,10,17, 7,11,
   5,10,10,11,10,11, 7,11, 5, 4,10,11,10, 4, 7,11,
   5,10,10,19,10,11, 7,11, 5, 4,10, 4,10, 4, 7,11,
   5,10,10, 4,10,11, 7,11, 5, 6,10, 4,10, 4, 7,11
};

static const uint8_t _z80_cyc_ed[0x100] = {
   4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
   4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
   4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
   4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
   8, 8,11,16, 4,10, 4, 5, 8, 8,11,16, 4,10, 4, 5,
   8, 8,11,16, 4,10, 4, 5, 8, 8,11,16, 4,10, 4, 5,
   8, 8,11,16, 4,10, 4,14, 8, 8,11,16, 4,10, 4,14,
   8, 8,11,16, 4,10, 4, 4, 8, 8,11,16, 4,10, 4, 4,
   4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
   4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
  12,12,12,12, 4, 4, 4, 4,12,12,12,12, 4, 4, 4, 4,
  12,12,12,12, 4, 4, 4, 4,12,12,12,12, 4, 4, 4, 4,
   4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
   4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
   4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
   4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4
};

static const uint8_t _z80_cyc_cb[0x100] = {
   4, 4, 4, 4, 4, 4,11, 4, 4, 4, 4, 4, 4, 4,11, 4,
   4, 4, 4, 4, 4, 4,11, 4, 4, 4, 4, 4, 4, 4,11, 4,
   4, 4, 4, 4, 4, 4,11, 4, 4, 4, 4, 4, 4, 4,11, 4,
   4, 4, 4, 4, 4, 4,11, 4, 4, 4, 4, 4, 4, 4,11, 4,
   4, 4, 4, 4, 4, 4, 8, 4, 4, 4, 4, 4, 4, 4, 8, 4,
   4, 4, 4, 4, 4, 4, 8, 4, 4, 4, 4, 4, 4, 4, 8, 4,
   4, 4, 4, 4, 4, 4, 8, 4, 4, 4, 4, 4, 4, 4, 8, 4,
   4, 4, 4, 4, 4, 4, 8, 4, 4, 4, 4, 4, 4, 4, 8, 4,
   4, 4, 4, 4, 4, 4,11, 4, 4, 4, 4, 4, 4, 4,11, 4,
   4, 4, 4, 4, 4, 4,11, 4, 4, 4, 4, 4, 4, 4,11, 4,
   4, 4, 4, 4, 4, 4,11, 4, 4, 4, 4, 4, 4, 4,11, 4,
   4, 4, 4, 4, 4, 4,11, 4, 4, 4, 4, 4, 4, 4,11, 4,
   4, 4, 4, 4, 4, 4,11, 4, 4, 4, 4, 4, 4, 4,11, 4,
   4, 4, 4, 4, 4, 4,11, 4, 4, 4, 4, 4, 4, 4,11, 4,
   4, 4, 4, 4, 4, 4,11, 4, 4, 4, 4, 4, 4, 4,11, 4,
   4, 4, 4, 4, 4, 4,11, 4, 4, 4, 4, 4, 4, 4,11, 4
};

static const uint8_t _z80_cyc_xycb[0x100] = {
  15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,
  15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,
  15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,
  15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,
  12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,
  12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,
  12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,
  12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,
  15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,
  15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,
  15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,
  15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,
  15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,
  15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,
  15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,
  15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15
};

static uint8_t _z80_emu_readop(cpu_z80_t* cpu)
{
  return dev_read(cpu->dev, SPACE_MAIN, Z80R_PC(cpu)++, 0xFF);
}
static uint16_t _z80_emu_readop16(cpu_z80_t* cpu)
{
  uint16_t v;
  v  = dev_read(cpu->dev, SPACE_MAIN, Z80R_PC(cpu)++, 0xFF);
  v |= dev_read(cpu->dev, SPACE_MAIN, Z80R_PC(cpu)++, 0xFF) << 8;
  return v;
}

static uint8_t _z80_emu_read8(cpu_z80_t* cpu, addr_t addr)
{
  return dev_read(cpu->dev, SPACE_MAIN, addr, 0xFF);
}
static uint16_t _z80_emu_read16(cpu_z80_t* cpu, addr_t addr)
{
  uint16_t v;
  v  = dev_read(cpu->dev, SPACE_MAIN, addr++, 0xFF);
  v |= dev_read(cpu->dev, SPACE_MAIN, addr++, 0xFF) << 8;
  return v;
}

static void _z80_emu_write8(cpu_z80_t* cpu, addr_t addr, uint8_t data)
{
  dev_write(cpu->dev, SPACE_MAIN, addr, data, 0xFF);
}
static void _z80_emu_write16(cpu_z80_t* cpu, addr_t addr, uint16_t data)
{
  dev_write(cpu->dev, SPACE_MAIN, addr++, (data >> 0) & 0xFF, 0xFF);
  dev_write(cpu->dev, SPACE_MAIN, addr++, (data >> 8) & 0xFF, 0xFF);
}

static uint8_t _z80_emu_pread(cpu_z80_t* cpu, addr_t addr)
{
  return dev_read(cpu->dev, Z80_SPACE_IO, addr, 0xFF);
}
static void _z80_emu_pwrite(cpu_z80_t* cpu, addr_t addr, uint8_t data)
{
  dev_write(cpu->dev, Z80_SPACE_IO, addr, data, 0xFF);
}

static uint8_t _z80_emu_regread_h(cpu_z80_t* cpu)
{
  if(cpu->insn_disp_used)
    return Z80R_H(cpu);
  switch(cpu->pfx & Z80PFX_XY) {
    default:
      return Z80R_H(cpu);
    case Z80PFX_IX:
      return Z80R_IXh(cpu);
    case Z80PFX_IY:
      return Z80R_IYh(cpu);
  }
}
static uint8_t _z80_emu_regread_l(cpu_z80_t* cpu)
{
  if(cpu->insn_disp_used)
    return Z80R_L(cpu);
  switch(cpu->pfx & Z80PFX_XY) {
    default:
      return Z80R_L(cpu);
    case Z80PFX_IX:
      return Z80R_IXl(cpu);
    case Z80PFX_IY:
      return Z80R_IYl(cpu);
  }
}
static uint8_t _z80_emu_regread_mhl(cpu_z80_t* cpu)
{
  int8_t disp;
  if(cpu->pfx & Z80PFX_XY) {
    disp = cpu->insn_disp;
  }
  switch(cpu->pfx & Z80PFX_XY) {
    default:
      return _z80_emu_read8(cpu, Z80R_HL(cpu));
    case Z80PFX_IX:
      return _z80_emu_read8(cpu, Z80R_IX(cpu) + disp);
    case Z80PFX_IY:
      return _z80_emu_read8(cpu, Z80R_IY(cpu) + disp);
  }
}
static uint16_t _z80_emu_regread_hl(cpu_z80_t* cpu)
{
  switch(cpu->pfx & Z80PFX_XY) {
    default:
      return Z80R_HL(cpu);
    case Z80PFX_IX:
      return Z80R_IX(cpu);
    case Z80PFX_IY:
      return Z80R_IY(cpu);
  }
}

static void _z80_emu_regwrite_h(cpu_z80_t* cpu, uint8_t data)
{
  if(cpu->insn_disp_used) {
    Z80R_H(cpu) = data;
    return;
  }
  switch(cpu->pfx & Z80PFX_XY) {
    default:
      Z80R_H(cpu) = data;
      break;
    case Z80PFX_IX:
      Z80R_IXh(cpu) = data;
      break;
    case Z80PFX_IY:
      Z80R_IYh(cpu) = data;
      break;
  }
}
static void _z80_emu_regwrite_l(cpu_z80_t* cpu, uint8_t data)
{
  if(cpu->insn_disp_used) {
    Z80R_L(cpu) = data;
    return;
  }
  switch(cpu->pfx & Z80PFX_XY) {
    default:
      Z80R_L(cpu) = data;
      break;
    case Z80PFX_IX:
      Z80R_IXl(cpu) = data;
      break;
    case Z80PFX_IY:
      Z80R_IYl(cpu) = data;
      break;
  }
}
static void _z80_emu_regwrite_mhl(cpu_z80_t* cpu, uint8_t data)
{
  int8_t disp;
  if(cpu->pfx & Z80PFX_XY) {
    disp = cpu->insn_disp;
  }
  switch(cpu->pfx & Z80PFX_XY) {
    default:
      _z80_emu_write8(cpu, Z80R_HL(cpu), data);
      break;
    case Z80PFX_IX:
      printf("IX(%04X)+%d=%04X <- %02X\n", Z80R_IX(cpu), disp, Z80R_IX(cpu) + disp, data);
      _z80_emu_write8(cpu, Z80R_IX(cpu) + disp, data);
      break;
    case Z80PFX_IY:
      printf("IY(%04X)+%d=%04X <- %02X\n", Z80R_IY(cpu), disp, Z80R_IY(cpu) + disp, data);
      _z80_emu_write8(cpu, Z80R_IY(cpu) + disp, data);
      break;
  }
}
static void _z80_emu_regwrite_hl(cpu_z80_t* cpu, uint16_t data)
{
  switch(cpu->pfx & Z80PFX_XY) {
    default:
      Z80R_HL(cpu) = data;
      break;
    case Z80PFX_IX:
      Z80R_IX(cpu) = data;
      break;
    case Z80PFX_IY:
      Z80R_IY(cpu) = data;
      break;
  }
}

static uint8_t _z80_emu_regread_r(cpu_z80_t* cpu, int val)
{
  switch(val) {
    case 0:
      return Z80R_B(cpu);
    case 1:
      return Z80R_C(cpu);
    case 2:
      return Z80R_D(cpu);
    case 3:
      return Z80R_E(cpu);
    case 4:
      return _z80_emu_regread_h(cpu);
    case 5:
      return _z80_emu_regread_l(cpu);
    case 6: /* (HL) */
      return _z80_emu_regread_mhl(cpu);
    case 7:
      return Z80R_A(cpu);
  }
  return -1;
}

static uint16_t _z80_emu_regread_rp(cpu_z80_t* cpu, int val)
{
  switch(val) {
    case 0:
      return Z80R_BC(cpu);
    case 1:
      return Z80R_DE(cpu);
    case 2:
      return _z80_emu_regread_hl(cpu);
    case 3:
      return Z80R_SP(cpu);
  }
  return -1;
}

static uint16_t _z80_emu_regread_rp2(cpu_z80_t* cpu, int val)
{
  switch(val) {
    case 0:
      return Z80R_BC(cpu);
    case 1:
      return Z80R_DE(cpu);
    case 2:
      return _z80_emu_regread_hl(cpu);
    case 3:
      return Z80R_AF(cpu);
  }
  return -1;
}

static void _z80_emu_regwrite_r(cpu_z80_t* cpu, int val, uint8_t data)
{
  switch(val) {
    case 0:
      Z80R_B(cpu) = data;
      break;
    case 1:
      Z80R_C(cpu) = data;
      break;
    case 2:
      Z80R_D(cpu) = data;
      break;
    case 3:
      Z80R_E(cpu) = data;
      break;
    case 4:
      _z80_emu_regwrite_h(cpu, data);
      break;
    case 5:
      _z80_emu_regwrite_l(cpu, data);
      break;
    case 6: /* (HL) */
      _z80_emu_regwrite_mhl(cpu, data);
      break;
    case 7:
      Z80R_A(cpu) = data;
      break;
  }
}

static void _z80_emu_regwrite_rp(cpu_z80_t* cpu, int val, uint16_t data)
{
  switch(val) {
    case 0:
      Z80R_BC(cpu) = data;
      break;
    case 1:
      Z80R_DE(cpu) = data;
      break;
    case 2:
      _z80_emu_regwrite_hl(cpu, data);
      break;
    case 3:
      Z80R_SP(cpu) = data;
      break;
  }
}

static void _z80_emu_regwrite_rp2(cpu_z80_t* cpu, int val, uint16_t data)
{
  switch(val) {
    case 0:
      Z80R_BC(cpu) = data;
      break;
    case 1:
      Z80R_DE(cpu) = data;
      break;
    case 2:
      _z80_emu_regwrite_hl(cpu, data);
      break;
    case 3:
      Z80R_AF(cpu) = data;
      break;
  }
}

static int _z80_emu_parity(uint8_t v)
{
  if(((v >> 0) ^ (v >> 1) ^
      (v >> 2) ^ (v >> 3) ^
      (v >> 4) ^ (v >> 5) ^
      (v >> 6) ^ (v >> 7)) & 1)
    return 0;
  else
    return 1;
}

static void _z80_emu_flags_parity(cpu_z80_t* cpu, uint8_t v)
{
  if(_z80_emu_parity(v))
    Z80R_F(cpu) |= Z80F_P;
  else
    Z80R_F(cpu) &= ~Z80F_P;
}
static void _z80_emu_flags_adc(cpu_z80_t* cpu, uint8_t l, uint8_t r, int c)
{
  uint8_t v = l + r + c;
  Z80R_F(cpu) = 0;
  if(v == 0) Z80R_F(cpu) |= Z80F_Z;
  Z80R_F(cpu) |= v & (Z80F_X|Z80F_Y|Z80F_S);

  if((l ^ r ^ 0x80) & (v ^ l) & 0x80) Z80R_F(cpu) |= Z80F_V;
  if(v < l + c) Z80R_F(cpu) |= Z80F_C;
  if((v & 0xF) < ((l & 0xF) + c)) Z80R_F(cpu) |= Z80F_H;
}
static void _z80_emu_flags_sbc(cpu_z80_t* cpu, uint8_t l, uint8_t r, int c)
{
  uint8_t v = l - r - c;
  Z80R_F(cpu) = 0;
  if(v == 0) Z80R_F(cpu) |= Z80F_Z;
  Z80R_F(cpu) |= v & (Z80F_X|Z80F_Y|Z80F_S);

  if((l ^ r) & (v ^ l) & 0x80) Z80R_F(cpu) |= Z80F_V;
  if(v > l - c) Z80R_F(cpu) |= Z80F_C;
  if((l & 0xF) < ((r & 0xF) + c)) Z80R_F(cpu) |= Z80F_H;
}
static void _z80_emu_flags_cp(cpu_z80_t* cpu, uint8_t l, uint8_t r, int c)
{
  _z80_emu_flags_sbc(cpu, l, r, c);
  Z80R_F(cpu) &= ~(Z80F_X|Z80F_Y);
  Z80R_F(cpu) |= r & (Z80F_X|Z80F_Y);
}
static void _z80_emu_flags_szp(cpu_z80_t* cpu, uint8_t v)
{
  Z80R_F(cpu) = 0;
  if(v == 0) Z80R_F(cpu) |= Z80F_Z;
  Z80R_F(cpu) |= v & (Z80F_X|Z80F_Y|Z80F_S);
  _z80_emu_flags_parity(cpu, v);
}
static void _z80_emu_flags_shift(cpu_z80_t* cpu, uint8_t v, int co)
{
  _z80_emu_flags_szp(cpu, v);
  Z80R_F(cpu) |= co;
}
static void _z80_emu_flags_inc(cpu_z80_t* cpu, uint8_t v)
{
  Z80R_F(cpu) &= ~(Z80F_N|Z80F_V|Z80F_X|Z80F_H|Z80F_Y|Z80F_Z|Z80F_S);
  if(v == 0) Z80R_F(cpu) |= Z80F_Z;
  if((v & 0xF) == 0) Z80R_F(cpu) |= Z80F_H;
  if(v == 0x80) Z80R_F(cpu) |= Z80F_V;
  Z80R_F(cpu) |= v & (Z80F_X|Z80F_Y|Z80F_S);
}
static void _z80_emu_flags_dec(cpu_z80_t* cpu, uint8_t v)
{
  Z80R_F(cpu) &= ~(Z80F_V|Z80F_X|Z80F_H|Z80F_Y|Z80F_Z|Z80F_S);
  Z80R_F(cpu) |= Z80F_N;
  if(v == 0) Z80R_F(cpu) |= Z80F_Z;
  if((v & 0xF) == 0xF) Z80R_F(cpu) |= Z80F_H;
  if(v == 0x7F) Z80R_F(cpu) |= Z80F_V;
  Z80R_F(cpu) |= v & (Z80F_X|Z80F_Y|Z80F_S);
}
static void _z80_emu_flags_add16(cpu_z80_t* cpu, uint16_t l, uint16_t r)
{
  uint16_t v = l + r;
  Z80R_F(cpu) &= ~(Z80F_C|Z80F_N|Z80F_X|Z80F_H|Z80F_Y);
  if((l & 0xF00) + (r & 0xF00) >= 0x1000) Z80R_F(cpu) |= Z80F_H;
  if(v < l) Z80R_F(cpu) |= Z80F_C;
  Z80R_F(cpu) |= (v >> 8) & (Z80F_X|Z80F_Y);
}
static void _z80_emu_flags_adc16(cpu_z80_t* cpu, uint16_t l, uint16_t r, int c)
{
  _z80_emu_flags_adc(cpu, (l >> 0) & 0xFF, (r >> 0) & 0xFF, c);
  _z80_emu_flags_adc(cpu, (l >> 8) & 0xFF, (r >> 8) & 0xFF, Z80R_F(cpu) & Z80F_C);
}
static void _z80_emu_flags_sbc16(cpu_z80_t* cpu, uint16_t l, uint16_t r, int c)
{
  _z80_emu_flags_sbc(cpu, (l >> 0) & 0xFF, (r >> 0) & 0xFF, c);
  _z80_emu_flags_sbc(cpu, (l >> 8) & 0xFF, (r >> 8) & 0xFF, Z80R_F(cpu) & Z80F_C);
}

static void _z80_emu_xyoff(cpu_z80_t* cpu)
{
  int8_t disp;
  if(cpu->pfx & Z80PFX_XY)
    disp = _z80_emu_readop(cpu);

  switch(cpu->pfx & Z80PFX_XY) {
    case Z80PFX_HL:
      Z80R_WZ(cpu) = Z80R_HL(cpu);
      break;
    case Z80PFX_IX:
      Z80R_WZ(cpu) = Z80R_IX(cpu) + disp;
      break;
    case Z80PFX_IY:
      Z80R_WZ(cpu) = Z80R_IY(cpu) + disp;
      break;
  }
}

static void _z80_emu_push(cpu_z80_t* cpu, uint16_t val)
{
  Z80R_SP(cpu) -= 2;
  _z80_emu_write16(cpu, Z80R_SP(cpu), val);
}
static uint16_t _z80_emu_pop(cpu_z80_t* cpu)
{
  uint16_t val;
  val = _z80_emu_read16(cpu, Z80R_SP(cpu));
  Z80R_SP(cpu) += 2;
  return val;
}

static int _z80_emu_cond(cpu_z80_t* cpu, int cc)
{
  int ret = !(cc & 1);
  int flg;
  switch(cc >> 1) {
    case 0: flg = Z80F_Z; break; /* NZ/Z */
    case 1: flg = Z80F_C; break; /* NC/C */
    case 2: flg = Z80F_P; break; /* PO/PE */
    case 3: flg = Z80F_S; break; /* P/M */
  }
  if(Z80R_F(cpu) & flg)
    ret ^= 1;

  return ret;
}

static void _z80_emu_cycle_op(cpu_z80_t* cpu)
{
  uint16_t tmp16, tmp16_2;
  uint8_t tmp8, tmp8_2;
  int8_t tmps8;
  int disp_ok = FALSE;

  int x,y,z,p,q;
  x = (cpu->ir >> 6) & 3;
  y = (cpu->ir >> 3) & 7;
  z = (cpu->ir >> 0) & 7;
  p = (cpu->ir >> 4) & 3;
  q = (cpu->ir >> 3) & 1;
  switch(x) {
    case 0: /* X=0 */
      switch(z) {
        case 0:
          switch(y) {
            case 0: /* NOP */
              break;
            case 1: /* EX AF,AF' */
              tmp16 = Z80R_AF(cpu);
              Z80R_AF(cpu) = Z80R_AF2(cpu);
              Z80R_AF2(cpu) = tmp16;
              break;
            case 2: /* DJNZ d */
              tmps8 = _z80_emu_readop(cpu);

              Z80R_B(cpu)--;
              if(Z80R_B(cpu)) {
                Z80R_WZ(cpu) = Z80R_PC(cpu) + tmps8;
                Z80R_PC(cpu) = Z80R_WZ(cpu);
                cpu->cyc_use += 5;
              }
              break;
            case 3: /* JR d */
              tmps8 = _z80_emu_readop(cpu);
              Z80R_WZ(cpu) = Z80R_PC(cpu) + tmps8;
              Z80R_PC(cpu) = Z80R_WZ(cpu);
              break;
            default: /* JR ccY{0..1}, d */
              tmps8 = _z80_emu_readop(cpu);

              if(_z80_emu_cond(cpu, y & 3)) {
                Z80R_WZ(cpu) = Z80R_PC(cpu) + tmps8;
                Z80R_PC(cpu) = Z80R_WZ(cpu);
                cpu->cyc_use += 5;
              }
              break;
          }
          break;
        case 1:
          switch(q) {
            case 0: /* LD rpP, nn */
              tmp16 = _z80_emu_readop16(cpu);
              _z80_emu_regwrite_rp(cpu, p, tmp16);
              break;
            case 1: /* ADD HL, rpP */
              tmp16 = _z80_emu_regread_hl(cpu);
              tmp16_2 = _z80_emu_regread_rp(cpu, p);

              _z80_emu_flags_add16(cpu, tmp16, tmp16_2);

              _z80_emu_regwrite_hl(cpu, tmp16+tmp16_2);
              break;
          }
          break;
        case 2:
          switch(q) {
            case 0:
              switch(p) {
                case 0: /* LD (BC),A */
                  _z80_emu_write8(cpu, Z80R_BC(cpu), Z80R_A(cpu));
                  break;
                case 1: /* LD (DE),A */
                  _z80_emu_write8(cpu, Z80R_DE(cpu), Z80R_A(cpu));
                  break;
                case 2: /* LD (nn),HL */
                  tmp16 = _z80_emu_readop16(cpu);
                  _z80_emu_write16(cpu, tmp16, _z80_emu_regread_hl(cpu));
                  break;
                case 3: /* LD (nn),A */
                  tmp16 = _z80_emu_readop16(cpu);
                  _z80_emu_write8(cpu, tmp16, Z80R_A(cpu));
                  break;
              }
              break;
            case 1:
              switch(p) {
                case 0: /* LD A,(BC) */
                  Z80R_A(cpu) = _z80_emu_read8(cpu, Z80R_BC(cpu));
                  break;
                case 1: /* LD A,(DE) */
                  Z80R_A(cpu) = _z80_emu_read8(cpu, Z80R_DE(cpu));
                  break;
                case 2: /* LD HL,(nn) */
                  tmp16 = _z80_emu_readop16(cpu);
                  _z80_emu_regwrite_hl(cpu, _z80_emu_read16(cpu, tmp16));
                  break;
                case 3: /* LD A,(nn) */
                  tmp16 = _z80_emu_readop16(cpu);
                  Z80R_A(cpu) = _z80_emu_read8(cpu, tmp16);
                  break;
              }
              break;
          }
          break;
        case 3: /* INC/DEC rpP */
          switch(q) {
            case 0: /* INC rpP */
              _z80_emu_regwrite_rp(cpu, p, _z80_emu_regread_rp(cpu, p)+1);
              break;
            case 1: /* DEC rpP */
              _z80_emu_regwrite_rp(cpu, p, _z80_emu_regread_rp(cpu, p)-1);
              break;
          }
          break;
        case 4: /* INC rY */
          tmp8 = _z80_emu_regread_r(cpu, y);
          tmp8++;
          _z80_emu_flags_inc(cpu, tmp8);
          _z80_emu_regwrite_r(cpu, y, tmp8);
          break;
        case 5: /* DEC rY */
          tmp8 = _z80_emu_regread_r(cpu, y);
          tmp8--;
          _z80_emu_flags_dec(cpu, tmp8);
          _z80_emu_regwrite_r(cpu, y, tmp8);
          break;
        case 6: /* LD rY, n */
          tmp8 = _z80_emu_readop(cpu);
          _z80_emu_regwrite_r(cpu, y, tmp8);
          break;
        case 7:
          switch(y) {
            case 0: /* RLCA */
              Z80R_F(cpu) &= ~(Z80F_C|Z80F_N|Z80F_X|Z80F_H|Z80F_Y);
              Z80R_F(cpu) |= Z80R_A(cpu) >> 7;
              Z80R_A(cpu) = (Z80R_A(cpu) << 1) | (Z80R_A(cpu) >> 7);
              Z80R_F(cpu) |= Z80R_A(cpu) & (Z80F_X|Z80F_Y);
              break;
            case 1: /* RRCA */
              Z80R_F(cpu) &= ~(Z80F_C|Z80F_N|Z80F_X|Z80F_H|Z80F_Y);
              Z80R_F(cpu) |= Z80R_A(cpu) & 1;
              Z80R_A(cpu) = (Z80R_A(cpu) >> 1) | (Z80R_A(cpu) << 7);
              Z80R_F(cpu) |= Z80R_A(cpu) & (Z80F_X|Z80F_Y);
              break;
            case 2: /* RLA */
              tmp8 = Z80R_F(cpu) & Z80F_C;
              Z80R_F(cpu) &= ~(Z80F_C|Z80F_N|Z80F_X|Z80F_H|Z80F_Y);
              Z80R_F(cpu) |= Z80R_A(cpu) >> 7;
              Z80R_A(cpu) = (Z80R_A(cpu) << 1) | tmp8;
              Z80R_F(cpu) |= Z80R_A(cpu) & (Z80F_X|Z80F_Y);
              break;
            case 3: /* RRA */
              tmp8 = Z80R_F(cpu) & Z80F_C;
              Z80R_F(cpu) &= ~(Z80F_C|Z80F_N|Z80F_X|Z80F_H|Z80F_Y);
              Z80R_F(cpu) |= Z80R_A(cpu) & 1;
              Z80R_A(cpu) = (Z80R_A(cpu) >> 1) | (tmp8 << 7);
              Z80R_F(cpu) |= Z80R_A(cpu) & (Z80F_X|Z80F_Y);
              break;
            case 4: /* DAA */
              /* TODO */
              printf("DAA!\n");
              break;
            case 5: /* CPL */
              Z80R_A(cpu) = ~Z80R_A(cpu);

              Z80R_F(cpu) &= ~(Z80F_X|Z80F_Y);
              Z80R_F(cpu) |= Z80F_N|Z80F_H;
              Z80R_F(cpu) |= Z80R_A(cpu) & (Z80F_X|Z80F_Y);
              break;
            case 6: /* SCF */
              Z80R_F(cpu) &= ~(Z80F_N|Z80F_H|Z80F_X|Z80F_Y);
              Z80R_F(cpu) |= Z80F_C;
              Z80R_F(cpu) |= Z80R_A(cpu) & (Z80F_X|Z80F_Y);
              break;
            case 7: /* CCF */
              Z80R_F(cpu) &= ~(Z80F_N|Z80F_H|Z80F_X|Z80F_Y);
              if(Z80R_F(cpu) & Z80F_C)
                Z80R_F(cpu) |= Z80F_H;
              Z80R_F(cpu) ^= Z80F_C;
              Z80R_F(cpu) |= Z80R_A(cpu) & (Z80F_X|Z80F_Y);
              break;
          }
          break;
      }
      break;
    case 1: /* X=1 */
      if((z == 6) && (y == 6)) { /* HALT */
        dev_port_assert(cpu->dev, Z80_PORT_HALT);
      }else{ /* LD rY,rZ */
        _z80_emu_regwrite_r(cpu, y, _z80_emu_regread_r(cpu, z));
      }
      break;
    case 2: /* X=2, aluY rZ */
      switch(y) {
        case 0: /* ADD A,rZ */
          tmp8 = _z80_emu_regread_r(cpu, z);
          _z80_emu_flags_adc(cpu, Z80R_A(cpu), tmp8, 0);
          Z80R_A(cpu) += tmp8;
          break;
        case 1: /* ADC A,rZ */
          tmp8 = _z80_emu_regread_r(cpu, z);
          tmp8_2 = Z80R_F(cpu) & Z80F_C;
          _z80_emu_flags_adc(cpu, Z80R_A(cpu), tmp8, tmp8_2);
          Z80R_A(cpu) += tmp8 + tmp8_2;
          break;
        case 2: /* SUB rZ */
          tmp8 = _z80_emu_regread_r(cpu, z);
          _z80_emu_flags_sbc(cpu, Z80R_A(cpu), tmp8, 0);
          Z80R_A(cpu) -= tmp8;
          break;
        case 3: /* SBC A,rZ */
          tmp8 = _z80_emu_regread_r(cpu, z);
          tmp8_2 = Z80R_F(cpu) & Z80F_C;
          _z80_emu_flags_sbc(cpu, Z80R_A(cpu), tmp8, tmp8_2);
          Z80R_A(cpu) -= tmp8 + tmp8_2;
          break;
        case 4: /* AND rZ */
          tmp8 = _z80_emu_regread_r(cpu, z);
          Z80R_F(cpu) |= Z80F_H;
          Z80R_A(cpu) &= tmp8;
          _z80_emu_flags_szp(cpu, Z80R_A(cpu));
          break;
        case 5: /* XOR rZ */
          tmp8 = _z80_emu_regread_r(cpu, z);
          Z80R_F(cpu) &= ~Z80F_H;
          Z80R_A(cpu) ^= tmp8;
          _z80_emu_flags_szp(cpu, Z80R_A(cpu));
          break;
        case 6: /* OR rZ */
          tmp8 = _z80_emu_regread_r(cpu, z);
          Z80R_F(cpu) &= ~Z80F_H;
          Z80R_A(cpu) |= tmp8;
          _z80_emu_flags_szp(cpu, Z80R_A(cpu));
          break;
        case 7: /* CP rZ */
          tmp8 = _z80_emu_regread_r(cpu, z);
          _z80_emu_flags_cp(cpu, Z80R_A(cpu), tmp8, 0);
          break;
      }
      break;
    case 3: /* X=3 */
      switch(z) {
        case 0: /* RET ccY */
          if(_z80_emu_cond(cpu, y)) {
            Z80R_WZ(cpu) = _z80_emu_pop(cpu);
            Z80R_PC(cpu) = Z80R_WZ(cpu);
          }
          break;
        case 1:
          switch(q) {
            case 0: /* POP rp2P */
              _z80_emu_regwrite_rp2(cpu, p, _z80_emu_read16(cpu, Z80R_SP(cpu)));
              Z80R_SP(cpu) += 2;
              break;
            case 1:
              switch(p) {
                case 0: /* RET */
                  Z80R_WZ(cpu) = _z80_emu_pop(cpu);
                  Z80R_PC(cpu) = Z80R_WZ(cpu);
                  break;
                case 1: /* EXX */
                  tmp16 = Z80R_BC(cpu); Z80R_BC(cpu) = Z80R_BC2(cpu); Z80R_BC2(cpu) = tmp16;
                  tmp16 = Z80R_DE(cpu); Z80R_DE(cpu) = Z80R_DE2(cpu); Z80R_DE2(cpu) = tmp16;
                  tmp16 = Z80R_HL(cpu); Z80R_HL(cpu) = Z80R_HL2(cpu); Z80R_HL2(cpu) = tmp16;
                  break;
                case 2: /* JP (HL) */
                  Z80R_PC(cpu) = _z80_emu_regread_hl(cpu);
                  break;
                case 3: /* LD SP,HL */
                  Z80R_SP(cpu) = _z80_emu_regread_hl(cpu);
                  break;
              }
              break;
          }
          break;
        case 2: /* JP ccY, nn */
          Z80R_WZ(cpu) = _z80_emu_readop16(cpu);

          if(_z80_emu_cond(cpu, y)) {
            Z80R_PC(cpu) = Z80R_WZ(cpu);
          }
          break;
        case 3:
          switch(y) {
            case 0: /* JP nn */
              Z80R_WZ(cpu) = _z80_emu_readop16(cpu);
              Z80R_PC(cpu) = Z80R_WZ(cpu);
              break;
            case 1: /* CB */
              cpu->npfx = Z80PFX_CB;
              if(cpu->pfx & Z80PFX_XY) {
                cpu->insn_disp = _z80_emu_readop(cpu);
                disp_ok = TRUE;
              }
              break;
            case 2: /* OUT (n),A */
              tmp8 = _z80_emu_readop(cpu);
              _z80_emu_pwrite(cpu, tmp8 | (Z80R_A(cpu) << 8), Z80R_A(cpu));
              break;
            case 3: /* IN A,(n) */
              tmp8 = _z80_emu_readop(cpu);
              Z80R_A(cpu) = _z80_emu_pread(cpu, tmp8 | (Z80R_A(cpu) << 8));
              break;
            case 4: /* EX (SP),HL */
              tmp16 = _z80_emu_regread_hl(cpu);
              _z80_emu_regwrite_hl(cpu, _z80_emu_read16(cpu, Z80R_SP(cpu)));
              _z80_emu_write16(cpu, Z80R_SP(cpu), tmp16);
              break;
            case 5: /* EX DE,HL */
              tmp16 = Z80R_DE(cpu);
              Z80R_DE(cpu) = Z80R_HL(cpu);
              Z80R_HL(cpu) = tmp16;
              break;
            case 6: /* DI */
              cpu->iff1 = cpu->iff2 = FALSE;
              break;
            case 7: /* EI */
              cpu->iff1 = cpu->iff2 = TRUE;
              cpu->after_ei = TRUE;
              break;
          }
          break;
        case 4: /* CALL ccY, nn */
          Z80R_WZ(cpu) = _z80_emu_readop16(cpu);
          if(_z80_emu_cond(cpu, y)) {
            _z80_emu_push(cpu, Z80R_PC(cpu));
            Z80R_PC(cpu) = Z80R_WZ(cpu);
          }
          break;
        case 5:
          switch(q) {
            case 0: /* PUSH rp2P */
              Z80R_SP(cpu) -= 2;
              _z80_emu_write16(cpu, Z80R_SP(cpu), _z80_emu_regread_rp2(cpu, p));
              break;
            case 1:
              switch(p) {
                case 0: /* CALL nn */
                  Z80R_WZ(cpu) = _z80_emu_readop16(cpu);
                  _z80_emu_push(cpu, Z80R_PC(cpu));
                  Z80R_PC(cpu) = Z80R_WZ(cpu);
                  break;
                case 1: /* DD */
                  cpu->npfx = Z80PFX_IX;
                  break;
                case 2: /* ED */
                  cpu->npfx = Z80PFX_ED;
                  break;
                case 3: /* FD */
                  cpu->npfx = Z80PFX_IY;
                  break;
              }
              break;
          }
          break;
        case 6: /* aluY n */
          switch(y) {
            case 0: /* ADD A,n */
              tmp8 = _z80_emu_readop(cpu);
              _z80_emu_flags_adc(cpu, Z80R_A(cpu), tmp8, 0);
              Z80R_A(cpu) += tmp8;
              break;
            case 1: /* ADC A,n */
              tmp8 = _z80_emu_readop(cpu);
              tmp8_2 = Z80R_F(cpu) & Z80F_C;
              _z80_emu_flags_adc(cpu, Z80R_A(cpu), tmp8, tmp8_2);
              Z80R_A(cpu) += tmp8 + tmp8_2;
              break;
            case 2: /* SUB n */
              tmp8 = _z80_emu_readop(cpu);
              _z80_emu_flags_sbc(cpu, Z80R_A(cpu), tmp8, 0);
              Z80R_A(cpu) -= tmp8;
              break;
            case 3: /* SBC A,n */
              tmp8 = _z80_emu_readop(cpu);
              tmp8_2 = Z80R_F(cpu) & Z80F_C;
              _z80_emu_flags_sbc(cpu, Z80R_A(cpu), tmp8, tmp8_2);
              Z80R_A(cpu) -= tmp8 + tmp8_2;
              break;
            case 4: /* AND n */
              tmp8 = _z80_emu_readop(cpu);
              Z80R_F(cpu) |= Z80F_H;
              Z80R_A(cpu) &= tmp8;
              _z80_emu_flags_szp(cpu, Z80R_A(cpu));
              break;
            case 5: /* XOR n */
              tmp8 = _z80_emu_readop(cpu);
              Z80R_F(cpu) &= ~Z80F_H;
              Z80R_A(cpu) ^= tmp8;
              _z80_emu_flags_szp(cpu, Z80R_A(cpu));
              break;
            case 6: /* OR n */
              tmp8 = _z80_emu_readop(cpu);
              Z80R_F(cpu) &= ~Z80F_H;
              Z80R_A(cpu) |= tmp8;
              _z80_emu_flags_szp(cpu, Z80R_A(cpu));
              break;
            case 7: /* CP n */
              tmp8 = _z80_emu_readop(cpu);
              _z80_emu_flags_cp(cpu, Z80R_A(cpu), tmp8, 0);
              break;
          }
          break;
        case 7: /* RST y*8 */
          Z80R_WZ(cpu) = y << 3;
          _z80_emu_push(cpu, Z80R_PC(cpu));
          Z80R_PC(cpu) = Z80R_WZ(cpu);
          break;
      }
      break;
  }
}

/* TODO: Undocumented XYCB instructions */
static void _z80_emu_cycle_cb(cpu_z80_t* cpu)
{
  int x,y,z,p,q;
  uint8_t tmp8, co;
  x = (cpu->ir >> 6) & 3;
  y = (cpu->ir >> 3) & 7;
  z = (cpu->ir >> 0) & 7;
  p = (cpu->ir >> 4) & 3;
  q = (cpu->ir >> 3) & 1;
  switch(x) {
    case 0: /* rotY rZ */
      switch(y) {
        case 0: /* RLC rZ */
          tmp8 = _z80_emu_regread_r(cpu, z);
          co = tmp8 >> 7;
          tmp8 = (tmp8 << 1) | (co);
          _z80_emu_flags_shift(cpu, tmp8, co);
          _z80_emu_regwrite_r(cpu, z, tmp8);
          break;
        case 1: /* RRC rZ */
          tmp8 = _z80_emu_regread_r(cpu, z);
          co = tmp8 & 1;
          tmp8 = (tmp8 >> 1) | (co << 7);
          _z80_emu_flags_shift(cpu, tmp8, co);
          _z80_emu_regwrite_r(cpu, z, tmp8);
          break;
        case 2: /* RL rZ */
          tmp8 = _z80_emu_regread_r(cpu, z);
          co = tmp8 >> 7;
          tmp8 = (tmp8 << 1) | (Z80R_F(cpu) & Z80F_C);
          _z80_emu_flags_shift(cpu, tmp8, co);
          _z80_emu_regwrite_r(cpu, z, tmp8);
          break;
        case 3: /* RR rZ */
          tmp8 = _z80_emu_regread_r(cpu, z);
          co = tmp8 & 1;
          tmp8 = (tmp8 >> 1) | ((Z80R_F(cpu) & Z80F_C) << 7);
          _z80_emu_flags_shift(cpu, tmp8, co);
          _z80_emu_regwrite_r(cpu, z, tmp8);
          break;
        case 4: /* SLA rZ */
          tmp8 = _z80_emu_regread_r(cpu, z);
          co = tmp8 >> 7;
          tmp8 = (tmp8 << 1);
          _z80_emu_flags_shift(cpu, tmp8, co);
          _z80_emu_regwrite_r(cpu, z, tmp8);
          break;
        case 5: /* SRA rZ */
          tmp8 = _z80_emu_regread_r(cpu, z);
          co = tmp8 & 1;
          tmp8 = (tmp8 >> 1) | (tmp8 & 0x80);
          _z80_emu_flags_shift(cpu, tmp8, co);
          _z80_emu_regwrite_r(cpu, z, tmp8);
          break;
        case 6: /* SLL rZ */
          tmp8 = _z80_emu_regread_r(cpu, z);
          co = tmp8 >> 7;
          tmp8 = (tmp8 << 1) | 1;
          _z80_emu_flags_shift(cpu, tmp8, co);
          _z80_emu_regwrite_r(cpu, z, tmp8);
          break;
        case 7: /* SRL rZ */
          tmp8 = _z80_emu_regread_r(cpu, z);
          co = tmp8 & 1;
          tmp8 = (tmp8 >> 1);
          _z80_emu_flags_shift(cpu, tmp8, co);
          _z80_emu_regwrite_r(cpu, z, tmp8);
          break;
      }
      break;
    case 1: /* BIT y,rZ */
      tmp8 = _z80_emu_regread_r(cpu, z);
      Z80R_F(cpu) &= ~(Z80F_N|Z80F_P|Z80F_X|Z80F_Y|Z80F_Z|Z80F_S);
      Z80R_F(cpu) |= Z80F_H;
      if(!(tmp8 & (1 << y)))
        Z80R_F(cpu) |= Z80F_Z|Z80F_P;

      Z80R_F(cpu) |= tmp8 & Z80F_S & (1 << y);
      /* TODO: X/Y from WZh? if z=6 */
      if(z != 6)
        Z80R_F(cpu) |= tmp8 & (Z80F_X|Z80F_Y) & (1 << y);
      break;
    case 2: /* RES y,rZ */
      _z80_emu_regwrite_r(cpu, z, _z80_emu_regread_r(cpu, z) & ~(1 << y));
      break;
    case 3: /* SET y,rZ */
      _z80_emu_regwrite_r(cpu, z, _z80_emu_regread_r(cpu, z) | (1 << y));
      break;
  }
}

static void _z80_emu_cycle_ed(cpu_z80_t* cpu)
{
  uint16_t tmp16, tmp16_2, tmp16_3;
  uint8_t tmp8, tmp8_2;
  int x,y,z,p,q;
  x = (cpu->ir >> 6) & 3;
  y = (cpu->ir >> 3) & 7;
  z = (cpu->ir >> 0) & 7;
  p = (cpu->ir >> 4) & 3;
  q = (cpu->ir >> 3) & 1;
  switch(x) {
    case 1:
      switch(z) {
        case 0:
          tmp8 = _z80_emu_pread(cpu, Z80R_BC(cpu));
          if(y != 6) { /* IN rY,(C) */
            _z80_emu_regwrite_r(cpu, y, tmp8);
          }
          Z80R_F(cpu) &= ~(Z80F_N|Z80F_P|Z80F_X|Z80F_H|Z80F_Y|Z80F_Z|Z80F_S);
          if(tmp8 == 0) Z80R_F(cpu) |= Z80F_Z;
          Z80R_F(cpu) |= tmp8 & (Z80F_X|Z80F_Y|Z80F_S);
          _z80_emu_flags_parity(cpu, tmp8);
          break;
        case 1:
          if(y == 6) { /* OUT (C),0/FF */
            if(cpu->z80model == Z80_MODEL_NMOS)
              tmp8 = 0;
            else
              tmp8 = 0xFF;
          }else{ /* OUT (C),rY */
            tmp8 = _z80_emu_regread_r(cpu, y);
          }
          _z80_emu_pwrite(cpu, Z80R_BC(cpu), tmp8);
          break;
        case 2:
          switch(q) {
            case 0: /* SBC HL, rpP */
              tmp16 = Z80R_HL(cpu);
              tmp16_2 = _z80_emu_regread_rp(cpu, p);
              tmp16_3 = tmp16 - tmp16_2 - (Z80R_F(cpu) & Z80F_C);

              _z80_emu_flags_sbc16(cpu, tmp16, tmp16_2, Z80R_F(cpu) & Z80F_C);

              Z80R_HL(cpu) = tmp16_3;
              break;
            case 1: /* ADC HL, rpP */
              tmp16 = Z80R_HL(cpu);
              tmp16_2 = _z80_emu_regread_rp(cpu, p);
              tmp16_3 = tmp16 + tmp16_2 + (Z80R_F(cpu) & Z80F_C);

              _z80_emu_flags_adc16(cpu, tmp16, tmp16_2, Z80R_F(cpu) & Z80F_C);

              Z80R_HL(cpu) = tmp16_3;
              break;
          }
          break;
        case 3:
          switch(q) {
            case 0: /* LD (nn),rpP */
              tmp16 = _z80_emu_readop16(cpu);
              _z80_emu_write16(cpu, tmp16, _z80_emu_regread_rp(cpu, p));
              break;
            case 1: /* LD rpP,(nn) */
              tmp16 = _z80_emu_readop16(cpu);
              _z80_emu_regwrite_rp(cpu, p, _z80_emu_read16(cpu, tmp16));
              break;
          }
          break;
        case 4: /* NEG */
          _z80_emu_flags_sbc(cpu, 0, Z80R_A(cpu), 0);
          Z80R_A(cpu) = -Z80R_A(cpu);
          break;
        case 5: /* RETI/RETN (they actually operate the same) */
          Z80R_WZ(cpu) = _z80_emu_pop(cpu);
          Z80R_PC(cpu) = Z80R_WZ(cpu);
          cpu->iff1 = cpu->iff2;
          break;
        case 6: /* IM imY */
          cpu->im = y & 3;
          if(cpu->im != 0)
            cpu->im--;
          break;
        case 7:
          switch(y) {
            case 0: /* LD I,A */
              cpu->i = Z80R_A(cpu);
              break;
            case 1: /* LD R,A */
              cpu->r = Z80R_A(cpu);
              break;
            case 2: /* LD A,I */
              Z80R_A(cpu) = cpu->i;

              Z80R_F(cpu) &= ~(Z80F_N|Z80F_P|Z80F_X|Z80F_H|Z80F_Y|Z80F_Z|Z80F_S);
              if(Z80R_A(cpu) == 0) Z80R_F(cpu) |= Z80F_Z;
              Z80R_F(cpu) |= Z80R_A(cpu) & (Z80F_X|Z80F_Y|Z80F_S);
              if(cpu->iff2) Z80R_F(cpu) |= Z80F_P;
              break;
            case 3: /* LD A,R */
              Z80R_A(cpu) = cpu->r;

              Z80R_F(cpu) &= ~(Z80F_N|Z80F_P|Z80F_X|Z80F_H|Z80F_Y|Z80F_Z|Z80F_S);
              if(Z80R_A(cpu) == 0) Z80R_F(cpu) |= Z80F_Z;
              Z80R_F(cpu) |= Z80R_A(cpu) & (Z80F_X|Z80F_Y|Z80F_S);
              if(cpu->iff2) Z80R_F(cpu) |= Z80F_P;
              break;
            case 4: /* RRD */
              Z80R_WZ(cpu) = Z80R_HL(cpu);
              tmp8 = _z80_emu_read8(cpu, Z80R_WZ(cpu)++);

              tmp8_2 = Z80R_A(cpu);
              Z80R_A(cpu) = (Z80R_A(cpu) & ~0xF) | (tmp8 & 0xF);
              tmp8 = (tmp8_2 >> 4) | (Z80R_A(cpu) << 4);
              _z80_emu_write8(cpu, Z80R_HL(cpu), tmp8);

              Z80R_F(cpu) &= ~(Z80F_N|Z80F_P|Z80F_X|Z80F_H|Z80F_Y|Z80F_Z|Z80F_S);
              if(Z80R_A(cpu) == 0) Z80R_F(cpu) |= Z80F_Z;
              Z80R_F(cpu) |= Z80R_A(cpu) & (Z80F_X|Z80F_Y|Z80F_S);
              _z80_emu_flags_parity(cpu, Z80R_A(cpu));
              break;
            case 5: /* RLD */
              Z80R_WZ(cpu) = Z80R_HL(cpu);
              tmp8 = _z80_emu_read8(cpu, Z80R_WZ(cpu)++);

              tmp8_2 = Z80R_A(cpu);
              Z80R_A(cpu) = (Z80R_A(cpu) & ~0xF) | (tmp8 >> 4);
              tmp8 = (tmp8_2 << 4) | (Z80R_A(cpu) & 0xF);
              _z80_emu_write8(cpu, Z80R_HL(cpu), tmp8);

              Z80R_F(cpu) &= ~(Z80F_N|Z80F_P|Z80F_X|Z80F_H|Z80F_Y|Z80F_Z|Z80F_S);
              if(Z80R_A(cpu) == 0) Z80R_F(cpu) |= Z80F_Z;
              Z80R_F(cpu) |= Z80R_A(cpu) & (Z80F_X|Z80F_Y|Z80F_S);
              _z80_emu_flags_parity(cpu, Z80R_A(cpu));
              break;
            case 6: /* NOP */
              break;
            case 7: /* NOP */
              break;
          }
          break;
      }
      break;
    case 2:
      if(!(z & 4) && (y & 4)) { /* bliYZ */
        switch(z) {
          case 0: /* LDx */
            tmp8 = _z80_emu_read8(cpu, Z80R_HL(cpu));
            _z80_emu_write8(cpu, Z80R_DE(cpu), tmp8);
            Z80R_BC(cpu)--;
            if(y & 1) {
              Z80R_HL(cpu)--;
              Z80R_DE(cpu)--;
            }else{
              Z80R_HL(cpu)++;
              Z80R_DE(cpu)++;
            }
            tmp8_2 = tmp8 + Z80R_A(cpu);

            Z80R_F(cpu) &= ~(Z80F_N|Z80F_P|Z80F_X|Z80F_H|Z80F_Y);

            if(Z80R_BC(cpu) != 0) Z80R_F(cpu) |= Z80F_P;
            if(tmp8_2 & (1 << 1)) Z80R_F(cpu) |= Z80F_Y;
            if(tmp8_2 & (1 << 3)) Z80R_F(cpu) |= Z80F_X;

            tmp8 = Z80R_F(cpu) & Z80F_P;
            break;

          case 1: /* CPx */
            tmp8 = _z80_emu_read8(cpu, Z80R_HL(cpu));
            _z80_emu_write8(cpu, Z80R_DE(cpu), tmp8);
            Z80R_BC(cpu)--;
            if(y & 1) {
              Z80R_HL(cpu)--;
            }else{
              Z80R_HL(cpu)++;
            }

            tmp8_2 = Z80R_A(cpu) - tmp8;

            Z80R_F(cpu) &= Z80F_C;
            Z80R_F(cpu) |= Z80F_N;

            if(Z80R_BC(cpu) != 0) Z80R_F(cpu) |= Z80F_P;
            if(Z80R_A(cpu) == tmp8) Z80R_F(cpu) |= Z80F_Z;
            if(tmp8_2 & 0x80) Z80R_F(cpu) |= Z80F_S;
            if((Z80R_A(cpu) & 0xF) < (tmp8 & 0xF)) { Z80R_F(cpu) |= Z80F_H; tmp8_2--; }
            if(tmp8_2 & (1 << 1)) Z80R_F(cpu) |= Z80F_Y;
            if(tmp8_2 & (1 << 3)) Z80R_F(cpu) |= Z80F_X;

            tmp8 = (Z80R_F(cpu) & Z80F_P) || (Z80R_F(cpu) & Z80F_Z);
            break;

          case 2: /* INx */
            tmp8 = _z80_emu_pread(cpu, Z80R_BC(cpu));
            _z80_emu_write8(cpu, Z80R_HL(cpu), tmp8);
            Z80R_B(cpu)--;
            if(y & 1) {
              Z80R_HL(cpu)--;
              tmp8_2 = tmp8 + ((Z80R_C(cpu)-1) & 0xFF);
            }else{
              Z80R_HL(cpu)++;
              tmp8_2 = tmp8 + ((Z80R_C(cpu)+1) & 0xFF);
            }

            Z80R_F(cpu) = 0;
            if(tmp8_2 < tmp8) Z80R_F(cpu) |= Z80F_H|Z80F_C;
            if(_z80_emu_parity((tmp8_2 & 7) ^ Z80R_B(cpu))) Z80R_F(cpu) |= Z80F_P;
            if(tmp8 & (1 << 7)) Z80R_F(cpu) |= Z80F_N;
            if(Z80R_B(cpu) == 0) Z80R_F(cpu) |= Z80F_Z;
            Z80R_F(cpu) |= Z80R_B(cpu) & (Z80F_X|Z80F_Y|Z80F_S);

            tmp8 = Z80R_F(cpu) & Z80F_Z;
            break;

          case 3: /* OUTx */
            tmp8 = _z80_emu_read8(cpu, Z80R_HL(cpu));
            _z80_emu_pwrite(cpu, Z80R_BC(cpu), tmp8);
            Z80R_B(cpu)--;
            if(y & 1) {
              Z80R_HL(cpu)--;
            }else{
              Z80R_HL(cpu)++;
            }
            tmp8_2 = tmp8 + Z80R_L(cpu);

            Z80R_F(cpu) = 0;
            if(tmp8_2 < tmp8) Z80R_F(cpu) |= Z80F_H|Z80F_C;
            if(_z80_emu_parity((tmp8_2 & 7) ^ Z80R_B(cpu))) Z80R_F(cpu) |= Z80F_P;
            if(tmp8 & (1 << 7)) Z80R_F(cpu) |= Z80F_N;
            if(Z80R_B(cpu) == 0) Z80R_F(cpu) |= Z80F_Z;
            Z80R_F(cpu) |= Z80R_B(cpu) & (Z80F_X|Z80F_Y|Z80F_S);

            tmp8 = Z80R_F(cpu) & Z80F_Z;
            break;
        }
        if(y & 2) { /* Repeat */
          if(tmp8) { /* If we're not done, don't advance PC */
            Z80R_PC(cpu)--;
            cpu->cyc_use += 5;
            cpu->npfx = -1;
          }
        }
      }else{ /* DNOP */
      }
      break;
    default: /* DNOP */
      break;
  }
}

void _z80_emu_cycle(hien_dev_t* dev)
{
  cpu_z80_t* cpu;
  cpu = dev->data;

  cpu->after_ldair = FALSE;
  cpu->after_ei = FALSE;
  cpu->r++;
  cpu->npfx = Z80PFX_NONE;

  if(cpu->pfx & Z80PFX_CB) {
    if(cpu->pfx & Z80PFX_XY)
      cpu->cyc_use = _z80_cyc_xycb[cpu->ir];
    else
      cpu->cyc_use = _z80_cyc_cb[cpu->ir];

    _z80_emu_cycle_cb(cpu);
  }else if(cpu->pfx & Z80PFX_ED) {
    cpu->cyc_use = _z80_cyc_ed[cpu->ir];
    _z80_emu_cycle_ed(cpu);
  }else{
    cpu->cyc_use = _z80_cyc_op[cpu->ir];
    _z80_emu_cycle_op(cpu);
  }

  switch(cpu->npfx) {
    case Z80PFX_NONE:
    case Z80PFX_IX:
    case Z80PFX_IY:
      cpu->pfx = cpu->npfx;
      cpu->insn_disp_used = FALSE;
      break;
    case Z80PFX_ED:
    case Z80PFX_CB:
      cpu->pfx |= cpu->npfx;
      break;
    case -1:
      break;
  }
}

void _z80_emu_nmi(hien_dev_t* dev, cpu_z80_t* cpu)
{
  (void)dev;
  cpu->iff1 = FALSE;
  cpuprof_sub_start(dev->prof, Z80R_PC(cpu), 2);

  Z80R_SP(cpu) -= 2;
  _z80_emu_write16(cpu, Z80R_SP(cpu), Z80R_PC(cpu));

  Z80R_WZ(cpu) = 0x0066;
  Z80R_PC(cpu) = Z80R_WZ(cpu);
  cpu->cyc_use += 11;

  cpu->ir = _z80_emu_readop(cpu);
}

void _z80_emu_int(hien_dev_t* dev, cpu_z80_t* cpu)
{
  (void)dev;
  cpu->iff1 = FALSE;
  cpu->iff2 = FALSE;
  cpu->cyc_use += 2; /* Interrupt latency */
  switch(cpu->im) {
    case 0: /* IM 0 - Read instruction from bus */
      cpu->ir = dev_port_read(dev, Z80_PORT_DBUS, 0xFF);
      break;
    case 1: /* IM 1 - Read RST 38h instruction */
      cpu->ir = 0xFF;
      cpuprof_sub_setmode(dev->prof, 1);
      break;
    case 2: /* IM 2 - Call [i:data] */
      cpuprof_sub_start(dev->prof, Z80R_PC(cpu), 1);

      Z80R_SP(cpu) -= 2;
      _z80_emu_write16(cpu, Z80R_SP(cpu), Z80R_PC(cpu));

      Z80R_WZ(cpu) = _z80_emu_read16(cpu, (cpu->i << 8) | dev_port_read(dev, Z80_PORT_DBUS, 0xFF));
      cpu->cyc_use += 17; /* call */
      Z80R_PC(cpu) = Z80R_WZ(cpu);

      cpu->ir = _z80_emu_readop(cpu);
      break;
  }
}

void _z80_emu_init(hien_dev_t* dev)
{
  (void)dev;

  /* Pre-calculate flags */
  if(_flag_init) {
    return;
  }
  _flag_init = TRUE;

}

void _z80_emu_delete(hien_dev_t* dev)
{
  (void)dev;
}
