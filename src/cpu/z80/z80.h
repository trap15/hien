/**********************************************************
*
* cpu/z80/z80.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _CPU_Z80_H_
#define _CPU_Z80_H_

#define Z80_SPACE_IO    (SPACE_USER+0)
#define Z80_SPACE_COUNT (SPACE_USER+1)

SPACE_COUNT_CHECK(Z80);

#define Z80_PORT_BUSACK (PORT_USER+0)
#define Z80_PORT_BUSREQ (PORT_USER+1)
#define Z80_PORT_HALT   (PORT_USER+2)
#define Z80_PORT_INT    (PORT_USER+3)
#define Z80_PORT_M1     (PORT_USER+4)
#define Z80_PORT_MREQ   (PORT_USER+5)
#define Z80_PORT_NMI    (PORT_USER+6)
#define Z80_PORT_RESET  (PORT_USER+7)
#define Z80_PORT_RFSH   (PORT_USER+8)
#define Z80_PORT_WAIT   (PORT_USER+9)
#define Z80_PORT_DBUS   (PORT_USER+10) /* Use for IM 0 */
#define Z80_PORT_ABUS   (PORT_USER+11) /* Use for RFSH */
#define Z80_PORT_COUNT  (PORT_USER+12)

PORT_COUNT_CHECK(Z80);

#define Z80_MODEL_NMOS 0
#define Z80_MODEL_CMOS 1

hien_dev_t* cpu_z80_new(hien_vm_t* vm, char* name, hz_t clock);
hien_dev_t* cpu_z80c_new(hien_vm_t* vm, char* name, hz_t clock);

extern hien_dev_info_t cpu_z80_def;
extern hien_dev_info_t cpu_z80c_def;

#endif
