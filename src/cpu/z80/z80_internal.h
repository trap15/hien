/**********************************************************
*
* cpu/z80/z80_internal.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _CPU_Z80_INTERNAL_H_
#define _CPU_Z80_INTERNAL_H_

typedef struct cpu_z80_t cpu_z80_t;

struct cpu_z80_t {
  /* State */
  int z80model;

  uint8_t ir; /* Instruction Register */

  int cyc_use;

  /* Z80 */
  hien_dev_t* dev;

  pair16_t prvpc, pc, sp, af, bc, de, hl, ix, iy, wz;
  pair16_t af2, bc2, de2, hl2;
  uint8_t  r,r2,i;
  int      iff1,iff2;
  int      im;

  int      rst_last;
  int      nmi_last;

  int      after_ei;
  int      after_ldair;

  int8_t insn_disp;
  int insn_disp_used;

  int pfx, npfx;
};

#define Z80PFX_NONE (0)
#define Z80PFX_IX   (1)
#define Z80PFX_IY   (2)
#define Z80PFX_CB   (4)
#define Z80PFX_ED   (8)
#define Z80PFX_XY   (Z80PFX_IX|Z80PFX_IY)

#define Z80R_Rv(Z)  ((Z).v)
#define Z80R_Rh(Z)  ((Z).i.h)
#define Z80R_Rl(Z)  ((Z).i.l)

#define Z80R_PC(Z)  Z80R_Rv((Z)->pc)
#define Z80R_PCh(Z) Z80R_Rh((Z)->pc)
#define Z80R_PCl(Z) Z80R_Rl((Z)->pc)

#define Z80R_SP(Z)  Z80R_Rv((Z)->sp)
#define Z80R_SPh(Z) Z80R_Rh((Z)->sp)
#define Z80R_SPl(Z) Z80R_Rl((Z)->sp)

#define Z80R_AF(Z)  Z80R_Rv((Z)->af)
#define Z80R_A(Z)   Z80R_Rh((Z)->af)
#define Z80R_F(Z)   Z80R_Rl((Z)->af)

#define Z80R_BC(Z)  Z80R_Rv((Z)->bc)
#define Z80R_B(Z)   Z80R_Rh((Z)->bc)
#define Z80R_C(Z)   Z80R_Rl((Z)->bc)

#define Z80R_DE(Z)  Z80R_Rv((Z)->de)
#define Z80R_D(Z)   Z80R_Rh((Z)->de)
#define Z80R_E(Z)   Z80R_Rl((Z)->de)

#define Z80R_HL(Z)  Z80R_Rv((Z)->hl)
#define Z80R_H(Z)   Z80R_Rh((Z)->hl)
#define Z80R_L(Z)   Z80R_Rl((Z)->hl)

#define Z80R_AF2(Z) Z80R_Rv((Z)->af2)
#define Z80R_A2(Z)  Z80R_Rh((Z)->af2)
#define Z80R_F2(Z)  Z80R_Rl((Z)->af2)

#define Z80R_BC2(Z) Z80R_Rv((Z)->bc2)
#define Z80R_B2(Z)  Z80R_Rh((Z)->bc2)
#define Z80R_C2(Z)  Z80R_Rl((Z)->bc2)

#define Z80R_DE2(Z) Z80R_Rv((Z)->de2)
#define Z80R_D2(Z)  Z80R_Rh((Z)->de2)
#define Z80R_E2(Z)  Z80R_Rl((Z)->de2)

#define Z80R_HL2(Z) Z80R_Rv((Z)->hl2)
#define Z80R_H2(Z)  Z80R_Rh((Z)->hl2)
#define Z80R_L2(Z)  Z80R_Rl((Z)->hl2)

#define Z80R_IX(Z)  Z80R_Rv((Z)->ix)
#define Z80R_IXh(Z) Z80R_Rh((Z)->ix)
#define Z80R_IXl(Z) Z80R_Rl((Z)->ix)

#define Z80R_IY(Z)  Z80R_Rv((Z)->iy)
#define Z80R_IYh(Z) Z80R_Rh((Z)->iy)
#define Z80R_IYl(Z) Z80R_Rl((Z)->iy)

#define Z80R_WZ(Z)  Z80R_Rv((Z)->wz)
#define Z80R_WZh(Z) Z80R_Rh((Z)->wz)
#define Z80R_WZl(Z) Z80R_Rl((Z)->wz)



#define Z80F_C (0x01)
#define Z80F_N (0x02)
#define Z80F_P (0x04)
#define Z80F_V Z80F_P
#define Z80F_X (0x08)
#define Z80F_H (0x10)
#define Z80F_Y (0x20)
#define Z80F_Z (0x40)
#define Z80F_S (0x80)

void _z80_emu_cycle(hien_dev_t* dev);
void _z80_emu_init(hien_dev_t* dev);
void _z80_emu_delete(hien_dev_t* dev);

void _z80_emu_nmi(hien_dev_t* dev, cpu_z80_t* cpu);
void _z80_emu_int(hien_dev_t* dev, cpu_z80_t* cpu);

void _z80_dis_do(hien_dev_t* dev, addr_t pc, char* buf);
void _z80_dis_init(hien_dev_t* dev);
void _z80_dis_delete(hien_dev_t* dev);

#endif
