/**********************************************************
*
* cpu/m6502/m6502_dis.c
*
**********************************************************/

#include "hien.h"
#include "m6502_internal.h"

void _m6502_dis_do(hien_dev_t *dev, addr_t pc, char *buf)
{
  (void)dev;
  (void)pc;
  (void)buf;
}

void _m6502_dis_init(hien_dev_t *dev)
{
  (void)dev;
}

void _m6502_dis_delete(hien_dev_t *dev)
{
  (void)dev;
}

