/**********************************************************
*
* cpu/m6502/m6502_ops.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/
#if 0

#include "hien.h"
#include "m6502_internal.h"

#define UNKOP {"---",0,            M65T_2, _m6502op_unk, }

_m6502_op_t _m6502_optbl[256] = {
/* 00 */
  {"BRK",M65MODE_IMP,  M65T_7, _m6502op_brk, },
  {"ORA",M65MODE_INDX, M65T_6, _m6502op_ora, },
  UNKOP,
  UNKOP,
/* 04 */
  UNKOP,
  {"ORA",M65MODE_ZP,   M65T_3, _m6502op_ora, },
  {"ASL",M65MODE_ZP,   M65T_5, _m6502op_asl, },
  UNKOP,
/* 08 */
  {"PHP",M65MODE_IMP,  M65T_3, _m6502op_php, },
  {"ORA",M65MODE_IMM,  M65T_2, _m6502op_ora, },
  {"ASL",M65MODE_IMP,  M65T_2, _m6502op_asl, },
  UNKOP,
/* 0C */
  UNKOP,
  {"ORA",M65MODE_ABS,  M65T_4, _m6502op_ora, },
  {"ASL",M65MODE_ABS,  M65T_6, _m6502op_asl, },
  UNKOP,
/* 10 */
  {"BPL",M65MODE_IMP,  M65T_2, _m6502op_brk, },
  {"ORA",M65MODE_INDY, M65T_5, _m6502op_ora, },
  UNKOP,
  UNKOP,
/* 14 */
  UNKOP,
  {"ORA",M65MODE_ZPX,  M65T_4, _m6502op_ora, },
  {"ASL",M65MODE_ZPX,  M65T_6, _m6502op_asl, },
  UNKOP,
/* 18 */
  {"CLC",M65MODE_IMP,  M65T_2, _m6502op_clc, },
  {"ORA",M65MODE_ABSY, M65T_4, _m6502op_ora, },
  UNKOP,
  UNKOP,
/* 1C */
  UNKOP,
  {"ORA",M65MODE_ABSX, M65T_4, _m6502op_ora, },
  {"ASL",M65MODE_ABSX, M65T_7, _m6502op_asl, },
  UNKOP,
/* 20 */

};
#endif
