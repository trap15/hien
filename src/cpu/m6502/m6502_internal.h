/**********************************************************
*
* cpu/m6502/m6502_internal.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _CPU_M6502_INTERNAL_H_
#define _CPU_M6502_INTERNAL_H_

#define M6502DEBUG 1

typedef struct cpu_m6502_t cpu_m6502_t;
typedef struct _m6502_op_t _m6502_op_t;

enum {
  M65T_0 = 1<<0,
  M65T_1 = 1<<1,
  M65T_2 = 1<<2,
  M65T_3 = 1<<3,
  M65T_4 = 1<<4,
  M65T_5 = 1<<5,
  M65T_6 = 1<<6,
  M65T_7 = 1<<7,
};

enum {
  M65BRK_BRK = 0,
  M65BRK_IRQ,
  M65BRK_NMI,
  M65BRK_RESET,
};

struct cpu_m6502_t {
  hien_dev_t* dev;

  /* CPU */
  uint8_t a, x, y, s, p;
  uint16_t pc;

  /* state */
  uint8_t ir;
  uint8_t bus;
  uint8_t tstate;
  uint8_t tgt_tstate;

  uint8_t res_poke;

  uint8_t res_latch, so_latch, nmi_latch;
  uint8_t nmi_pend;

  uint8_t brk_type;
};

#define M65F_C (0x01)
#define M65F_Z (0x02)
#define M65F_I (0x04)
#define M65F_D (0x08)
#define M65F_B (0x10)
#define M65F_5 (0x20)
#define M65F_V (0x40)
#define M65F_N (0x80)

void _m6502_emu_cycle(hien_dev_t* dev);
void _m6502_emu_init(hien_dev_t* dev);
void _m6502_emu_delete(hien_dev_t* dev);

void _m6502_dis_do(hien_dev_t* dev, addr_t pc, char* buf);
void _m6502_dis_init(hien_dev_t* dev);
void _m6502_dis_delete(hien_dev_t* dev);

enum {
  M65MODE_IMP = 0,
  M65MODE_IMM,
  M65MODE_ZP,
  M65MODE_ZPX,
  M65MODE_ZPY,
  M65MODE_ABS,
  M65MODE_ABSX,
  M65MODE_ABSY,
  M65MODE_INDX,
  M65MODE_INDY,
};

struct _m6502_op_t {
  char *name;
  uint8_t adrmode;
  uint8_t tgt_tstate;
  void (*handle)(hien_dev_t* dev, cpu_m6502_t* cpu);
};

extern _m6502_op_t _m6502_optbl[256];

#endif
