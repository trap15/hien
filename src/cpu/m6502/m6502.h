/**********************************************************
*
* cpu/m6502/m6502.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _CPU_M6502_H_
#define _CPU_M6502_H_

#define M6502_SPACE_COUNT (SPACE_USER+0)

SPACE_COUNT_CHECK(M6502);

#define M6502_PORT_CLK1   (PORT_USER+0)
#define M6502_PORT_CLK2   (PORT_USER+1)
#define M6502_PORT_nIRQ   (PORT_USER+2)
#define M6502_PORT_nNMI   (PORT_USER+3)
#define M6502_PORT_SYNC   (PORT_USER+4)
#define M6502_PORT_RDY    (PORT_USER+5)
#define M6502_PORT_nRES   (PORT_USER+6)
#define M6502_PORT_nSO    (PORT_USER+7)
#define M6502_PORT_COUNT  (PORT_USER+8)

PORT_COUNT_CHECK(M6502);

hien_dev_t* cpu_m6502_new(hien_vm_t* vm, char* name, hz_t clock);

extern hien_dev_info_t cpu_m6502_def;

#endif
