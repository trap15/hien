/**********************************************************
*
* cpu/m6502/m6502.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"
#include "m6502_internal.h"

static void cpu_m6502_update(hien_dev_t* dev, uintmax_t data, uintmax_t mask)
{
  cpu_m6502_t* cpu;
  (void)mask;
  cpu = dev->data;

  switch(data) {
    /* Falling edge */
    case 0:
      /* Update output clocks */
      dev_port_assert(dev, M6502_PORT_CLK1);
      dev_port_deassert(dev, M6502_PORT_CLK2);

      /* SYNC changes on falling edge */
      if(cpu->tstate & M65T_0) {
#if M6502DEBUG
        printf("SYNC!\n");
#endif
        dev_port_assert(dev, M6502_PORT_SYNC);
      }else{
        dev_port_deassert(dev, M6502_PORT_SYNC);
      }
      break;

    /* Rising edge */
    case 1:
      /* Update output clocks */
      dev_port_deassert(dev, M6502_PORT_CLK1);
      dev_port_assert(dev, M6502_PORT_CLK2);

      /* Reset handling */
      if(cpu->res_latch == 0) { /* Check for reset */
        if(cpu->res_poke || !dev_port_is_asserted(dev, M6502_PORT_nRES)) {
#if M6502DEBUG
          printf("Reset low!\n");
#endif
          cpu->res_latch = 1;
          cpu->res_poke = 0;
        }
      }else if((cpu->res_latch == 1) && dev_port_is_asserted(dev, M6502_PORT_nRES)) { /* Reset was latched, await raise */
#if M6502DEBUG
        printf("Reset high!\n");
#endif
        cpu->p &= ~(M65F_D);
        cpu->p |=  (M65F_I|M65F_B|M65F_5);

        cpu->tstate = M65T_1;
        cpu->ir = 0x00;
        cpu->brk_type = M65BRK_RESET;

        cpu->so_latch = 1;
        cpu->nmi_latch = 1;
        cpu->nmi_pend = 0;

        cpu->res_latch = 0;
      }
      if(cpu->res_latch) /* /RES is low */
        break;

      /* Handle /SO */
      if(!dev_port_is_asserted(dev, M6502_PORT_nSO) && cpu->so_latch) /* /SO is falling edge triggered */
        cpu->p |= M65F_V;
      cpu->so_latch = dev_port_is_asserted(dev, M6502_PORT_nSO);

      /* Handle /NMI latching */
      if(!dev_port_is_asserted(dev, M6502_PORT_nNMI) && cpu->nmi_latch) /* /NMI is falling edge triggered */
        cpu->nmi_pend = 1;
      cpu->nmi_latch = dev_port_is_asserted(dev, M6502_PORT_nNMI);

      /* Handle RDY */
      if(!dev_port_is_asserted(dev, M6502_PORT_RDY)) /* If RDY is low, halt the CPU */
        break;

      /* otherwise do normal things */

      /* Handle interrupts */
      if(cpu->tstate & M65T_0) {
        if(!dev_port_is_asserted(dev, M6502_PORT_nIRQ) && !(cpu->p & M65F_I)) { /* /IRQ is triggered on low if P.I is clear */
#if M6502DEBUG
          printf("Go go IRQ!\n");
#endif
          cpu->tstate = M65T_1;
          cpu->ir = 0x00;
          cpu->brk_type = M65BRK_IRQ;
        }else if(cpu->nmi_pend) { /* Handle /NMI */
#if M6502DEBUG
          printf("Go go NMI!\n");
#endif
          cpu->tstate = M65T_1;
          cpu->ir = 0x00;
          cpu->brk_type = M65BRK_NMI;

          cpu->nmi_pend = 0;
        }
      }

      _m6502_emu_cycle(dev);
      break;
  }
}

static uintmax_t cpu_m6502_read(hien_dev_t* dev, addr_t addr, uintmax_t mask)
{
  return dev_read(dev, SPACE_MAIN, addr, mask);
}

static void cpu_m6502_write(hien_dev_t* dev, addr_t addr, uintmax_t data, uintmax_t mask)
{
  dev_write(dev, SPACE_MAIN, addr, data, mask);
}

static void cpu_m6502_reset(hien_dev_t* dev)
{
  cpu_m6502_t* cpu;
  cpu = dev->data;

  _m6502_emu_init(dev);

  cpu->res_latch = 0;
  cpu->res_poke = 1;
}

static void cpu_m6502_delete(hien_dev_t* dev)
{
  cpu_m6502_t* cpu;
  cpu = dev->data;

  _m6502_emu_delete(dev);

  mem_free(cpu);
}

hien_dev_t* cpu_m6502_new(hien_vm_t* vm, char* name, hz_t clock)
{
  hien_dev_t* dev;
  cpu_m6502_t* cpu;

  cpu = mem_alloc_type(cpu_m6502_t);

  dev = dev_new(vm, name, clock);
  dev->delete = cpu_m6502_delete;
  dev->reset  = cpu_m6502_reset;

  dev_port_set_callbacks(dev, PORT_CLOCK, NULL, cpu_m6502_update);

  dev_space_setup(dev, SPACE_MAIN, 8);
  dev_space_set_callbacks(dev, SPACE_MAIN, cpu_m6502_read, cpu_m6502_write);

  dev_port_setup(dev, M6502_PORT_CLK1, 1,  PORT_OUTPUT);
  dev_port_setup(dev, M6502_PORT_CLK2, 1,  PORT_OUTPUT);
  dev_port_setup(dev, M6502_PORT_nIRQ, 1,  PORT_INPUT);
  dev_port_setup(dev, M6502_PORT_nNMI, 1,  PORT_INPUT);
  dev_port_setup(dev, M6502_PORT_SYNC, 1,  PORT_OUTPUT);
  dev_port_setup(dev, M6502_PORT_RDY,  1,  PORT_INPUT);
  dev_port_setup(dev, M6502_PORT_nRES, 1,  PORT_INPUT);
  dev_port_setup(dev, M6502_PORT_nSO,  1,  PORT_INPUT);

  dev_port_assert(dev, M6502_PORT_CLK1);
  dev_port_deassert(dev, M6502_PORT_CLK2);

  dev_port_deassert(dev, M6502_PORT_SYNC);
  dev_port_assert(dev, M6502_PORT_nIRQ);
  dev_port_assert(dev, M6502_PORT_nNMI);
  dev_port_assert(dev, M6502_PORT_RDY);
  dev_port_assert(dev, M6502_PORT_nSO);
  dev_port_assert(dev, M6502_PORT_nRES);

  dev->data = cpu;
  cpu->dev = dev;

  _m6502_emu_init(dev);

  dev_reset(dev);

  vm_state_add_uint8(vm, &cpu->a, name, "regs", "a", NULL);
  vm_state_add_uint8(vm, &cpu->x, name, "regs", "x", NULL);
  vm_state_add_uint8(vm, &cpu->y, name, "regs", "y", NULL);
  vm_state_add_uint8(vm, &cpu->s, name, "regs", "s", NULL);
  vm_state_add_uint8(vm, &cpu->p, name, "regs", "p", NULL);
  vm_state_add_uint16(vm, &cpu->pc, name, "regs", "pc", NULL);

  vm_state_add_uint8(vm, &cpu->ir, name, "state", "ir", NULL);
  vm_state_add_uint8(vm, &cpu->bus, name, "state", "bus", NULL);
  vm_state_add_uint8(vm, &cpu->tstate, name, "state", "tstate", NULL);

  vm_state_add_uint8(vm, &cpu->res_poke, name, "state", "res_poke", NULL);
  vm_state_add_uint8(vm, &cpu->res_latch, name, "state", "res_latch", NULL);
  vm_state_add_uint8(vm, &cpu->so_latch, name, "state", "so_latch", NULL);
  vm_state_add_uint8(vm, &cpu->nmi_latch, name, "state", "nmi_latch", NULL);
  vm_state_add_uint8(vm, &cpu->nmi_pend, name, "state", "nmi_pend", NULL);
  vm_state_add_uint8(vm, &cpu->brk_type, name, "state", "brk_type", NULL);

  return dev;
}

hien_dev_info_t cpu_m6502_def = {
  .type = "M6502",
  .ctor = "cpu_m6502_new",
  .use_clk = TRUE,
  .has_save = 1,
};
