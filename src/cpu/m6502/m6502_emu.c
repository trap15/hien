/**********************************************************
*
* cpu/m6502/m6502_emu.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"
#include "m6502_internal.h"

static uint8_t m65read8(hien_dev_t* dev, addr_t adr)
{
  return dev_read(dev, SPACE_MAIN, adr, 0xFF);
}

static void _m6502_emu_op(hien_dev_t* dev, cpu_m6502_t* cpu)
{
  (void)dev;
#if M6502DEBUG
  printf("Do %02X!\n", cpu->ir);
#endif
  cpu->tgt_tstate = M65T_2;
}

void _m6502_emu_cycle(hien_dev_t* dev)
{
  cpu_m6502_t* cpu;
  cpu = dev->data;

  if(cpu->tstate & M65T_0) { /* Fetch */
#if M6502DEBUG
  printf("Fetch from %04X!\n", cpu->pc);
#endif
    cpu->ir = m65read8(dev, cpu->pc++);
  }else if(cpu->tstate & M65T_1) {
    _m6502_emu_op(dev, cpu);
  }
  cpu->tstate <<= 1;
  if(cpu->tstate == cpu->tgt_tstate) { /* We're done. */
    cpu->tstate = M65T_0;
  }
}

void _m6502_emu_init(hien_dev_t* dev)
{
  (void)dev;
}

void _m6502_emu_delete(hien_dev_t* dev)
{
  (void)dev;
}
