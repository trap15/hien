/**********************************************************
*
* helper.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
***********************************************************
*
* For addresses, use the type addr_t.
*
**********************************************************/

#ifndef _HELPER_H_
#define _HELPER_H_

STATIC_ASSERT(ENDIAN_BE || ENDIAN_LE, unsupported_endianness__not_be_or_le);

#define INLINE static inline

/* Integer type definitions */
typedef uint32_t addr_t;

/* Define pair16_t and pair32_t */
#if ENDIAN_BE
typedef union {
  uint16_t v;
  struct {
    uint8_t h;
    uint8_t l;
  } i;
} pair16_t;

typedef union {
  uint32_t v;
  struct {
    pair16_t h;
    pair16_t l;
  } i;
} pair32_t;
#elif ENDIAN_LE
typedef union {
  uint16_t v;
  struct {
    uint8_t l;
    uint8_t h;
  } i;
} pair16_t;

typedef union {
  uint32_t v;
  struct {
    pair16_t l;
    pair16_t h;
  } i;
} pair32_t;
#endif

#endif
