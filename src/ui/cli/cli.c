/**********************************************************
*
* main.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
***********************************************************
*
* Main Hien UI code. This is where everything happens.
*
**********************************************************/

#include "hien.h"

#include <unistd.h>

#if POSIX
#include <termios.h>
#elif WIN32
#include <windows.h>
#include <conio.h>
#endif

typedef struct {

  int need_refresh;
  int cur_song;

#if POSIX
  struct termios old_tios;
  struct termios tios;
  int fd;
#endif

} ui_cli_t; /* Hey Beavis... uhuhuhuhuhu */

void ui_init(hien_vm_t* vm)
{
  ui_cli_t* cli;
  cli = mem_alloc_type(ui_cli_t);
  vm->ui_data = cli;

#if POSIX
  cli->fd = STDIN_FILENO;
  tcgetattr(cli->fd, &cli->old_tios);
  cli->tios = cli->old_tios;
  cli->tios.c_lflag &= ~(ICANON|ECHO|ECHOCTL|ECHONL);
  tcsetattr(cli->fd, TCSANOW, &cli->tios);
#endif
}

void ui_finish(hien_vm_t* vm)
{
  ui_cli_t* cli;
  cli = vm->ui_data;

#if POSIX
  tcsetattr(cli->fd, TCSANOW, &cli->old_tios);
#endif
  mem_free(vm->ui_data);
  vm->ui_data = NULL;
}

static int ui_input_getchar(hien_vm_t* vm)
{
  ui_cli_t* cli;
  cli = vm->ui_data;

#if POSIX
  struct timeval tv;
  fd_set fds;
  tv.tv_sec = 0;
  tv.tv_usec = 0;
  FD_ZERO(&fds);
  FD_SET(cli->fd, &fds);
  select(cli->fd+1, &fds, NULL, NULL, &tv);
  if(FD_ISSET(0,&fds))
    return getchar();
#elif WIN32
  if(kbhit())
    return getch();
#endif
  return 0;
}



void ui_stop_song(hien_vm_t* vm)
{
  sndplay_stop(vm->sndplay);
}

void ui_change_song(hien_vm_t* vm, int id)
{
  ui_cli_t* cli;
  cli = vm->ui_data;

  if(!sndplay_id_in_bounds(vm->sndplay, id))
    return;

  sndplay_play(vm->sndplay, id);
  cli->cur_song = id;
  cli->need_refresh = 1;
}

static void ui_load_board(hien_vm_t* vm, hien_board_desc_t* brd)
{
  board_load(vm->board, brd);
  sndplay_load(vm->sndplay, vm->board->desc->sndplay);
  ui_change_song(vm, vm->sndplay->desc->default_id);
  osd_snd_connect_outputs(vm);
}

static void ui_cli_handle_input(hien_vm_t* vm)
{
  ui_cli_t* cli;
  cli = vm->ui_data;

  switch(ui_input_getchar(vm)) {
    case 0:
      break;
    case '+':
      ui_change_song(vm, cli->cur_song+1);
      break;
    case '-':
      ui_change_song(vm, cli->cur_song-1);
      break;
    case '\n':
      ui_change_song(vm, cli->cur_song);
      break;
    case ' ':
      ui_stop_song(vm);
      break;
    case 'z':
      vm_state_save(vm, "foo");
      break;
    case 'x':
      vm_state_load(vm, "foo");
      cli->need_refresh = 1;
      break;
    case 'q':
      vm_stop(vm);
      break;
  }
}

#if 0
static void ui_switch_board(hien_vm_t* vm, hien_board_desc_t* brd)
{
  osd_snd_disconnect_outputs(vm);
  ui_load_board(vm, brd);
}
#endif

void ui_run(hien_vm_t* vm)
{
  ui_cli_t* cli;
  cli = vm->ui_data;

  if(!sndplay_waiting(vm->sndplay))
    ui_cli_handle_input(vm);

  if(cli->need_refresh) {
    printf("Playing song #%d\n", cli->cur_song);
    fflush(stdout);
    cli->need_refresh = 0;
  }
}

int main(int argc, char** argv)
{
  hien_vm_t* vm;
  hien_board_desc_t* brd;

  _save_argc = argc;
  _save_argv = argv;

  printf("Hien (c)2013-2014 Alex 'trap15' Marshall\n");
  printf("All rights reserved.\n\n");

  if(argc == 1) {
    fprintf(stderr, "Usage:\n\t%s [setname]\n", argv[0]);
    return EXIT_FAILURE;
  }

  mem_init();

  vm = vm_new("main", VMTYPE_SNDPLAY);

  brd = board_desc_generate(vm);
  if(brd == NULL) {
    fprintf(stderr, "couldn't load board '%s'\n", vm->config->board);
    vm_delete(vm);
    mem_finish();
    return EXIT_FAILURE;
  }

  printf("Interface: + for next song, - for previous song\n"
         "           Enter for restart song, Space for stop\n"
         "           q to quit\n\n");

  ui_load_board(vm, brd);

  config_handle_options(vm->config, _save_argc, _save_argv);
  vm_main(vm);

  osd_snd_disconnect_outputs(vm);
  vm_delete(vm);

  mem_finish();

  return EXIT_SUCCESS;
}

