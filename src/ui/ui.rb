if $ui == "cli"
  addModuleNoHdr("ui", "cli")
elsif $ui == "phoenix" or $ui == "phoenix-gtk" or $ui == "phoenix-qt"
  addModuleNoHdr("ui", "phoenix")
else
  abort("Bad UI name "+$ui)
end

