addMainObj("phoenix_c.o")
addMainObj("phoenix_core.o")
addMainObj("main_window.o")
addDir($basedir+"/phoenix")

if $platform == "win32"
  $srcflags += " -DPHOENIX_WINDOWS"
  $ldflags += " -lkernel32 -luser32 -lgdi32 -ladvapi32 -lole32 -lcomctl32 -lcomdlg32 -lshlwapi"
  addMainObj("phoenix/phoenix.o")
elsif $platform == "macosx"
  $srcflags += " -DPHOENIX_COCOA"
  $ldflags += " -framework Cocoa -framework Carbon"
  addMainObj("phoenix/phoenix_cocoa.o")
elsif $platform == "linux" or $platform == "bsd"
  if $ui == "phoenix-gtk"
    $srcflags += " -DPHOENIX_GTK `pkg-config --cflags gtk+-2.0`"
    $ldflags += " `pkg-config --libs gtk+-2.0`"
  elsif $ui == "phoenix-qt"
    $srcflags += " -DPHOENIX_QT `pkg-config --cflags QtCore QtGui`"
    $ldflags += " `pkg-config --libs QtCore QtGui`"
  end
  addMainObj("phoenix/phoenix.o")
else
  $srcflags += " -DPHOENIX_REFERENCE"
  $ldflags += ""
  addMainObj("phoenix/phoenix.o")
end

$srcflags += " -Isrc/"+$basedir
$ldflags += " -lstdc++"
