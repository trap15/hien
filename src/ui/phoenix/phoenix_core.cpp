#include <nall/nall.hpp>
#include <phoenix/phoenix.hpp>
using namespace nall;
using namespace phoenix;

#include "phoenix_top.hpp"

static HienUI::Program *program;

HienUI::Program::Program(int argc, char **argv)
{
  Application::processEvents();
  mainwin = new HienUI::MainWindow;

  Application::main = {&HienUI::Program::main, this};
};

HienUI::Program::~Program()
{
  osd_snd_disconnect_outputs(vm);
  vm_delete(vm);
}

bool HienUI::Program::initialize()
{
  hien_board_desc_t* brd;

  vm = vm_new("main", VMTYPE_SNDPLAY);

  if(vm->config->board != NULL)
    mem_free(vm->config->board);
  vm->config->board = mem_strdup("omegaf");

  brd = board_desc_generate(vm);
  if(brd == NULL) {
    fprintf(stderr, "couldn't load board '%s'\n", vm->config->board);
    return false;
  }

  load_board(brd);

  config_handle_options(vm->config, _save_argc, _save_argv);

  return true;
}

void HienUI::Program::load_board(hien_board_desc_t* brd)
{
  board_load(vm->board, brd);
  sndplay_load(vm->sndplay, vm->board->desc->sndplay);
  change_song(vm->sndplay->desc->default_id);
  osd_snd_connect_outputs(vm);
}

void HienUI::Program::change_song(int id)
{
  if(!sndplay_id_in_bounds(vm->sndplay, id))
    return;

  sndplay_play(vm->sndplay, id);
  cur_song = id;

  update_ui();
}

void HienUI::Program::stop_song()
{
  sndplay_stop(vm->sndplay);
}

void HienUI::Program::update_ui()
{
}

void HienUI::Program::main()
{
  while(osd_snd_need_more_samples(vm)) {
    vm_run(vm, HZ_TO_TIMETICK(vm->config->sync_rate));
  }
}

void HienUI::Program::activate()
{
  mainwin->setVisible();
  mainwin->setMenuVisible();
  mainwin->setFocused();
}

static void vm_thread(void* param)
{
  HienUI::Program* prg = (HienUI::Program*)param;

  vm_main(prg->vm);
}

int phoenix_main(int argc, char** argv)
{
  Application::setName(program_name);

  Application::Cocoa::onQuit = [&] {
    Application::quit();
  };

  Application::Cocoa::onActivate = [&] {
    program->activate();
  };

  program = new HienUI::Program(argc, argv);
  if(program->initialize()) {
    fprintf(stderr, "running~\n");
    Application::processEvents();
    Application::run();
  }
  delete program;
  return EXIT_SUCCESS;
}
