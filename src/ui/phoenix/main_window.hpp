#ifndef _UI_MAIN_WINDOW_HPP_
#define _UI_MAIN_WINDOW_HPP_

namespace HienUI {
  struct MainWindow : Window {
    VerticalLayout layout;
      HorizontalLayout actionLayout;
        Button applyButton;
        Button fileButton;
        Button pathButton;
      HorizontalLayout controlLayout;
        Label optionsLabel;
        CheckButton manifestFlag;
        CheckButton deltaFlag;

    Menu nullMenu;

    MainWindow();
  };
  extern MainWindow* mainwin;
}

#endif
