#include <nall/nall.hpp>
#include <phoenix/phoenix.hpp>
using namespace nall;
using namespace phoenix;

#include "phoenix_top.hpp"

HienUI::MainWindow* HienUI::mainwin = NULL;

HienUI::MainWindow::MainWindow()
{
  setTitle(program_name);
  setResizable(false);

  nullMenu.setText("Hurr");
  append(nullMenu);

  layout.setMargin(5);
  applyButton.setText("Apply Patch");
  fileButton.setText("Create File Patch");
  pathButton.setText("Create Folder Patch");
  optionsLabel.setText("Patch creation options:");
  manifestFlag.setText("Embed manifest");
  deltaFlag.setText("Delta mode");

  append(layout);
  layout.append(actionLayout, {0, 0}, 5);
    actionLayout.append(applyButton, {0, 0}, 5);
    actionLayout.append(fileButton, {0, 0}, 5);
    actionLayout.append(pathButton, {0, 0}, 0);
  layout.append(controlLayout, {0, 0});
    controlLayout.append(optionsLabel, {0, 0}, 5);
    controlLayout.append(manifestFlag, {0, 0}, 5);
    controlLayout.append(deltaFlag, {0, 0});

  applyButton.onActivate = [=] { };
  fileButton.onActivate = [=] { };
  pathButton.onActivate = [=] { };

  Size desktopSize = Desktop::size();
  Size windowSize = layout.minimumSize();
  Position center = {
    (desktopSize.width  - windowSize.width ) / 2,
    (desktopSize.height - windowSize.height) / 2
  };
  setSmartGeometry({center, windowSize});

  onClose = [&] {
    setVisible(false);
    if(Intrinsics::platform() != Intrinsics::Platform::OSX) {
      Application::quit();
    }
  };

  setVisible();
  setMenuVisible();
  setFocused();
}
