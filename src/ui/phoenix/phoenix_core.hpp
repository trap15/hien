#ifndef _UI_PHOENIX_CORE_HPP_
#define _UI_PHOENIX_CORE_HPP_

extern "C" int phoenix_main(int argc, char** argv);

namespace HienUI {
  struct Program {
    Program(int argc, char** argv);
    ~Program();

    bool initialize();
    void activate();

    void load_board(hien_board_desc_t* brd);

    void change_song(int id);
    void stop_song();

    void update_ui();
    void main();

    hien_vm_t* vm;

    int cur_song;
  };
}

#endif
