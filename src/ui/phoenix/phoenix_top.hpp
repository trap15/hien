#ifndef _UI_PHOENIX_TOP_HPP_
#define _UI_PHOENIX_TOP_HPP_

const static string program_name = "Hien";

namespace HienUI {
  struct Program;
  struct MainWindow;
}

extern "C" {
#define new _new
#define delete _delete
#define KUSO_NO_MEM_OVERRIDE 1
#define NO_FILE_OVERRIDE 1
#include "hien.h"
#undef new
#undef delete
}

#include "phoenix_core.hpp"
#include "main_window.hpp"

#endif
