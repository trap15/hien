/**********************************************************
*
* phoenix_c.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"
#include <unistd.h>

int phoenix_main(int argc, char** argv);

void ui_init(hien_vm_t* vm)
{
  (void)vm;
}

void ui_finish(hien_vm_t* vm)
{
  (void)vm;
}

void ui_run(hien_vm_t* vm)
{
  (void)vm;
}

int main(int argc, char** argv)
{
  int ret;
  _save_argc = argc;
  _save_argv = argv;

  printf("Hien (c)2013-2014 Alex 'trap15' Marshall\n");
  printf("All rights reserved.\n\n");

  mem_init();

  ret = phoenix_main(argc, argv);

  mem_finish();

  return ret;
}

