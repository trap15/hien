/**********************************************************
*
* prof_view.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

#include <unistd.h>

#define TABWIDTH 2
static void print_tab(int level)
{
  printf("%*s", TABWIDTH*level, " ");
}

void cpuprof_print_sub(hien_cpuprof_t* prof, hien_cpuprof_sub_t* sub, int indent, int space)
{
  uint64_t avg_cyc, avg_insn, avg_byte;

  avg_cyc = sub->cycles / sub->count;
  avg_insn = sub->insns / sub->count;
  avg_byte = sub->bytes / sub->count;

  print_tab(indent); printf(" %12llu | %12llu | %12llu |", sub->cycles, sub->insns, sub->bytes);
                     printf(" %12llu | %12llu | %12llu |", avg_cyc, avg_insn, avg_byte);
                     printf(" %5llu | 0x%08X | ", sub->count, sub->start);
  print_tab(space);  printf("%s\n", sub->name);

  hien_cpuprof_sub_t** iter = kuso_pvec_iter(sub->subs, hien_cpuprof_sub_t*);
  for(; *iter != NULL; iter++) {
    cpuprof_print_sub(prof, *iter, indent, space+1);
  }
}

void cpuprof_print(hien_cpuprof_t* prof)
{
  uint32_t i;
  printf("Profiler for %s\n", prof->devname);
  for(i = 0; i < prof->modecnt; i++) {
    print_tab(1); printf("Mode %u:\n", i);
    print_tab(2); printf("Calls       : %llu\n", prof->calls[i]);
    print_tab(2); printf("Cycles      : %llu\n", prof->cycles[i]);
    print_tab(2); printf("Instructions: %llu\n", prof->insns[i]);
    print_tab(2); printf("Bytes       : %llu\n", prof->bytes[i]);
    print_tab(2); printf("Subroutine Information:\n");
    print_tab(3); printf("    Cycles    | Instructions |     Bytes    |  Avg.Cycles  | Avg.Instrucs |   Avg.Bytes  | Calls |   Address  | Name \n");
    print_tab(2); printf("------------------------------------------------------------------------------------------------------------------------------------------\n");
    cpuprof_print_sub(prof, prof->modes[i], 3, 0);
  }
}

int main(int argc, char** argv)
{
  hien_cpuprof_t* prof;
  hien_file_t* fp;
  printf("Hien Profiler Viewer (c)2013-2014 Alex 'trap15' Marshall\n");
  printf("All rights reserved.\n\n");

  if(argc == 1) {
    fprintf(stderr, "Usage:\n\t%s [file.prof]\n", argv[0]);
    return EXIT_FAILURE;
  }

  mem_init();

  fp = file_open(argv[1], FILE_READ);
  prof = cpuprof_new(NULL,NULL,1);
  cpuprof_read(prof, fp);
  cpuprof_print(prof);
  cpuprof_delete(prof);
  file_close(fp);

  mem_finish();

  return EXIT_SUCCESS;
}
