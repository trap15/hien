/**********************************************************
*
* dummyosd.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

void ui_init(hien_vm_t* vm)
{
  (void)vm;
}

void ui_finish(hien_vm_t* vm)
{
  (void)vm;
}

void osd_snd_init(hien_vm_t* vm)
{
  (void)vm;
}

void osd_snd_finish(hien_vm_t* vm)
{
  (void)vm;
}

void osd_snd_connect_outputs(hien_vm_t* vm)
{
  (void)vm;
}

void osd_snd_disconnect_outputs(hien_vm_t* vm)
{
  (void)vm;
}

int osd_snd_need_more_samples(hien_vm_t* vm)
{
  (void)vm;
  return 0;
}

void osd_snd_change_settings(hien_vm_t* vm)
{
  (void)vm;
}
