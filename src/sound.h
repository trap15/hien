/**********************************************************
*
* sound.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _SOUND_H_
#define _SOUND_H_

/* Typedefs */
typedef int32_t hien_sample_t;
#define HIEN_SAMPLE_MAX ((hien_sample_t) 0x7FFFFFFF)
#define HIEN_SAMPLE_MIN ((hien_sample_t)-0x80000000)

typedef struct hien_sound_t hien_sound_t;
typedef struct hien_sound_out_t hien_sound_out_t;
typedef struct hien_sound_mgr_t hien_sound_mgr_t;

/* Individual sound sources */
struct hien_sound_t {
  hien_sound_out_t* out;

  double vol;
  hien_sample_t smp;
};

hien_sound_t* sound_new(void);
void sound_delete(hien_sound_t* snd);
void sound_add_sample(hien_sound_t* snd, hien_sample_t smp);
void sound_set_volume(hien_sound_t* snd, double vol);

/* Sound output unit (such as speaker) */
struct hien_sound_out_t {
  hien_sound_mgr_t* mgr;

  kuso_pvec_t* snds; /* hien_sound_t* */
  kuso_fifo_t* smps;

  hien_file_t* logfp;
};

hien_sound_out_t* sound_out_new(void);
void sound_out_delete(hien_sound_out_t* out);
void sound_out_add(hien_sound_out_t* out, hien_sound_t* snd);
void sound_out_remove(hien_sound_out_t* out, hien_sound_t* snd);
void sound_out_openlog(hien_sound_out_t* out, char* fn);
void sound_out_closelog(hien_sound_out_t* out);

/* Sound manager */
struct hien_sound_mgr_t {
  hien_vm_t* vm;

  kuso_pvec_t* outs; /* hien_sound_out_t* */

  hz_t rate;
  timetick_t limit;
  hien_timer_t* utmr;
};

hien_sound_mgr_t* sound_mgr_new(hien_vm_t* vm, hz_t rate);
void sound_mgr_delete(hien_sound_mgr_t* mgr);
void sound_mgr_add(hien_sound_mgr_t* mgr, hien_sound_out_t* out);
void sound_mgr_remove(hien_sound_mgr_t* mgr, hien_sound_out_t* out);

#endif

