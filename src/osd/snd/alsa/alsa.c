/**********************************************************
*
* alsa.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

#include <poll.h>
#include <alsa/asoundlib.h>
#include <signal.h>
#include <time.h>

#define ALSAPATH "default"
#define SIGAUDIO (SIGRTMIN+1)

typedef struct {
  /* Config */
  unsigned int bufsz;
  unsigned int chans;
  unsigned int rate;

  /* State */
  hien_vm_t* vm;
  int stopped;
  int16_t* smpbuf;

  /* ALSA */
  snd_pcm_t* hnd;
  snd_pcm_hw_params_t* hwp;
  snd_pcm_sw_params_t* swp;

  /* Timers */
  timer_t tmr_id;
} osdsnd_alsa_t;

static int _osd_alsa_inited = 0;
static osdsnd_alsa_t* _g_osdalsa = NULL; /* ONLY USE THIS in the signal callback!!!! */

static inline int16_t _osd_alsa_pull_sample(osdsnd_alsa_t* osd, hien_vm_t* vm, int ch)
{
  (void)osd;
  hien_sound_out_t* out;
  hien_sample_t smp;
  out = kuso_pvec_get(vm->sndmgr->outs, ch);
  smp = kuso_fifo_pull(out->smps);
  smp /= 0x10000;
  if(smp >= 0x8000) smp = 0x7FFF;
  if(smp < -0x8000) smp =-0x8000;
  return smp;
}

static void _osd_alsa_irq(int sig, siginfo_t* si, void* uc)
{
  int err;
  osdsnd_alsa_t* alsa = _g_osdalsa;
  snd_pcm_sframes_t frames, f;
  size_t s, c;

  (void)sig;
  (void)si;
  (void)uc;

  if(alsa->stopped)
    return;

  if((err = snd_pcm_wait(alsa->hnd, 0)) < 0) {
    fprintf(stderr, "poll failed (%s)\n", strerror(errno));
    return;
  }

  if((frames = snd_pcm_avail_update(alsa->hnd)) < 0) {
    if(frames == -EPIPE) {
      fprintf(stderr, "xrun occured\n");
    }else{
      fprintf(stderr, "unknown ALSA avail update return value (%d)\n", (int)frames);
    }
  }

  if(frames > alsa->bufsz) frames = alsa->bufsz;

  for(s=0, f = 0; f < frames; f++) {
    for(c = 0; c < alsa->chans; c++, s++) {
      alsa->smpbuf[s] = _osd_alsa_pull_sample(alsa, alsa->vm, c);
    }
  }

  if((err = snd_pcm_writei(alsa->hnd, alsa->smpbuf, frames)) < 0) {
    fprintf(stderr, "write failed (%s)\n", snd_strerror(err));
  }
}

static void _osd_alsa_apply_hw(osdsnd_alsa_t* alsa)
{
  int err;

  unsigned int rate = alsa->rate;
  _g_osdalsa = alsa;

  if((err = snd_pcm_hw_params_set_access(alsa->hnd, alsa->hwp, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) {
    fprintf(stderr, "cannot set access type (%s)\n", snd_strerror(err));
    exit(1);
  }
  if((err = snd_pcm_hw_params_set_format(alsa->hnd, alsa->hwp, SND_PCM_FORMAT_S16)) < 0) {
    fprintf(stderr, "cannot set sample format (%s)\n", snd_strerror(err));
    exit(1);
  }
  if((err = snd_pcm_hw_params_set_rate(alsa->hnd, alsa->hwp, rate, 0)) < 0) {
    fprintf(stderr, "cannot set sample rate (%s)\n", snd_strerror(err));
    exit(1);
  }
  if((err = snd_pcm_hw_params_set_channels(alsa->hnd, alsa->hwp, alsa->chans)) < 0) {
    fprintf(stderr, "cannot set channel count (%s)\n", snd_strerror(err));
    exit(1);
  }
  if((err = snd_pcm_hw_params(alsa->hnd, alsa->hwp)) < 0) {
    fprintf(stderr, "cannot set parameters (%s)\n", snd_strerror(err));
    exit(1);
  }
}

static void _osd_alsa_apply_sw(osdsnd_alsa_t* alsa)
{
  int err;

  if((err = snd_pcm_sw_params_set_avail_min(alsa->hnd, alsa->swp, alsa->bufsz)) < 0) {
    fprintf(stderr, "cannot set minimum available count (%s)\n", snd_strerror(err));
    exit(1);
  }
  if((err = snd_pcm_sw_params_set_start_threshold(alsa->hnd, alsa->swp, 0U)) < 0) {
    fprintf(stderr, "cannot set start mode (%s)\n", snd_strerror(err));
    exit(1);
  }
  if((err = snd_pcm_sw_params(alsa->hnd, alsa->swp)) < 0) {
    fprintf(stderr, "cannot set software parameters (%s)\n", snd_strerror(err));
    exit(1);
  }

  if((err = snd_pcm_prepare(alsa->hnd)) < 0) {
    fprintf(stderr, "cannot prepare audio interface for use (%s)\n", snd_strerror(err));
    exit(1);
  }
}

static void _osd_alsa_apply(osdsnd_alsa_t* alsa)
{
  _osd_alsa_apply_hw(alsa);
  _osd_alsa_apply_sw(alsa);
}

void osd_snd_init(hien_vm_t* vm)
{
  int err;
  osdsnd_alsa_t* alsa;

  if(_osd_alsa_inited) {
    fprintf(stderr, "alsa osd already initialized once; does not support double init");
    exit(1);
  }

  _osd_alsa_inited = 1;

  alsa = mem_alloc_type(osdsnd_alsa_t);
  vm->osdsnd_data = alsa;
  alsa->vm = vm;

  alsa->bufsz = vm->config->snd.buflen;
  alsa->chans = 1;
  alsa->rate = vm->config->snd.rate ?: 44100;
  alsa->stopped = 1;
  alsa->smpbuf = mem_alloc_cnt_type(alsa->bufsz * alsa->chans, int16_t);

  /* Set up ALSA things */
  if((err = snd_pcm_open(&alsa->hnd, ALSAPATH, SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
    fprintf(stderr, "cannot open audio device %s (%s)\n", ALSAPATH, snd_strerror(err));
    exit(1);
  }

  if((err = snd_pcm_hw_params_malloc(&alsa->hwp)) < 0) {
    fprintf(stderr, "cannot allocate hardware parameter structure (%s)\n", snd_strerror(err));
    exit(1);
  }
  if((err = snd_pcm_hw_params_any(alsa->hnd, alsa->hwp)) < 0) {
    fprintf(stderr, "cannot initialize hardware parameter structure (%s)\n", snd_strerror(err));
    exit(1);
  }

  _osd_alsa_apply_hw(alsa);

  if((err = snd_pcm_sw_params_malloc(&alsa->swp)) < 0) {
    fprintf(stderr, "cannot allocate software parameters structure (%s)\n", snd_strerror(err));
    exit(1);
  }
  if((err = snd_pcm_sw_params_current(alsa->hnd, alsa->swp)) < 0) {
    fprintf(stderr, "cannot initialize software parameters structure (%s)\n", snd_strerror(err));
    exit(1);
  }

  _osd_alsa_apply_sw(alsa);

  /* Setup timer with UNIX signals for filling the playback buffer */
  struct sigaction siga;
  struct itimerspec tmr_spec;
  struct sigevent sigev;

  siga.sa_flags = SA_SIGINFO;
  siga.sa_sigaction = _osd_alsa_irq;
  sigemptyset(&siga.sa_mask);
  if(sigaction(SIGAUDIO, &siga, NULL) == -1) {
    fprintf(stderr, "sigaction failed\n");
    exit(1);
  }

  /* Create the timer */
  memset(&sigev, 0, sizeof(struct sigevent));
  sigev.sigev_notify = SIGEV_SIGNAL;
  sigev.sigev_signo = SIGAUDIO;
  sigev.sigev_value.sival_ptr = &alsa->tmr_id;
  if(timer_create(CLOCK_REALTIME, &sigev, &alsa->tmr_id) == -1) {
    fprintf(stderr, "timer_create failed\n");
    exit(1);
  }

  /* Set the length and go */
  tmr_spec.it_value.tv_sec = 0;
  tmr_spec.it_value.tv_nsec = alsa->bufsz * 1000000000 / alsa->rate;
  tmr_spec.it_interval.tv_sec = tmr_spec.it_value.tv_sec;
  tmr_spec.it_interval.tv_nsec = tmr_spec.it_value.tv_nsec;
  if(timer_settime(alsa->tmr_id, 0, &tmr_spec, NULL) == -1) {
    fprintf(stderr, "timer_settime failed\n");
    exit(1);
  }
}

void osd_snd_finish(hien_vm_t* vm)
{
  osdsnd_alsa_t* alsa;
  alsa = vm->osdsnd_data;

  alsa->stopped = 1;

  if(timer_delete(alsa->tmr_id) == -1) {
    fprintf(stderr, "timer_delete failed\n");
  }

  snd_pcm_close(alsa->hnd);
  if(alsa->smpbuf != NULL) {
    mem_free(alsa->smpbuf);
    alsa->smpbuf = NULL;
  }

  mem_free(vm->osdsnd_data);
  vm->osdsnd_data = NULL;

  _osd_alsa_inited = 0;
}

void osd_snd_connect_outputs(hien_vm_t* vm)
{
  osdsnd_alsa_t* alsa;
  alsa = vm->osdsnd_data;

  alsa->chans = kuso_pvec_size(vm->sndmgr->outs);
  alsa->bufsz = vm->config->snd.buflen;
  if(alsa->smpbuf != NULL)
    mem_free(alsa->smpbuf);
  alsa->smpbuf = mem_alloc_cnt_type(alsa->bufsz * alsa->chans, int16_t);

  _osd_alsa_apply(alsa);

  alsa->stopped = 0;
}

void osd_snd_disconnect_outputs(hien_vm_t* vm)
{
  osdsnd_alsa_t* alsa;
  alsa = vm->osdsnd_data;

  alsa->stopped = 1;
  if(alsa->smpbuf != NULL) {
    mem_free(alsa->smpbuf);
    alsa->smpbuf = NULL;
  }
}

int osd_snd_need_more_samples(hien_vm_t* vm)
{
  osdsnd_alsa_t* alsa;

  alsa = vm->osdsnd_data;

  hien_sound_out_t** iter = kuso_pvec_iter(vm->sndmgr->outs, hien_sound_out_t*);
  for(; *iter != NULL; iter++) {
    if(kuso_fifo_count((*iter)->smps) < alsa->bufsz)
      return 1;
  }

  return 0;
}

void osd_snd_change_settings(hien_vm_t* vm)
{
  (void)vm;
}
