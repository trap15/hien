/**********************************************************
*
* file.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

void osd_snd_init(hien_vm_t* vm)
{
  (void)vm;
}

void osd_snd_finish(hien_vm_t* vm)
{
  (void)vm;
}

void osd_snd_connect_outputs(hien_vm_t* vm)
{
  char fname[256];
  size_t i;
  hien_sound_out_t** iter = kuso_pvec_iter(vm->sndmgr->outs, hien_sound_out_t*);
  for(i = 0; *iter != NULL; iter++, i++) {
    snprintf(fname, 256, "output%lu.wav", i);
    sound_out_openlog(*iter, fname);
  }
}

void osd_snd_disconnect_outputs(hien_vm_t* vm)
{
  hien_sound_out_t** iter = kuso_pvec_iter(vm->sndmgr->outs, hien_sound_out_t*);
  for(; *iter != NULL; iter++) {
    sound_out_closelog(*iter);
  }
}

int osd_snd_need_more_samples(hien_vm_t* vm)
{
  (void)vm;
  /* Unlocked for maximum speed */
  return 1;
}

void osd_snd_change_settings(hien_vm_t* vm)
{
  (void)vm;
}
