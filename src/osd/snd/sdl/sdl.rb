addMainObj("sdl.o")

if $osd_snd == "sdl"
  $cflags += " `sdl-config --cflags`"
  $ldflags += " `sdl-config --static-libs`"
elsif $osd_snd == "sdl2"
  $libs += " -lSDL2"
  $cflags += " `sdl2-config --cflags`"
  $ldflags += " `sdl2-config --static-libs`"
end
