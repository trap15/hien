/**********************************************************
*
* sdl.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#define DECLSPEC
#define SDLCALL
#include <SDL.h>

#include "hien.h"

typedef struct {
  /* Config */
  unsigned int bufsz;
  unsigned int chans;
  unsigned int rate;

  /* State */
  hien_vm_t* vm;
  int16_t* smpbuf;

} osdsnd_sdl_t;

static inline int16_t _osd_sdl_pull_sample(osdsnd_sdl_t* osd, hien_vm_t* vm, int ch)
{
  (void)osd;
  hien_sound_out_t* out;
  hien_sample_t smp;
  out = kuso_pvec_get(vm->sndmgr->outs, ch);
  smp = kuso_fifo_pull(out->smps);
  smp /= 0x10000;
  if(smp >= 0x8000) smp = 0x7FFF;
  if(smp < -0x8000) smp =-0x8000;
  return smp;
}

static void _osd_sdl_irq(void* udata, Uint8* stream, int length)
{
  size_t s, f, c, len;

  osdsnd_sdl_t* osd = udata;

  len = length/2;
  if(len > osd->bufsz) len = osd->bufsz;

  for(s=0, f = 0; f < len; f++) {
    for(c = 0; c < osd->chans; c++, s++) {
      osd->smpbuf[s] = _osd_sdl_pull_sample(osd, osd->vm, c);
    }
  }

  memcpy(stream, osd->smpbuf, length);
}

static void _osd_sdl_apply(osdsnd_sdl_t* osd)
{
  SDL_AudioSpec wanted;
  wanted.freq = osd->rate;
  wanted.format = AUDIO_S16;
  wanted.channels = osd->chans;
  wanted.samples = osd->bufsz;
  wanted.callback = _osd_sdl_irq;
  wanted.userdata = osd;

  if(SDL_OpenAudio(&wanted, NULL) < 0) {
    fprintf(stderr, "Couldn't open audio %s\n", SDL_GetError());
    exit(1);
  }
}

void osd_snd_init(hien_vm_t* vm)
{
  osdsnd_sdl_t* osd;

  osd = mem_alloc_type(osdsnd_sdl_t);
  vm->osdsnd_data = osd;
  osd->vm = vm;

  osd->bufsz = vm->config->snd.buflen;
  osd->chans = 1;
  osd->rate = vm->config->snd.rate;
  osd->smpbuf = mem_alloc_cnt_type(osd->bufsz * osd->chans, int16_t);

  _osd_sdl_apply(osd);
  SDL_PauseAudio(1);
}

void osd_snd_finish(hien_vm_t* vm)
{
  osdsnd_sdl_t* osd;
  osd = vm->osdsnd_data;

  SDL_PauseAudio(1);
  SDL_CloseAudio();

  if(osd->smpbuf != NULL) {
    mem_free(osd->smpbuf);
    osd->smpbuf = NULL;
  }

  mem_free(vm->osdsnd_data);
  vm->osdsnd_data = NULL;
}

void osd_snd_connect_outputs(hien_vm_t* vm)
{
  osdsnd_sdl_t* osd;
  osd = vm->osdsnd_data;

  osd->chans = kuso_pvec_size(vm->sndmgr->outs);
  osd->bufsz = vm->config->snd.buflen;
  if(osd->smpbuf != NULL)
    mem_free(osd->smpbuf);
  osd->smpbuf = mem_alloc_cnt_type(osd->bufsz * osd->chans, int16_t);

  SDL_PauseAudio(1);
  SDL_CloseAudio();
  _osd_sdl_apply(osd);
  SDL_PauseAudio(0);
}

void osd_snd_disconnect_outputs(hien_vm_t* vm)
{
  osdsnd_sdl_t* osd;
  osd = vm->osdsnd_data;

  SDL_PauseAudio(1);
  SDL_CloseAudio();

  if(osd->smpbuf != NULL) {
    mem_free(osd->smpbuf);
    osd->smpbuf = NULL;
  }
}

int osd_snd_need_more_samples(hien_vm_t* vm)
{
  osdsnd_sdl_t* osd;
  osd = vm->osdsnd_data;

  hien_sound_out_t** iter = kuso_pvec_iter(vm->sndmgr->outs, hien_sound_out_t*);
  for(; *iter != NULL; iter++) {
    if(kuso_fifo_count((*iter)->smps) < osd->bufsz*2)
      return 1;
  }

  return 0;
}

void osd_snd_change_settings(hien_vm_t* vm)
{
  (void)vm;
}
