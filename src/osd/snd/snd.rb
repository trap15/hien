if $osd_snd == "file"
  addModuleNoHdr("osd/snd", "file")
elsif $osd_snd == "sdl2"
  addModuleNoHdr("osd/snd", "sdl")
elsif $osd_snd == "sdl"
  addModuleNoHdr("osd/snd", "sdl")
elsif $osd_snd == "alsa"
  addModuleNoHdr("osd/snd", "alsa")
else
  abort("Bad OSD name "+$osd)
end
