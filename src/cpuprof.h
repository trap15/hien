/**********************************************************
*
* cpuprof.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _CPUPROF_H_
#define _CPUPROF_H_

typedef struct hien_cpuprof_t hien_cpuprof_t;
typedef struct hien_cpuprof_sub_t hien_cpuprof_sub_t;

struct hien_cpuprof_sub_t {
  hien_cpuprof_sub_t* parent; /* Calling sub */

  char* name; /* Probably just auto-gen for now */

  uint32_t mode; /* Execution mode */

  addr_t start; /* Address of this sub */
  uint64_t cycles; /* Total cycles spent in this sub */
  uint64_t insns; /* Total number of instructions executed */
  uint64_t bytes; /* Total number of bytes executed */

  uint64_t count; /* Times called */

  kuso_pvec_t* subs; /* Child subs (hien_cpuprof_sub_t*) */
};

struct hien_cpuprof_t {
  hien_vm_t* vm;
  struct hien_dev_t* dev;
  char* devname;

  int enable;
  int dirty;
  hien_file_t* fp;

  uint32_t modecnt;

  uint64_t* calls;
  uint64_t* cycles;
  uint64_t* insns;
  uint64_t* bytes;
  hien_cpuprof_sub_t** modes; /* Execution mode 'subs' */
  hien_cpuprof_sub_t** modesubs; /* Current subs for each mode */
  int curmode;
};

hien_cpuprof_t* cpuprof_new(hien_vm_t* vm, struct hien_dev_t* dev, int modecnt);
void cpuprof_delete(hien_cpuprof_t* prof);
void cpuprof_reset(hien_cpuprof_t* prof);

void cpuprof_enable(hien_cpuprof_t* prof, char* fn);
void cpuprof_disable(hien_cpuprof_t* prof);

void cpuprof_sub_start(hien_cpuprof_t* prof, addr_t pc, int mode);
void cpuprof_sub_cycle(hien_cpuprof_t* prof, addr_t pc, size_t cycles);
void cpuprof_sub_insn(hien_cpuprof_t* prof, addr_t pc, size_t insns);
void cpuprof_sub_byte(hien_cpuprof_t* prof, addr_t pc, size_t bytes);
void cpuprof_sub_end(hien_cpuprof_t* prof, addr_t pc);
void cpuprof_sub_setmode(hien_cpuprof_t* prof, int mode);

void cpuprof_write(hien_cpuprof_t* prof, hien_file_t* fp);
int cpuprof_read(hien_cpuprof_t* prof, hien_file_t* fp);

#endif
