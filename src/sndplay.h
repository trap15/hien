/**********************************************************
*
* sndplay.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _SNDPLAY_H_
#define _SNDPLAY_H_

/* Typedefs */
typedef struct hien_sndplay_t hien_sndplay_t;
typedef struct hien_sndplay_desc_t hien_sndplay_desc_t;
typedef struct hien_sndplay_cmd_t hien_sndplay_cmd_t;

struct hien_sndplay_cmd_t {
  int cmdtype;
  int subcmd;

  timetick_t time;
};

struct hien_sndplay_t {
  hien_vm_t* vm;

  hien_timer_t* delay_tmr;

  kuso_pvec_t* cmdbuf;

  hien_sndplay_desc_t* desc;
  void* data;
};

struct hien_sndplay_desc_t {
  void  (*reset)(hien_sndplay_t* sndp);
  void  (*play)(hien_sndplay_t* sndp, int sndid);
  void  (*stop)(hien_sndplay_t* sndp);

  int stop_id;
  int default_id;
  int min_id;
  int max_id;

  timetick_t boot_time; /* Time to boot */
  timetick_t chg_time; /* Time to switch songs */
};

hien_sndplay_t* sndplay_new(hien_vm_t* vm);
void sndplay_delete(hien_sndplay_t* sndp);

void sndplay_load(hien_sndplay_t* sndp, hien_sndplay_desc_t* desc);
void sndplay_unload(hien_sndplay_t* sndp);

void sndplay_reset(hien_sndplay_t* sndp);
void sndplay_play(hien_sndplay_t* sndp, int sndid);
void sndplay_stop(hien_sndplay_t* sndp);

int sndplay_waiting(hien_sndplay_t* sndp);

int sndplay_id_in_bounds(hien_sndplay_t* sndp, int next);

#endif

