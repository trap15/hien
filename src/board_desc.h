/**********************************************************
*
* board_desc.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _BOARD_DESC_H_
#define _BOARD_DESC_H_

typedef struct hien_board_desc_t hien_board_desc_t;

struct hien_board_desc_t {
  char* name;
  char* year;
  char* mfg; /* Manufacturer/Distributer */

  void* (*new)(hien_board_t* brd);
  void  (*delete)(hien_board_t* brd);
  void  (*reset)(hien_board_t* brd);
  kuso_pvec_t* (*load)(hien_board_t* brd);
  kuso_pvec_t* (*unload)(hien_board_t* brd);

  int save_supp; /* 0 = no, 1 = yes, -1 = partial */

  hien_sndplay_desc_t* sndplay;
};

hien_board_desc_t* board_desc_generate(hien_vm_t* vm);

#endif
