/**********************************************************
*
* devinfo.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

const hien_dev_info_t* devinfo_find(char* type)
{
  size_t i;
  if(type == NULL)
    return NULL;

  for(i = 0; i < devlistcnt; i++) {
    if(strcmp(devlist[i]->type, type) == 0)
      return devlist[i];
  }

  return NULL;
}
