/**********************************************************
*
* osd.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _OSD_H_
#define _OSD_H_

/***** SOUND *****/
void osd_snd_init(hien_vm_t* vm);
void osd_snd_finish(hien_vm_t* vm);
void osd_snd_connect_outputs(hien_vm_t* vm);
void osd_snd_disconnect_outputs(hien_vm_t* vm);
int osd_snd_need_more_samples(hien_vm_t* vm);
void osd_snd_change_settings(hien_vm_t* vm);

#endif
