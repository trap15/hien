/**********************************************************
*
* board.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _BOARD_H_
#define _BOARD_H_

#define CRCUNKNOWN -1

struct hien_board_t {
  hien_vm_t* vm;

  kuso_pvec_t* devs;

  hien_board_desc_t* desc;
  void* data;
};

hien_board_t* board_new(hien_vm_t* vm);
void board_delete(hien_board_t* brd);
void board_reset(hien_board_t* brd);

void board_load(hien_board_t* brd, hien_board_desc_t* desc);
void board_unload(hien_board_t* brd);

#endif
