/**********************************************************
*
* file.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#define NO_FILE_OVERRIDE

#include "hien.h"
#include <wordexp.h>
#include <errno.h>
#include <sys/stat.h>

char* file_expand_path(char* path)
{
  wordexp_t p;
  char* str = NULL;

  switch(wordexp(path, &p, 0)) {
    case 0:
      if((p.we_wordv != NULL) && (p.we_wordc != 0))
        break;
      /* fall-thru */
    default:
      goto _done;
  }

  if(p.we_wordv[0] != NULL)
    str = mem_strdup(p.we_wordv[0]);

  wordfree(&p);

_done:
  return str;
}

static int file_try_open(hien_file_t* fp, char* fn, int flags)
{
  int ret = TRUE;
  char* open_str;
  switch(flags & FILE_READWRITE) {
    case FILE_READ:      open_str = "rb"; break;
    case FILE_WRITE:     open_str = "wb+"; break;
    default:
    case FILE_READWRITE: open_str = "rb+"; break;
  }

  fp->usepath = mem_strdup(fn);
  fp->fp = fopen(fp->usepath, open_str);
  if(fp->fp == NULL) {
    mem_free(fp->usepath);
    fp->usepath = NULL;
    ret = FALSE;
  }

  return ret;
}

static int file_make_path(char* fn, int boff)
{
  char* path;
  char* nfn = NULL;
  char delimchr[] = "/\\";
  size_t i;
  ssize_t off;
  int mret;
  int ret = FALSE;

  for(i = 0; i < (sizeof(delimchr) / sizeof(char)); i++) {
    path = strchr(fn+boff, delimchr[i]);
    if(path != NULL)
      break;
  }
  if(path == NULL)
    goto _done;
  if(path != fn+strlen(fn)) {
    off = path - fn;
    if(path != fn) {
      nfn = mem_strdup(fn);
      nfn[off] = '\0';

      if(mkdir(nfn, S_IRWXU|S_IRGRP|S_IROTH) == -1)
        mret = errno;
      else
        mret = EEXIST;

      mem_free(nfn);
      nfn = NULL;

      if(errno != EEXIST)
        goto _done;
    }
    file_make_path(fn, off + 1);
  }
  ret = TRUE;

_done:
  if(nfn != NULL)
    mem_free(nfn);
  return ret;
}

hien_file_t* file_open(char* fn, int flags)
{
  hien_file_t* fp;

  fp = mem_alloc_type(hien_file_t);

  if(file_try_open(fp, fn, flags))
    goto _done;

  if((flags & FILE_MKDIR) && (flags & FILE_WRITE)) {
    if(file_make_path(fn, 0)) {
      if(file_try_open(fp, fn, flags))
        goto _done;
    }
  }

  hienlog(NULL,LOG_CRIT, "File open fail %s\n", fn);

  file_close(fp);
  fp = NULL;

_done:
  return fp;
}

void file_close(hien_file_t* fp)
{
  if(fp->usepath != NULL)
    mem_free(fp->usepath);
  if(fp->fp != NULL)
    fclose(fp->fp);
  mem_free(fp);
}

size_t file_read(void* buf, size_t size, hien_file_t* fp)
{
  return fread(buf, 1, size, fp->fp);
}

size_t file_write(void* buf, size_t size, hien_file_t* fp)
{
  return fwrite(buf, 1, size, fp->fp);
}

void file_seek(hien_file_t* fp, int whence, size_t offset)
{
  switch(whence) {
    case FILE_START:
      fseek(fp->fp, offset, SEEK_SET);
      break;
    case FILE_CUR:
      fseek(fp->fp, offset, SEEK_CUR);
      break;
    case FILE_END:
      fseek(fp->fp, offset, SEEK_END);
      break;
  }
}

size_t file_tell(hien_file_t* fp)
{
  return ftell(fp->fp);
}

size_t file_size(hien_file_t* fp)
{
  size_t opos, size;
  opos = file_tell(fp);
  file_seek(fp, FILE_END, 0);
  size = file_tell(fp);
  file_seek(fp, FILE_START, opos);
  return size;
}

size_t file_write8(uint8_t data, hien_file_t* fp)
{
  return file_write(&data, 1, fp);
}
size_t file_write16(uint16_t data, hien_file_t* fp)
{
  return file_write16B(data, fp);
}
size_t file_write32(uint32_t data, hien_file_t* fp)
{
  return file_write32B(data, fp);
}
size_t file_write64(uint64_t data, hien_file_t* fp)
{
  return file_write64B(data, fp);
}
size_t file_read8(uint8_t* data, hien_file_t* fp)
{
  return file_read(data, 1, fp);
}
size_t file_read16(uint16_t* data, hien_file_t* fp)
{
  return file_read16B(data, fp);
}
size_t file_read32(uint32_t* data, hien_file_t* fp)
{
  return file_read32B(data, fp);
}
size_t file_read64(uint64_t* data, hien_file_t* fp)
{
  return file_read64B(data, fp);
}

size_t file_write8B(uint8_t data, hien_file_t* fp)
{
  return file_write8(data, fp);
}
size_t file_write16B(uint16_t data, hien_file_t* fp)
{
  size_t ret = 0;
  ret += file_write8B(data>>8, fp);
  ret += file_write8B(data>>0, fp);
  return ret;
}
size_t file_write32B(uint32_t data, hien_file_t* fp)
{
  size_t ret = 0;
  ret += file_write16B(data>>16, fp);
  ret += file_write16B(data>>0, fp);
  return ret;
}
size_t file_write64B(uint64_t data, hien_file_t* fp)
{
  size_t ret = 0;
  ret += file_write32B(data>>32, fp);
  ret += file_write32B(data>>0, fp);
  return ret;
}
size_t file_read8B(uint8_t* data, hien_file_t* fp)
{
  return file_read8(data, fp);
}
size_t file_read16B(uint16_t* data, hien_file_t* fp)
{
  uint8_t raw[2];
  size_t ret = 0;
  ret += file_read8B(&raw[0], fp);
  ret += file_read8B(&raw[1], fp);
  *data = (raw[0] << 8) | raw[1];
  return ret;
}
size_t file_read32B(uint32_t* data, hien_file_t* fp)
{
  uint16_t raw[2];
  size_t ret = 0;
  ret += file_read16B(&raw[0], fp);
  ret += file_read16B(&raw[1], fp);
  *data = (raw[0] << 16) | raw[1];
  return ret;
}
size_t file_read64B(uint64_t* data, hien_file_t* fp)
{
  uint32_t raw[2];
  size_t ret = 0;
  ret += file_read32B(&raw[0], fp);
  ret += file_read32B(&raw[1], fp);
  *data = ((uint64_t)raw[0] << 32) | raw[1];
  return ret;
}

size_t file_write8L(uint8_t data, hien_file_t* fp)
{
  return file_write8(data, fp);
}
size_t file_write16L(uint16_t data, hien_file_t* fp)
{
  size_t ret = 0;
  ret += file_write8L(data>>0, fp);
  ret += file_write8L(data>>8, fp);
  return ret;
}
size_t file_write32L(uint32_t data, hien_file_t* fp)
{
  size_t ret = 0;
  ret += file_write16L(data>>0, fp);
  ret += file_write16L(data>>16, fp);
  return ret;
}
size_t file_write64L(uint64_t data, hien_file_t* fp)
{
  size_t ret = 0;
  ret += file_write32L(data>>0, fp);
  ret += file_write32L(data>>32, fp);
  return ret;
}
size_t file_read8L(uint8_t* data, hien_file_t* fp)
{
  return file_read8(data, fp);
}
size_t file_read16L(uint16_t* data, hien_file_t* fp)
{
  uint8_t raw[2];
  size_t ret = 0;
  ret += file_read8L(&raw[1], fp);
  ret += file_read8L(&raw[0], fp);
  *data = (raw[0] << 8) | raw[1];
  return ret;
}
size_t file_read32L(uint32_t* data, hien_file_t* fp)
{
  uint16_t raw[2];
  size_t ret = 0;
  ret += file_read16L(&raw[1], fp);
  ret += file_read16L(&raw[0], fp);
  *data = (raw[0] << 16) | raw[1];
  return ret;
}
size_t file_read64L(uint64_t* data, hien_file_t* fp)
{
  uint32_t raw[2];
  size_t ret = 0;
  ret += file_read32L(&raw[1], fp);
  ret += file_read32L(&raw[0], fp);
  *data = ((uint64_t)raw[0] << 32) | raw[1];
  return ret;
}


FILE* _yes_I_really_know_what_Im_doing_fopen(const char* path, const char* mode)
{
  return fopen(path, mode);
}

size_t _yes_I_really_know_what_Im_doing_fread(void* ptr, size_t size, size_t count, FILE* fp)
{
  return fread(ptr, size, count, fp);
}

size_t _yes_I_really_know_what_Im_doing_fwrite(void* ptr, size_t size, size_t count, FILE* fp)
{
  return fwrite(ptr, size, count, fp);
}

int _yes_I_really_know_what_Im_doing_fseek(FILE* fp, long int offset, int origin)
{
  return fseek(fp, offset, origin);
}

int _yes_I_really_know_what_Im_doing_fclose(FILE* fp)
{
  return fclose(fp);
}

