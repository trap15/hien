/**********************************************************
*
* cpuprof.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

#if CPU_PROFILER
static int cpuprof_can_profile(hien_cpuprof_t* prof)
{
  if(prof == NULL)
    return FALSE;
  return prof->enable;
}

static hien_cpuprof_sub_t* cpuprof_new_sub(hien_cpuprof_t* prof, hien_cpuprof_sub_t* parent, addr_t pc, int mode, char* name)
{
  hien_cpuprof_sub_t* sub;
  (void)prof;
  sub = mem_alloc_type(hien_cpuprof_sub_t);

  sub->parent = parent;
  if(name != NULL)
    sub->name = mem_strdup(name);
  sub->start = pc;
  sub->cycles = 0;
  sub->insns = 0;
  sub->bytes = 0;
  sub->count = 0;
  sub->mode = mode;
  sub->subs = kuso_pvec_new(0);

  if(parent != NULL) {
    kuso_pvec_append(parent->subs, sub);
  }

  return sub;
}

static void cpuprof_delete_sub(hien_cpuprof_t* prof, hien_cpuprof_sub_t* sub, int mode)
{
  hien_cpuprof_sub_t** iter = kuso_pvec_iter(sub->subs, hien_cpuprof_sub_t*);
  for(; *iter != NULL; iter++) {
    cpuprof_delete_sub(prof, *iter, mode);
  }
  kuso_pvec_delete(sub->subs);
  if(prof->modesubs[mode] == sub) {
    prof->modesubs[mode] = sub->parent;
  }
  if(sub->name != NULL)
    mem_free(sub->name);
  mem_free(sub);
}

static void cpuprof_new_core(hien_cpuprof_t* prof, hien_vm_t* vm, hien_dev_t* dev, int modecnt)
{
  uint32_t i;
  char* name;

  prof->vm = vm;
  prof->dev = dev;
  if(dev != NULL)
    prof->devname = mem_strdup(prof->dev->name);

  prof->curmode = 0;
  prof->enable = FALSE;
  prof->dirty = FALSE;
  prof->fp = NULL;
  prof->modecnt = modecnt;
  prof->modes = mem_alloc_cnt_type(modecnt, hien_cpuprof_sub_t*);
  prof->modesubs = mem_alloc_cnt_type(modecnt, hien_cpuprof_sub_t*);
  prof->calls = mem_alloc_cnt_type(modecnt, uint64_t);
  prof->cycles = mem_alloc_cnt_type(modecnt, uint64_t);
  prof->insns = mem_alloc_cnt_type(modecnt, uint64_t);
  prof->bytes = mem_alloc_cnt_type(modecnt, uint64_t);

  for(i = 0; i < prof->modecnt; i++) {
    name = NULL;
    kuso_asprintf(&name, "Mode %d Top", i);
    prof->modes[i] = cpuprof_new_sub(prof, NULL, 0, i, name);
    mem_free(name);

    prof->modesubs[i] = prof->modes[i];
    prof->modesubs[i]->count = 1;
    prof->calls[i] = 0;
    prof->cycles[i] = 0;
    prof->insns[i] = 0;
    prof->bytes[i] = 0;
  }
}

static void cpuprof_delete_core(hien_cpuprof_t* prof)
{
  uint32_t i;

  if(prof->dirty && prof->fp) {
    cpuprof_write(prof, prof->fp);
    prof->dirty = FALSE;
  }

  if(prof->fp) {
    file_close(prof->fp);
    prof->fp = NULL;
  }

  for(i = 0; i < prof->modecnt; i++) {
    cpuprof_delete_sub(prof, prof->modes[i], i);
    prof->modesubs[i] = NULL;
  }
  mem_free(prof->calls);
  mem_free(prof->cycles);
  mem_free(prof->insns);
  mem_free(prof->bytes);
  mem_free(prof->modes);
  mem_free(prof->modesubs);
  mem_free(prof->devname);
}

hien_cpuprof_t* cpuprof_new(hien_vm_t* vm, hien_dev_t* dev, int modecnt)
{
  hien_cpuprof_t* prof;

  prof = mem_alloc_type(hien_cpuprof_t);

  cpuprof_new_core(prof, vm, dev, modecnt);

  return prof;
}

void cpuprof_delete(hien_cpuprof_t* prof)
{
  if(prof == NULL)
    return;

  cpuprof_delete_core(prof);
  mem_free(prof);
}

void cpuprof_reset(hien_cpuprof_t* prof)
{
  if(prof == NULL)
    return;

  hien_vm_t* vm;
  hien_dev_t* dev;
  int modecnt;

  vm = prof->vm;
  dev = prof->dev;
  modecnt = prof->modecnt;

  cpuprof_delete_core(prof);
  cpuprof_new_core(prof, vm, dev, modecnt);
}

static hien_cpuprof_sub_t* cpuprof_sub_get(hien_cpuprof_t* prof, addr_t pc, int mode)
{
  char* name = NULL;
  hien_cpuprof_sub_t* sub;
  hien_cpuprof_sub_t** iter = kuso_pvec_iter(prof->modesubs[mode]->subs, hien_cpuprof_sub_t*);
  for(; *iter != NULL; iter++) {
    if((*iter)->start == pc)
      return *iter;
  }

  name = NULL;
  kuso_asprintf(&name, "sub_%X", pc);
  sub = cpuprof_new_sub(prof, prof->modesubs[mode], pc, mode, name);
  mem_free(name);
  return sub;
}

void cpuprof_sub_start(hien_cpuprof_t* prof, addr_t pc, int mode)
{
  if(!cpuprof_can_profile(prof))
    return;

  hien_cpuprof_sub_t* sub;
  if(mode == -1) mode = prof->curmode;

  sub = cpuprof_sub_get(prof, pc, mode);
  sub->count++;

  prof->modesubs[mode] = sub;
  prof->calls[mode]++;

  prof->dirty = TRUE;
}
void cpuprof_sub_cycle(hien_cpuprof_t* prof, addr_t pc, size_t cycles)
{
  if(!cpuprof_can_profile(prof))
    return;

  hien_cpuprof_sub_t* sub;
  (void)pc;
  sub = prof->modesubs[prof->curmode];

  sub->cycles += cycles;
  prof->cycles[prof->curmode] += cycles;

  prof->dirty = TRUE;
}
void cpuprof_sub_insn(hien_cpuprof_t* prof, addr_t pc, size_t insns)
{
  if(!cpuprof_can_profile(prof))
    return;

  hien_cpuprof_sub_t* sub;
  (void)pc;
  sub = prof->modesubs[prof->curmode];

  sub->insns += insns;
  prof->insns[prof->curmode] += insns;

  prof->dirty = TRUE;
}
void cpuprof_sub_byte(hien_cpuprof_t* prof, addr_t pc, size_t bytes)
{
  if(!cpuprof_can_profile(prof))
    return;

  hien_cpuprof_sub_t* sub;
  (void)pc;
  sub = prof->modesubs[prof->curmode];

  sub->bytes += bytes;
  prof->bytes[prof->curmode] += bytes;

  prof->dirty = TRUE;
}
void cpuprof_sub_end(hien_cpuprof_t* prof, addr_t pc)
{
  if(!cpuprof_can_profile(prof))
    return;

  hien_cpuprof_sub_t* sub;
  (void)pc;
  sub = prof->modesubs[prof->curmode];

  if(sub->parent == NULL)
    prof->curmode = 0;
  else
    prof->modesubs[prof->curmode] = sub->parent;

  prof->dirty = TRUE;
}
void cpuprof_sub_setmode(hien_cpuprof_t* prof, int mode)
{
  if(!cpuprof_can_profile(prof))
    return;

  prof->curmode = mode;
}

static void cpuprof_write_sub(hien_cpuprof_t* prof, hien_cpuprof_sub_t* sub, hien_file_t* fp)
{
  file_write32(strlen(sub->name), fp);
  file_write(sub->name, strlen(sub->name), fp);

  file_write32(sub->mode, fp);
  file_write64(sub->start, fp);

  file_write64(sub->cycles, fp);
  file_write64(sub->insns, fp);
  file_write64(sub->bytes, fp);

  file_write64(sub->count, fp);

  file_write32(kuso_pvec_size(sub->subs), fp);
  hien_cpuprof_sub_t** iter = kuso_pvec_iter(sub->subs, hien_cpuprof_sub_t*);
  for(; *iter != NULL; iter++) {
    cpuprof_write_sub(prof, *iter, fp);
  }
}

void cpuprof_write(hien_cpuprof_t* prof, hien_file_t* fp)
{
  if(prof == NULL)
    return;

  uint32_t i;
  uint64_t* sdata[2];

  sdata[0] = mem_alloc_cnt_type(prof->modecnt, uint64_t);
  sdata[1] = mem_alloc_cnt_type(prof->modecnt, uint64_t);
  file_write32(0x484E5046, fp); /* HNPF */
  file_write32(strlen(prof->devname), fp);
  file_write(prof->devname, strlen(prof->devname), fp);
  file_write32(prof->modecnt, fp);
  for(i = 0; i < prof->modecnt; i++) {
    file_write64(prof->calls[i], fp);
    file_write64(prof->cycles[i], fp);
    file_write64(prof->insns[i], fp);
    file_write64(prof->bytes[i], fp);
    sdata[0][i] = file_tell(fp);
    file_write64(0xDEADBEEF0BADDA7A, fp);
  }
  for(i = 0; i < prof->modecnt; i++) {
    sdata[1][i] = file_tell(fp);
    cpuprof_write_sub(prof, prof->modes[i], fp);
  }
  for(i = 0; i < prof->modecnt; i++) {
    file_seek(fp, FILE_START, sdata[0][i]);
    file_write64(sdata[1][i], fp);
  }
  mem_free(sdata[0]);
  mem_free(sdata[1]);
  prof->dirty = FALSE;
}

static hien_cpuprof_sub_t* cpuprof_read_sub(hien_cpuprof_t* prof, hien_cpuprof_sub_t* parent, hien_file_t* fp)
{
  hien_cpuprof_sub_t* sub;
  uint32_t sz, i;
  uint32_t tmp32;
  uint64_t tmp64;

  sub = cpuprof_new_sub(prof, parent, 0, 0, NULL);

  file_read32(&tmp32, fp);
  sub->name = mem_alloc(tmp32+1);
  file_read(sub->name, tmp32, fp);
  sub->name[tmp32] = '\0';

  file_read32(&sub->mode, fp);
  file_read64(&tmp64, fp);
  sub->start = tmp64;

  file_read64(&sub->cycles, fp);
  file_read64(&sub->insns, fp);
  file_read64(&sub->bytes, fp);

  file_read64(&sub->count, fp);

  file_read32(&sz, fp);
  for(i = 0; i < sz; i++) {
    cpuprof_read_sub(prof, sub, fp);
  }

  return sub;
}

int cpuprof_read(hien_cpuprof_t* prof, hien_file_t* fp)
{
  if(prof == NULL)
    return FALSE;

  int ret = FALSE;
  uint32_t i;
  uint64_t* sdata;
  uint32_t tmp32;

  file_read32(&tmp32, fp);
  if(tmp32 != 0x484E5046) /* HNPF */
    goto _fail;

  file_read32(&tmp32, fp);
  prof->devname = mem_alloc(tmp32+1);
  file_read(prof->devname, tmp32, fp);
  prof->devname[tmp32] = '\0';

  file_read32(&prof->modecnt, fp);
  sdata = mem_alloc_cnt_type(prof->modecnt, uint64_t);

  cpuprof_new_core(prof, NULL,NULL, prof->modecnt);

  for(i = 0; i < prof->modecnt; i++) {
    cpuprof_delete_sub(prof, prof->modes[i], i);
    file_read64(&prof->calls[i], fp);
    file_read64(&prof->cycles[i], fp);
    file_read64(&prof->insns[i], fp);
    file_read64(&prof->bytes[i], fp);
    file_read64(&sdata[i], fp);
  }
  for(i = 0; i < prof->modecnt; i++) {
    file_seek(fp, FILE_START, sdata[i]);
    prof->modes[i] = cpuprof_read_sub(prof, NULL, fp);
  }
  mem_free(sdata);
  prof->dirty = TRUE;

  ret = TRUE;
_fail:
  return ret;
}

void cpuprof_enable(hien_cpuprof_t* prof, char* fn)
{
  if(prof == NULL)
    return;

  cpuprof_reset(prof);
  prof->enable = TRUE;
  prof->fp = file_open(fn, FILE_WRITE|FILE_MKDIR);
  prof->dirty = TRUE;
}
void cpuprof_disable(hien_cpuprof_t* prof)
{
  if(prof == NULL)
    return;

  cpuprof_reset(prof);
  prof->enable = FALSE;
  prof->dirty = FALSE;
}

#else
hien_cpuprof_t* cpuprof_new(hien_vm_t* vm, struct hien_dev_t* dev, int modecnt)
{
  (void)vm;
  (void)dev;
  (void)modecnt;
  return NULL;
}
void cpuprof_delete(hien_cpuprof_t* prof)
{
  (void)prof;
}
void cpuprof_reset(hien_cpuprof_t* prof)
{
  (void)prof;
}

void cpuprof_sub_start(hien_cpuprof_t* prof, addr_t pc, int mode)
{
  (void)prof;
  (void)pc;
  (void)mode;
}
void cpuprof_sub_cycle(hien_cpuprof_t* prof, addr_t pc, size_t cycles)
{
  (void)prof;
  (void)pc;
  (void)cycles;
}
void cpuprof_sub_insn(hien_cpuprof_t* prof, addr_t pc, size_t insns)
{
  (void)prof;
  (void)pc;
  (void)insns;
}
void cpuprof_sub_byte(hien_cpuprof_t* prof, addr_t pc, size_t bytes)
{
  (void)prof;
  (void)pc;
  (void)bytes;
}
void cpuprof_sub_end(hien_cpuprof_t* prof, addr_t pc)
{
  (void)prof;
  (void)pc;
}
void cpuprof_sub_setmode(hien_cpuprof_t* prof, int mode)
{
  (void)prof;
  (void)mode;
}

void cpuprof_write(hien_cpuprof_t* prof, hien_file_t* fp)
{
  (void)prof;
  (void)fp;
}
int cpuprof_read(hien_cpuprof_t* prof, hien_file_t* fp)
{
  (void)prof;
  (void)fp;
  return FALSE;
}
void cpuprof_enable(hien_cpuprof_t* prof, char* fn)
{
  (void)prof;
  (void)fn;
}
void cpuprof_disable(hien_cpuprof_t* prof)
{
  (void)prof;
}
#endif
