/**********************************************************
*
* board_desc.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"
#include "libtcc.h"

static hien_board_desc_t* board_desc_compile(hien_vm_t* vm, char* code)
{
  hien_board_desc_t* brd;
  TCCState* tcs;

  tcs = tcc_new();
  if(!tcs) {
    hienlog(vm,LOG_CRIT, "Kiban Compile ERROR: Could not create tcc state\n");
    return NULL;
  }

  tcc_set_output_type(tcs, TCC_OUTPUT_MEMORY);
  tcc_add_include_path(tcs, "sdk");
  tcc_set_options(tcs, TCC_CFLAGS);

  if(tcc_compile_string(tcs, code) < 0) {
    hienlog(vm,LOG_CRIT, "Kiban Compile ERROR: Could not compile code\n");
    return NULL;
  }

  /* relocate the code */
  if(tcc_relocate(tcs, TCC_RELOCATE_AUTO) < 0) {
    hienlog(vm,LOG_CRIT, "Kiban Compile ERROR: Could not relocate\n");
    return NULL;
  }

  /* get entry symbol */
  brd = tcc_get_symbol(tcs, "brd_drv_desc");
  if(brd == NULL) {
    hienlog(vm,LOG_CRIT, "Kiban Compile ERROR: Could not get brd_drv_desc\n");
    return NULL;
  }

  return brd;
}

hien_board_desc_t* board_desc_generate(hien_vm_t* vm)
{
  hien_board_desc_t* brd;
  char* code;

  code = kiban_generate(vm);
  if(code == NULL)
    return NULL;

#if DEBUG
  hien_file_t* fp;
  fp = file_open("debugdrv.c", FILE_WRITE);
  if(fp) {
    file_write(code, strlen(code)+1, fp);
    file_close(fp);
  }
#endif

  brd = board_desc_compile(vm, code);
  mem_free(code);

  return brd;
}
