/**********************************************************
*
* board.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

#define BOARD_DEV_BASE_CNT 4

hien_board_t* board_new(hien_vm_t* vm)
{
  hien_board_t* brd;

  brd = mem_alloc_type(hien_board_t);
  brd->vm = vm;

  brd->devs = kuso_pvec_new(BOARD_DEV_BASE_CNT);

  brd->desc = NULL;
  brd->data = NULL;

  return brd;
}

void board_delete(hien_board_t* brd)
{
  board_unload(brd);

  kuso_pvec_delete(brd->devs);

  mem_free(brd);
}

void board_reset(hien_board_t* brd)
{
  if(brd->desc && brd->desc->reset) {
    brd->desc->reset(brd);
  }

  hien_dev_t** iter = kuso_pvec_iter(brd->devs, hien_dev_t*);
  for(; *iter != NULL; iter++) {
    dev_reset(*iter);
  }
}

void board_load(hien_board_t* brd, hien_board_desc_t* desc)
{
  kuso_pvec_t* info;
  brd->desc = desc;

  if(brd->desc && brd->desc->new)
    brd->data = brd->desc->new(brd);

  if(brd->desc && brd->desc->load) {
    info = brd->desc->load(brd);
    /* do info stuff */
    (void)info;
  }

  board_reset(brd);
}

void board_unload(hien_board_t* brd)
{
  kuso_pvec_t* info;
  hien_dev_t* dev;

  if(brd->desc && brd->desc->unload) {
    info = brd->desc->unload(brd);
    /* do info stuff */
    (void)info;
  }

  if(brd->desc && brd->desc->delete)
    brd->desc->delete(brd);

  while((dev = kuso_pvec_pop(brd->devs))) {
    dev_delete(dev);
  }
}
