/**********************************************************
*
* config.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _CONFIG_H_
#define _CONFIG_H_

enum {
  CFG_TYPE_NONE=0,
  CFG_TYPE_STR,
  CFG_TYPE_INT,
  CFG_TYPE_BOOL,
  CFG_TYPE_DEC,
  CFG_TYPE_OTHER,
};

typedef struct hien_config_t hien_config_t;
typedef struct hien_cfg_entry_t hien_cfg_entry_t;

#define CFG_NOBOUND (~0)

struct hien_cfg_entry_t {
  char* path;
  int type;
  char* deflt;

  int (*change)(hien_config_t* conf, hien_cfg_entry_t* ent, char* val_s, int val_i, double val_d, void* data);

  int max, min;

  void* data;
};

struct hien_config_t {
  hien_vm_t* vm;

  kuso_pvec_t* cfglist; /* hien_cfg_entry_t* */

  char* rompath;
  char* stapath;

  char* board;

  int loglvl;

  int sync_rate;
  double speed;

  struct {
    int rate;
    int buflen;
    double volume;
  } snd;
};

hien_config_t* config_new(hien_vm_t* vm, char* file);
void config_delete(hien_config_t* conf);

void config_add_entry(hien_config_t* conf, char* pfx, hien_cfg_entry_t ent, void* data);
void config_add_entries(hien_config_t* conf, char* pfx, hien_cfg_entry_t* ents, void* data);
int config_handle_option(hien_config_t* conf, int no, char* optname, char* optval);

void config_set_default(hien_config_t* conf);
int config_load(hien_config_t* conf, char* file);
int config_save(hien_config_t* conf, char* file);

void config_handle_cmdline(hien_config_t* conf, int argc, char* argv[]);
void config_handle_options(hien_config_t* conf, int argc, char* argv[]);

void config_set_rom_path(hien_config_t* conf, char* path);
void config_set_state_path(hien_config_t* conf, char* path);

void config_set_snd_samp_rate(hien_config_t* conf, int rate);
void config_set_snd_buflen(hien_config_t* conf, int len);
void config_set_snd_volume(hien_config_t* conf, double vol);

char* config_make_rom_path(hien_config_t* conf, char* loc, char* label);
char* config_make_def_path(hien_config_t* conf, char* name, char* ext);
char* config_make_state_path(hien_config_t* conf, char* name, char* path);

void config_set_profile_list(hien_config_t* conf, char* list);

#endif
