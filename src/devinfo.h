/**********************************************************
*
* devinfo.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _DEVINFO_H_
#define _DEVINFO_H_

typedef struct hien_dev_info_t hien_dev_info_t;

struct hien_dev_info_t {
  char* type;
  char* ctor;
  int use_clk;
  int has_save; /* 0 = no, 1 = yes, -1 = partial */
};

const hien_dev_info_t* devinfo_find(char* type);

#endif
