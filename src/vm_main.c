/**********************************************************
*
* vm_main.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

#include <unistd.h>

void vm_main(hien_vm_t* vm)
{
  while(vm->running) {
    vm_run(vm, HZ_TO_TIMETICK(vm->config->sync_rate));
    while(!osd_snd_need_more_samples(vm)) {
      usleep(10);
      ui_run(vm);
    }
  }
}

