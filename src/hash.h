/**********************************************************
*
* hash.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _HASH_H_
#define _HASH_H_

uint32_t crc32(void* data, size_t size);

#endif

