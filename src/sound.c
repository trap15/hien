/**********************************************************
*
* sound.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

/* TODO: Something like blip buffer would be a good idea */

#define SOUND_BUFFER_MAX_SIZE (8192) /* Maybe make the sample FIFOs resize based on config settings? */

/* Sound device */
hien_sound_t* sound_new(void)
{
  hien_sound_t* snd;

  snd = mem_alloc_type(hien_sound_t);
  snd->vol = 1.0;
  snd->out = NULL;

  return snd;
}

void sound_delete(hien_sound_t* snd)
{
  mem_free(snd);
}

void sound_add_sample(hien_sound_t* snd, hien_sample_t smp)
{
  snd->smp = smp;
}

void sound_set_volume(hien_sound_t* snd, double vol)
{
  snd->vol = vol;
}

/* Sound output */
static void sound_out_update(hien_sound_out_t* out, hien_sound_mgr_t* mgr)
{
  double scale;
  int64_t fsmp;
  hien_sample_t smp, psmp;
  (void)mgr;

  scale = mgr->vm->config->snd.volume;
  smp = 0;
  hien_sound_t** iter = kuso_pvec_iter(out->snds, hien_sound_t*);
  for(; *iter != NULL; iter++) {
    psmp = (*iter)->smp;
    fsmp = (int64_t)psmp * (*iter)->vol / kuso_pvec_size(out->snds);
    fsmp *= scale;
    fsmp += smp;
    if(fsmp > HIEN_SAMPLE_MAX) {
      hienlog(mgr->vm,LOG_WARN, "Sample overflow!\n");
      smp = HIEN_SAMPLE_MAX;
      break;
    }else if(fsmp < HIEN_SAMPLE_MIN) {
      hienlog(mgr->vm,LOG_WARN, "Sample underflow!\n");
      smp = HIEN_SAMPLE_MIN;
      break;
    }
    smp = fsmp;
  }

  if(out->logfp != NULL) {
    file_write(&smp, sizeof(hien_sample_t), out->logfp);
  }
  kuso_fifo_push(out->smps, smp);
}

hien_sound_out_t* sound_out_new(void)
{
  hien_sound_out_t* out;

  out = mem_alloc_type(hien_sound_out_t);
  out->snds = kuso_pvec_new(0);
  out->smps = kuso_fifo_new(SOUND_BUFFER_MAX_SIZE);
  out->logfp = NULL;
  out->mgr = NULL;

  return out;
}

void sound_out_delete(hien_sound_out_t* out)
{
  hien_sound_t* snd;
  sound_out_closelog(out);
  while((snd = kuso_pvec_pop(out->snds))) {
    sound_delete(snd);
  }
  kuso_pvec_delete(out->snds);
  kuso_fifo_delete(out->smps);
  mem_free(out);
}

void sound_out_add(hien_sound_out_t* out, hien_sound_t* snd)
{
  kuso_pvec_append(out->snds, snd);
  snd->out = out;
}

void sound_out_remove(hien_sound_out_t* out, hien_sound_t* snd)
{
  size_t i;
  hien_sound_t** iter = kuso_pvec_iter(out->snds, hien_sound_t*);
  for(i = 0; *iter != NULL; iter++, i++) {
    if(*iter == snd) {
      kuso_pvec_remove(out->snds, i);
      return;
    }
  }
}

void sound_out_openlog(hien_sound_out_t* out, char* fn)
{
  uint8_t tmp[4];
  out->logfp = file_open(fn, FILE_WRITE);
  file_write("RIFF", 4, out->logfp);
  tmp[0] = tmp[1] = tmp[2] = tmp[3] = 0;
  file_write(tmp, 4, out->logfp);
  file_write("WAVE", 4, out->logfp);

  file_write("fmt ", 4, out->logfp);
  tmp[0] = 0x10;
  file_write(tmp, 4, out->logfp);
  tmp[0] = 1;
  file_write(tmp, 2, out->logfp);
  file_write(tmp, 2, out->logfp);
  tmp[0] = out->mgr->rate >> 0;
  tmp[1] = out->mgr->rate >> 8;
  tmp[2] = out->mgr->rate >> 16;
  tmp[3] = out->mgr->rate >> 24;
  file_write(tmp, 4, out->logfp);
  tmp[0] = (out->mgr->rate*4) >> 0;
  tmp[1] = (out->mgr->rate*4) >> 8;
  tmp[2] = (out->mgr->rate*4) >> 16;
  tmp[3] = (out->mgr->rate*4) >> 24;
  file_write(tmp, 4, out->logfp);
  tmp[0] = 4;
  tmp[1] = tmp[2] = tmp[3] = 0;
  file_write(tmp, 2, out->logfp);
  tmp[0] = 32;
  file_write(tmp, 2, out->logfp);

  file_write("data", 4, out->logfp);
  tmp[0] = tmp[1] = tmp[2] = tmp[3] = 0;
  file_write(tmp, 4, out->logfp);
}

void sound_out_closelog(hien_sound_out_t* out)
{
  size_t sz;
  uint8_t tmp[4];
  if(out->logfp != NULL) {
    sz = file_tell(out->logfp);

    file_seek(out->logfp, FILE_START, 4);
    tmp[0] = (sz+36) >> 0;
    tmp[1] = (sz+36) >> 8;
    tmp[2] = (sz+36) >> 16;
    tmp[3] = (sz+36) >> 24;
    file_write(tmp, 4, out->logfp);

    file_seek(out->logfp, FILE_START, 40);
    tmp[0] = sz >> 0;
    tmp[1] = sz >> 8;
    tmp[2] = sz >> 16;
    tmp[3] = sz >> 24;
    file_write(tmp, 4, out->logfp);

    file_close(out->logfp);
    out->logfp = NULL;
  }
}


/* Sound manager */
static void sound_mgr_update(hien_timer_t* tmr, void* data)
{
  hien_sound_mgr_t* mgr = data;
  (void)tmr;

  hien_sound_out_t** iter = kuso_pvec_iter(mgr->outs, hien_sound_out_t*);
  for(; *iter != NULL; iter++) {
    sound_out_update(*iter, mgr);
  }
}

hien_sound_mgr_t* sound_mgr_new(hien_vm_t* vm, hz_t rate)
{
  hien_sound_mgr_t* mgr;

  mgr = mem_alloc_type(hien_sound_mgr_t);
  mgr->vm = vm;
  mgr->rate = rate;
  mgr->limit = HZ_TO_TIMETICK(rate);
  mgr->outs = kuso_pvec_new(0);

  mgr->utmr = tmr_new();
  tmr_enable(mgr->utmr, TRUE);
  tmr_set_callback(mgr->utmr, sound_mgr_update, mgr);
  tmr_set_periodic_hz(mgr->utmr, rate);
  tmr_mgr_add(mgr->vm->tmrmgr, mgr->utmr);

  return mgr;
}

void sound_mgr_delete(hien_sound_mgr_t* mgr)
{
  hien_sound_out_t* out;
  while((out = kuso_pvec_pop(mgr->outs))) {
    sound_out_delete(out);
  }
  kuso_pvec_delete(mgr->outs);
  mem_free(mgr);
}

void sound_mgr_add(hien_sound_mgr_t* mgr, hien_sound_out_t* out)
{
  kuso_pvec_append(mgr->outs, out);
  out->mgr = mgr;
}

void sound_mgr_remove(hien_sound_mgr_t* mgr, hien_sound_out_t* out)
{
  size_t i;
  hien_sound_out_t** iter = kuso_pvec_iter(mgr->outs, hien_sound_out_t*);
  for(i = 0; *iter != NULL; iter++, i++) {
    if(*iter == out) {
      kuso_pvec_remove(mgr->outs, i);
      (*iter)->mgr = NULL;
      break;
    }
  }
}
