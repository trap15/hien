/**********************************************************
*
* dev.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _DEV_H_
#define _DEV_H_

/* Typedefs */
typedef struct hien_dev_t hien_dev_t;
typedef struct hien_port_t hien_port_t;
typedef struct hien_space_t hien_space_t;

#define SPACE_MAX   6
#define PORT_MAX    16

#define SPACE_MAIN  0
#define SPACE_USER  (SPACE_MAIN+1)

#define PORT_CLOCK  0
#define PORT_USER   (PORT_CLOCK+1)

#define SPACE_COUNT_CHECK(x) \
  STATIC_ASSERT(CPP_EVAL2(x, _SPACE_COUNT) < SPACE_MAX, too_many_spaces__##x)
#define PORT_COUNT_CHECK(x) \
  STATIC_ASSERT(CPP_EVAL2(x, _PORT_COUNT) < PORT_MAX, too_many_ports__##x)

#define PORT_INPUT  1
#define PORT_OUTPUT 2
#define PORT_INOUT  (PORT_INPUT|PORT_OUTPUT)
#define PORT_ANALOG 4

struct hien_port_t {
  size_t width;
  size_t mask;
  size_t assert_cmp;

  int       flag;
  uintmax_t val;
  analog_t  aval;

  uintmax_t (*read)(hien_dev_t* dev, uintmax_t mask);
  void      (*write)(hien_dev_t* dev, uintmax_t data, uintmax_t mask);
  analog_t  (*analog_read)(hien_dev_t* dev);
  void      (*analog_write)(hien_dev_t* dev, analog_t data);
};

struct hien_space_t {
  size_t width;

  uintmax_t (*read)(hien_dev_t* dev, addr_t addr, uintmax_t mask);
  void      (*write)(hien_dev_t* dev, addr_t addr, uintmax_t data, uintmax_t mask);

  uintmax_t data; /* Used for open-bus */
};

struct hien_dev_t {
  hien_vm_t* vm;
  hien_timer_t* ctmr;
  hz_t clock;

  hien_cpuprof_t* prof;

  char* name;

  void (*delete)(hien_dev_t* dev);
  void (*reset)(hien_dev_t* dev);

  hien_space_t space[SPACE_MAX];
  hien_port_t  port[PORT_MAX];

  void* data;
  void* drv_data;
};

hien_dev_t* dev_new(hien_vm_t* vm, char* name, hz_t clock);
void dev_delete(hien_dev_t* dev);
void dev_reset(hien_dev_t* dev);
hz_t dev_change_clock(hien_dev_t* dev, hz_t clock);

uintmax_t dev_read(hien_dev_t* dev, int space, addr_t addr, uintmax_t mask);
void dev_write(hien_dev_t* dev, int space, addr_t addr, uintmax_t data, uintmax_t mask);
uintmax_t dev_space_get_openbus(hien_dev_t* dev, int space, uintmax_t mask);

void dev_space_setup(hien_dev_t* dev, int space, size_t width);
void dev_space_set_callbacks(hien_dev_t* dev, int space,
                        uintmax_t (*read)(hien_dev_t* dev, addr_t addr, uintmax_t mask),
                        void (*write)(hien_dev_t* dev, addr_t addr, uintmax_t data, uintmax_t mask));

uintmax_t dev_port_read(hien_dev_t* dev, int port, uintmax_t mask);
void dev_port_write(hien_dev_t* dev, int port, uintmax_t data, uintmax_t mask);
uintmax_t dev_port_get_openbus(hien_dev_t* dev, int port, uintmax_t mask);
void dev_port_assert(hien_dev_t* dev, int port);
void dev_port_deassert(hien_dev_t* dev, int port);
int dev_port_is_asserted(hien_dev_t* dev, int port);
void dev_port_invert(hien_dev_t* dev, int port);

analog_t dev_port_analog_read(hien_dev_t* dev, int port);
void dev_port_analog_write(hien_dev_t* dev, int port, analog_t data);
analog_t dev_port_analog_get_openbus(hien_dev_t* dev, int port);

void dev_port_setup(hien_dev_t* dev, int port, size_t width, int flag);
void dev_port_set_callbacks(hien_dev_t* dev, int port,
                        uintmax_t (*read)(hien_dev_t* dev, uintmax_t mask),
                        void (*write)(hien_dev_t* dev, uintmax_t data, uintmax_t mask));
void dev_port_set_analog_callbacks(hien_dev_t* dev, int port,
                        analog_t (*read)(hien_dev_t* dev),
                        void (*write)(hien_dev_t* dev, analog_t data));

#define COMBINE_DATA(old, new, mask) (old) = (((old) & ~(mask)) | ((new) & (mask)))

#endif

