/**********************************************************
*
* vm.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _VM_H_
#define _VM_H_

enum {
  VMTYPE_NORMAL   = 0,
  VMTYPE_SNDPLAY  = 1 << 0,
  //other         = 1 << 1,
  //other         = 1 << 2,
  VMTYPE_ALL      = 0xFF,
};

/* VM */
struct hien_vm_t {
  char* name;

  int running;
  int type;

  timetick_t   uptime_lo;
  unsigned int uptime_hi; /* In milliseconds */

  hien_config_t*    config;
  hien_board_t*     board;
  hien_timer_mgr_t* tmrmgr;
  hien_sound_mgr_t* sndmgr;
  hien_state_mgr_t* statemgr;

  hien_sndplay_t*   sndplay;

  void* osdsnd_data;
  void* ui_data;
};

extern int _save_argc;
extern char** _save_argv;

hien_vm_t* vm_new(char* name, int type);
void vm_delete(hien_vm_t* vm);
void vm_reset(hien_vm_t* vm);
void vm_run(hien_vm_t* vm, timetick_t ticks);
void vm_stop(hien_vm_t* vm);
void vm_main(hien_vm_t* vm);

int vm_state_save(hien_vm_t* vm, char* name);
int vm_state_load(hien_vm_t* vm, char* name);

void vm_add_timer(hien_vm_t* vm, hien_timer_t* tmr);
void vm_add_device(hien_vm_t* vm, hien_dev_t* dev);
void vm_add_sound_out(hien_vm_t* vm, hien_sound_out_t* out);

void vm_remove_timer(hien_vm_t* vm, hien_timer_t* tmr);
void vm_remove_device(hien_vm_t* vm, hien_dev_t* dev);
void vm_remove_sound_out(hien_vm_t* vm, hien_sound_out_t* out);

#define VM_STATE_ADD_PROTO(name, type) \
void name(hien_vm_t* vm, type* val, char* path, ...); \
void name##_arr(hien_vm_t* vm, size_t cnt, type* val, char* path, ...); \
void name##_va(hien_vm_t* vm, type* val, char* pathf, va_list path); \
void name##_arr_va(hien_vm_t* vm, size_t cnt, type* val, char* pathf, va_list path);

/*
void vm_state_add_foo(hien_vm_t* vm, Y* val, char* path, ...);
void vm_state_add_foo_arr(hien_vm_t* vm, size_t cnt, Y* val, char* path, ...);
void vm_state_add_foo_va(hien_vm_t* vm, Y* val, char* pathf, va_list path);
void vm_state_add_foo_arr_va(hien_vm_t* vm, size_t cnt, Y* val, char* pathf, va_list path);
*/
VM_STATE_ADD_PROTO(vm_state_add_int, int)
VM_STATE_ADD_PROTO(vm_state_add_uint, unsigned int)
VM_STATE_ADD_PROTO(vm_state_add_float, float)
VM_STATE_ADD_PROTO(vm_state_add_double, double)
VM_STATE_ADD_PROTO(vm_state_add_int64, int64_t)
VM_STATE_ADD_PROTO(vm_state_add_int32, int32_t)
VM_STATE_ADD_PROTO(vm_state_add_int16, int16_t)
VM_STATE_ADD_PROTO(vm_state_add_int8, int8_t)
VM_STATE_ADD_PROTO(vm_state_add_uint64, uint64_t)
VM_STATE_ADD_PROTO(vm_state_add_uint32, uint32_t)
VM_STATE_ADD_PROTO(vm_state_add_uint16, uint16_t)
VM_STATE_ADD_PROTO(vm_state_add_uint8, uint8_t)
VM_STATE_ADD_PROTO(vm_state_add_timetick, timetick_t)
VM_STATE_ADD_PROTO(vm_state_add_addr, addr_t)
VM_STATE_ADD_PROTO(vm_state_add_analog, analog_t)

/* For debugging log stuff */
#define VM_TIMESTAMP(vm) ((((double)(vm)->uptime_hi)/1000.0) + ((double)(vm)->uptime_lo/(double)TIMETICK_PER_SECOND))

#endif
