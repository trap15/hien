/**********************************************************
*
* apu/x1_010/x1_010.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
* based on previous work by:
*   Luca Elia (l.elia@tin.it)
*   Manbow-J (manbowj@hama.freemail.ne.jp)
*
**********************************************************/

#include "hien.h"

#define X1_010_CHANNELS 16
#define STRM_VOL_BASE  (24*256/15) /* Volume base for streaming channels */
#define WAVE_VOL_BASE  (32*256/15) /* Volume base for waveform channels */

typedef struct apu_x1_010_t apu_x1_010_t;

struct apu_x1_010_t {
  struct {
    uint8_t key_state;
    uint8_t mode;
    uint8_t repeat;

    uint16_t freq;
    struct {
      int32_t vol_l, vol_r;
      uint8_t start;
      uint8_t end;

      uint32_t pcm_step;
      uint32_t pcm_start;
      uint32_t pcm_end;

      uint32_t pos;
    } strm;
    struct {
      uint8_t wav_num;
      uint8_t env_freq;
      uint8_t env_num;

      uint32_t wav_start;
      uint32_t env_start;

      uint32_t wav_step;
      uint32_t env_step;

      uint32_t wav_pos;
      uint32_t env_pos;
    } wave;

    int32_t level_l, level_r;
  } chan[X1_010_CHANNELS];

  uint8_t mem[0x2000];

  uint8_t wave_ticks;

  int32_t out_level_l, out_level_r;
  analog_t ao_l, ao_r;
};

static void _x1_010_chan_keyoff(apu_x1_010_t* apu, int ch)
{
  apu->chan[ch].key_state = 0;
  apu->chan[ch].level_l = 0;
  apu->chan[ch].level_r = 0;
}

static void _x1_010_chan_keyon(apu_x1_010_t* apu, int ch)
{
  apu->chan[ch].strm.pos = 0;
  apu->chan[ch].wave.wav_pos = 0;
  apu->chan[ch].wave.env_pos = 0;

  apu->chan[ch].key_state = 1;
}

static int8_t _x1_010_read_pcm(hien_dev_t* dev, apu_x1_010_t* apu, int ch, uint32_t pos)
{
  pos += apu->chan[ch].strm.pcm_start;
  return dev_read(dev, X1_010_SPACE_SAMPLE, pos, 0xFF);
}

static int8_t _x1_010_read_wav(hien_dev_t* dev, apu_x1_010_t* apu, int ch, uint32_t pos)
{
  (void)dev;
  pos += apu->chan[ch].wave.wav_start;
  return apu->mem[pos + 0x1000];
}

static uint8_t _x1_010_read_env(hien_dev_t* dev, apu_x1_010_t* apu, int ch, uint32_t pos)
{
  (void)dev;
  pos += apu->chan[ch].wave.env_start;
  return apu->mem[pos + 0x0000];
}

static void _x1_010_update_one_strm(hien_dev_t* dev, apu_x1_010_t* apu, int ch)
{
  int8_t smp;
  uint32_t pos;

  pos = apu->chan[ch].strm.pos >> 8;

  if(apu->chan[ch].strm.pcm_start + pos >= apu->chan[ch].strm.pcm_end) {
    _x1_010_chan_keyoff(apu, ch);
    return;
  }

  smp = _x1_010_read_pcm(dev, apu, ch, pos);
  apu->chan[ch].strm.pos += apu->chan[ch].strm.pcm_step;

  apu->chan[ch].level_l = smp * apu->chan[ch].strm.vol_l / 256;
  apu->chan[ch].level_r = smp * apu->chan[ch].strm.vol_r / 256;
}

static void _x1_010_update_one_wave(hien_dev_t* dev, apu_x1_010_t* apu, int ch)
{
  int8_t smp;
  uint8_t env;
  uint32_t wav_pos, env_pos;
  int vol_l, vol_r;

  wav_pos = apu->chan[ch].wave.wav_pos >> 8;
  env_pos = apu->chan[ch].wave.env_pos >> 10;

  if((env_pos > 0x7F) && apu->chan[ch].repeat) {
    _x1_010_chan_keyoff(apu, ch);
    return;
  }

  smp = _x1_010_read_wav(dev, apu, ch, wav_pos & 0x7F);
  env = _x1_010_read_env(dev, apu, ch, env_pos & 0x7F);

  apu->chan[ch].wave.wav_pos += apu->chan[ch].wave.wav_step;
  apu->chan[ch].wave.env_pos += apu->chan[ch].wave.env_step;

  vol_l = ((env >> 4) & 0xF) * WAVE_VOL_BASE;
  vol_r = ((env >> 0) & 0xF) * WAVE_VOL_BASE;

  apu->chan[ch].level_l = smp * vol_l / 256;
  apu->chan[ch].level_r = smp * vol_r / 256;
}

static void _x1_010_update_one(hien_dev_t* dev, apu_x1_010_t* apu, int ch)
{
  if(!apu->chan[ch].key_state) /* not playing */
    return;
  switch(apu->chan[ch].mode) {
    case 0:
      _x1_010_update_one_strm(dev, apu, ch);
      break;
    case 1:
      if(apu->wave_ticks == 0)
        _x1_010_update_one_wave(dev, apu, ch);
      break;
  }
}

static void apu_x1_010_update(hien_dev_t* dev, uintmax_t data, uintmax_t mask)
{
  apu_x1_010_t* apu;
  int i;
  (void)mask;
  apu = dev->data;

  if(data != 1) /* do nothing on falling edge */
    return;

  apu->out_level_l = 0;
  apu->out_level_r = 0;
  for(i = 0; i < X1_010_CHANNELS; i++) {
    _x1_010_update_one(dev, apu, i);
    apu->out_level_l += apu->chan[i].level_l / X1_010_CHANNELS;
    apu->out_level_r += apu->chan[i].level_r / X1_010_CHANNELS;
  }

  apu->wave_ticks++;
  apu->wave_ticks &= 0x3F; /* Wave only updates every 64 ticks */

  apu->ao_l = A_FIX2V(apu->out_level_l, A_5V);
  apu->ao_r = A_FIX2V(apu->out_level_r, A_5V);
  dev_port_analog_write(dev, X1_010_PORT_AO_L, apu->ao_l);
  dev_port_analog_write(dev, X1_010_PORT_AO_R, apu->ao_r);
}

static uintmax_t apu_x1_010_read(hien_dev_t* dev, addr_t addr, uintmax_t mask)
{
  apu_x1_010_t* apu;
  apu = dev->data;

  return apu->mem[addr & 0x1FFF] & mask;
}

static void _x1_010_write_chan(apu_x1_010_t* apu, int ch, int reg, uint8_t data)
{
  switch(reg) {
    case 0: /* Status */
      if(apu->chan[ch].key_state && !(data & 1)) { /* Key-off */
        _x1_010_chan_keyoff(apu, ch);
      }else if(!apu->chan[ch].key_state && (data & 1)) { /* key-on */
        _x1_010_chan_keyon(apu, ch);
      }
      apu->chan[ch].key_state = (data >> 0) & 1;
      apu->chan[ch].mode = (data >> 1) & 1;
      apu->chan[ch].repeat = (data >> 2) & 1;
      if(data & ~7) {
        /* TODO ? */
      }
      break;
    case 1: /* Strm:Volume / Wave:WaveNum */
      /* streaming */
      apu->chan[ch].strm.vol_l = ((data >> 4) & 0xF) * STRM_VOL_BASE;
      apu->chan[ch].strm.vol_r = ((data >> 0) & 0xF) * STRM_VOL_BASE;
      /* waveform */
      apu->chan[ch].wave.wav_num = (data >> 0) & 0xFF;
      apu->chan[ch].wave.wav_start = apu->chan[ch].wave.wav_num << 7;
      break;
    case 2: /* Frequency Low */
      apu->chan[ch].freq &= ~0xFF;
      apu->chan[ch].freq |= (data >> 0) & 0xFF;
      apu->chan[ch].strm.pcm_step = apu->chan[ch].freq & 0xFF;
      apu->chan[ch].wave.wav_step = apu->chan[ch].freq;
      break;
    case 3: /* Frequency High */
      /* waveform */
      apu->chan[ch].freq &= ~0xFF00;
      apu->chan[ch].freq |= (data << 8) & 0xFF00;
      apu->chan[ch].strm.pcm_step = apu->chan[ch].freq & 0xFF;
      apu->chan[ch].wave.wav_step = apu->chan[ch].freq;
      break;
    case 4: /* Strm:Start / Wave:EnvTime */
      /* streaming */
      apu->chan[ch].strm.start = (data >> 0) & 0xFF;
      apu->chan[ch].strm.pcm_start = apu->chan[ch].strm.start << 12;
      /* waveform */
      apu->chan[ch].wave.env_freq = (data >> 0) & 0xFF;
      apu->chan[ch].wave.env_step = apu->chan[ch].wave.env_freq;
      break;
    case 5: /* Strm:End / Wave:EnvNum */
      /* streaming */
      apu->chan[ch].strm.end = (data >> 0) & 0xFF;
      apu->chan[ch].strm.pcm_end = (0x100-apu->chan[ch].strm.end) << 12;
      /* waveform */
      apu->chan[ch].wave.env_num = (data >> 0) & 0xFF;
      apu->chan[ch].wave.env_start = apu->chan[ch].wave.env_num << 7;
      break;
  }
}

static void apu_x1_010_write(hien_dev_t* dev, addr_t addr, uintmax_t data, uintmax_t mask)
{
  apu_x1_010_t* apu;
  int chan, reg;
  (void)apu;
  apu = dev->data;
  data &= mask;
  addr &= 0x1FFF;

  chan = addr >> 3;
  reg = addr & 7;

  if(chan < X1_010_CHANNELS)
    _x1_010_write_chan(apu, chan, reg, data);

  apu->mem[addr] = data;
}

static void apu_x1_010_reset(hien_dev_t* dev)
{
  apu_x1_010_t* apu;
  int i;
  apu = dev->data;

  apu->ao_l = A_0V;
  apu->ao_r = A_0V;

  apu->out_level_l = 0;
  apu->out_level_r = 0;
  apu->wave_ticks = 0;

  for(i = 0; i < 0x2000; i++) {
    apu_x1_010_write(dev, i, 0, 0xFF);
  }
}

static void apu_x1_010_delete(hien_dev_t* dev)
{
  apu_x1_010_t* apu;
  apu = dev->data;

  mem_free(apu);
}

hien_dev_t* apu_x1_010_new(hien_vm_t* vm, char* name, hz_t clock)
{
  hien_dev_t* dev;
  apu_x1_010_t* apu;
  int i;

  dev = dev_new(vm, name, clock);
  dev->delete = apu_x1_010_delete;
  dev->reset = apu_x1_010_reset;

  dev_port_set_callbacks(dev, PORT_CLOCK, NULL, apu_x1_010_update);

  dev_space_setup(dev, SPACE_MAIN, 8);
  dev_space_set_callbacks(dev, SPACE_MAIN, apu_x1_010_read, apu_x1_010_write);

  apu = mem_alloc_type(apu_x1_010_t);

  dev->data = apu;

  dev_reset(dev);

  vm_state_add_uint8_arr(vm, 0x2000, apu->mem, name, "mem", NULL);

  vm_state_add_uint8(vm, &apu->wave_ticks, name, "wave_ticks", NULL);
  vm_state_add_int32(vm, &apu->out_level_l, name, "out_levelL", NULL);
  vm_state_add_int32(vm, &apu->out_level_r, name, "out_levelR", NULL);
  vm_state_add_analog(vm, &apu->ao_l, name, "aoL", NULL);
  vm_state_add_analog(vm, &apu->ao_r, name, "aoR", NULL);

  for(i = 0; i < X1_010_CHANNELS; i++) {
    char* cname = NULL;
    kuso_asprintf(&cname, "chan%d", i);

    vm_state_add_int32(vm, &apu->chan[i].level_l, name, cname, "levelL", NULL);
    vm_state_add_int32(vm, &apu->chan[i].level_r, name, cname, "levelR", NULL);
    vm_state_add_uint8(vm, &apu->chan[i].key_state, name, cname, "key_state", NULL);
    vm_state_add_uint8(vm, &apu->chan[i].mode, name, cname, "mode", NULL);
    vm_state_add_uint8(vm, &apu->chan[i].repeat, name, cname, "repeat", NULL);
    vm_state_add_uint16(vm, &apu->chan[i].freq, name, cname, "freq", NULL);

    vm_state_add_int32(vm, &apu->chan[i].strm.vol_l, name, cname, "stream", "volL", NULL);
    vm_state_add_int32(vm, &apu->chan[i].strm.vol_r, name, cname, "stream", "volR", NULL);
    vm_state_add_uint8(vm, &apu->chan[i].strm.start, name, cname, "stream", "start", NULL);
    vm_state_add_uint8(vm, &apu->chan[i].strm.end, name, cname, "stream", "end", NULL);
    vm_state_add_uint32(vm, &apu->chan[i].strm.pcm_step, name, cname, "stream", "pcm_step", NULL);
    vm_state_add_uint32(vm, &apu->chan[i].strm.pcm_start, name, cname, "stream", "pcm_start", NULL);
    vm_state_add_uint32(vm, &apu->chan[i].strm.pcm_end, name, cname, "stream", "pcm_end", NULL);
    vm_state_add_uint32(vm, &apu->chan[i].strm.pos, name, cname, "stream", "pos", NULL);

    vm_state_add_uint8(vm, &apu->chan[i].wave.wav_num, name, cname, "wave", "wav_num", NULL);
    vm_state_add_uint8(vm, &apu->chan[i].wave.env_freq, name, cname, "wave", "env_freq", NULL);
    vm_state_add_uint8(vm, &apu->chan[i].wave.env_num, name, cname, "wave", "env_num", NULL);
    vm_state_add_uint32(vm, &apu->chan[i].wave.wav_start, name, cname, "wave", "wav_start", NULL);
    vm_state_add_uint32(vm, &apu->chan[i].wave.env_start, name, cname, "wave", "env_start", NULL);
    vm_state_add_uint32(vm, &apu->chan[i].wave.wav_step, name, cname, "wave", "wav_step", NULL);
    vm_state_add_uint32(vm, &apu->chan[i].wave.env_step, name, cname, "wave", "env_step", NULL);
    vm_state_add_uint32(vm, &apu->chan[i].wave.wav_pos, name, cname, "wave", "wav_pos", NULL);
    vm_state_add_uint32(vm, &apu->chan[i].wave.env_pos, name, cname, "wave", "env_pos", NULL);

    mem_free(cname);
  }

  return dev;
}

hien_dev_info_t apu_x1_010_def = {
  .type = "X1-010",
  .ctor = "apu_x1_010_new",
  .use_clk = TRUE,
  .has_save = 1,
};
