/**********************************************************
*
* apu/x1_010/x1_010.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _APU_X1_010_H_
#define _APU_X1_010_H_

#define X1_010_SPACE_SAMPLE (SPACE_USER+1)
#define X1_010_SPACE_COUNT  (SPACE_USER+1)
SPACE_COUNT_CHECK(X1_010);

#define X1_010_PORT_AO_L  (PORT_USER+0)
#define X1_010_PORT_AO_R  (PORT_USER+1)
#define X1_010_PORT_COUNT (PORT_USER+2)
PORT_COUNT_CHECK(X1_010);

hien_dev_t* apu_x1_010_new(hien_vm_t* vm, char* name, hz_t clock);

extern hien_dev_info_t apu_x1_010_def;

#endif
