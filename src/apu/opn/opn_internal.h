/**********************************************************
*
* apu/opn/opn_internal.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _APU_OPN_INTERNAL_H_
#define _APU_OPN_INTERNAL_H_

#include "opn_timer.h"
#include "opn_fm.h"
#include "opn_adpcm.h"
#include "opn_rhythm.h"

#endif
