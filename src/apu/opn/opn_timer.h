/**********************************************************
*
* apu/opn/opn_timer.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _APU_OPN_TIMER_H_
#define _APU_OPN_TIMER_H_

typedef struct apu_opn_timer_t apu_opn_timer_t;

struct apu_opn_timer_t {
  uint16_t top; /* Max value for the timer */
  uint16_t freq;
  uint16_t step;
  int en;
  int flag;

  int pre;
  int pre_ticks;
};

void apu_opn_timer_reload(apu_opn_timer_t* tmr);
void apu_opn_timer_reset(apu_opn_timer_t* tmr);
int apu_opn_timer_update(apu_opn_timer_t* tmr);
void apu_opn_timer_set_prescale(apu_opn_timer_t* tmr, int pre);
void apu_opn_timer_init(apu_opn_timer_t* tmr, int top);
void apu_opn_timer_state_add(hien_vm_t* vm, apu_opn_timer_t* tmr, char* path, ...);

#endif
