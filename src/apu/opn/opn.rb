# All
addObj("opn_fm.o")
addObj("opn_rhythm.o")
addObj("opn_adpcm.o")
addObj("opn_timer.o")

# YM2203
addHdr("ym2203.h")
addObj("ym2203.o")
addObj("ym2203_fm.o")
addDev("apu_ym2203")

# YM2608
addHdr("ym2608.h")
addObj("ym2608.o")
addObj("ym2608_fm.o")
addObj("ym2608_rhythm.o")
addDev("apu_ym2608")
