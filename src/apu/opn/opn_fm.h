/**********************************************************
*
* apu/opn/opn_fm.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _APU_OPN_FM_H_
#define _APU_OPN_FM_H_

typedef struct apu_opn_fm_oper_pg_t apu_opn_fm_oper_pg_t;
typedef struct apu_opn_fm_oper_eg_t apu_opn_fm_oper_eg_t;
typedef struct apu_opn_fm_oper_t apu_opn_fm_oper_t;
typedef struct apu_opn_fm_algo_t apu_opn_fm_algo_t;
typedef struct apu_opn_fm_chan_t apu_opn_fm_chan_t;
typedef struct apu_opn_fm_lfo_t apu_opn_fm_lfo_t;
typedef struct apu_opn_fm_state_t apu_opn_fm_state_t;

struct apu_opn_fm_oper_pg_t {
  int dt, mul;
  int fnum;
  int block;

  /* State */
  uint32_t phase; /* 20 bits */
};

struct apu_opn_fm_oper_eg_t {
  int tl;
  int ks, ar;
  int dr;
  int sr;
  int sl, rr;
  int ssgeg;
  int amon; /* 1 bit */

  /* State */
  int state;
  int rate; /* 6 bits */
  int atten; /* 10 bits */
  int precalc_sl;
};

struct apu_opn_fm_oper_t {
  apu_opn_fm_oper_pg_t pg;
  apu_opn_fm_oper_eg_t eg;

  /* OP state */
  int key;
  int csm_key;

  int key_old;

  /* Cached data */
  int kcode;
};

struct apu_opn_fm_algo_t {
  int32_t   m1,  m2,  c1,  c2,  mem, out;
  int32_t *om1,*om2,*oc1,*oc2,*omem;

  int fb, con;
};

struct apu_opn_fm_chan_t {
  apu_opn_fm_oper_t op[4];

  apu_opn_fm_algo_t algo;

  int32_t op0fb[2];
  int lr; /* 1/1 bit */
  int ams; /* 2 bits */
  int pms; /* 3 bits */
};

struct apu_opn_fm_lfo_t {
  int freq; /* 3 bits */
  int on; /* 1 bit */

  /* State */
  int ticks; /* 7 bits */
  int period;

  int pos; /* 7 bits */

  int pmod; /* 5 bits */
  int amod; /* 7 bits */
};

struct apu_opn_fm_state_t {
  uint16_t eg_ctr;
  int eg_cycle;

  int sch;

  int csm;
  int ch3slot;
};

void apu_opn_fm_pg_reset(apu_opn_fm_oper_pg_t* pg);
void apu_opn_fm_eg_reset(apu_opn_fm_oper_eg_t* eg);
void apu_opn_fm_oper_reset(apu_opn_fm_oper_t* oper);
void apu_opn_fm_algo_reset(apu_opn_fm_algo_t* algo);
void apu_opn_fm_chan_reset(apu_opn_fm_chan_t* chan);
void apu_opn_fm_lfo_reset(apu_opn_fm_lfo_t* lfo);
void apu_opn_fm_state_reset(apu_opn_fm_state_t* state);

void apu_opn_fm_keyon(apu_opn_fm_chan_t* chan, apu_opn_fm_oper_t* oper, int ch3slot);
void apu_opn_fm_keyoff(apu_opn_fm_chan_t* chan, apu_opn_fm_oper_t* oper, int ch3slot);

void apu_opn_fm_lfo_on(apu_opn_fm_lfo_t* lfo);
void apu_opn_fm_lfo_off(apu_opn_fm_lfo_t* lfo);

void apu_opn_fm_eg_recalc(apu_opn_fm_oper_eg_t* eg, int kcode);
void apu_opn_fm_eg_set_state(apu_opn_fm_oper_eg_t* eg, int kcode, int state);
int32_t apu_opn_fm_eg_atten(apu_opn_fm_oper_eg_t* eg);
void apu_opn_fm_oper_update_kcode(apu_opn_fm_oper_t* oper);
int32_t apu_opn_fm_lfo_pmod(apu_opn_fm_lfo_t* lfo, int fmod, apu_opn_fm_chan_t* chan);
int32_t apu_opn_fm_lfo_amod(apu_opn_fm_lfo_t* lfo, apu_opn_fm_chan_t* chan, apu_opn_fm_oper_t* oper);

int32_t apu_opn_fm_pg_update(apu_opn_fm_oper_pg_t* pg, int fnum, int block, int kcode, int pmod);
int32_t apu_opn_fm_eg_update(apu_opn_fm_oper_eg_t* eg, int kcode, int eg_ctr);
int32_t apu_opn_fm_op_update(unsigned int fc, unsigned int eg_atten);
int32_t apu_opn_fm_oper_update(apu_opn_fm_chan_t* chan, apu_opn_fm_oper_t* oper, int32_t in, apu_opn_fm_state_t* state, apu_opn_fm_lfo_t* lfo, int ch3slot);
void apu_opn_fm_algo_update(apu_opn_fm_algo_t* algo);
void apu_opn_fm_lfo_update(apu_opn_fm_lfo_t* lfo);

void apu_opn_fm_chan_update_m1(apu_opn_fm_chan_t* chan, apu_opn_fm_state_t* state, apu_opn_fm_lfo_t* lfo, int ch3slot);
void apu_opn_fm_chan_update_m2(apu_opn_fm_chan_t* chan, apu_opn_fm_state_t* state, apu_opn_fm_lfo_t* lfo, int ch3slot);
void apu_opn_fm_chan_update_c1(apu_opn_fm_chan_t* chan, apu_opn_fm_state_t* state, apu_opn_fm_lfo_t* lfo, int ch3slot);
void apu_opn_fm_chan_update_c2(apu_opn_fm_chan_t* chan, apu_opn_fm_state_t* state, apu_opn_fm_lfo_t* lfo, int ch3slot);
void apu_opn_fm_chan_update(apu_opn_fm_chan_t* chan, int op, apu_opn_fm_state_t* state, apu_opn_fm_lfo_t* lfo, int ch3slot);

void apu_opn_fm_pg_state_add(hien_vm_t* vm, apu_opn_fm_oper_pg_t* state, char* path, ...);
void apu_opn_fm_eg_state_add(hien_vm_t* vm, apu_opn_fm_oper_eg_t* state, char* path, ...);
void apu_opn_fm_oper_state_add(hien_vm_t* vm, apu_opn_fm_oper_t* state, char* path, ...);
void apu_opn_fm_algo_state_add(hien_vm_t* vm, apu_opn_fm_algo_t* state, char* path, ...);
void apu_opn_fm_chan_state_add(hien_vm_t* vm, apu_opn_fm_chan_t* chan, char* path, ...);
void apu_opn_fm_lfo_state_add(hien_vm_t* vm, apu_opn_fm_lfo_t* state, char* path, ...);
void apu_opn_fm_state_state_add(hien_vm_t* vm, apu_opn_fm_state_t* state, char* path, ...);

#endif
