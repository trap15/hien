/**********************************************************
*
* apu/opn/ym2608_internal.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _APU_YM2608_INTERNAL_H_
#define _APU_YM2608_INTERNAL_H_

typedef struct apu_ym2608_t apu_ym2608_t;

struct apu_ym2608_t {
  hien_dev_t* psg;

  struct {
    apu_opn_fm_state_t state;
    apu_opn_fm_chan_t chan[6];
    apu_opn_fm_lfo_t lfo;

    int pre_ticks;

    int ticks;
    int32_t l,r;
  } fm;

  struct {
    apu_opn_rhythm_state_t state;
    apu_opn_rhythm_chan_t chan[6];

    int pre_ticks;

    int ticks;
    int32_t l,r;
  } rhythm;

  analog_t ao[2];

  apu_opn_timer_t timer[2];

  uint8_t regsel;
  int regext;
  uint8_t regs[2][256];

  /* Emulation state */
  int busy; /* 6 bits */

  int irqen; /* 5 bits */

  struct {
    analog_t chans[3];
    int sets[3];
    analog_t ao;
  } psg_intf;

  struct {
    int psg, fm;
    int timer[2];
    int rhythm;
    int raw;
  } prescale;
};

void apu_ym2608_update_output(hien_dev_t* dev, apu_ym2608_t* apu);

void apu_ym2608_fm_update(hien_dev_t* dev, apu_ym2608_t* apu);
void apu_ym2608_fm_reset(hien_dev_t* dev, apu_ym2608_t* apu);
uint8_t apu_ym2608_fm_read(hien_dev_t* dev, apu_ym2608_t* apu, int side, int reg);
void apu_ym2608_fm_write(hien_dev_t* dev, apu_ym2608_t* apu, int side, int reg, uint8_t data);
void apu_ym2608_fm_state_add(hien_vm_t* vm, apu_ym2608_t* apu, char* path, ...);

void apu_ym2608_rhythm_update(hien_dev_t* dev, apu_ym2608_t* apu);
void apu_ym2608_rhythm_reset(hien_dev_t* dev, apu_ym2608_t* apu);
void apu_ym2608_rhythm_write(hien_dev_t* dev, apu_ym2608_t* apu, int reg, uint8_t data);
void apu_ym2608_rhythm_state_add(hien_vm_t* vm, apu_ym2608_t* apu, char* path, ...);

#endif
