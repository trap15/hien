/**********************************************************
*
* apu/opn/ym2203_fm.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

/* Yamaha OPN (YM2203) FM core */

#include "hien.h"
#include "opn_internal.h"
#include "ym2203_internal.h"

#define OPER_OUT 0

/*
888     888 8888888b.  8888888b.        d8888 88888888888 8888888888 
888     888 888   Y88b 888  "Y88b      d88888     888     888        
888     888 888    888 888    888     d88P888     888     888        
888     888 888   d88P 888    888    d88P 888     888     8888888    
888     888 8888888P"  888    888   d88P  888     888     888        
888     888 888        888    888  d88P   888     888     888        
Y88b. .d88P 888        888  .d88P d8888888888     888     888        
 "Y88888P"  888        8888888P" d88P     888     888     8888888888 
*/
static void apu_ym2203_fm_output(hien_dev_t* dev, apu_ym2203_t* apu)
{
  int32_t level = 0;

  /* TODO: Verify the actual mixing and output levels */

#if OPER_OUT
  if(OPER_OUT&(1<<0)) level += apu->fm.chan[0].algo.out;
  if(OPER_OUT&(1<<1)) level += apu->fm.chan[1].algo.out;
  if(OPER_OUT&(1<<2)) level += apu->fm.chan[2].algo.out;
#else
  level = apu->fm.chan[0].algo.out +
          apu->fm.chan[1].algo.out +
          apu->fm.chan[2].algo.out;
#endif
  level /= 3;

  level <<= 18;

  apu->fm.ao = A_FIX2V(level, A_5V);
  dev_port_analog_write(dev, YM2203_PORT_OP_O, apu->fm.ao);
}

void apu_ym2203_fm_update(hien_dev_t* dev, apu_ym2203_t* apu)
{
  int ch, cyc;
  if(apu->fm.pre_ticks == 0) {
    ch = apu->fm.ticks / 4;
    cyc = apu->fm.ticks % 4;

    if(apu->fm.ticks == 0) {
      apu_ym2203_fm_output(dev, apu);
    }

    apu_opn_fm_chan_update(&apu->fm.chan[ch], cyc, &apu->fm.state, NULL, apu->fm.state.ch3slot && (ch == 3));

    apu->fm.ticks++;
    apu->fm.ticks %= 3*4;

    /* Update the EG counter */
    if(apu->fm.ticks == 0) {
      apu->fm.state.eg_cycle++;
      apu->fm.state.eg_cycle %= 3;
      if(apu->fm.state.eg_cycle == 0)
        apu->fm.state.eg_ctr++;
    }

    apu->fm.pre_ticks = apu->prescale.fm;
  }
  apu->fm.pre_ticks--;
}

/*
8888888b.  8888888888        d8888 8888888b.  
888   Y88b 888              d88888 888  "Y88b 
888    888 888             d88P888 888    888 
888   d88P 8888888        d88P 888 888    888 
8888888P"  888           d88P  888 888    888 
888 T88b   888          d88P   888 888    888 
888  T88b  888         d8888888888 888  .d88P 
888   T88b 8888888888 d88P     888 8888888P"  
*/
uint8_t apu_ym2203_fm_read(hien_dev_t* dev, apu_ym2203_t* apu, int reg)
{
  uint8_t ret = 0;
  int ch;
  int op;

  (void)dev;

  ch = (reg >> 0) & 3;
  op = (reg >> 2) & 3;
  if(ch == 3)
    return 0xFF;
  switch(reg) {
    case 0x30 ... 0x3F: /* DT/Multi */
      ret |= apu->fm.chan[ch].op[op].pg.dt << 4;
      ret |= apu->fm.chan[ch].op[op].pg.mul;
      break;
    case 0x40 ... 0x4F: /* TL */
      ret |= apu->fm.chan[ch].op[op].eg.tl;
      break;
    case 0x50 ... 0x5F: /* KS/AR */
      ret |= apu->fm.chan[ch].op[op].eg.ks << 6;
      ret |= apu->fm.chan[ch].op[op].eg.ar;
      break;
    case 0x60 ... 0x6F: /* DR */
      ret |= apu->fm.chan[ch].op[op].eg.dr;
      break;
    case 0x70 ... 0x7F: /* SR */
      ret |= apu->fm.chan[ch].op[op].eg.sr;
      break;
    case 0x80 ... 0x8F: /* SL/RR */
      ret |= apu->fm.chan[ch].op[op].eg.sl << 4;
      ret |= apu->fm.chan[ch].op[op].eg.rr << 4;
      break;
    case 0x90 ... 0x9F: /* SSG-EG */
      ret |= apu->fm.chan[ch].op[op].eg.ssgeg;
      break;
    case 0xA0 ... 0xA3: /* FNUM1 */
      ret |= apu->fm.chan[ch].op[0].pg.fnum & 0xFF;
      break;
    case 0xA4 ... 0xA7: /* BLK/FNUM2 */
      ret |= (apu->fm.chan[ch].op[0].pg.fnum >> 8) & 0x7;
      ret |= apu->fm.chan[ch].op[0].pg.block << 3;
      break;
    case 0xA8 ... 0xAB: /* 3CH FNUM1 */
      ret |= apu->fm.chan[2].op[ch+1].pg.fnum & 0xFF;
      break;
    case 0xAC ... 0xAF: /* 3CH BLK/FNUM2 */
      ret |= (apu->fm.chan[2].op[ch+1].pg.fnum >> 8) & 0x7;
      ret |= apu->fm.chan[2].op[ch+1].pg.fnum << 3;
      break;
    case 0xB0 ... 0xB3: /* FB/CONNECT */
      ret |= apu->fm.chan[ch].algo.con;
      ret |= apu->fm.chan[ch].algo.fb << 3;
      break;
  }
  return ret;
}

/*
888       888 8888888b.  8888888 88888888888 8888888888 
888   o   888 888   Y88b   888       888     888        
888  d8b  888 888    888   888       888     888        
888 d888b 888 888   d88P   888       888     8888888    
888d88888b888 8888888P"    888       888     888        
88888P Y88888 888 T88b     888       888     888        
8888P   Y8888 888  T88b    888       888     888        
888P     Y888 888   T88b 8888888     888     8888888888 
*/
void apu_ym2203_fm_write(hien_dev_t* dev, apu_ym2203_t* apu, int reg, uint8_t data)
{
  int ch;
  int op;
  int ch3slot;

  (void)dev;

  apu->busy = 48;

  ch = (reg >> 0) & 3;
  op = (reg >> 2) & 3;
  if(ch == 3)
    return;
  switch(reg) {
    case 0x28: /* Key on/off */
      ch = data & 3;
      if(ch == 3)
        return;
      ch3slot = apu->fm.state.ch3slot && (ch == 3);
      if(BIT(data, 4)) apu_opn_fm_keyon (&apu->fm.chan[ch], &apu->fm.chan[ch].op[0], ch3slot);
      else             apu_opn_fm_keyoff(&apu->fm.chan[ch], &apu->fm.chan[ch].op[0], ch3slot);
      if(BIT(data, 5)) apu_opn_fm_keyon (&apu->fm.chan[ch], &apu->fm.chan[ch].op[2], ch3slot);
      else             apu_opn_fm_keyoff(&apu->fm.chan[ch], &apu->fm.chan[ch].op[2], ch3slot);
      if(BIT(data, 6)) apu_opn_fm_keyon (&apu->fm.chan[ch], &apu->fm.chan[ch].op[1], ch3slot);
      else             apu_opn_fm_keyoff(&apu->fm.chan[ch], &apu->fm.chan[ch].op[1], ch3slot);
      if(BIT(data, 7)) apu_opn_fm_keyon (&apu->fm.chan[ch], &apu->fm.chan[ch].op[3], ch3slot);
      else             apu_opn_fm_keyoff(&apu->fm.chan[ch], &apu->fm.chan[ch].op[3], ch3slot);
      break;
    case 0x30 ... 0x3F: /* DT/Multi */
      apu->fm.chan[ch].op[op].pg.dt = (data >> 4) & 7;
      apu->fm.chan[ch].op[op].pg.mul = data & 0xF;
      break;
    case 0x40 ... 0x4F: /* TL */
      apu->fm.chan[ch].op[op].eg.tl = data & 0x7F;
      break;
    case 0x50 ... 0x5F: /* KS/AR */
      apu->fm.chan[ch].op[op].eg.ks = (data >> 6) & 3;
      apu->fm.chan[ch].op[op].eg.ar = (data >> 0) & 0x1F;
      break;
    case 0x60 ... 0x6F: /* DR */
      apu->fm.chan[ch].op[op].eg.dr = (data >> 0) & 0x1F;
      break;
    case 0x70 ... 0x7F: /* SR */
      apu->fm.chan[ch].op[op].eg.sr = (data >> 0) & 0x1F;
      break;
    case 0x80 ... 0x8F: /* SL/RR */
      apu->fm.chan[ch].op[op].eg.sl = (data >> 4) & 0xF;
      apu->fm.chan[ch].op[op].eg.rr = (data >> 0) & 0xF;
      if(apu->fm.chan[ch].op[op].eg.sl != 0xF)
        apu->fm.chan[ch].op[op].eg.precalc_sl = apu->fm.chan[ch].op[op].eg.sl << 5;
      else
        apu->fm.chan[ch].op[op].eg.precalc_sl = BITMASK(10);
      break;
    case 0x90 ... 0x9F: /* SSG-EG */
      apu->fm.chan[ch].op[op].eg.ssgeg = (data >> 0) & 0xF;
      if(apu->fm.chan[ch].op[op].eg.ssgeg)
        hienlog(dev->vm, LOG_ERR, "OPN: SSG-EG not supported!!\n");
      break;
    case 0xA0 ... 0xA3: /* FNUM1 */
      apu->fm.chan[ch].op[0].pg.fnum &= ~0x0FF;
      apu->fm.chan[ch].op[0].pg.fnum |= data;
      apu_opn_fm_oper_update_kcode(&apu->fm.chan[ch].op[0]);
      break;
    case 0xA4 ... 0xA7: /* BLK/FNUM2 */
      apu->fm.chan[ch].op[0].pg.fnum &= ~0x700;
      apu->fm.chan[ch].op[0].pg.fnum |= (data & 7) << 8;
      apu->fm.chan[ch].op[0].pg.block = (data >> 3) & 7;
      apu_opn_fm_oper_update_kcode(&apu->fm.chan[ch].op[0]);
      break;
    case 0xA8 ... 0xAB: /* 3CH FNUM1 */
      apu->fm.chan[2].op[ch+1].pg.fnum &= ~0x0FF;
      apu->fm.chan[2].op[ch+1].pg.fnum |= data;
      apu_opn_fm_oper_update_kcode(&apu->fm.chan[2].op[ch+1]);
      break;
    case 0xAC ... 0xAF: /* 3CH BLK/FNUM2 */
      apu->fm.chan[2].op[ch+1].pg.fnum &= ~0x700;
      apu->fm.chan[2].op[ch+1].pg.fnum |= (data & 7) << 8;
      apu->fm.chan[2].op[ch+1].pg.block = (data >> 3) & 7;
      apu_opn_fm_oper_update_kcode(&apu->fm.chan[2].op[ch+1]);
      break;
    case 0xB0 ... 0xB3: /* FB/CONNECT */
      apu->fm.chan[ch].algo.con = (data >> 0) & 7;
      apu->fm.chan[ch].algo.fb = (data >> 3) & 7;
      break;
  }
}

/*
8888888b.  8888888888 .d8888b.  8888888888 88888888888 
888   Y88b 888       d88P  Y88b 888            888     
888    888 888       Y88b.      888            888     
888   d88P 8888888    "Y888b.   8888888        888     
8888888P"  888           "Y88b. 888            888     
888 T88b   888             "888 888            888     
888  T88b  888       Y88b  d88P 888            888     
888   T88b 8888888888 "Y8888P"  8888888888     888     
*/
void apu_ym2203_fm_reset(hien_dev_t* dev, apu_ym2203_t* apu)
{
  int i;
  (void)dev;
  apu_opn_fm_chan_reset(&apu->fm.chan[0]);
  apu_opn_fm_chan_reset(&apu->fm.chan[1]);
  apu_opn_fm_chan_reset(&apu->fm.chan[2]);

  apu_opn_fm_state_reset(&apu->fm.state);

  apu->fm.pre_ticks = 0;
  apu->fm.ticks = 0;
  apu->fm.ao = A_0V;

  for(i = 0x30; i < 0xB4; i++)
    apu_ym2203_fm_write(dev, apu, i, 0);
}

void apu_ym2203_fm_state_add(hien_vm_t* vm, apu_ym2203_t* apu, char* path, ...)
{
  char* expath;
  va_list v;
  int i;

  va_start(v, path);
  expath = state_build_path_va(path, v);
  va_end(v);

  vm_state_add_int(vm, &apu->fm.ticks, expath, "ticks", NULL);
  vm_state_add_int(vm, &apu->fm.pre_ticks, expath, "pre_ticks", NULL);
  vm_state_add_analog(vm, &apu->fm.ao, expath, "ao", NULL);

  apu_opn_fm_state_state_add(vm, &apu->fm.state, expath, "state", NULL);
  for(i = 0; i < 3; i++) {
    char* cname = NULL;
    kuso_asprintf(&cname, "chan%d", i);
    apu_opn_fm_chan_state_add(vm, &apu->fm.chan[i], expath, cname, NULL);
  }

  mem_free(expath);
}
