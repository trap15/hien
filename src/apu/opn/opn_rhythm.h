/**********************************************************
*
* apu/opn/opn_rhythm.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _APU_OPN_RHYTHM_H_
#define _APU_OPN_RHYTHM_H_

typedef struct apu_opn_rhythm_state_t apu_opn_rhythm_state_t;

typedef struct apu_opn_rhythm_chan_t apu_opn_rhythm_chan_t;

struct apu_opn_rhythm_chan_t {
  int on;

  int pos;
  int start, end;

  int lr; /* 1/1 bit */
  int il; /* 5 bits */

  adpcm_decode_t dec;

  int32_t out;
};

struct apu_opn_rhythm_state_t {
  const uint8_t* srcbuf;
  size_t buflen;

  int rtl; /* 6 bits */
};

void apu_opn_rhythm_chan_reset(apu_opn_rhythm_chan_t* chan);
void apu_opn_rhythm_state_reset(apu_opn_rhythm_state_t* state);

void apu_opn_rhythm_set_srcbuf(apu_opn_rhythm_state_t* state, const void* buf, size_t len);

void apu_opn_rhythm_keyon(apu_opn_rhythm_chan_t* chan);
void apu_opn_rhythm_keyoff(apu_opn_rhythm_chan_t* chan);

void apu_opn_rhythm_chan_update(apu_opn_rhythm_chan_t* chan, apu_opn_rhythm_state_t* state);

void apu_opn_rhythm_state_state_add(hien_vm_t* vm, apu_opn_rhythm_state_t* state, char* path, ...);
void apu_opn_rhythm_chan_state_add(hien_vm_t* vm, apu_opn_rhythm_chan_t* chan, char* path, ...);

#endif
