/**********************************************************
*
* apu/opn/opn_rhythm.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

/* Yamaha OPN Rhythm */

#include "hien.h"
#include "opn_internal.h"

void apu_opn_rhythm_chan_reset(apu_opn_rhythm_chan_t* chan)
{
  adpcm_decode_ym_adpcma_init(&chan->dec);
  chan->on = 0;
  chan->pos = 0;
  chan->start = 0;
  chan->end = 0;
  chan->lr = 0;
  chan->il = 0x1F;
  chan->out = 0;
}
void apu_opn_rhythm_state_reset(apu_opn_rhythm_state_t* state)
{
  state->rtl = 0x3F;
}

void apu_opn_rhythm_set_srcbuf(apu_opn_rhythm_state_t* state, const void* buf, size_t len)
{
  state->srcbuf = buf;
  state->buflen = len;
}

void apu_opn_rhythm_keyon(apu_opn_rhythm_chan_t* chan)
{
  adpcm_decode_ym_adpcma_reset(&chan->dec);
  chan->out = 0;
  chan->pos = chan->start << 1;
  chan->on = TRUE;
}
void apu_opn_rhythm_keyoff(apu_opn_rhythm_chan_t* chan)
{
  chan->on = FALSE;
  chan->out = 0;
}

static uint8_t apu_opn_rhythm_get_sample(apu_opn_rhythm_state_t* state, int pos)
{
  uint8_t val;
  int addr;
  addr = (pos >> 1) % state->buflen;
  val = state->srcbuf[addr];
  if(!(pos & 1)) {
    val >>= 4;
  }
  val &= 0xF;
  return val;
}

static int32_t apu_opn_rhythm_chan_atten(apu_opn_rhythm_chan_t* chan, apu_opn_rhythm_state_t* state, int32_t smp)
{
/* TODO: Should be able to get rid of these XORs and make it mul=8+,shift=8- if I decide to not be lazy */
  int vol = (state->rtl^0x3F) + (chan->il^0x1F);
  int mul, shift;
  if(vol >= 63) {
    return 0;
  }else{
    mul   = 15 - ((vol >> 0) & BITMASK(3));
    shift =  1 + ((vol >> 3) & BITMASK(3));
  }

  return (smp * mul) >> shift;
}

void apu_opn_rhythm_chan_update(apu_opn_rhythm_chan_t* chan, apu_opn_rhythm_state_t* state)
{
  int32_t smp;
  uint8_t raw;

  if(!chan->on)
    return;

  if((chan->pos & BITMASK(21)) == ((chan->end<<1) & BITMASK(21))) {
    apu_opn_rhythm_keyoff(chan);
    return;
  }

  /* Get raw sample */
  raw = apu_opn_rhythm_get_sample(state, chan->pos);
  chan->pos++;
  smp = adpcm_decode_next(&chan->dec, raw);

  chan->out = apu_opn_rhythm_chan_atten(chan, state, smp);
}


void apu_opn_rhythm_state_state_add(hien_vm_t* vm, apu_opn_rhythm_state_t* state, char* path, ...)
{
  char* expath;
  va_list v;
  va_start(v, path);
  expath = state_build_path_va(path, v);
  va_end(v);

  vm_state_add_int(vm, &state->rtl, expath, "rtl", NULL);

  mem_free(expath);
}

void apu_opn_rhythm_chan_state_add(hien_vm_t* vm, apu_opn_rhythm_chan_t* chan, char* path, ...)
{
  char* expath;
  va_list v;
  va_start(v, path);
  expath = state_build_path_va(path, v);
  va_end(v);

  vm_state_add_int(vm, &chan->on, expath, "on", NULL);
  vm_state_add_int(vm, &chan->pos, expath, "pos", NULL);
  vm_state_add_int(vm, &chan->start, expath, "start", NULL);
  vm_state_add_int(vm, &chan->end, expath, "end", NULL);
  vm_state_add_int(vm, &chan->lr, expath, "lr", NULL);
  vm_state_add_int(vm, &chan->il, expath, "il", NULL);
  vm_state_add_int32(vm, &chan->out, expath, "out", NULL);

  adpcm_state_add(vm, &chan->dec, expath, "adpcm", NULL);

  mem_free(expath);
}
