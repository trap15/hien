/**********************************************************
*
* apu/opn/ym2608.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

/* Yamaha OPNA (YM2608) core */

#include "hien.h"
#include "opn_internal.h"
#include "ym2608_internal.h"

/*
888     888 8888888b.  8888888b.        d8888 88888888888 8888888888
888     888 888   Y88b 888  "Y88b      d88888     888     888
888     888 888    888 888    888     d88P888     888     888
888     888 888   d88P 888    888    d88P 888     888     8888888
888     888 8888888P"  888    888   d88P  888     888     888
888     888 888        888    888  d88P   888     888     888
Y88b. .d88P 888        888  .d88P d8888888888     888     888
 "Y88888P"  888        8888888P" d88P     888     888     8888888888
*/
static void apu_ym2608_update_irqs(hien_dev_t* dev, apu_ym2608_t* apu)
{
  int irq = FALSE;

  if(apu->timer[0].flag && BIT(apu->irqen, 0))
    irq = TRUE;
  if(apu->timer[1].flag && BIT(apu->irqen, 1))
    irq = TRUE;

  if(irq)
    dev_port_assert(dev, YM2608_PORT_IRQ);
  else
    dev_port_deassert(dev, YM2608_PORT_IRQ);
}

static void apu_ym2608_update_prescalers(hien_dev_t* dev, apu_ym2608_t* apu)
{
  static const int prescale_psg[4] = { 1, 1, 4, 2 };
  static const int prescale_fm[4]  = { 2, 2, 6, 3 };
  (void)dev;

  apu->prescale.psg = prescale_psg[apu->prescale.raw] * 2;
  apu->prescale.fm = prescale_fm[apu->prescale.raw];
  apu->prescale.rhythm = prescale_fm[apu->prescale.raw] * 6 * 4 * 3;
  apu->prescale.timer[0] = prescale_fm[apu->prescale.raw] * 6 * 4;
  apu->prescale.timer[1] = prescale_fm[apu->prescale.raw] * 6 * 4 * 16;

  dev_change_clock(apu->psg, dev->clock / apu->prescale.psg);

  apu_opn_timer_set_prescale(&apu->timer[0], apu->prescale.timer[0]);
  apu_opn_timer_set_prescale(&apu->timer[1], apu->prescale.timer[1]);
}

void apu_ym2608_update_output(hien_dev_t* dev, apu_ym2608_t* apu)
{
  int32_t l,r;

  /* TODO: Verify the actual mixing and output levels */

  l = ((apu->fm.l << 17) + (apu->rhythm.l << 19));
  r = ((apu->fm.r << 17) + (apu->rhythm.r << 19));

  apu->ao[0] = A_FIX2V(l, A_5V);
  apu->ao[1] = A_FIX2V(r, A_5V);

  dev_port_analog_write(dev, YM2608_PORT_OP_O_L, apu->ao[0]);
  dev_port_analog_write(dev, YM2608_PORT_OP_O_R, apu->ao[1]);
}

static void apu_ym2608_update(hien_dev_t* dev, uintmax_t data, uintmax_t mask)
{
  apu_ym2608_t* apu;
  (void)mask;
  apu = dev->data;

  if(data != 1) /* do nothing on falling edge */
    return;

  if(apu->busy > 0) apu->busy--;

  if(apu_opn_timer_update(&apu->timer[0])) {
    apu->fm.chan[2].op[0].csm_key = TRUE;
    apu->fm.chan[2].op[1].csm_key = TRUE;
    apu->fm.chan[2].op[2].csm_key = TRUE;
    apu->fm.chan[2].op[3].csm_key = TRUE;
  }
  apu_opn_timer_update(&apu->timer[1]);
  apu_ym2608_fm_update(dev, apu);
  apu_ym2608_rhythm_update(dev, apu);
  apu_ym2608_update_irqs(dev, apu);
}

/*
88888888888 8888888 888b     d888 8888888888 8888888b.
    888       888   8888b   d8888 888        888   Y88b
    888       888   88888b.d88888 888        888    888
    888       888   888Y88888P888 8888888    888   d88P
    888       888   888 Y888P 888 888        8888888P"
    888       888   888  Y8P  888 888        888 T88b
    888       888   888   "   888 888        888  T88b
    888     8888888 888       888 8888888888 888   T88b
*/
static void apu_ym2608_timer_reset(hien_dev_t* dev, apu_ym2608_t* apu, int idx)
{
  apu_opn_timer_reset(&apu->timer[idx]);

  apu_ym2608_update_irqs(dev, apu);
}

/*
8888888b.  8888888888        d8888 8888888b.
888   Y88b 888              d88888 888  "Y88b
888    888 888             d88P888 888    888
888   d88P 8888888        d88P 888 888    888
8888888P"  888           d88P  888 888    888
888 T88b   888          d88P   888 888    888
888  T88b  888         d8888888888 888  .d88P
888   T88b 8888888888 d88P     888 8888888P"
*/
static uintmax_t apu_ym2608_read(hien_dev_t* dev, addr_t addr, uintmax_t mask)
{
  uint8_t ret;
  apu_ym2608_t* apu;
  apu = dev->data;
  ret = 0;

  addr &= 3;
  switch(addr) {
    case 2: /* Status 1 */
      /* TODO: ADPCM-B status bits */
      /* fall-thru */
    case 0: /* Status 0 */
      ret |= (apu->busy ? 1 : 0) << 7;
      ret |= apu->timer[0].flag << 0;
      ret |= apu->timer[1].flag << 1;
      break;
    case 1: /* Register read */
      switch(apu->regsel) {
        case 0x00 ... 0x0F:
          ret = dev_read(apu->psg, SPACE_MAIN, addr, mask);
          break;
        case 0xFF: /* Device ID Register */
          ret = 1; /* I guess! */
          break;
        default:
          ret = 0;
          break;
      }
      break;
    case 3: /* ADPCM-B data */
      /* TODO: This */
      switch(apu->regsel) {
        case 0x08: /* ADPCM read */
          break;
        case 0x0F: /* ADC result */
          break;
      }
      break;
  }

  return ret & mask;
}

/*
888       888 8888888b.  8888888 88888888888 8888888888
888   o   888 888   Y88b   888       888     888
888  d8b  888 888    888   888       888     888
888 d888b 888 888   d88P   888       888     8888888
888d88888b888 8888888P"    888       888     888
88888P Y88888 888 T88b     888       888     888
8888P   Y8888 888  T88b    888       888     888
888P     Y888 888   T88b 8888888     888     8888888888
*/
static void apu_ym2608_write(hien_dev_t* dev, addr_t addr, uintmax_t data, uintmax_t mask)
{
  apu_ym2608_t* apu;
  apu = dev->data;

  addr &= 3;
  data &= 0xFF;
  mask &= 0xFF;
  switch(addr) {
    case 0: /* Register Select */
      COMBINE_DATA(apu->regsel, data, mask);
      apu->regext = FALSE;
      dev_write(apu->psg, SPACE_MAIN, addr, data, mask);
      switch(apu->regsel) {
        case 0x2D: /* Prescaler enable */
          apu->prescale.raw |= 2;
          apu_ym2608_update_prescalers(dev, apu);
          break;
        case 0x2E: /* Prescaler select */
          apu->prescale.raw |= 1;
          apu_ym2608_update_prescalers(dev, apu);
          break;
        case 0x2F: /* Prescaler clear */
          apu->prescale.raw = 0;
          apu_ym2608_update_prescalers(dev, apu);
          break;
      }
      break;
    case 1: /* Register Write */
      if(apu->regext)
        break;
      COMBINE_DATA(apu->regs[0][apu->regsel], data, mask);
      switch(apu->regsel) {
        /* PSG registers */
        case 0x00 ... 0x0F:
          dev_write(apu->psg, SPACE_MAIN, addr, data, mask);
          break;
        /* Rhythm registers */
        case 0x10 ... 0x1F:
          apu_ym2608_rhythm_write(dev, apu, apu->regsel, data);
          break;
        /* FM registers */
        case 0x30 ... 0xB7:
        case 0x28:
          apu_ym2608_fm_write(dev, apu, 0, apu->regsel, data);
          break;
        /* Control */
        case 0x21: /* Test port */
          break;
        case 0x22: /* LFO Freq. */
          apu->fm.lfo.freq = data & 7;
          if(BIT(data, 3)) {
            apu_opn_fm_lfo_on(&apu->fm.lfo);
          }else{
            apu_opn_fm_lfo_off(&apu->fm.lfo);
          }
          break;
        case 0x24: /* Timer-A MSB */
          apu->timer[0].freq &= 3;
          apu->timer[0].freq |= data << 2;
          break;
        case 0x25: /* Timer-A LSB */
          apu->timer[0].freq &= ~3;
          apu->timer[0].freq |= data & 3;
          break;
        case 0x26: /* Timer-B */
          apu->timer[1].freq = data;
          break;
        case 0x27: /* Control */
          switch(data & 0xC0) {
            case 0x80: /* CSM */
              apu->fm.state.csm = TRUE;
              apu->fm.state.ch3slot = TRUE;
              break;
            case 0x40: case 0xC0: /* 3slot */
              apu->fm.state.csm = FALSE;
              apu->fm.state.ch3slot = TRUE;
              break;
            default:
              apu->fm.state.csm = FALSE;
              apu->fm.state.ch3slot = FALSE;
              break;
          }
          if(BIT(data, 5)) apu->timer[1].flag = FALSE;
          if(BIT(data, 4)) apu->timer[0].flag = FALSE;
          apu->timer[1].en = BIT(data,3);
          apu->timer[0].en = BIT(data,2);
          if(BIT(data, 1)) apu_opn_timer_reload(&apu->timer[1]);
          else             apu->timer[1].step = 0;
          if(BIT(data, 0)) apu_opn_timer_reload(&apu->timer[0]);
          else             apu->timer[0].step = 0;
          break;
        case 0x29: /* SCH, IRQ En */
          apu->fm.state.sch = BIT(data, 7);
          apu->irqen = data & BITMASK(5);
          break;
      }
      break;
    case 2: /* Register Select Extended */
      COMBINE_DATA(apu->regsel, data, mask);
      apu->regext = TRUE;
      break;
    case 3: /* Register Write Extended */
      if(!apu->regext)
        break;
      COMBINE_DATA(apu->regs[1][apu->regsel], data, mask);
      switch(apu->regsel) {
        /* ADPCM registers */
        case 0x00 ... 0x0F:
          /* TODO: This */
          break;
        /* FM registers */
        case 0x30 ... 0xB7:
          apu_ym2608_fm_write(dev, apu, 1, apu->regsel, data);
          break;
      }
      break;
  }
}

/*
8888888b.   .d88888b.  8888888b. 88888888888 .d8888b.
888   Y88b d88P" "Y88b 888   Y88b    888    d88P  Y88b
888    888 888     888 888    888    888    Y88b.
888   d88P 888     888 888   d88P    888     "Y888b.
8888888P"  888     888 8888888P"     888        "Y88b.
888        888     888 888 T88b      888          "888
888        Y88b. .d88P 888  T88b     888    Y88b  d88P
888         "Y88888P"  888   T88b    888     "Y8888P"
*/

static uintmax_t apu_ym2608_read_ioa(hien_dev_t* dev, uintmax_t mask)
{
  apu_ym2608_t* apu;
  apu = dev->data;

  return dev_port_read(apu->psg, YM2149_PORT_IOA, mask);
}
static uintmax_t apu_ym2608_read_iob(hien_dev_t* dev, uintmax_t mask)
{
  apu_ym2608_t* apu;
  apu = dev->data;

  return dev_port_read(apu->psg, YM2149_PORT_IOB, mask);
}

static void apu_ym2608_write_ioa(hien_dev_t* dev, uintmax_t data, uintmax_t mask)
{
  apu_ym2608_t* apu;
  apu = dev->data;

  dev_port_write(apu->psg, YM2149_PORT_IOA, data, mask);
}
static void apu_ym2608_write_iob(hien_dev_t* dev, uintmax_t data, uintmax_t mask)
{
  apu_ym2608_t* apu;
  apu = dev->data;

  dev_port_write(apu->psg, YM2149_PORT_IOB, data, mask);
}

static void apu_ym2608_analog_set(hien_dev_t* dev, int ch, analog_t data)
{
  apu_ym2608_t* apu;
  apu = dev->data;

  apu->psg_intf.chans[ch] = data;
  apu->psg_intf.sets[ch] = TRUE;

  if(apu->psg_intf.sets[0] && apu->psg_intf.sets[1] && apu->psg_intf.sets[2]) {
    apu->psg_intf.sets[0] = FALSE;
    apu->psg_intf.sets[1] = FALSE;
    apu->psg_intf.sets[2] = FALSE;
    /* TODO: How does this actually work */
    apu->psg_intf.ao = apu->psg_intf.chans[0] +
                       apu->psg_intf.chans[1] +
                       apu->psg_intf.chans[2];
    apu->psg_intf.ao /= 3;
    dev_port_analog_write(dev, YM2608_PORT_AO, apu->psg_intf.ao);
  }
}

static void apu_ym2608_shuttle_ao_a(hien_dev_t* dev, analog_t data)
{
  hien_dev_t* apu;
  apu = dev->drv_data;

  apu_ym2608_analog_set(apu, 0, data);
}
static void apu_ym2608_shuttle_ao_b(hien_dev_t* dev, analog_t data)
{
  hien_dev_t* apu;
  apu = dev->drv_data;

  apu_ym2608_analog_set(apu, 1, data);
}
static void apu_ym2608_shuttle_ao_c(hien_dev_t* dev, analog_t data)
{
  hien_dev_t* apu;
  apu = dev->drv_data;

  apu_ym2608_analog_set(apu, 2, data);
}
/*
8888888b.  8888888888 .d8888b.  8888888888 88888888888
888   Y88b 888       d88P  Y88b 888            888
888    888 888       Y88b.      888            888
888   d88P 8888888    "Y888b.   8888888        888
8888888P"  888           "Y88b. 888            888
888 T88b   888             "888 888            888
888  T88b  888       Y88b  d88P 888            888
888   T88b 8888888888 "Y8888P"  8888888888     888
*/
static void apu_ym2608_reset(hien_dev_t* dev)
{
  apu_ym2608_t* apu;
  apu = dev->data;

  apu->busy = 0;
  apu->regext = FALSE;

  apu->irqen = BITMASK(5);

  apu->ao[0] = A_0V;
  apu->ao[1] = A_0V;

  apu_ym2608_timer_reset(dev, apu, 0);
  apu_ym2608_timer_reset(dev, apu, 1);
  apu_ym2608_fm_reset(dev, apu);
  apu_ym2608_rhythm_reset(dev, apu);
  dev_reset(apu->psg);

  apu->prescale.raw = 2;
  apu_ym2608_update_prescalers(dev, apu);

  apu->psg_intf.sets[0] = FALSE;
  apu->psg_intf.sets[1] = FALSE;
  apu->psg_intf.sets[2] = FALSE;
}

/*
8888888b.  8888888888 888      8888888888 88888888888 8888888888
888  "Y88b 888        888      888            888     888
888    888 888        888      888            888     888
888    888 8888888    888      8888888        888     8888888
888    888 888        888      888            888     888
888    888 888        888      888            888     888
888  .d88P 888        888      888            888     888
8888888P"  8888888888 88888888 8888888888     888     8888888888
*/
static void apu_ym2608_delete(hien_dev_t* dev)
{
  apu_ym2608_t* apu;
  apu = dev->data;

  vm_remove_device(dev->vm, apu->psg);

  mem_free(apu);
}

/*
888b    888 8888888888 888       888
8888b   888 888        888   o   888
88888b  888 888        888  d8b  888
888Y88b 888 8888888    888 d888b 888
888 Y88b888 888        888d88888b888
888  Y88888 888        88888P Y88888
888   Y8888 888        8888P   Y8888
888    Y888 8888888888 888P     Y888
*/
hien_dev_t* apu_ym2608_new(hien_vm_t* vm, char* name, hz_t clock)
{
  hien_dev_t* dev;
  apu_ym2608_t* apu;

  dev = dev_new(vm, name, clock);
  dev->delete = apu_ym2608_delete;
  dev->reset = apu_ym2608_reset;

  dev_port_set_callbacks(dev, PORT_CLOCK, NULL, apu_ym2608_update);

  dev_space_setup(dev, SPACE_MAIN, 8);
  dev_space_set_callbacks(dev, SPACE_MAIN, apu_ym2608_read, apu_ym2608_write);

  dev_port_setup(dev, YM2608_PORT_IRQ,   1, PORT_OUTPUT);
  dev_port_setup(dev, YM2608_PORT_IOA,   8, PORT_INOUT);
  dev_port_setup(dev, YM2608_PORT_IOB,   8, PORT_INOUT);
  dev_port_setup(dev, YM2608_PORT_AO,    0, PORT_OUTPUT | PORT_ANALOG);
  dev_port_setup(dev, YM2608_PORT_OP_O_L,0, PORT_OUTPUT | PORT_ANALOG);
  dev_port_setup(dev, YM2608_PORT_OP_O_R,0, PORT_OUTPUT | PORT_ANALOG);

  dev_port_set_callbacks(dev, YM2608_PORT_IOA,  apu_ym2608_read_ioa, apu_ym2608_write_ioa);
  dev_port_set_callbacks(dev, YM2608_PORT_IOB,  apu_ym2608_read_iob, apu_ym2608_write_iob);

  apu = mem_alloc_type(apu_ym2608_t);
  dev->data = apu;

  char* psgname;
  psgname = state_build_path(name, "psg", NULL);
  apu->psg = apu_ym2149_new(vm, psgname, clock);
  mem_free(psgname);
  apu->psg->drv_data = dev;

  dev_port_set_analog_callbacks(apu->psg, YM2149_PORT_AO_A, NULL, apu_ym2608_shuttle_ao_a);
  dev_port_set_analog_callbacks(apu->psg, YM2149_PORT_AO_B, NULL, apu_ym2608_shuttle_ao_b);
  dev_port_set_analog_callbacks(apu->psg, YM2149_PORT_AO_C, NULL, apu_ym2608_shuttle_ao_c);

  vm_add_device(vm, apu->psg);

  /* Init timers */
  apu_opn_timer_init(&apu->timer[0], 1024);
  apu_opn_timer_init(&apu->timer[1], 256);

  dev_reset(dev);

  apu_ym2608_fm_state_add(vm, apu, name, "fm", NULL);
  apu_ym2608_rhythm_state_add(vm, apu, name, "rhythm", NULL);
  apu_opn_timer_state_add(vm, &apu->timer[0], name, "timerA", NULL);
  apu_opn_timer_state_add(vm, &apu->timer[1], name, "timerB", NULL);

  vm_state_add_int(vm, &apu->prescale.psg, name, "prescale", "psg", NULL);
  vm_state_add_int(vm, &apu->prescale.fm, name, "prescale", "fm", NULL);
  vm_state_add_int(vm, &apu->prescale.timer[0], name, "prescale", "timerA", NULL);
  vm_state_add_int(vm, &apu->prescale.timer[1], name, "prescale", "timerB", NULL);
  vm_state_add_int(vm, &apu->prescale.rhythm, name, "prescale", "rhythm", NULL);
  vm_state_add_int(vm, &apu->prescale.raw, name, "prescale", "raw", NULL);

  vm_state_add_analog_arr(vm, 2, apu->ao, name, "ao", NULL);

  vm_state_add_int(vm, &apu->busy, name, "busy", NULL);
  vm_state_add_int(vm, &apu->irqen, name, "irqen", NULL);

  vm_state_add_uint8(vm, &apu->regsel, name, "regsel", NULL);
  vm_state_add_int(vm, &apu->regext, name, "regext", NULL);
  vm_state_add_uint8_arr(vm, 256, apu->regs[0], name, "regsA", NULL);
  vm_state_add_uint8_arr(vm, 256, apu->regs[1], name, "regsB", NULL);

  vm_state_add_analog(vm, &apu->psg_intf.chans[0], name, "psg_intf", "chan0_ao", NULL);
  vm_state_add_analog(vm, &apu->psg_intf.chans[1], name, "psg_intf", "chan1_ao", NULL);
  vm_state_add_analog(vm, &apu->psg_intf.chans[2], name, "psg_intf", "chan2_ao", NULL);

  vm_state_add_int(vm, &apu->psg_intf.sets[0], name, "psg_intf", "chan0_set", NULL);
  vm_state_add_int(vm, &apu->psg_intf.sets[1], name, "psg_intf", "chan1_set", NULL);
  vm_state_add_int(vm, &apu->psg_intf.sets[2], name, "psg_intf", "chan2_set", NULL);

  vm_state_add_analog(vm, &apu->psg_intf.ao, name, "psg_intf", "ao", NULL);

  return dev;
}

hien_dev_info_t apu_ym2608_def = {
  .type = "YM2608",
  .ctor = "apu_ym2608_new",
  .use_clk = TRUE,
  .has_save = 1,
};
