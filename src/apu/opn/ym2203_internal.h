/**********************************************************
*
* apu/opn/ym2203_internal.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _APU_YM2203_INTERNAL_H_
#define _APU_YM2203_INTERNAL_H_

typedef struct apu_ym2203_t apu_ym2203_t;

struct apu_ym2203_t {
  hien_dev_t* psg;

  struct {
    apu_opn_fm_state_t state;
    apu_opn_fm_chan_t chan[3];

    int ticks;

    int pre_ticks;
    analog_t ao;
  } fm;

  apu_opn_timer_t timer[2];

  uint8_t regsel;
  uint8_t regs[256];

  /* Emulation state */
  int busy; /* 1 bit */

  struct {
    int psg, fm;
    int timer[2];
    int raw;
  } prescale;
};

void apu_ym2203_fm_update(hien_dev_t* dev, apu_ym2203_t* apu);
uint8_t apu_ym2203_fm_read(hien_dev_t* dev, apu_ym2203_t* apu, int reg);
void apu_ym2203_fm_write(hien_dev_t* dev, apu_ym2203_t* apu, int reg, uint8_t data);
void apu_ym2203_fm_reset(hien_dev_t* dev, apu_ym2203_t* apu);
void apu_ym2203_fm_state_add(hien_vm_t* vm, apu_ym2203_t* apu, char* path, ...);

#endif
