/**********************************************************
*
* apu/opn/ym2608_rhythm.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

/* Yamaha OPN (YM2608) Rhythm core */

#include "hien.h"
#include "opn_internal.h"
#include "ym2608_internal.h"

#include "opn_rhythm_tbls.h"

/*
888     888 8888888b.  8888888b.        d8888 88888888888 8888888888 
888     888 888   Y88b 888  "Y88b      d88888     888     888        
888     888 888    888 888    888     d88P888     888     888        
888     888 888   d88P 888    888    d88P 888     888     8888888    
888     888 8888888P"  888    888   d88P  888     888     888        
888     888 888        888    888  d88P   888     888     888        
Y88b. .d88P 888        888  .d88P d8888888888     888     888        
 "Y88888P"  888        8888888P" d88P     888     888     8888888888 
*/
static void apu_ym2608_rhythm_output_one(apu_opn_rhythm_chan_t* chan, int32_t* l, int32_t* r)
{
  if(BIT(chan->lr, 1))
    *l += chan->out;
  if(BIT(chan->lr, 0))
    *r += chan->out;
}

static void apu_ym2608_rhythm_output(hien_dev_t* dev, apu_ym2608_t* apu)
{
  int32_t l = 0;
  int32_t r = 0;

  /* TODO: Verify the actual mixing and output levels */

  apu_ym2608_rhythm_output_one(&apu->rhythm.chan[0], &l, &r);
  apu_ym2608_rhythm_output_one(&apu->rhythm.chan[1], &l, &r);
  apu_ym2608_rhythm_output_one(&apu->rhythm.chan[2], &l, &r);
  apu_ym2608_rhythm_output_one(&apu->rhythm.chan[3], &l, &r);
  apu_ym2608_rhythm_output_one(&apu->rhythm.chan[4], &l, &r);
  apu_ym2608_rhythm_output_one(&apu->rhythm.chan[5], &l, &r);
  l /= 6;
  r /= 6;

  apu->rhythm.l = l;
  apu->rhythm.r = r;

  apu_ym2608_update_output(dev, apu);
}

void apu_ym2608_rhythm_update(hien_dev_t* dev, apu_ym2608_t* apu)
{
  if(apu->rhythm.pre_ticks == 0) {
    apu_opn_rhythm_chan_update(&apu->rhythm.chan[0], &apu->rhythm.state);
    apu_opn_rhythm_chan_update(&apu->rhythm.chan[1], &apu->rhythm.state);
    apu_opn_rhythm_chan_update(&apu->rhythm.chan[2], &apu->rhythm.state);
    apu_opn_rhythm_chan_update(&apu->rhythm.chan[3], &apu->rhythm.state);
    if(apu->rhythm.ticks == 0) {
      apu_opn_rhythm_chan_update(&apu->rhythm.chan[4], &apu->rhythm.state);
      apu_opn_rhythm_chan_update(&apu->rhythm.chan[5], &apu->rhythm.state);
    }
    apu_ym2608_rhythm_output(dev, apu);

    apu->rhythm.ticks ^= 1;

    apu->rhythm.pre_ticks = apu->prescale.rhythm;
  }
  apu->rhythm.pre_ticks--;
}

/*
888       888 8888888b.  8888888 88888888888 8888888888 
888   o   888 888   Y88b   888       888     888        
888  d8b  888 888    888   888       888     888        
888 d888b 888 888   d88P   888       888     8888888    
888d88888b888 8888888P"    888       888     888        
88888P Y88888 888 T88b     888       888     888        
8888P   Y8888 888  T88b    888       888     888        
888P     Y888 888   T88b 8888888     888     8888888888 
*/
void apu_ym2608_rhythm_write(hien_dev_t* dev, apu_ym2608_t* apu, int reg, uint8_t data)
{
  int ch;

  (void)dev;

  ch = (reg >> 0) & 7;
  if(ch >= 6)
    return;

  switch(reg) {
    case 0x10: /* Damp/Key */
      for(ch = 0; ch < 6; ch++) {
        if(BIT(data, ch)) {
          if(BIT(data, 7)) apu_opn_rhythm_keyoff(&apu->rhythm.chan[ch]);
          else             apu_opn_rhythm_keyon( &apu->rhythm.chan[ch]);
        }
      }
      break;
    case 0x11: /* Rhythm TL */
      apu->rhythm.state.rtl = data & BITMASK(6);
      break;
    case 0x18 ... 0x1D: /* LR/IL */
      apu->rhythm.chan[ch].lr = (data >> 6) & 3;
      apu->rhythm.chan[ch].il = (data >> 0) & 0x1F;
      break;
  }
}

/*
8888888b.  8888888888 .d8888b.  8888888888 88888888888 
888   Y88b 888       d88P  Y88b 888            888     
888    888 888       Y88b.      888            888     
888   d88P 8888888    "Y888b.   8888888        888     
8888888P"  888           "Y88b. 888            888     
888 T88b   888             "888 888            888     
888  T88b  888       Y88b  d88P 888            888     
888   T88b 8888888888 "Y8888P"  8888888888     888     
*/
void apu_ym2608_rhythm_reset(hien_dev_t* dev, apu_ym2608_t* apu)
{
  (void)dev;
  int ch;
  for(ch = 0; ch < 6; ch++) {
    apu_opn_rhythm_chan_reset(&apu->rhythm.chan[ch]);
    apu->rhythm.chan[ch].start = _opn_tbl_rhythm_addrs[ch*2+0];
    apu->rhythm.chan[ch].end   = _opn_tbl_rhythm_addrs[ch*2+1];
  }

  apu_opn_rhythm_state_reset(&apu->rhythm.state);
  apu_opn_rhythm_set_srcbuf(&apu->rhythm.state, _opn_tbl_rhythm_rom, 0x2000);

  apu->rhythm.pre_ticks = 0;
  apu->rhythm.ticks = 0;
  apu->rhythm.l = 0;
  apu->rhythm.r = 0;
}

void apu_ym2608_rhythm_state_add(hien_vm_t* vm, apu_ym2608_t* apu, char* path, ...)
{
  char* expath;
  va_list v;
  int i;

  va_start(v, path);
  expath = state_build_path_va(path, v);
  va_end(v);

  vm_state_add_int(vm, &apu->rhythm.ticks, expath, "ticks", NULL);
  vm_state_add_int(vm, &apu->rhythm.pre_ticks, expath, "pre_ticks", NULL);
  vm_state_add_int32(vm, &apu->rhythm.l, expath, "l", NULL);
  vm_state_add_int32(vm, &apu->rhythm.r, expath, "r", NULL);

  apu_opn_rhythm_state_state_add(vm, &apu->rhythm.state, expath, "state", NULL);
  for(i = 0; i < 6; i++) {
    char* cname = NULL;
    kuso_asprintf(&cname, "chan%d", i);
    apu_opn_rhythm_chan_state_add(vm, &apu->rhythm.chan[i], expath, cname, NULL);
  }

  mem_free(expath);
}
