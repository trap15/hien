/**********************************************************
*
* apu/opn/opn_adpcm.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _APU_OPN_ADPCM_H_
#define _APU_OPN_ADPCM_H_

typedef struct apu_opn_adpcm_t apu_opn_adpcm_t;

struct apu_opn_adpcm_t {
  hien_timer_t* tmr;
};

#endif
