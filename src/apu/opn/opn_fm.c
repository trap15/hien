/**********************************************************
*
* apu/opn/opn_fm.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

/* Yamaha OPN FM part */

/* Big ups to Nemesis and his helpful posts on spritesmind.
 * Important posts:
 * OP unit: http://gendev.spritesmind.net/forum/viewtopic.php?p=6114#6114
 * EG unit: http://gendev.spritesmind.net/forum/viewtopic.php?p=5716#5716
 * PG unit: http://gendev.spritesmind.net/forum/viewtopic.php?p=6177#6177
 * More EG unit (corrections, lots of SSG-EG and CSM info):
 *          http://gendev.spritesmind.net/forum/viewtopic.php?p=7967#7967
 * LFO:     http://gendev.spritesmind.net/forum/viewtopic.php?p=8905#8905
 *          http://gendev.spritesmind.net/forum/viewtopic.php?p=8908#8908
 *          http://gendev.spritesmind.net/forum/viewtopic.php?p=9005#9005
 *          http://gendev.spritesmind.net/forum/viewtopic.php?p=16436#16436
 * DAC:     http://gendev.spritesmind.net/forum/viewtopic.php?p=6258#6258
 */
#include "hien.h"
#include "opn_internal.h"
#include "opn_fm_tbls.h"

#define OPN_EGSTATE_ATK 0
#define OPN_EGSTATE_DEC 1
#define OPN_EGSTATE_SUS 2
#define OPN_EGSTATE_REL 3

/*
8888888b.   .d8888b.       888     888 888b    888 8888888 88888888888
888   Y88b d88P  Y88b      888     888 8888b   888   888       888
888    888 888    888      888     888 88888b  888   888       888
888   d88P 888             888     888 888Y88b 888   888       888
8888888P"  888  88888      888     888 888 Y88b888   888       888
888        888    888      888     888 888  Y88888   888       888
888        Y88b  d88P      Y88b. .d88P 888   Y8888   888       888
888         "Y8888P88       "Y88888P"  888    Y888 8888888     888
*/
int32_t apu_opn_fm_pg_update(apu_opn_fm_oper_pg_t* pg, int fnum, int block, int kcode, int pmod)
{
  int fcode;
  int detune;

  /* Calculate detune */
  detune = _opn_tbl_dt[kcode][pg->dt];

  /* Calculate Freq-code */
  fcode = fnum + pmod;

  /* Should figure out a nicer way to do this, can't break C standard of no negative shifts */
  if(block == 0) {
    fcode >>= 1;
  }else{
    fcode <<= block - 1;
  }

  /* Apply detune to freq-code */
  fcode += detune;
  fcode &= BITMASK(17); /* Fcode is 17bit here */

  /* Apply multiplier to freq-code */
  fcode *= _opn_tbl_mul[pg->mul];
  fcode >>= 1;
  fcode &= BITMASK(20); /* Fcode is 20bit here */

  /* Increment phase */
  pg->phase += fcode;
  pg->phase &= BITMASK(20); /* Phase is 20bits always */

  return pg->phase >> 10;
}

/*
8888888888 .d8888b.       888     888 888b    888 8888888 88888888888
888       d88P  Y88b      888     888 8888b   888   888       888
888       888    888      888     888 88888b  888   888       888
8888888   888             888     888 888Y88b 888   888       888
888       888  88888      888     888 888 Y88b888   888       888
888       888    888      888     888 888  Y88888   888       888
888       Y88b  d88P      Y88b. .d88P 888   Y8888   888       888
8888888888 "Y8888P88       "Y88888P"  888    Y888 8888888     888
*/
/* Re-calculate the EG rate */
void apu_opn_fm_eg_recalc(apu_opn_fm_oper_eg_t* eg, int kcode)
{
  int r, rate;

  /* Pick the Rate used */
  switch(eg->state) {
    case OPN_EGSTATE_ATK:
      r = eg->ar;
      break;
    case OPN_EGSTATE_DEC:
      r = eg->dr;
      break;
    case OPN_EGSTATE_SUS:
      r = eg->sr;
      break;
    case OPN_EGSTATE_REL:
      if(eg->rr == 0) r = 0;
      else            r = (eg->rr << 1) | 1;
      break;
    default:
      r = 0;
      break;
  }

  /* If rate register is 0, rate = 0 */
  if(r == 0) rate = 0;
  else       rate = (r * 2) + _opn_tbl_rks[kcode][eg->ks];

  /* Rate is saturated 6-bit */
  if(rate > 63) rate = 63;

  if(eg->state == OPN_EGSTATE_ATK && rate >= 62) { /* Attack lock */
    eg->atten = 0;
  }

  eg->rate = rate;
}

/* Set EG state (and recalculate EG rate) */
void apu_opn_fm_eg_set_state(apu_opn_fm_oper_eg_t* eg, int kcode, int state)
{
  eg->state = state;

  apu_opn_fm_eg_recalc(eg, kcode);
}

/* Get the current EG attenuation */
int32_t apu_opn_fm_eg_atten(apu_opn_fm_oper_eg_t* eg)
{
  int32_t atten;
  atten  = eg->atten;
  /* TL simply adds to the actual EG attenuation */
  atten += eg->tl << 3;

  /* TODO: Does attenuation actually saturate?
   * Significant problems occur if it doesn't so hmm...
   */
  if(atten > BITMASK(10)) atten = BITMASK(10);

  return atten;
}

/* Update envelope generator for one operator */
int32_t apu_opn_fm_eg_update(apu_opn_fm_oper_eg_t* eg, int kcode, int eg_ctr)
{
  int rate;
  int cycle;
  int atten_inc;

  rate = eg->rate;
  /* Update occurs when eg_ctr & mask == 0 */
  if(eg_ctr & _opn_tbl_ctr_mask[rate])
    return apu_opn_fm_eg_atten(eg);

  cycle = eg_ctr >> _opn_tbl_ctr_shift[rate];
  atten_inc = _opn_tbl_attinc[rate][cycle & 7];

  /* Attack has a different attenuation increment algo */
  if(eg->state == OPN_EGSTATE_ATK) {
    atten_inc = (~eg->atten * atten_inc) >> 4;
  }

  if(!(eg->state == OPN_EGSTATE_ATK && rate >= 62)) { /* Attack lock */
    eg->atten += atten_inc;
  }

  /* Handle switching states and overflow */
  switch(eg->state) {
    case OPN_EGSTATE_ATK:
      /* Attack makes atten mask to 10bit (for proper 'negative' handling) */
      eg->atten &= BITMASK(10);
      if(eg->atten == 0)
        apu_opn_fm_eg_set_state(eg, kcode, OPN_EGSTATE_DEC);
      break;

    case OPN_EGSTATE_DEC:
      if(eg->atten >= eg->precalc_sl)
        apu_opn_fm_eg_set_state(eg, kcode, OPN_EGSTATE_SUS);
      /* fall-thru */
    default:
      /* All non-attack states make atten saturate */
      if(eg->atten >= BITMASK(10))
        eg->atten = BITMASK(10);
      break;
  }

  return apu_opn_fm_eg_atten(eg);
}

/*
 .d88888b.  8888888b.       888     888 888b    888 8888888 88888888888
d88P" "Y88b 888   Y88b      888     888 8888b   888   888       888
888     888 888    888      888     888 88888b  888   888       888
888     888 888   d88P      888     888 888Y88b 888   888       888
888     888 8888888P"       888     888 888 Y88b888   888       888
888     888 888             888     888 888  Y88888   888       888
Y88b. .d88P 888             Y88b. .d88P 888   Y8888   888       888
 "Y88888P"  888              "Y88888P"  888    Y888 8888888     888
*/
int32_t apu_opn_fm_op_update(unsigned int fc, unsigned int eg_atten)
{
  int sine; /* 4.8 bit */
  int atten; /* 5.8 bit */
  int num; /* 13 bit */
  int shift; /* 5 bit */
  int bit9;

  /* Top two bits are used for mirroring, bottom 8 are the sine table index */
  bit9 = BIT(fc, 9);
  if(BIT(fc, 8)) {
    fc = ~fc;
  }
  fc &= BITMASK(8);

  sine = _opn_tbl_sin[fc];

  atten = sine + (eg_atten << 2);

  shift = (atten >> 8) & 0x1F; /* Top 5 bits are shift */
  num = _opn_tbl_pow[atten & BITMASK(8)] << 2; /* Bottom 8 are power table index */
  num >>= shift;
  num &= BITMASK(13); /* Output is 13bit + sign */

  return bit9 ? -num : num;
}

/*
 .d88888b.  8888888b.  8888888888 8888888b.         d8888 88888888888 .d88888b.  8888888b.
d88P" "Y88b 888   Y88b 888        888   Y88b       d88888     888    d88P" "Y88b 888   Y88b
888     888 888    888 888        888    888      d88P888     888    888     888 888    888
888     888 888   d88P 8888888    888   d88P     d88P 888     888    888     888 888   d88P
888     888 8888888P"  888        8888888P"     d88P  888     888    888     888 8888888P"
888     888 888        888        888 T88b     d88P   888     888    888     888 888 T88b
Y88b. .d88P 888        888        888  T88b   d8888888888     888    Y88b. .d88P 888  T88b
 "Y88888P"  888        8888888888 888   T88b d88P     888     888     "Y88888P"  888   T88b
*/
int32_t apu_opn_fm_oper_update(apu_opn_fm_chan_t* chan, apu_opn_fm_oper_t* oper, int32_t in, apu_opn_fm_state_t* state, apu_opn_fm_lfo_t* lfo, int ch3slot)
{
  unsigned int phase; /* 10 bit */
  unsigned int atten; /* 4.6 bit */

  int key = oper->key | (oper->csm_key & state->csm);

  int kcode, fnum, block;
  if(ch3slot) {
    kcode = oper->kcode;
    fnum = oper->pg.fnum;
    block = oper->pg.block;
  }else{
    kcode = chan->op[0].kcode;
    fnum = chan->op[0].pg.fnum;
    block = chan->op[0].pg.block;
  }

  if(key != oper->key_old) {
    if(key) { /* Key On */
      /* Reset PG */
      oper->pg.phase = 0;

      apu_opn_fm_eg_set_state(&oper->eg, kcode, OPN_EGSTATE_ATK);
    }else{ /* Key Off */
      apu_opn_fm_eg_set_state(&oper->eg, kcode, OPN_EGSTATE_REL);
    }
    oper->csm_key = FALSE;
  }
  oper->key_old = key;

  /* Calculate phase */
  phase = (in >> 1) & BITMASK(10); /* Modulation drops bottom bit, then use next 10bits */
  if(lfo != NULL) { /* Has LFO */
    phase += apu_opn_fm_pg_update(&oper->pg, fnum, block, kcode, apu_opn_fm_lfo_pmod(lfo, fnum, chan));
  }else{
    phase += apu_opn_fm_pg_update(&oper->pg, fnum, block, kcode, 0);
  }
  phase &= BITMASK(10); /* Phase is 10bit */

  /* Calculate attenuation */
  if(state->eg_cycle)
    atten = apu_opn_fm_eg_atten(&oper->eg);
  else
    atten = apu_opn_fm_eg_update(&oper->eg, kcode, state->eg_ctr);

  if(lfo != NULL) {
    atten += apu_opn_fm_lfo_amod(lfo, chan, oper);
    /* TODO: Does attenuation actually saturate? */
    if(atten > BITMASK(10)) atten = BITMASK(10);
  }

  /* Calculate output */
  return apu_opn_fm_op_update(phase, atten);
}

void apu_opn_fm_oper_update_kcode(apu_opn_fm_oper_t* oper)
{
  oper->kcode  = oper->pg.block << 2;
  oper->kcode |= _opn_tbl_kc[oper->pg.fnum >> 8];
}

/*
       d8888 888      .d8888b.   .d88888b.  8888888b.  8888888 88888888888 888    888 888b     d888
      d88888 888     d88P  Y88b d88P" "Y88b 888   Y88b   888       888     888    888 8888b   d8888
     d88P888 888     888    888 888     888 888    888   888       888     888    888 88888b.d88888
    d88P 888 888     888        888     888 888   d88P   888       888     8888888888 888Y88888P888
   d88P  888 888     888  88888 888     888 8888888P"    888       888     888    888 888 Y888P 888
  d88P   888 888     888    888 888     888 888 T88b     888       888     888    888 888  Y8P  888
 d8888888888 888     Y88b  d88P Y88b. .d88P 888  T88b    888       888     888    888 888   "   888
d88P     888 88888888 "Y8888P88  "Y88888P"  888   T88b 8888888     888     888    888 888       888
*/
void apu_opn_fm_algo_update(apu_opn_fm_algo_t* algo)
{
  algo->m1 = 0;
  algo->c1 = 0;
  algo->m2 = 0;
  algo->c2 = 0;
  algo->out = 0;

  switch(algo->con) {
/* M1---C1---MEM---M2---C2---> */
    case 0:
      algo->om1  = &algo->c1;
      algo->oc1  = &algo->mem;
      algo->omem = &algo->m2;
      algo->om2  = &algo->c2;
      algo->oc2  = &algo->out;
      break;
/* M1---+---MEM---M2---C2--->
 * C1---+
 */
    case 1:
      algo->om1  = &algo->mem;
      algo->oc1  = &algo->mem;
      algo->omem = &algo->m2;
      algo->om2  = &algo->c2;
      algo->oc2  = &algo->out;
      break;
/* M1--------------+---C2--->
 * C1---MEM---M2---+
 */
    case 2:
      algo->om1  = &algo->c2;
      algo->oc1  = &algo->mem;
      algo->omem = &algo->m2;
      algo->om2  = &algo->c2;
      algo->oc2  = &algo->out;
      break;
/* M1---C1---MEM---+---C2--->
              M2---+
*/
    case 3:
      algo->om1  = &algo->c1;
      algo->oc1  = &algo->mem;
      algo->omem = &algo->c2;
      algo->om2  = &algo->c2;
      algo->oc2  = &algo->out;
      break;
/* M1---C1---+
 *           +--->
 * M2---C2---+
 */
    case 4:
      algo->om1  = &algo->c1;
      algo->oc1  = &algo->out;
      algo->om2  = &algo->c2;
      algo->oc2  = &algo->out;
      algo->omem = &algo->mem;
      break;
/*      +---C1---+
 * M1---+-MEM-M2-+--->
 *      +---C2---+
 */
    case 5:
      algo->om1  = NULL;
      algo->oc1  = &algo->out;
      algo->omem = &algo->m2;
      algo->om2  = &algo->out;
      algo->oc2  = &algo->out;
      break;
/* M1---C1---+
 *      M2---+--->
 *      C2---+
 */
    case 6:
      algo->om1  = &algo->c1;
      algo->oc1  = &algo->out;
      algo->om2  = &algo->out;
      algo->oc2  = &algo->out;
      algo->omem = &algo->mem;
      break;
/* M1---+
 * C1---+
 *      +--->
 * M2---+
 * C2---+
 */
    case 7:
      algo->om1  = &algo->out;
      algo->oc1  = &algo->out;
      algo->om2  = &algo->out;
      algo->oc2  = &algo->out;
      algo->omem = &algo->mem;
      break;
  }

  /* Copy out the MEM buffer and reset it */
  *algo->omem = algo->mem;
  algo->mem = 0;
}

/*
 .d8888b.  888    888        d8888 888b    888 888b    888 8888888888 888
d88P  Y88b 888    888       d88888 8888b   888 8888b   888 888        888
888    888 888    888      d88P888 88888b  888 88888b  888 888        888
888        8888888888     d88P 888 888Y88b 888 888Y88b 888 8888888    888
888        888    888    d88P  888 888 Y88b888 888 Y88b888 888        888
888    888 888    888   d88P   888 888  Y88888 888  Y88888 888        888
Y88b  d88P 888    888  d8888888888 888   Y8888 888   Y8888 888        888
 "Y8888P"  888    888 d88P     888 888    Y888 888    Y888 8888888888 88888888
*/
void apu_opn_fm_chan_update_m1(apu_opn_fm_chan_t* chan, apu_opn_fm_state_t* state, apu_opn_fm_lfo_t* lfo, int ch3slot)
{
  chan->algo.m1 = chan->op0fb[1] + chan->op0fb[0];
  chan->op0fb[0] = chan->op0fb[1];

  /* TODO: Can't find anything conclusive on this.
   * 9-fb seems right.
   */
  switch(chan->algo.fb) {
    case 0: chan->algo.m1 = 0; break;
    case 1: case 2: case 3: case 4: case 5: case 6: case 7:
      chan->algo.m1 >>= 9 - chan->algo.fb;
      break;
  }

  chan->op0fb[1] = apu_opn_fm_oper_update(chan, &chan->op[0], chan->algo.m1, state, lfo, ch3slot);

  if(chan->algo.om1 == NULL) { /* Algo 5 special case */
    chan->algo.c1 = chan->op0fb[0];
    chan->algo.mem = chan->op0fb[0];
    chan->algo.c2 = chan->op0fb[0];
  }else{
    *chan->algo.om1 += chan->op0fb[0];
  }
}

void apu_opn_fm_chan_update_m2(apu_opn_fm_chan_t* chan, apu_opn_fm_state_t* state, apu_opn_fm_lfo_t* lfo, int ch3slot)
{
  *chan->algo.om2 += apu_opn_fm_oper_update(chan, &chan->op[1], chan->algo.m2, state, lfo, ch3slot);
}

void apu_opn_fm_chan_update_c1(apu_opn_fm_chan_t* chan, apu_opn_fm_state_t* state, apu_opn_fm_lfo_t* lfo, int ch3slot)
{
  *chan->algo.oc1 += apu_opn_fm_oper_update(chan, &chan->op[2], chan->algo.c1, state, lfo, ch3slot);
}

void apu_opn_fm_chan_update_c2(apu_opn_fm_chan_t* chan, apu_opn_fm_state_t* state, apu_opn_fm_lfo_t* lfo, int ch3slot)
{
  *chan->algo.oc2 += apu_opn_fm_oper_update(chan, &chan->op[3], chan->algo.c2, state, lfo, ch3slot);
}

static int32_t apu_opn_fm_chan_saturate_accum(int32_t val)
{
#define ACCUMSIZE 14
  if(val > BITMASK(ACCUMSIZE-1))
    return BITMASK(ACCUMSIZE-1);
  else if(val < -(1<<(ACCUMSIZE-1)))
    return -(1<<(ACCUMSIZE-1));
  else
    return val;
}

/* Update the channel for each cycle */
void apu_opn_fm_chan_update(apu_opn_fm_chan_t* chan, int op, apu_opn_fm_state_t* state, apu_opn_fm_lfo_t* lfo, int ch3slot)
{
  switch(op) {
    case 0:
      apu_opn_fm_algo_update(&chan->algo);
      apu_opn_fm_chan_update_m1(chan, state, lfo, ch3slot);
      break;
    case 1:
      apu_opn_fm_chan_update_m2(chan, state, lfo, ch3slot);
      break;
    case 2:
      apu_opn_fm_chan_update_c1(chan, state, lfo, ch3slot);
      break;
    case 3:
      apu_opn_fm_chan_update_c2(chan, state, lfo, ch3slot);
      break;
  }
  /* Keep outputs in range */
  chan->algo.m1 = apu_opn_fm_chan_saturate_accum(chan->algo.m1);
  chan->algo.m2 = apu_opn_fm_chan_saturate_accum(chan->algo.m2);
  chan->algo.c1 = apu_opn_fm_chan_saturate_accum(chan->algo.c1);
  chan->algo.c2 = apu_opn_fm_chan_saturate_accum(chan->algo.c2);
  chan->algo.mem= apu_opn_fm_chan_saturate_accum(chan->algo.mem);
  chan->algo.out= apu_opn_fm_chan_saturate_accum(chan->algo.out);
}

/*
888      8888888888 .d88888b.
888      888       d88P" "Y88b
888      888       888     888
888      8888888   888     888
888      888       888     888
888      888       888     888
888      888       Y88b. .d88P
88888888 888        "Y88888P"
*/
void apu_opn_fm_lfo_update(apu_opn_fm_lfo_t* lfo)
{
  if(!lfo->on)
    return;

  lfo->ticks++;
  if(lfo->ticks < lfo->period)
    return;
  lfo->ticks = 0;

  lfo->pos++;
  lfo->pos &= BITMASK(7);

  lfo->pmod = lfo->pos >> 2;
  lfo->amod = lfo->pos;
}

int32_t apu_opn_fm_lfo_amod(apu_opn_fm_lfo_t* lfo, apu_opn_fm_chan_t* chan, apu_opn_fm_oper_t* oper)
{
  /* TODO: Verify */
  if(!oper->eg.amon)
    return 0;

  return _opn_tbl_lfo_amod[chan->ams][lfo->amod];
}

int32_t apu_opn_fm_lfo_pmod(apu_opn_fm_lfo_t* lfo, int fnum, apu_opn_fm_chan_t* chan)
{
  int32_t mod = 0;
  mod += _opn_tbl_lfo_pmod_exp1[chan->pms][fnum >> 5][lfo->pmod];
  mod += _opn_tbl_lfo_pmod_exp2[chan->pms][fnum&BITMASK(5)][lfo->pmod];
  if(mod < 0) {
    mod >>= 9;
    mod |= BITMASK(9) << 23;
  }else{
    mod >>= 9;
  }
  return mod;
}

void apu_opn_fm_lfo_on(apu_opn_fm_lfo_t* lfo)
{
  if(!lfo->on) {
    lfo->pos = 0;
    lfo->pmod = 0;
    lfo->amod = 0;
    lfo->on = TRUE;
  }

  lfo->period = _opn_tbl_lfo_period[lfo->freq];
}

void apu_opn_fm_lfo_off(apu_opn_fm_lfo_t* lfo)
{
  lfo->on = FALSE;
}

void apu_opn_fm_lfo_pm_calc_one(int fn, int pms, int lv, int bit)
{
  int tbl, base, sz;
  int slv, val;

  if(bit < 5) {
    tbl = 1;
    base = 0;
    sz = BITMASK(5);
  }else{
    tbl = 0;
    base = 5;
    sz = BITMASK(6);
  }

  slv = lv & 7;
  if(lv & 8) {
    slv ^= 7;
  }
  val = _opn_tbl_lfo_pmod[pms][slv] << bit;

  if(lv & 16)
    val = -val;

  if(tbl == 0)
    _opn_tbl_lfo_pmod_exp1[pms][(fn >> base) & sz][lv] += val;
  else
    _opn_tbl_lfo_pmod_exp2[pms][(fn >> base) & sz][lv] += val;
}

void apu_opn_fm_lfo_precalc_pmod(void)
{
  int fn,pms,lv,bit;
  for(fn = 0; fn < 1<<11; fn++) {
    for(pms = 0; pms < 8; pms++) {
      for(lv = 0; lv < 32; lv++) {
        _opn_tbl_lfo_pmod_exp1[pms][fn>>5][lv] = 0;
        _opn_tbl_lfo_pmod_exp2[pms][fn&BITMASK(5)][lv] = 0;

        for(bit = 0; bit < 11; bit++)
          if(fn & (1<<bit))
            apu_opn_fm_lfo_pm_calc_one(fn, pms, lv, bit);
      }
    }
  }
}

void apu_opn_fm_lfo_precalc_amod(void)
{
  int lv,val;
  for(lv = 0; lv < 128; lv++) {
    val = lv & 63;
    if(lv & 64)
      val = 63 - val;
    _opn_tbl_lfo_amod[0][lv] = 0;
    _opn_tbl_lfo_amod[1][lv] = val >> 2;
    _opn_tbl_lfo_amod[2][lv] = val;
    _opn_tbl_lfo_amod[3][lv] = val << 1;
  }
}

void apu_opn_fm_lfo_precalc(void)
{
  apu_opn_fm_lfo_precalc_amod();
  apu_opn_fm_lfo_precalc_pmod();
}

/*
888    d8P  8888888888 Y88b   d88P
888   d8P   888         Y88b d88P
888  d8P    888          Y88o88P
888d88K     8888888       Y888P
8888888b    888            888
888  Y88b   888            888
888   Y88b  888            888
888    Y88b 8888888888     888
*/
void apu_opn_fm_keyon(apu_opn_fm_chan_t* chan, apu_opn_fm_oper_t* oper, int ch3slot)
{
  (void)chan;
  (void)ch3slot;
  if(oper->key == 1)
    return;
  oper->key = 1;
}

void apu_opn_fm_keyoff(apu_opn_fm_chan_t* chan, apu_opn_fm_oper_t* oper, int ch3slot)
{
  (void)chan;
  (void)ch3slot;
  if(oper->key == 0)
    return;
  oper->key = 0;
}

/*
8888888b.  8888888888 .d8888b.  8888888888 88888888888
888   Y88b 888       d88P  Y88b 888            888
888    888 888       Y88b.      888            888
888   d88P 8888888    "Y888b.   8888888        888
8888888P"  888           "Y88b. 888            888
888 T88b   888             "888 888            888
888  T88b  888       Y88b  d88P 888            888
888   T88b 8888888888 "Y8888P"  8888888888     888
*/
void apu_opn_fm_pg_reset(apu_opn_fm_oper_pg_t* pg)
{
  pg->dt = 0;
  pg->mul = 0;
  pg->fnum = 0;
  pg->block = 0;
  /* State */
  pg->phase = 0;
}

void apu_opn_fm_eg_reset(apu_opn_fm_oper_eg_t* eg)
{
  eg->tl = 0;
  eg->ks = 0;
  eg->ar = 0;
  eg->dr = 0;
  eg->sr = 0;
  eg->sl = 0;
  eg->rr = 0;
  eg->ssgeg = 0;
  eg->amon = FALSE;
  /* State */
  eg->state = OPN_EGSTATE_REL;
  eg->rate = 0;
  eg->atten = BITMASK(10);
}

void apu_opn_fm_oper_reset(apu_opn_fm_oper_t* oper)
{
  apu_opn_fm_pg_reset(&oper->pg);
  apu_opn_fm_eg_reset(&oper->eg);
  oper->key = 0;
  oper->csm_key = 0;
  oper->key_old = 0;
  oper->kcode = 0;
}

void apu_opn_fm_algo_reset(apu_opn_fm_algo_t* algo)
{
  algo->m1  = 0;
  algo->m2  = 0;
  algo->c1  = 0;
  algo->c2  = 0;
  algo->mem = 0;
  algo->out = 0;

  algo->om1  = NULL;
  algo->om2  = NULL;
  algo->oc1  = NULL;
  algo->oc2  = NULL;
  algo->omem = NULL;

  algo->fb  = 0;
  algo->con = 0;
}

void apu_opn_fm_chan_reset(apu_opn_fm_chan_t* chan)
{
  apu_opn_fm_oper_reset(&chan->op[0]);
  apu_opn_fm_oper_reset(&chan->op[1]);
  apu_opn_fm_oper_reset(&chan->op[2]);
  apu_opn_fm_oper_reset(&chan->op[3]);

  apu_opn_fm_algo_reset(&chan->algo);

  chan->op0fb[0] = 0;
  chan->op0fb[1] = 0;

  chan->lr = BITMASK(2);
  chan->ams = 0;
  chan->pms = 0;
}

void apu_opn_fm_lfo_reset(apu_opn_fm_lfo_t* lfo)
{
  lfo->on = FALSE;
  lfo->freq = 0;

  lfo->pos = 0;
  lfo->ticks = 0;
  lfo->amod = 0;
  lfo->pmod = 0;

  if(_opn_tbl_lfo_pmod_exp1[0][0][0] == -1)
    apu_opn_fm_lfo_precalc();
}

void apu_opn_fm_state_reset(apu_opn_fm_state_t* state)
{
  state->eg_ctr = 0;
  state->eg_cycle = 0;

  state->sch = FALSE;
  state->csm = FALSE;
  state->ch3slot = FALSE;
}







void apu_opn_fm_state_state_add(hien_vm_t* vm, apu_opn_fm_state_t* state, char* path, ...)
{
  char* expath;
  va_list v;
  va_start(v, path);
  expath = state_build_path_va(path, v);
  va_end(v);

  vm_state_add_uint16(vm, &state->eg_ctr, expath, "eg_ctr", NULL);
  vm_state_add_int(vm, &state->eg_cycle, expath, "eg_cycle", NULL);
  vm_state_add_int(vm, &state->sch, expath, "sch", NULL);
  vm_state_add_int(vm, &state->csm, expath, "csm", NULL);
  vm_state_add_int(vm, &state->ch3slot, expath, "ch3slot", NULL);

  mem_free(expath);
}

void apu_opn_fm_chan_state_add(hien_vm_t* vm, apu_opn_fm_chan_t* chan, char* path, ...)
{
  char* expath;
  va_list v;
  int i;

  va_start(v, path);
  expath = state_build_path_va(path, v);
  va_end(v);

  vm_state_add_int32_arr(vm, 2, chan->op0fb, expath, "op0fb", NULL);

  vm_state_add_int(vm, &chan->lr, expath, "lr", NULL);
  vm_state_add_int(vm, &chan->ams, expath, "ams", NULL);
  vm_state_add_int(vm, &chan->pms, expath, "pms", NULL);

  apu_opn_fm_algo_state_add(vm, &chan->algo, expath, "algo", NULL);

  for(i = 0; i < 4; i++) {
    char* cname = NULL;
    kuso_asprintf(&cname, "op%d", i);
    apu_opn_fm_oper_state_add(vm, &chan->op[i], expath, cname, NULL);
  }

  mem_free(expath);
}

void apu_opn_fm_algo_state_add(hien_vm_t* vm, apu_opn_fm_algo_t* algo, char* path, ...)
{
  char* expath;
  va_list v;
  va_start(v, path);
  expath = state_build_path_va(path, v);
  va_end(v);

  vm_state_add_int32(vm, &algo->m1, expath, "m1", NULL);
  vm_state_add_int32(vm, &algo->m2, expath, "m2", NULL);
  vm_state_add_int32(vm, &algo->c1, expath, "c1", NULL);
  vm_state_add_int32(vm, &algo->c2, expath, "c2", NULL);
  vm_state_add_int32(vm, &algo->mem, expath, "mem", NULL);
  vm_state_add_int32(vm, &algo->out, expath, "out", NULL);
  vm_state_add_int(vm, &algo->fb, expath, "fb", NULL);
  vm_state_add_int(vm, &algo->con, expath, "con", NULL);

  mem_free(expath);
}

void apu_opn_fm_oper_state_add(hien_vm_t* vm, apu_opn_fm_oper_t* oper, char* path, ...)
{
  char* expath;
  va_list v;
  va_start(v, path);
  expath = state_build_path_va(path, v);
  va_end(v);

  vm_state_add_int(vm, &oper->key, expath, "key", NULL);
  vm_state_add_int(vm, &oper->csm_key, expath, "csm_key", NULL);
  vm_state_add_int(vm, &oper->key_old, expath, "key_old", NULL);
  vm_state_add_int(vm, &oper->kcode, expath, "kcode", NULL);

  apu_opn_fm_pg_state_add(vm, &oper->pg, expath, "pg", NULL);
  apu_opn_fm_eg_state_add(vm, &oper->eg, expath, "eg", NULL);

  mem_free(expath);
}

void apu_opn_fm_pg_state_add(hien_vm_t* vm, apu_opn_fm_oper_pg_t* pg, char* path, ...)
{
  char* expath;
  va_list v;
  va_start(v, path);
  expath = state_build_path_va(path, v);
  va_end(v);

  vm_state_add_int(vm, &pg->dt, expath, "dt", NULL);
  vm_state_add_int(vm, &pg->mul, expath, "mul", NULL);
  vm_state_add_int(vm, &pg->fnum, expath, "fnum", NULL);
  vm_state_add_int(vm, &pg->block, expath, "block", NULL);

  vm_state_add_uint32(vm, &pg->phase, expath, "phase", NULL);

  mem_free(expath);
}

void apu_opn_fm_eg_state_add(hien_vm_t* vm, apu_opn_fm_oper_eg_t* eg, char* path, ...)
{
  char* expath;
  va_list v;
  va_start(v, path);
  expath = state_build_path_va(path, v);
  va_end(v);

  vm_state_add_int(vm, &eg->tl, expath, "tl", NULL);
  vm_state_add_int(vm, &eg->ks, expath, "ks", NULL);
  vm_state_add_int(vm, &eg->ar, expath, "ar", NULL);
  vm_state_add_int(vm, &eg->dr, expath, "dr", NULL);
  vm_state_add_int(vm, &eg->sr, expath, "sr", NULL);
  vm_state_add_int(vm, &eg->sl, expath, "sl", NULL);
  vm_state_add_int(vm, &eg->rr, expath, "rr", NULL);
  vm_state_add_int(vm, &eg->ssgeg, expath, "ssgeg", NULL);
  vm_state_add_int(vm, &eg->amon, expath, "amon", NULL);
  vm_state_add_int(vm, &eg->state, expath, "state", NULL);
  vm_state_add_int(vm, &eg->rate, expath, "rate", NULL);
  vm_state_add_int(vm, &eg->atten, expath, "atten", NULL);
  vm_state_add_int(vm, &eg->precalc_sl, expath, "precalc_sl", NULL);

  mem_free(expath);
}

void apu_opn_fm_lfo_state_add(hien_vm_t* vm, apu_opn_fm_lfo_t* lfo, char* path, ...)
{
  char* expath;
  va_list v;
  va_start(v, path);
  expath = state_build_path_va(path, v);
  va_end(v);

  vm_state_add_int(vm, &lfo->freq, expath, "freq", NULL);
  vm_state_add_int(vm, &lfo->on, expath, "on", NULL);

  vm_state_add_int(vm, &lfo->ticks, expath, "ticks", NULL);
  vm_state_add_int(vm, &lfo->period, expath, "period", NULL);

  vm_state_add_int(vm, &lfo->pos, expath, "pos", NULL);

  vm_state_add_int(vm, &lfo->pmod, expath, "pmod", NULL);
  vm_state_add_int(vm, &lfo->amod, expath, "amod", NULL);

  mem_free(expath);
}
