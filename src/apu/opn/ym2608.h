/**********************************************************
*
* apu/opn/ym2608.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _APU_YM2608_H_
#define _APU_YM2608_H_

#define YM2608_PORT_IRQ     (PORT_USER+0)
#define YM2608_PORT_IOA     (PORT_USER+1)
#define YM2608_PORT_IOB     (PORT_USER+2)
#define YM2608_PORT_AO      (PORT_USER+3) /* Analog output (0~5V) */
#define YM2608_PORT_OP_O_L  (PORT_USER+4) /* FM output (serial) */
#define YM2608_PORT_OP_O_R  (PORT_USER+5) /* FM output (serial) */
#define YM2608_PORT_COUNT   (PORT_USER+6)

PORT_COUNT_CHECK(YM2608);

hien_dev_t* apu_ym2608_new(hien_vm_t* vm, char* name, hz_t clock);

extern hien_dev_info_t apu_ym2608_def;

#endif
