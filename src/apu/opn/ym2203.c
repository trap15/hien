/**********************************************************
*
* apu/opn/ym2203.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

/* Yamaha OPN (YM2203) core */

#include "hien.h"
#include "opn_internal.h"
#include "ym2203_internal.h"

/*
88888888888 8888888 888b     d888 8888888888 8888888b.
    888       888   8888b   d8888 888        888   Y88b
    888       888   88888b.d88888 888        888    888
    888       888   888Y88888P888 8888888    888   d88P
    888       888   888 Y888P 888 888        8888888P"
    888       888   888  Y8P  888 888        888 T88b
    888       888   888   "   888 888        888  T88b
    888     8888888 888       888 8888888888 888   T88b
*/
static void apu_ym2203_timer_irqs(hien_dev_t* dev, apu_ym2203_t* apu)
{
  if(apu->timer[0].flag || apu->timer[1].flag)
    dev_port_assert(dev, YM2203_PORT_IRQ);
  else
    dev_port_deassert(dev, YM2203_PORT_IRQ);
}

static void apu_ym2203_timer_reset(hien_dev_t* dev, apu_ym2203_t* apu, int idx)
{
  apu_opn_timer_reset(&apu->timer[idx]);

  apu_ym2203_timer_irqs(dev, apu);
}

/*
888     888 8888888b.  8888888b.        d8888 88888888888 8888888888
888     888 888   Y88b 888  "Y88b      d88888     888     888
888     888 888    888 888    888     d88P888     888     888
888     888 888   d88P 888    888    d88P 888     888     8888888
888     888 8888888P"  888    888   d88P  888     888     888
888     888 888        888    888  d88P   888     888     888
Y88b. .d88P 888        888  .d88P d8888888888     888     888
 "Y88888P"  888        8888888P" d88P     888     888     8888888888
*/
static void apu_ym2203_update_prescalers(hien_dev_t* dev, apu_ym2203_t* apu)
{
  static const int prescale_psg[4] = { 1, 1, 4, 2 };
  static const int prescale_fm[4]  = { 2, 2, 6, 3 };
  (void)dev;

  apu->prescale.psg = prescale_psg[apu->prescale.raw];
  apu->prescale.fm = prescale_fm[apu->prescale.raw];
  apu->prescale.timer[0] = prescale_fm[apu->prescale.raw] * 3 * 4;
  apu->prescale.timer[1] = prescale_fm[apu->prescale.raw] * 3 * 4 * 16;

  dev_change_clock(apu->psg, dev->clock / apu->prescale.psg);

  apu_opn_timer_set_prescale(&apu->timer[0], apu->prescale.timer[0]);
  apu_opn_timer_set_prescale(&apu->timer[1], apu->prescale.timer[1]);
}

static void apu_ym2203_update(hien_dev_t* dev, uintmax_t data, uintmax_t mask)
{
  apu_ym2203_t* apu;
  (void)mask;
  apu = dev->data;

  if(data != 1) /* do nothing on falling edge */
    return;

  if(apu->busy > 0) apu->busy--;

  if(apu_opn_timer_update(&apu->timer[0])) {
    apu->fm.chan[2].op[0].csm_key = TRUE;
    apu->fm.chan[2].op[1].csm_key = TRUE;
    apu->fm.chan[2].op[2].csm_key = TRUE;
    apu->fm.chan[2].op[3].csm_key = TRUE;
  }
  apu_opn_timer_update(&apu->timer[1]);
  apu_ym2203_fm_update(dev, apu);
  apu_ym2203_timer_irqs(dev, apu);
}

/*
8888888b.  8888888888        d8888 8888888b.
888   Y88b 888              d88888 888  "Y88b
888    888 888             d88P888 888    888
888   d88P 8888888        d88P 888 888    888
8888888P"  888           d88P  888 888    888
888 T88b   888          d88P   888 888    888
888  T88b  888         d8888888888 888  .d88P
888   T88b 8888888888 d88P     888 8888888P"
*/
static uintmax_t apu_ym2203_read(hien_dev_t* dev, addr_t addr, uintmax_t mask)
{
  uint8_t ret;
  apu_ym2203_t* apu;
  apu = dev->data;
  ret = 0xFF;

  addr &= 1;
  if(addr == 0) { /* Status */
    ret = (apu->busy ? 1 : 0) << 7;
    ret |= apu->timer[0].flag << 0;
    ret |= apu->timer[1].flag << 1;
  }else{ /* Register Read */
    ret = apu->regs[apu->regsel];
    switch(apu->regsel) {
      case 0x00 ... 0x0F:
        ret = dev_read(apu->psg, SPACE_MAIN, addr, mask);
        break;
      /* FM registers */
      case 0x30 ... 0xB2:
      case 0x28:
        ret = apu_ym2203_fm_read(dev, apu, apu->regsel);
        break;
      /* Control */
      case 0x21: /* Test port */
      case 0x24: /* Timer-A MSB */
      case 0x25: /* Timer-A LSB */
      case 0x26: /* Timer-B */
      case 0x27: /* Control */
      case 0x2D: /* Prescaler enable */
      case 0x2E: /* Prescaler select */
      case 0x2F: /* Prescaler clear */
        break;
    }
  }

  return ret & mask;
}

/*
888       888 8888888b.  8888888 88888888888 8888888888
888   o   888 888   Y88b   888       888     888
888  d8b  888 888    888   888       888     888
888 d888b 888 888   d88P   888       888     8888888
888d88888b888 8888888P"    888       888     888
88888P Y88888 888 T88b     888       888     888
8888P   Y8888 888  T88b    888       888     888
888P     Y888 888   T88b 8888888     888     8888888888
*/
static void apu_ym2203_write(hien_dev_t* dev, addr_t addr, uintmax_t data, uintmax_t mask)
{
  apu_ym2203_t* apu;
  apu = dev->data;

  addr &= 1;
  data &= 0xFF;
  mask &= 0xFF;
  if(addr == 0) { /* Regsel Write */
    COMBINE_DATA(apu->regsel, data, mask);
    dev_write(apu->psg, SPACE_MAIN, addr, data, mask);
    switch(apu->regsel) {
      case 0x2D: /* Prescaler enable */
        apu->prescale.raw |= 2;
        apu_ym2203_update_prescalers(dev, apu);
        break;
      case 0x2E: /* Prescaler select */
        apu->prescale.raw |= 1;
        apu_ym2203_update_prescalers(dev, apu);
        break;
      case 0x2F: /* Prescaler clear */
        apu->prescale.raw = 0;
        apu_ym2203_update_prescalers(dev, apu);
        break;
    }
  }else{ /* Register Write */
    COMBINE_DATA(apu->regs[apu->regsel], data, mask);
    switch(apu->regsel) {
      /* PSG registers */
      case 0x00 ... 0x0F:
        dev_write(apu->psg, SPACE_MAIN, addr, data, mask);
        break;
      /* FM registers */
      case 0x30 ... 0xB2:
      case 0x28:
        apu_ym2203_fm_write(dev, apu, apu->regsel, data);
        break;
      /* Control */
      case 0x21: /* Test port */
        break;
      case 0x24: /* Timer-A MSB */
        apu->timer[0].freq &= 3;
        apu->timer[0].freq |= data << 2;
        break;
      case 0x25: /* Timer-A LSB */
        apu->timer[0].freq &= ~3;
        apu->timer[0].freq |= data & 3;
        break;
      case 0x26: /* Timer-B */
        apu->timer[1].freq = data;
        break;
      case 0x27: /* Control */
        /* CSM/3SLOT mode */
        switch(data & 0xC0) {
          case 0x80: /* CSM */
            apu->fm.state.csm = TRUE;
            apu->fm.state.ch3slot = TRUE;
            break;
          case 0x40: case 0xC0: /* 3slot */
            apu->fm.state.csm = FALSE;
            apu->fm.state.ch3slot = TRUE;
            break;
          default:
            apu->fm.state.csm = FALSE;
            apu->fm.state.ch3slot = FALSE;
            break;
        }
        if(BIT(data, 5)) apu->timer[1].flag = FALSE;
        if(BIT(data, 4)) apu->timer[0].flag = FALSE;
        apu->timer[1].en = BIT(data,3);
        apu->timer[0].en = BIT(data,2);
        if(BIT(data, 1)) apu_opn_timer_reload(&apu->timer[1]);
        else             apu->timer[1].step = 0;
        if(BIT(data, 0)) apu_opn_timer_reload(&apu->timer[0]);
        else             apu->timer[0].step = 0;
        break;
    }
  }
}

/*
8888888b.   .d88888b.  8888888b. 88888888888 .d8888b.
888   Y88b d88P" "Y88b 888   Y88b    888    d88P  Y88b
888    888 888     888 888    888    888    Y88b.
888   d88P 888     888 888   d88P    888     "Y888b.
8888888P"  888     888 8888888P"     888        "Y88b.
888        888     888 888 T88b      888          "888
888        Y88b. .d88P 888  T88b     888    Y88b  d88P
888         "Y88888P"  888   T88b    888     "Y8888P"
*/

static uintmax_t apu_ym2203_read_ioa(hien_dev_t* dev, uintmax_t mask)
{
  apu_ym2203_t* apu;
  apu = dev->data;

  return dev_port_read(apu->psg, YM2149_PORT_IOA, mask);
}
static uintmax_t apu_ym2203_read_iob(hien_dev_t* dev, uintmax_t mask)
{
  apu_ym2203_t* apu;
  apu = dev->data;

  return dev_port_read(apu->psg, YM2149_PORT_IOB, mask);
}

static void apu_ym2203_write_ioa(hien_dev_t* dev, uintmax_t data, uintmax_t mask)
{
  apu_ym2203_t* apu;
  apu = dev->data;

  dev_port_write(apu->psg, YM2149_PORT_IOA, data, mask);
}
static void apu_ym2203_write_iob(hien_dev_t* dev, uintmax_t data, uintmax_t mask)
{
  apu_ym2203_t* apu;
  apu = dev->data;

  dev_port_write(apu->psg, YM2149_PORT_IOB, data, mask);
}

static void apu_ym2203_shuttle_ao_a(hien_dev_t* dev, analog_t data)
{
  hien_dev_t* apu;
  apu = dev->drv_data;

  dev_port_analog_write(apu, YM2203_PORT_AO_A, data);
}
static void apu_ym2203_shuttle_ao_b(hien_dev_t* dev, analog_t data)
{
  hien_dev_t* apu;
  apu = dev->drv_data;

  dev_port_analog_write(apu, YM2203_PORT_AO_B, data);
}
static void apu_ym2203_shuttle_ao_c(hien_dev_t* dev, analog_t data)
{
  hien_dev_t* apu;
  apu = dev->drv_data;

  dev_port_analog_write(apu, YM2203_PORT_AO_C, data);
}
/*
8888888b.  8888888888 .d8888b.  8888888888 88888888888
888   Y88b 888       d88P  Y88b 888            888
888    888 888       Y88b.      888            888
888   d88P 8888888    "Y888b.   8888888        888
8888888P"  888           "Y88b. 888            888
888 T88b   888             "888 888            888
888  T88b  888       Y88b  d88P 888            888
888   T88b 8888888888 "Y8888P"  8888888888     888
*/
static void apu_ym2203_reset(hien_dev_t* dev)
{
  apu_ym2203_t* apu;
  apu = dev->data;

  apu->busy = 0;

  apu_ym2203_timer_reset(dev, apu, 0);
  apu_ym2203_timer_reset(dev, apu, 1);

  apu_ym2203_fm_reset(dev, apu);
  dev_reset(apu->psg);

  apu->prescale.raw = 2;
  apu_ym2203_update_prescalers(dev, apu);
}

/*
8888888b.  8888888888 888      8888888888 88888888888 8888888888
888  "Y88b 888        888      888            888     888
888    888 888        888      888            888     888
888    888 8888888    888      8888888        888     8888888
888    888 888        888      888            888     888
888    888 888        888      888            888     888
888  .d88P 888        888      888            888     888
8888888P"  8888888888 88888888 8888888888     888     8888888888
*/
static void apu_ym2203_delete(hien_dev_t* dev)
{
  apu_ym2203_t* apu;
  apu = dev->data;

  vm_remove_device(dev->vm, apu->psg);

  mem_free(apu);
}

/*
888b    888 8888888888 888       888
8888b   888 888        888   o   888
88888b  888 888        888  d8b  888
888Y88b 888 8888888    888 d888b 888
888 Y88b888 888        888d88888b888
888  Y88888 888        88888P Y88888
888   Y8888 888        8888P   Y8888
888    Y888 8888888888 888P     Y888
*/
hien_dev_t* apu_ym2203_new(hien_vm_t* vm, char* name, hz_t clock)
{
  hien_dev_t* dev;
  apu_ym2203_t* apu;

  dev = dev_new(vm, name, clock);
  dev->delete = apu_ym2203_delete;
  dev->reset = apu_ym2203_reset;

  dev_port_set_callbacks(dev, PORT_CLOCK, NULL, apu_ym2203_update);

  dev_space_setup(dev, SPACE_MAIN, 8);
  dev_space_set_callbacks(dev, SPACE_MAIN, apu_ym2203_read, apu_ym2203_write);

  dev_port_setup(dev, YM2203_PORT_IRQ,   1, PORT_OUTPUT);
  dev_port_setup(dev, YM2203_PORT_IOA,   8, PORT_INOUT);
  dev_port_setup(dev, YM2203_PORT_IOB,   8, PORT_INOUT);
  dev_port_setup(dev, YM2203_PORT_AO_A,  0, PORT_OUTPUT | PORT_ANALOG);
  dev_port_setup(dev, YM2203_PORT_AO_B,  0, PORT_OUTPUT | PORT_ANALOG);
  dev_port_setup(dev, YM2203_PORT_AO_C,  0, PORT_OUTPUT | PORT_ANALOG);
  dev_port_setup(dev, YM2203_PORT_OP_O,  0, PORT_OUTPUT | PORT_ANALOG);
  dev_port_setup(dev, YM2203_PORT_IC,    1, PORT_INPUT);

  dev_port_set_callbacks(dev, YM2203_PORT_IOA,  apu_ym2203_read_ioa, apu_ym2203_write_ioa);
  dev_port_set_callbacks(dev, YM2203_PORT_IOB,  apu_ym2203_read_iob, apu_ym2203_write_iob);

  apu = mem_alloc_type(apu_ym2203_t);

  char* psgname;
  psgname = state_build_path(name, "psg", NULL);
  apu->psg = apu_ym2149_new(vm, psgname, clock);
  mem_free(psgname);
  apu->psg->drv_data = dev;

  dev_port_set_analog_callbacks(apu->psg, YM2149_PORT_AO_A, NULL, apu_ym2203_shuttle_ao_a);
  dev_port_set_analog_callbacks(apu->psg, YM2149_PORT_AO_B, NULL, apu_ym2203_shuttle_ao_b);
  dev_port_set_analog_callbacks(apu->psg, YM2149_PORT_AO_C, NULL, apu_ym2203_shuttle_ao_c);

  vm_add_device(vm, apu->psg);

  /* Init timers */
  apu_opn_timer_init(&apu->timer[0], 1024);
  apu_opn_timer_init(&apu->timer[1], 256);

  dev->data = apu;

  dev_reset(dev);

  apu_ym2203_fm_state_add(vm, apu, name, "fm", NULL);
  apu_opn_timer_state_add(vm, &apu->timer[0], name, "timerA", NULL);
  apu_opn_timer_state_add(vm, &apu->timer[1], name, "timerB", NULL);

  vm_state_add_int(vm, &apu->prescale.psg, name, "prescale", "psg", NULL);
  vm_state_add_int(vm, &apu->prescale.fm, name, "prescale", "fm", NULL);
  vm_state_add_int(vm, &apu->prescale.timer[0], name, "prescale", "timerA", NULL);
  vm_state_add_int(vm, &apu->prescale.timer[1], name, "prescale", "timerB", NULL);
  vm_state_add_int(vm, &apu->prescale.raw, name, "prescale", "raw", NULL);

  vm_state_add_int(vm, &apu->busy, name, "busy", NULL);

  vm_state_add_uint8(vm, &apu->regsel, name, "regsel", NULL);
  vm_state_add_uint8_arr(vm, 256, apu->regs, name, "regs", NULL);

  return dev;
}

hien_dev_info_t apu_ym2203_def = {
  .type = "YM2203",
  .ctor = "apu_ym2203_new",
  .use_clk = TRUE,
  .has_save = 1,
};
