/**********************************************************
*
* apu/opn/opn_timer.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

/* Yamaha OPN timers */

#include "hien.h"
#include "opn_internal.h"

void apu_opn_timer_reload(apu_opn_timer_t* tmr)
{
  if(tmr->step == 0) {
    tmr->step = tmr->top - tmr->freq;
  }
}

void apu_opn_timer_reset(apu_opn_timer_t* tmr)
{
  tmr->step = 0;
  tmr->freq = 0;
  tmr->flag = FALSE;
  tmr->en = FALSE;
}

int apu_opn_timer_update(apu_opn_timer_t* tmr)
{
  int ov = FALSE;

  if(tmr->pre_ticks == 0) {
    if(tmr->step != 0) {
      tmr->step--;
      if(tmr->step == 0) { /* Timer has expired */
        if(tmr->en)
          tmr->flag = TRUE;

        ov = TRUE;
        apu_opn_timer_reload(tmr);
      }
    }

    tmr->pre_ticks = tmr->pre;
  }
  tmr->pre_ticks--;
  return ov;
}

void apu_opn_timer_set_prescale(apu_opn_timer_t* tmr, int pre)
{
  tmr->pre = pre;
}

void apu_opn_timer_init(apu_opn_timer_t* tmr, int top)
{
  tmr->top = top;
  tmr->pre_ticks = 0;
}

void apu_opn_timer_state_add(hien_vm_t* vm, apu_opn_timer_t* tmr, char* path, ...)
{
  char* expath;
  va_list v;
  va_start(v, path);
  expath = state_build_path_va(path, v);
  va_end(v);

  vm_state_add_uint16(vm, &tmr->top, expath, "top", NULL);
  vm_state_add_uint16(vm, &tmr->freq, expath, "freq", NULL);
  vm_state_add_uint16(vm, &tmr->step, expath, "step", NULL);

  vm_state_add_int(vm, &tmr->en, expath, "en", NULL);
  vm_state_add_int(vm, &tmr->flag, expath, "flag", NULL);

  vm_state_add_int(vm, &tmr->pre, expath, "prescale", NULL);
  vm_state_add_int(vm, &tmr->pre_ticks, expath, "pre_ticks", NULL);

  mem_free(expath);
}
