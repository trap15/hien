/**********************************************************
*
* apu/opn/ym2203.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _APU_YM2203_H_
#define _APU_YM2203_H_

#define YM2203_PORT_IRQ     (PORT_USER+0)
#define YM2203_PORT_IOA     (PORT_USER+1)
#define YM2203_PORT_IOB     (PORT_USER+2)
#define YM2203_PORT_AO_A    (PORT_USER+3) /* Analog Channel A output (0~1V) */
#define YM2203_PORT_AO_B    (PORT_USER+4) /* Analog Channel B output (0~1V) */
#define YM2203_PORT_AO_C    (PORT_USER+5) /* Analog Channel C output (0~1V) */
#define YM2203_PORT_OP_O    (PORT_USER+6) /* FM output (serial) */
#define YM2203_PORT_IC      (PORT_USER+7) /* Reset */
#define YM2203_PORT_COUNT   (PORT_USER+8)

PORT_COUNT_CHECK(YM2203);

hien_dev_t* apu_ym2203_new(hien_vm_t* vm, char* name, hz_t clock);

extern hien_dev_info_t apu_ym2203_def;

#endif
