/**********************************************************
*
* apu/oki6295/oki6295.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _APU_OKI6295_H_
#define _APU_OKI6295_H_

#define OKI6295_SPACE_SAMPLES (SPACE_USER+0)
#define OKI6295_SPACE_COUNT   (SPACE_USER+1)

SPACE_COUNT_CHECK(OKI6295);

#define OKI6295_PORT_SS    (PORT_USER+0) /* Sampling frequency select */
#define OKI6295_PORT_DAO   (PORT_USER+1) /* Voice synthesis output */
#define OKI6295_PORT_COUNT (PORT_USER+2)

PORT_COUNT_CHECK(OKI6295);

hien_dev_t* apu_oki6295_new(hien_vm_t* vm, char* name, hz_t clock);

extern hien_dev_info_t apu_oki6295_def;

#endif
