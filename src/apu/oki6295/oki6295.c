/**********************************************************
*
* apu/oki6295/oki6295.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

typedef struct apu_oki6295_t apu_oki6295_t;

struct apu_oki6295_t {
  struct {
    int vol;
    /* These addresses are in nibbles */
    addr_t addr;
    addr_t stop;
    /* Emulation State */
    int on;
    adpcm_decode_t dec;
  } chan[4];

  /* Emulation state */
  int divr;
  int latch, latched;
  int ticks;
  analog_t ao;
};

/* Derived from table Reduction Specification from OKI6295 datasheet */
static const uintmax_t _oki6295_reduction[16] = {
  0x20, 0x16, 0x10, 0x0B,
  0x08, 0x06, 0x04, 0x03,
  0x02, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00
};

/* Returns 17bit signed sample */
static int32_t apu_oki6295_gen_sample(hien_dev_t* dev, apu_oki6295_t* apu, int ch)
{
  int32_t smp;
  int nib;
  nib = dev_read(dev, OKI6295_SPACE_SAMPLES, apu->chan[ch].addr >> 1, 0xFF);
  if(!BIT(apu->chan[ch].addr, 0)) {
    nib >>= 4;
  }
  nib &= 0xF;

  smp = adpcm_decode_next(&apu->chan[ch].dec, nib) * _oki6295_reduction[apu->chan[ch].vol];

  apu->chan[ch].addr++;
  if(apu->chan[ch].addr >= apu->chan[ch].stop) {
    apu->chan[ch].on = 0;
  }

  return smp;
}

static void apu_oki6295_update(hien_dev_t* dev, uintmax_t data, uintmax_t mask)
{
  int i;
  int32_t smp, csmp;
  apu_oki6295_t* apu;
  (void)mask;
  apu = dev->data;

  if(data != 1) /* do nothing on falling edge */
    return;

  if(apu->ticks++ < apu->divr)
    return;

  apu->ticks = 0;
  smp = 0;
  for(i = 0; i < 4; i++) {
    if(apu->chan[i].on) {
      csmp = apu_oki6295_gen_sample(dev, apu, i);
    }else{
      csmp = 0;
    }
    smp += csmp;
  }

  /* smp is 19bit signed */
  smp <<= 13;
  apu->ao = A_FIX2V(smp, A_5V);
  dev_port_analog_write(dev, OKI6295_PORT_DAO, apu->ao);
}

static uintmax_t apu_oki6295_read(hien_dev_t* dev, addr_t addr, uintmax_t mask)
{
  int i;
  int res;
  apu_oki6295_t* apu;
  apu = dev->data;
  (void)addr;

  res = 0xF0;
  for(i = 0; i < 4; i++) {
    if(apu->chan[i].on)
      res |= 1 << i;
  }

  return res & mask;
}

static void apu_oki6295_write(hien_dev_t* dev, addr_t addr, uintmax_t data, uintmax_t mask)
{
  int i;
  addr_t base;
  apu_oki6295_t* apu;
  apu = dev->data;
  (void)addr;
  (void)mask;

  if(apu->latched) {
    for(i = 0; i < 4; i++) {
      if(!BIT(data, i+4))
        continue;

      if(apu->chan[i].on) /* TODO: What really happens? */
        continue;

      base = apu->latch << 3;
      apu->chan[i].addr = (dev_read(dev, OKI6295_SPACE_SAMPLES, base+0, 0xFF) << 16) |
                          (dev_read(dev, OKI6295_SPACE_SAMPLES, base+1, 0xFF) <<  8) |
                          (dev_read(dev, OKI6295_SPACE_SAMPLES, base+2, 0xFF) <<  0);
      apu->chan[i].stop = (dev_read(dev, OKI6295_SPACE_SAMPLES, base+3, 0xFF) << 16) |
                          (dev_read(dev, OKI6295_SPACE_SAMPLES, base+4, 0xFF) <<  8) |
                          (dev_read(dev, OKI6295_SPACE_SAMPLES, base+5, 0xFF) <<  0);

      apu->chan[i].addr &= 0x3FFFF;
      apu->chan[i].stop &= 0x3FFFF;

      apu->chan[i].addr <<= 1;
      apu->chan[i].stop <<= 1;

      if(apu->chan[i].addr < apu->chan[i].stop) {
        apu->chan[i].on = 1;
        apu->chan[i].vol = data & 0xF;
        adpcm_decode_oki_reset(&apu->chan[i].dec);
      }
    }
    apu->latched = 0;
  }else if(BIT(data, 7)) { /* Latch */
    apu->latch = data & 0x7F;
    apu->latched = 1;
  }else{ /* Silence */
    for(i = 0; i < 4; i++) {
      if(!BIT(data, i+3))
        continue;

      apu->chan[i].on = 0;
    }
  }
}

static void apu_oki6295_ss_change(hien_dev_t* dev, uintmax_t data, uintmax_t mask)
{
  apu_oki6295_t* apu;
  apu = dev->data;

  apu->divr = (data&mask) ? 132 : 165;
}

static void apu_oki6295_reset(hien_dev_t* dev)
{
  int i;
  apu_oki6295_t* apu;
  apu = dev->data;

  apu->ticks = 0;
  for(i = 0; i < 4; i++) {
    adpcm_decode_oki_init(&apu->chan[i].dec);
    apu->chan[i].on = 0;
  }
}

static void apu_oki6295_delete(hien_dev_t* dev)
{
  apu_oki6295_t* apu;
  apu = dev->data;

  mem_free(apu);
}

hien_dev_t* apu_oki6295_new(hien_vm_t* vm, char* name, hz_t clock)
{
  hien_dev_t* dev;
  apu_oki6295_t* apu;
  int i;

  dev = dev_new(vm, name, clock);
  dev->delete = apu_oki6295_delete;
  dev->reset = apu_oki6295_reset;

  dev_port_set_callbacks(dev, PORT_CLOCK, NULL, apu_oki6295_update);

  dev_space_setup(dev, SPACE_MAIN,            8);
  dev_space_setup(dev, OKI6295_SPACE_SAMPLES, 8);
  dev_space_set_callbacks(dev, SPACE_MAIN,            apu_oki6295_read, apu_oki6295_write);
  dev_space_set_callbacks(dev, OKI6295_SPACE_SAMPLES, NULL,             NULL);

  dev_port_setup(dev, OKI6295_PORT_SS,  1, PORT_INPUT);
  dev_port_setup(dev, OKI6295_PORT_DAO, 0, PORT_OUTPUT | PORT_ANALOG);

  dev_port_set_callbacks(dev, OKI6295_PORT_SS, NULL, apu_oki6295_ss_change);

  apu = mem_alloc_type(apu_oki6295_t);
  dev->data = apu;
  dev_reset(dev);

  /* Set up base SS setting. Drivers will override this anyways */
  dev_port_assert(dev, OKI6295_PORT_SS);

  vm_state_add_int(vm, &apu->divr, name, "clk_divr", NULL);
  vm_state_add_int(vm, &apu->latch, name, "reg_latch", NULL);
  vm_state_add_int(vm, &apu->latched, name, "reg_latch_flag", NULL);
  vm_state_add_int(vm, &apu->ticks, name, "ticks", NULL);
  vm_state_add_analog(vm, &apu->ao, name, "ao", NULL);

  for(i = 0; i < 4; i++) {
    char* cname = NULL;
    kuso_asprintf(&cname, "chan%d", i);
    vm_state_add_int(vm, &apu->chan[0].vol, name, cname, "vol", NULL);
    vm_state_add_addr(vm, &apu->chan[0].addr, name, cname, "addr", NULL);
    vm_state_add_addr(vm, &apu->chan[0].stop, name, cname, "stop", NULL);
    vm_state_add_int(vm, &apu->chan[0].on, name, cname, "on", NULL);

    adpcm_state_add(vm, &apu->chan[i].dec, name, cname, "dec", NULL);
    mem_free(cname);
  }

  return dev;
}

hien_dev_info_t apu_oki6295_def = {
  .type = "OKI6295",
  .ctor = "apu_oki6295_new",
  .use_clk = TRUE,
  .has_save = 1,
};
