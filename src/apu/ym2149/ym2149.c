/**********************************************************
*
* apu/ym2149/ym2149.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

/* Yamaha SSG (YM2149) core */

#include "hien.h"
#include "ym2149_internal.h"
#include "ym2149_tbls.h"

#define ENV_SHAPE_CONT Ob4(1000)
#define ENV_SHAPE_ATT  Ob4(0100)
#define ENV_SHAPE_ALT  Ob4(0010)
#define ENV_SHAPE_HOLD Ob4(0001)

/*
888     888 8888888b.  8888888b.        d8888 88888888888 8888888888
888     888 888   Y88b 888  "Y88b      d88888     888     888
888     888 888    888 888    888     d88P888     888     888
888     888 888   d88P 888    888    d88P 888     888     8888888
888     888 8888888P"  888    888   d88P  888     888     888
888     888 888        888    888  d88P   888     888     888
Y88b. .d88P 888        888  .d88P d8888888888     888     888
 "Y88888P"  888        8888888P" d88P     888     888     8888888888
*/
static analog_t apu_ym2149_chan_render(apu_ym2149_chan_t* chan, int noiselv, int envlv)
{
  int out_en;
  int level;

  out_en  = chan->tone  | chan->out; /* Tone */
  out_en &= chan->noise | noiselv;   /* Noise */

  if(chan->mode) { /* Envelope volume */
    level = out_en ? envlv : 0;
    level = _ym2149_dac[level];
  }else{ /* Per-channel volume */
    level = out_en ? chan->level : 0;
    level = _ym2149_dac_perchan[level];
  }

  chan->ao = A_FIX2V(level, A_5V);
  return chan->ao;
}

static void apu_ym2149_chan_render_all(hien_dev_t* dev, apu_ym2149_t* apu)
{
  apu_ym2149_chan_render(&apu->chan[0], apu->env.level, apu->noise.out);
  apu_ym2149_chan_render(&apu->chan[1], apu->env.level, apu->noise.out);
  apu_ym2149_chan_render(&apu->chan[2], apu->env.level, apu->noise.out);

  dev_port_analog_write(dev, YM2149_PORT_AO_A, apu->chan[0].ao);
  dev_port_analog_write(dev, YM2149_PORT_AO_B, apu->chan[1].ao);
  dev_port_analog_write(dev, YM2149_PORT_AO_C, apu->chan[2].ao);
}

static void apu_ym2149_noise_update_core(apu_ym2149_noise_t* noise)
{
  noise->ticks++;
  if(noise->ticks >= noise->freq) {
    noise->ticks = 0;

    if(BIT(noise->lfsr, 0) ^ BIT(noise->lfsr, 1))
      noise->out ^= 1;

    /* Shift in new bit */
    noise->lfsr |= (BIT(noise->lfsr, 0) ^ BIT(noise->lfsr, 3)) << 17;
    noise->lfsr >>= 1;
  }
}

static void apu_ym2149_chan_update_core(apu_ym2149_chan_t* chan)
{
  chan->ticks++;
  if(chan->ticks >= chan->freq) {
    chan->ticks = 0;
    chan->out ^= 1;
  }
}

static void apu_ym2149_chan_update(apu_ym2149_t* apu)
{
  apu_ym2149_chan_update_core(&apu->chan[0]);
  apu_ym2149_chan_update_core(&apu->chan[1]);
  apu_ym2149_chan_update_core(&apu->chan[2]);
  apu_ym2149_noise_update_core(&apu->noise);
}

static void apu_ym2149_env_update(apu_ym2149_env_t* env)
{
  if(!env->hold) {
    env->ticks++;
    if(env->ticks >= env->freq) {
      env->ticks = 0;

      /* Advance envelope */
      env->step--;

      if(env->step < 0) {
        /* 'Reset' envelope */
        if(env->shape_latch & ENV_SHAPE_ALT) {
          env->shape_latch ^= ENV_SHAPE_ATT;
        }
        if(env->shape_latch & ENV_SHAPE_HOLD) {
          env->hold = TRUE;
          env->step = 0;
        }else{
          env->step = 0x1F;
        }
      }
    }
  }

  env->level = env->step;
  if(env->shape_latch & ENV_SHAPE_ATT)
    env->level ^= 0x1F;
}

static void apu_ym2149_update(hien_dev_t* dev, uintmax_t data, uintmax_t mask)
{
  apu_ym2149_t* apu;
  (void)mask;
  apu = dev->data;

  if(data != 1) /* do nothing on falling edge */
    return;

  if(apu->tick.chan == 0)
    apu_ym2149_chan_update(apu);
  if(apu->tick.env == 0)
    apu_ym2149_env_update(&apu->env);

  if((apu->tick.chan == 0) || (apu->tick.env == 0))
    apu_ym2149_chan_render_all(dev, apu);

  apu->tick.chan++;
  apu->tick.env++;

  apu->tick.chan %= apu->prescale.chan;
  apu->tick.env %= apu->prescale.env;
}

static void apu_ym2149_update_prescale(hien_dev_t* dev, apu_ym2149_t* apu)
{
  uint8_t prescale_chan[2] = { 2, 4 };
  uint8_t prescale_env[2]  = { 1, 2 };
  (void)dev;

  apu->prescale.chan = prescale_chan[apu->div];
  apu->prescale.env  = prescale_env[apu->div];
}

/*
8888888b.  8888888888        d8888 8888888b.
888   Y88b 888              d88888 888  "Y88b
888    888 888             d88P888 888    888
888   d88P 8888888        d88P 888 888    888
8888888P"  888           d88P  888 888    888
888 T88b   888          d88P   888 888    888
888  T88b  888         d8888888888 888  .d88P
888   T88b 8888888888 d88P     888 8888888P"
*/
static uintmax_t apu_ym2149_read(hien_dev_t* dev, addr_t addr, uintmax_t mask)
{
  apu_ym2149_t* apu;
  apu = dev->data;
  (void)addr;

  return apu->regs[apu->regsel] & mask;
}

/*
888       888 8888888b.  8888888 88888888888 8888888888
888   o   888 888   Y88b   888       888     888
888  d8b  888 888    888   888       888     888
888 d888b 888 888   d88P   888       888     8888888
888d88888b888 8888888P"    888       888     888
88888P Y88888 888 T88b     888       888     888
8888P   Y8888 888  T88b    888       888     888
888P     Y888 888   T88b 8888888     888     8888888888
*/
static void apu_ym2149_write(hien_dev_t* dev, addr_t addr, uintmax_t data, uintmax_t mask)
{
  apu_ym2149_t* apu;
  apu = dev->data;

  addr &= 1;
  data &= 0xFF;
  mask &= 0xFF;
  switch(addr) {
    case 0: /* Register Select */
      COMBINE_DATA(apu->regsel, data, mask);
      break;
    case 1: /* Register Write */
      COMBINE_DATA(apu->regs[apu->regsel], data, mask);
      switch(apu->regsel) {
        case 0x00: case 0x02: case 0x04: /* Fine tune */
          apu->chan[apu->regsel>>1].freq &= ~0xFF;
          apu->chan[apu->regsel>>1].freq |= data;
          break;
        case 0x01: case 0x03: case 0x05: /* Coarse tune */
          data &= 0xF;
          apu->chan[apu->regsel>>1].freq &= ~0xF00;
          apu->chan[apu->regsel>>1].freq |= (data << 8);
          break;
        case 0x06: /* Noise period */
          apu->noise.freq = data & 0x1F;
          break;
        case 0x07: /* Control */
          apu->chan[0].tone  = BIT(data, 0);
          apu->chan[1].tone  = BIT(data, 1);
          apu->chan[2].tone  = BIT(data, 2);
          apu->chan[0].noise = BIT(data, 3);
          apu->chan[1].noise = BIT(data, 4);
          apu->chan[2].noise = BIT(data, 5);
          apu->ioa_dir       = BIT(data, 6);
          apu->iob_dir       = BIT(data, 7);
          break;
        case 0x08: case 0x09: case 0x0A: /* Level */
          apu->chan[apu->regsel-8].mode = BIT(data,4);
          apu->chan[apu->regsel-8].level = data & 0xF;
          break;
        case 0xB: /* Envelope frequency (fine) */
          apu->env.freq &= ~0x00FF;
          apu->env.freq |= data;
          break;
        case 0xC: /* Envelope frequency (coarse) */
          apu->env.freq &= ~0xFF00;
          apu->env.freq |= data << 8;
          break;
        case 0xD: /* Envelope shape */
          apu->env.shape = data & 0xF;
          apu->env.shape_latch = apu->env.shape;
          if(!(apu->env.shape & ENV_SHAPE_CONT)) { /* Rewrite it to have CONT set. */
            apu->env.shape_latch |= ENV_SHAPE_CONT;
            apu->env.shape_latch |= ENV_SHAPE_HOLD;
            if(apu->env.shape_latch & ENV_SHAPE_ATT)
              apu->env.shape_latch |= ENV_SHAPE_ALT;
            else
              apu->env.shape_latch &= ~ENV_SHAPE_ALT;
          }
          apu->env.hold = FALSE;
          apu->env.step = 0x1F;
          break;
        case 0xE: /* I/O Port A */
          apu->ioa = data;
          break;
        case 0xF: /* I/O Port B */
          apu->iob = data;
          break;
      }
      break;
  }
}

/*
8888888b.   .d88888b.  8888888b. 88888888888 .d8888b.
888   Y88b d88P" "Y88b 888   Y88b    888    d88P  Y88b
888    888 888     888 888    888    888    Y88b.
888   d88P 888     888 888   d88P    888     "Y888b.
8888888P"  888     888 8888888P"     888        "Y88b.
888        888     888 888 T88b      888          "888
888        Y88b. .d88P 888  T88b     888    Y88b  d88P
888         "Y88888P"  888   T88b    888     "Y8888P"
*/
static void apu_ym2149_write_nsel(hien_dev_t* dev, uintmax_t data, uintmax_t mask)
{
  apu_ym2149_t* apu;
  apu = dev->data;
  (void)mask;

  apu->div = (data & 1) ? FALSE : TRUE;
  apu_ym2149_update_prescale(dev, apu);
}
static uintmax_t apu_ym2149_read_ioa(hien_dev_t* dev, uintmax_t mask)
{
  apu_ym2149_t* apu;
  apu = dev->data;

  return apu->ioa & mask;
}
static uintmax_t apu_ym2149_read_iob(hien_dev_t* dev, uintmax_t mask)
{
  apu_ym2149_t* apu;
  apu = dev->data;

  return apu->iob & mask;
}

/*
8888888b.  8888888888 .d8888b.  8888888888 88888888888
888   Y88b 888       d88P  Y88b 888            888
888    888 888       Y88b.      888            888
888   d88P 8888888    "Y888b.   8888888        888
8888888P"  888           "Y88b. 888            888
888 T88b   888             "888 888            888
888  T88b  888       Y88b  d88P 888            888
888   T88b 8888888888 "Y8888P"  8888888888     888
*/
static void apu_ym2149_chan_reset(apu_ym2149_chan_t* chan)
{
  chan->freq = 0;
  chan->mode = 0;
  chan->level = 0;
  chan->tone = TRUE;
  chan->noise = TRUE;
  chan->out = 0;
  chan->ticks = 0;
  chan->ao = A_0V;
}
static void apu_ym2149_noise_reset(apu_ym2149_noise_t* noise)
{
  noise->freq = 0;
  noise->out = 0;
  noise->ticks = 0;

  noise->lfsr = 1;
}
static void apu_ym2149_env_reset(apu_ym2149_env_t* env)
{
  env->freq = 0;
  env->shape = 0;
  env->shape_latch = 0;
  env->hold = TRUE;
  env->level = 0;
  env->step = 0;
  env->ticks = 0;
}
static void apu_ym2149_reset(hien_dev_t* dev)
{
  apu_ym2149_t* apu;
  apu = dev->data;

  apu_ym2149_chan_reset(&apu->chan[0]);
  apu_ym2149_chan_reset(&apu->chan[1]);
  apu_ym2149_chan_reset(&apu->chan[2]);

  apu_ym2149_noise_reset(&apu->noise);
  apu_ym2149_env_reset(&apu->env);
}

/*
8888888b.  8888888888 888      8888888888 88888888888 8888888888
888  "Y88b 888        888      888            888     888
888    888 888        888      888            888     888
888    888 8888888    888      8888888        888     8888888
888    888 888        888      888            888     888
888    888 888        888      888            888     888
888  .d88P 888        888      888            888     888
8888888P"  8888888888 88888888 8888888888     888     8888888888
*/
static void apu_ym2149_delete(hien_dev_t* dev)
{
  apu_ym2149_t* apu;
  apu = dev->data;

  mem_free(apu);
}

/*
888b    888 8888888888 888       888
8888b   888 888        888   o   888
88888b  888 888        888  d8b  888
888Y88b 888 8888888    888 d888b 888
888 Y88b888 888        888d88888b888
888  Y88888 888        88888P Y88888
888   Y8888 888        8888P   Y8888
888    Y888 8888888888 888P     Y888
*/

hien_dev_t* apu_ym2149_new(hien_vm_t* vm, char* name, hz_t clock)
{
  hien_dev_t* dev;
  apu_ym2149_t* apu;
  int i;

  dev = dev_new(vm, name, clock);
  dev->delete = apu_ym2149_delete;
  dev->reset = apu_ym2149_reset;

  dev_port_set_callbacks(dev, PORT_CLOCK, NULL, apu_ym2149_update);

  dev_space_setup(dev, SPACE_MAIN, 8);
  dev_space_set_callbacks(dev, SPACE_MAIN, apu_ym2149_read, apu_ym2149_write);

  dev_port_setup(dev, YM2149_PORT_IOA,   8, PORT_INOUT);
  dev_port_setup(dev, YM2149_PORT_IOB,   8, PORT_INOUT);
  dev_port_setup(dev, YM2149_PORT_AO_A,  0, PORT_OUTPUT | PORT_ANALOG);
  dev_port_setup(dev, YM2149_PORT_AO_B,  0, PORT_OUTPUT | PORT_ANALOG);
  dev_port_setup(dev, YM2149_PORT_AO_C,  0, PORT_OUTPUT | PORT_ANALOG);
  dev_port_setup(dev, YM2149_PORT_nSEL,  1, PORT_INPUT);

  dev_port_set_callbacks(dev, YM2149_PORT_nSEL, NULL, apu_ym2149_write_nsel);
  dev_port_set_callbacks(dev, YM2149_PORT_IOA,  apu_ym2149_read_ioa, NULL);
  dev_port_set_callbacks(dev, YM2149_PORT_IOB,  apu_ym2149_read_iob, NULL);

  apu = mem_alloc_type(apu_ym2149_t);
  dev->data = apu;

  dev_port_assert(dev, YM2149_PORT_nSEL);
  dev_port_deassert(dev, YM2149_PORT_nSEL);

  dev_reset(dev);

  vm_state_add_int(vm, &apu->div, name, "clk_div", NULL);
  vm_state_add_uint8(vm, &apu->prescale.chan, name, "prescale", "chan", NULL);
  vm_state_add_uint8(vm, &apu->prescale.env, name, "prescale", "env", NULL);
  vm_state_add_uint8(vm, &apu->tick.chan, name, "tick", "chan", NULL);
  vm_state_add_uint8(vm, &apu->tick.env, name, "tick", "env", NULL);
  vm_state_add_int(vm, &apu->ioa_dir, name, "io", "a_dir", NULL);
  vm_state_add_int(vm, &apu->iob_dir, name, "io", "b_dir", NULL);
  vm_state_add_uint8(vm, &apu->ioa, name, "io", "a_data", NULL);
  vm_state_add_uint8(vm, &apu->iob, name, "io", "b_data", NULL);
  vm_state_add_uint8(vm, &apu->regsel, name, "reg", "sel", NULL);
  vm_state_add_uint8_arr(vm, 0x10, apu->regs, name, "reg", "data", NULL);

  vm_state_add_uint16(vm, &apu->env.freq, name, "env", "freq", NULL);
  vm_state_add_uint16(vm, &apu->env.ticks, name, "env", "ticks", NULL);
  vm_state_add_uint8(vm, &apu->env.shape, name, "env", "shape", NULL);
  vm_state_add_uint8(vm, &apu->env.shape_latch, name, "env", "shape_latch", NULL);
  vm_state_add_uint8(vm, &apu->env.hold, name, "env", "hold", NULL);
  vm_state_add_uint8(vm, &apu->env.level, name, "env", "level", NULL);
  vm_state_add_int8(vm, &apu->env.step, name, "env", "step", NULL);

  vm_state_add_uint8(vm, &apu->noise.freq, name, "noise", "freq", NULL);
  vm_state_add_uint8(vm, &apu->noise.ticks, name, "noise", "ticks", NULL);
  vm_state_add_uint32(vm, &apu->noise.lfsr, name, "noise", "lfsr", NULL);
  vm_state_add_uint8(vm, &apu->noise.out, name, "noise", "out", NULL);

  for(i = 0; i < 3; i++) {
    char* cname = NULL;
    kuso_asprintf(&cname, "chan%d", i);

    vm_state_add_uint16(vm, &apu->chan[i].freq, name, cname, "freq", NULL);
    vm_state_add_uint16(vm, &apu->chan[i].ticks, name, cname, "ticks", NULL);
    vm_state_add_analog(vm, &apu->chan[i].ao, name, cname, "ao", NULL);
    vm_state_add_uint8(vm, &apu->chan[i].mode, name, cname, "mode", NULL);
    vm_state_add_uint8(vm, &apu->chan[i].level, name, cname, "level", NULL);
    vm_state_add_uint8(vm, &apu->chan[i].tone, name, cname, "tone", NULL);
    vm_state_add_uint8(vm, &apu->chan[i].noise, name, cname, "noise", NULL);
    vm_state_add_uint8(vm, &apu->chan[i].out, name, cname, "out", NULL);

    mem_free(cname);
  }

  return dev;
}

hien_dev_info_t apu_ym2149_def = {
  .type = "YM2149",
  .ctor = "apu_ym2149_new",
  .use_clk = TRUE,
  .has_save = 1,
};
