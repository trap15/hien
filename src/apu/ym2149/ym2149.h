/**********************************************************
*
* apu/ym2149/ym2149.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _APU_YM2149_H_
#define _APU_YM2149_H_

#define YM2149_PORT_IOA     (PORT_USER+0)
#define YM2149_PORT_IOB     (PORT_USER+1)
#define YM2149_PORT_AO_A    (PORT_USER+2) /* Analog Channel A output (0~1V) */
#define YM2149_PORT_AO_B    (PORT_USER+3) /* Analog Channel B output (0~1V) */
#define YM2149_PORT_AO_C    (PORT_USER+4) /* Analog Channel C output (0~1V) */
#define YM2149_PORT_nSEL    (PORT_USER+5) /* Clock Divide */
#define YM2149_PORT_COUNT   (PORT_USER+6)

PORT_COUNT_CHECK(YM2149);

hien_dev_t* apu_ym2149_new(hien_vm_t* vm, char* name, hz_t clock);

extern hien_dev_info_t apu_ym2149_def;

#endif
