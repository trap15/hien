/**********************************************************
*
* apu/ym2149/ym2149_internal.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _APU_YM2149_INTERNAL_H_
#define _APU_YM2149_INTERNAL_H_

typedef struct apu_ym2149_chan_t apu_ym2149_chan_t;
typedef struct apu_ym2149_noise_t apu_ym2149_noise_t;
typedef struct apu_ym2149_env_t apu_ym2149_env_t;
typedef struct apu_ym2149_t apu_ym2149_t;

struct apu_ym2149_chan_t {
  uint16_t freq; /* 12 bit */
  uint8_t mode; /* 1 bit */
  uint8_t level; /* 4 bit */
  uint8_t tone; /* 1 bit */
  uint8_t noise; /* 1 bit */

  /* Emulation state */
  uint8_t out; /* 1 bit. Output state (set for high, clear for low) */
  uint16_t ticks; /* Increases every 16 ticks. wraps at freq. */

  analog_t ao; /* Analog output */
};

struct apu_ym2149_noise_t {
  uint8_t freq; /* 5 bit */

  /* Emulation state */
  uint32_t lfsr;
  uint8_t out; /* 1 bit. Output state (set for high, clear for low) */
  uint8_t ticks; /* Increases every 16 ticks. wraps at freq. */
};

struct apu_ym2149_env_t {
  uint16_t freq; /* 16 bit */
  uint8_t shape; /* 4 bits */

  /* Emulation state */
  uint8_t shape_latch; /* 4 bits */
  uint8_t hold; /* 1 bit */
  uint8_t level; /* 5 bits */
  int8_t step; /* 5 bits */
  uint16_t ticks; /* wraps at freq */
};

struct apu_ym2149_t {
  int div;

  apu_ym2149_chan_t chan[3];
  apu_ym2149_noise_t noise;
  apu_ym2149_env_t env;

  struct {
    uint8_t chan, env;
  } prescale;

  struct {
    uint8_t chan, env;
  } tick;

  int ioa_dir, iob_dir;
  uint8_t ioa, iob;

  uint8_t regs[0x10];
  uint8_t regsel;
};

#endif
