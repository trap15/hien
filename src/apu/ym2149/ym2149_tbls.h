/**********************************************************
*
* apu/ym2149/ym2149_tbls.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _APU_YM2149_TBLS_H_
#define _APU_YM2149_TBLS_H_

/* PSG DAC values.
 * Derived from fig.1 in the YM2149 datasheet.
 *
 * Algorithm:
 *   0x7FFFFFFF * 2^((x-31)/4)
 */

static const uintmax_t _ym2149_dac[32] = {
  0x009837F0, 0x00B504F3, 0x00D744FC, 0x00FFFFFF,
  0x01306FE0, 0x016A09E6, 0x01AE89F9, 0x01FFFFFF,
  0x0260DFC1, 0x02D413CC, 0x035D13F3, 0x03FFFFFF,
  0x04C1BF82, 0x05A82799, 0x06BA27E6, 0x07FFFFFF,
  0x09837F05, 0x0B504F33, 0x0D744FCC, 0x0FFFFFFF,
  0x1306FE0A, 0x16A09E66, 0x1AE89F99, 0x1FFFFFFF,
  0x260DFC14, 0x2D413CCC, 0x35D13F32, 0x3FFFFFFF,
  0x4C1BF828, 0x5A827999, 0x6BA27E64, 0x7FFFFFFF,
};

/* Simplified DAC table for per-channel volume */
static const uintmax_t _ym2149_dac_perchan[16] = {
  0x00B504F3, 0x00FFFFFF,
  0x016A09E6, 0x01FFFFFF,
  0x02D413CC, 0x03FFFFFF,
  0x05A82799, 0x07FFFFFF,
  0x0B504F33, 0x0FFFFFFF,
  0x16A09E66, 0x1FFFFFFF,
  0x2D413CCC, 0x3FFFFFFF,
  0x5A827999, 0x7FFFFFFF,
};

#endif
