/**********************************************************
*
* apu/sn76496/sn76496.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _APU_SN76496_H_
#define _APU_SN76496_H_

#define SN76496_PORT_READY   (PORT_USER+0)
#define SN76496_PORT_AO      (PORT_USER+1) /* Audio output */
#define SN76496_PORT_AI      (PORT_USER+2) /* Audio input */
#define SN76496_PORT_COUNT   (PORT_USER+3)

PORT_COUNT_CHECK(SN76496);

hien_dev_t* apu_sn76496_new(hien_vm_t* vm, char* name, hz_t clock);

extern hien_dev_info_t apu_sn76496_def;

#endif
