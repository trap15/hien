/**********************************************************
*
* apu/sn76496/sn76496.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

typedef struct apu_sn76496_t apu_sn76496_t;

struct apu_sn76496_t {
  struct {
    uint8_t vol;
    uint16_t tone;
    uint8_t noise;

    /* Emulation State */
    uint32_t lfsr;
    uint16_t ticks;
    uint8_t out;

    int32_t level;
  } chan[4];

  uint8_t latch;
  int32_t out_level;
  analog_t ao;
};

/* TODO: Verify */
static const uintmax_t _sn76496_dac[16] = {
  0xFFFFFFFF, 0xCB59185D, 0xA1866BA7, 0x804DCE79,
  0x65EA59FD, 0x50F44D88, 0x404DE61F, 0x331426AE,
  0x2892C18A, 0x203A7E5B, 0x19999999, 0x1455B5A2,
  0x10270AC3, 0x0CD494A5, 0x0A3108FF, 0x00000000,
};

static void _sn76496_render_chan(apu_sn76496_t* apu, int ch)
{
  apu->chan[ch].level = _sn76496_dac[apu->chan[ch].vol] / 2;
  if(!apu->chan[ch].out)
    apu->chan[ch].level = -apu->chan[ch].level;
}

static void _sn76496_update_tone(apu_sn76496_t* apu, int ch)
{
  apu->chan[ch].ticks--;
  if(apu->chan[ch].ticks <= 0) {
    apu->chan[ch].out ^= 1;
    apu->chan[ch].ticks = apu->chan[ch].tone;
  }
}

static void _sn76496_update_noise(apu_sn76496_t* apu, int ch)
{
  int tap1, tap2;
  apu->chan[ch].ticks--;
  if(apu->chan[ch].ticks <= 0) {
    switch(apu->chan[ch].noise & 3) {
      case 0: apu->chan[ch].ticks = 0x10; break;
      case 1: apu->chan[ch].ticks = 0x20; break;
      case 2: apu->chan[ch].ticks = 0x40; break;
      case 3: apu->chan[ch].ticks = apu->chan[2].tone; break;
    }

    tap1 = BIT(apu->chan[ch].lfsr, 2);
    tap2 = BIT(apu->chan[ch].noise, 2) ? BIT(apu->chan[ch].lfsr, 3) : 0;

    if(tap1 ^ tap2)
      apu->chan[ch].lfsr |= 1 << 17;
    apu->chan[ch].lfsr >>= 1;
  }

  apu->chan[ch].out = apu->chan[ch].lfsr & 1;
}

static void apu_sn76496_update(hien_dev_t* dev, uintmax_t data, uintmax_t mask)
{
  apu_sn76496_t* apu;
  (void)mask;
  apu = dev->data;

  if(data != 1) /* do nothing on falling edge */
    return;

  _sn76496_update_tone(apu, 0);
  _sn76496_update_tone(apu, 1);
  _sn76496_update_tone(apu, 2);
  _sn76496_update_noise(apu, 3);

  _sn76496_render_chan(apu, 0);
  _sn76496_render_chan(apu, 1);
  _sn76496_render_chan(apu, 2);
  _sn76496_render_chan(apu, 3);

  apu->out_level = (apu->chan[0].level / 4) +
                   (apu->chan[1].level / 4) +
                   (apu->chan[2].level / 4) +
                   (apu->chan[3].level / 4);
  apu->ao = A_FIX2V(apu->out_level, A_5V);
  dev_port_analog_write(dev, SN76496_PORT_AO, apu->ao + dev_port_analog_read(dev, SN76496_PORT_AI));

  dev_port_deassert(dev, SN76496_PORT_READY);
}

static void apu_sn76496_write(hien_dev_t* dev, addr_t addr, uintmax_t data, uintmax_t mask)
{
  apu_sn76496_t* apu;
  apu = dev->data;

  (void)apu;
  (void)addr;
  data &= mask;

  if(BIT(data, 7)) { /* Latch/Data */
    apu->latch = (data >> 4) & 7;
    switch(apu->latch) {
      case 0: /* Tone0 */ COMBINE_DATA(apu->chan[0].tone, data, 0xF); break;
      case 1: /* Vol0  */ COMBINE_DATA(apu->chan[0].vol,  data, 0xF); break;
      case 2: /* Tone1 */ COMBINE_DATA(apu->chan[1].tone, data, 0xF); break;
      case 3: /* Vol1  */ COMBINE_DATA(apu->chan[1].vol,  data, 0xF); break;
      case 4: /* Tone2 */ COMBINE_DATA(apu->chan[2].tone, data, 0xF); break;
      case 5: /* Vol2  */ COMBINE_DATA(apu->chan[2].vol,  data, 0xF); break;
      case 6: /* Noise */ COMBINE_DATA(apu->chan[3].noise,data, 0x7); break;
      case 7: /* Vol3  */ COMBINE_DATA(apu->chan[3].vol,  data, 0xF); break;
    }
  }else{ /* Data */
    switch(apu->latch) {
      case 0: /* Tone0 */ COMBINE_DATA(apu->chan[0].tone, data<<4, 0x3F<<4); break;
      case 1: /* Vol0  */ COMBINE_DATA(apu->chan[0].vol,  data<<0, 0x0F<<0); break;
      case 2: /* Tone1 */ COMBINE_DATA(apu->chan[1].tone, data<<4, 0x3F<<4); break;
      case 3: /* Vol1  */ COMBINE_DATA(apu->chan[1].vol,  data<<0, 0x0F<<0); break;
      case 4: /* Tone2 */ COMBINE_DATA(apu->chan[2].tone, data<<4, 0x3F<<4); break;
      case 5: /* Vol2  */ COMBINE_DATA(apu->chan[2].vol,  data<<0, 0x0F<<0); break;
      case 6: /* Noise */ COMBINE_DATA(apu->chan[3].noise,data<<0, 0x07<<0); break;
      case 7: /* Vol3  */ COMBINE_DATA(apu->chan[3].vol,  data<<0, 0x0F<<0); break;
    }
  }
  if(apu->chan[apu->latch>>1].tone & 0x3FF)
    apu->chan[apu->latch>>1].tone &= 0x3FF;
  else
    apu->chan[apu->latch>>1].tone |= 0x400;

  if(apu->latch == 6) {
    apu->chan[3].lfsr = 0x10000;
  }

  dev_port_assert(dev, SN76496_PORT_READY);
}

static void apu_sn76496_reset(hien_dev_t* dev)
{
  apu_sn76496_t* apu;
  apu = dev->data;

  apu->latch = 0;

  apu->chan[0].tone = 0;
  apu->chan[1].tone = 0;
  apu->chan[2].tone = 0;
  apu->chan[3].noise= 0;

  apu->chan[0].vol = 0xF;
  apu->chan[1].vol = 0xF;
  apu->chan[2].vol = 0xF;
  apu->chan[3].vol = 0xF;

  apu->chan[0].out = 0;
  apu->chan[1].out = 0;
  apu->chan[2].out = 0;
  apu->chan[3].out = 0;

  apu->chan[0].ticks = 0;
  apu->chan[1].ticks = 0;
  apu->chan[2].ticks = 0;
  apu->chan[3].ticks = 0;

  apu->chan[0].level = 0;
  apu->chan[1].level = 0;
  apu->chan[2].level = 0;
  apu->chan[3].level = 0;

  apu->chan[3].lfsr = 0x10000;
}

static void apu_sn76496_delete(hien_dev_t* dev)
{
  apu_sn76496_t* apu;
  apu = dev->data;

  mem_free(apu);
}

hien_dev_t* apu_sn76496_new(hien_vm_t* vm, char* name, hz_t clock)
{
  hien_dev_t* dev;
  apu_sn76496_t* apu;
  int i;

  clock /= 16; /* Internal clock divider */

  dev = dev_new(vm, name, clock);
  dev->delete = apu_sn76496_delete;
  dev->reset = apu_sn76496_reset;

  dev_port_set_callbacks(dev, PORT_CLOCK, NULL, apu_sn76496_update);

  dev_space_setup(dev, SPACE_MAIN, 8);
  dev_space_set_callbacks(dev, SPACE_MAIN, NULL, apu_sn76496_write);

  dev_port_setup(dev, SN76496_PORT_READY, 1, PORT_OUTPUT);
  dev_port_setup(dev, SN76496_PORT_AO,    0, PORT_OUTPUT | PORT_ANALOG);
  dev_port_setup(dev, SN76496_PORT_AI,    0, PORT_INPUT  | PORT_ANALOG);

  apu = mem_alloc_type(apu_sn76496_t);

  dev->data = apu;

  dev_reset(dev);

  vm_state_add_uint8(vm, &apu->latch, name, "latch", NULL);
  vm_state_add_int32(vm, &apu->out_level, name, "out_level", NULL);
  vm_state_add_analog(vm, &apu->ao, name, "ao", NULL);

  for(i = 0; i < 4; i++) {
    char* cname = NULL;
    kuso_asprintf(&cname, "chan%d", i);

    vm_state_add_int32(vm, &apu->chan[i].level, name, cname, "level", NULL);
    vm_state_add_uint32(vm, &apu->chan[i].lfsr, name, cname, "lfsr", NULL);
    vm_state_add_uint16(vm, &apu->chan[i].tone, name, cname, "tone", NULL);
    vm_state_add_uint16(vm, &apu->chan[i].ticks, name, cname, "ticks", NULL);
    vm_state_add_uint8(vm, &apu->chan[i].vol, name, cname, "vol", NULL);
    vm_state_add_uint8(vm, &apu->chan[i].noise, name, cname, "noise", NULL);
    vm_state_add_uint8(vm, &apu->chan[i].out, name, cname, "out", NULL);

    mem_free(cname);
  }

  return dev;
}

hien_dev_info_t apu_sn76496_def = {
  .type = "SN76496",
  .ctor = "apu_sn76496_new",
  .use_clk = TRUE,
  .has_save = 1,
};
