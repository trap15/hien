/**********************************************************
*
* apu/adpcm/adpcm.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

#include "oki_tbls.h"
#include "ym_adpcma_tbls.h"

int32_t adpcm_decode_next(adpcm_decode_t* dec, int nib)
{
  dec->sig += dec->step_tbl[dec->step*16 + nib];

  if(dec->sig > BITMASK(dec->sigsz-1))
    dec->sig = BITMASK(dec->sigsz-1);
  else if(dec->sig < -(1 << (dec->sigsz-1)))
    dec->sig = -(1 << (dec->sigsz-1));

  dec->step += dec->adj_tbl[nib & 7];
  if(dec->step >= dec->stepcnt)
    dec->step = dec->stepcnt-1;
  else if(dec->step < 0)
    dec->step = 0;

  return dec->sig;
}


void adpcm_decode_oki_reset(adpcm_decode_t* dec)
{
  dec->sig = -2;
  dec->step = 0;
}

void adpcm_decode_oki_init(adpcm_decode_t* dec)
{
  adpcm_decode_oki_reset(dec);
  dec->stepcnt = 49;
  dec->sigsz = 12;
  dec->step_tbl = _oki_step_tbl;
  dec->adj_tbl = _oki_adj_tbl;
}

void adpcm_decode_ym_adpcma_reset(adpcm_decode_t* dec)
{
  dec->sig = -2;
  dec->step = 0;
}

void adpcm_decode_ym_adpcma_init(adpcm_decode_t* dec)
{
  adpcm_decode_ym_adpcma_reset(dec);
  dec->stepcnt = 49;
  dec->sigsz = 12;
  dec->step_tbl = _ym_adpcma_step_tbl;
  dec->adj_tbl = _ym_adpcma_adj_tbl;
}

void adpcm_state_add(hien_vm_t* vm, adpcm_decode_t* dec, char* path, ...)
{
  char* expath;
  va_list v;
  va_start(v, path);
  expath = state_build_path_va(path, v);
  va_end(v);

  vm_state_add_int32(vm, &dec->sig, expath, "signal", NULL);
  vm_state_add_int32(vm, &dec->step, expath, "step", NULL);

  mem_free(expath);
}
