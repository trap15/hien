/**********************************************************
*
* apu/adpcm/adpcm.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

/* This isn't actually a device, just a flexible ADPCM decoder. */

#ifndef _APU_ADPCM_H_
#define _APU_ADPCM_H_

typedef struct adpcm_decode_t adpcm_decode_t;

struct adpcm_decode_t {
  int32_t sig, step;
  int sigsz; /* in bits */
  int stepcnt;
  const int* step_tbl; /* stepcnt*16 entries */
  const int* adj_tbl; /* 8 entries */
};

int32_t adpcm_decode_next(adpcm_decode_t* dec, int nib);

void adpcm_decode_oki_init(adpcm_decode_t* dec);
void adpcm_decode_oki_reset(adpcm_decode_t* dec);

void adpcm_decode_ym_adpcma_init(adpcm_decode_t* dec);
void adpcm_decode_ym_adpcma_reset(adpcm_decode_t* dec);

void adpcm_state_add(hien_vm_t* vm, adpcm_decode_t* dec, char* path, ...);

#endif
