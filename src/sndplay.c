/**********************************************************
*
* sndplay.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

enum {
  SNDPLAY_CMD_PLAY,
  SNDPLAY_CMD_STOP,
  SNDPLAY_CMD_RESET
};

static timetick_t sndplay_process_one(hien_sndplay_t* sndp)
{
  timetick_t tm;
  hien_sndplay_cmd_t* cmd;

  if(kuso_pvec_size(sndp->cmdbuf) == 0)
    return 0;

  cmd = kuso_pvec_remove(sndp->cmdbuf, 0);
  tm = cmd->time;

  switch(cmd->cmdtype) {
    case SNDPLAY_CMD_RESET:
      if(sndp->desc && sndp->desc->reset) {
        sndp->desc->reset(sndp);
        break;
      }
      /* Fall-thru */
    case SNDPLAY_CMD_STOP:
      if(sndp->desc && sndp->desc->stop) {
        sndp->desc->stop(sndp);
        break;
      }else{ /* If there's no stop callback, send the stop sound code instead */
        cmd->subcmd = sndp->desc->stop_id;
        /* Fall-thru */
      }
    case SNDPLAY_CMD_PLAY:
      if(sndp->desc && sndp->desc->play)
        sndp->desc->play(sndp, cmd->subcmd);
      break;
  }

  mem_free(cmd);
  return tm;
}

static void sndplay_set_timer(hien_timer_t* tmr, timetick_t tm)
{
  if(tm == 0) return;

  tmr_set_oneshot(tmr, tm);
  tmr_reset(tmr, 0);
  tmr_enable(tmr, TRUE);
}

static void sndplay_delay_cb(hien_timer_t* tmr, void* data)
{
  hien_sndplay_t* sndp = data;
  sndplay_set_timer(tmr, sndplay_process_one(sndp));
}

static void sndplay_cmd_add(hien_sndplay_t* sndp, int cmdtype, int subcmd, timetick_t time)
{
  hien_sndplay_cmd_t* cmd;
  cmd = mem_alloc_type(hien_sndplay_cmd_t);
  cmd->cmdtype = cmdtype;
  cmd->subcmd = subcmd;
  cmd->time = time;

  kuso_pvec_append(sndp->cmdbuf, cmd);
  if(!sndplay_waiting(sndp))
    sndplay_delay_cb(sndp->delay_tmr, sndp);
}

hien_sndplay_t* sndplay_new(hien_vm_t* vm)
{
  hien_sndplay_t* sndp;

  sndp = mem_alloc_type(hien_sndplay_t);
  sndp->vm = vm;

  sndp->desc = NULL;

  sndp->delay_tmr = tmr_new();
  tmr_set_callback(sndp->delay_tmr, sndplay_delay_cb, sndp);
  tmr_mgr_add(sndp->vm->tmrmgr, sndp->delay_tmr);

  sndp->cmdbuf = kuso_pvec_new(0);

  return sndp;
}

void sndplay_delete(hien_sndplay_t* sndp)
{
  sndplay_unload(sndp);

  tmr_mgr_remove(sndp->vm->tmrmgr, sndp->delay_tmr);
  tmr_delete(sndp->delay_tmr);
  kuso_pvec_delete(sndp->cmdbuf);

  mem_free(sndp);
}

void sndplay_load(hien_sndplay_t* sndp, hien_sndplay_desc_t* desc)
{
  sndp->desc = desc;

  sndplay_reset(sndp);
}

void sndplay_unload(hien_sndplay_t* sndp)
{
  sndp->desc = NULL;
}

void sndplay_reset(hien_sndplay_t* sndp)
{
  sndplay_cmd_add(sndp, SNDPLAY_CMD_RESET, 0, sndp->desc->boot_time);
}

void sndplay_play(hien_sndplay_t* sndp, int sndid)
{
  sndplay_stop(sndp);
  sndplay_cmd_add(sndp, SNDPLAY_CMD_PLAY, sndid, sndp->desc->chg_time);
}

void sndplay_stop(hien_sndplay_t* sndp)
{
  sndplay_cmd_add(sndp, SNDPLAY_CMD_STOP, 0, sndp->desc->chg_time);
}

int sndplay_waiting(hien_sndplay_t* sndp)
{
  return tmr_enabled(sndp->delay_tmr);
}

int sndplay_id_in_bounds(hien_sndplay_t* sndp, int next)
{
  if(next > sndp->desc->max_id)
    return FALSE;
  if(next < sndp->desc->min_id)
    return FALSE;

  return TRUE;
}
