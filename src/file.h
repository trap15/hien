/**********************************************************
*
* file.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _FILE_H_
#define _FILE_H_

#define FILE_READ      1
#define FILE_WRITE     2
#define FILE_MKDIR     4
#define FILE_READWRITE (FILE_READ | FILE_WRITE)

#define FILE_START 0
#define FILE_CUR   1
#define FILE_END   2

/* Typedefs */
typedef struct hien_file_t hien_file_t;

struct hien_file_t {
  char* usepath;
  FILE* fp;
};

hien_file_t* file_open(char* fn, int flags);
void file_close(hien_file_t* fp);
char* file_expand_path(char* path);

size_t file_read(void* buf, size_t size, hien_file_t* fp);
size_t file_write(void* buf, size_t size, hien_file_t* fp);

void file_seek(hien_file_t* fp, int whence, size_t offset);
size_t file_tell(hien_file_t* fp);
size_t file_size(hien_file_t* fp);

size_t file_write8(uint8_t data, hien_file_t* fp);
size_t file_write16(uint16_t data, hien_file_t* fp);
size_t file_write32(uint32_t data, hien_file_t* fp);
size_t file_write64(uint64_t data, hien_file_t* fp);
size_t file_read8(uint8_t* data, hien_file_t* fp);
size_t file_read16(uint16_t* data, hien_file_t* fp);
size_t file_read32(uint32_t* data, hien_file_t* fp);
size_t file_read64(uint64_t* data, hien_file_t* fp);

size_t file_write8B(uint8_t data, hien_file_t* fp);
size_t file_write16B(uint16_t data, hien_file_t* fp);
size_t file_write32B(uint32_t data, hien_file_t* fp);
size_t file_write64B(uint64_t data, hien_file_t* fp);
size_t file_read8B(uint8_t* data, hien_file_t* fp);
size_t file_read16B(uint16_t* data, hien_file_t* fp);
size_t file_read32B(uint32_t* data, hien_file_t* fp);
size_t file_read64B(uint64_t* data, hien_file_t* fp);

size_t file_write8L(uint8_t data, hien_file_t* fp);
size_t file_write16L(uint16_t data, hien_file_t* fp);
size_t file_write32L(uint32_t data, hien_file_t* fp);
size_t file_write64L(uint64_t data, hien_file_t* fp);
size_t file_read8L(uint8_t* data, hien_file_t* fp);
size_t file_read16L(uint16_t* data, hien_file_t* fp);
size_t file_read32L(uint32_t* data, hien_file_t* fp);
size_t file_read64L(uint64_t* data, hien_file_t* fp);

#ifndef NO_FILE_OVERRIDE

#undef fopen
#undef fread
#undef fwrite
#undef fseek
#undef fclose

#define fopen(x,y)      STATIC_ASSERT_ZERO(0, fopen_disabled__use_hien_file_functions)
#define fread(x,y,z,w)  STATIC_ASSERT_ZERO(0, fread_disabled__use_hien_file_functions)
#define fwrite(x,y,z,w) STATIC_ASSERT_ZERO(0, fwrite_disabled__use_hien_file_functions)
#define fseek(x,y,z)    STATIC_ASSERT_ZERO(0, fseek_disabled__use_hien_file_functions)
#define fclose(x)       STATIC_ASSERT_ZERO(0, fclose_disabled__use_hien_file_functions)

#endif

FILE* _yes_I_really_know_what_Im_doing_fopen(const char* path, const char* mode);
size_t _yes_I_really_know_what_Im_doing_fread(void* ptr, size_t size, size_t count, FILE* fp);
size_t _yes_I_really_know_what_Im_doing_fwrite(void* ptr, size_t size, size_t count, FILE* fp);
int _yes_I_really_know_what_Im_doing_fseek(FILE* fp, long int offset, int origin);
int _yes_I_really_know_what_Im_doing_fclose(FILE* fp);

#endif

