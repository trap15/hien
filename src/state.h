/**********************************************************
*
* state.h
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#ifndef _STATE_H_
#define _STATE_H_

typedef struct hien_state_t hien_state_t;
typedef struct hien_state_mgr_t hien_state_mgr_t;

enum {
  STATE_TYPE_NONE=0,
  STATE_TYPE_INT,
  STATE_TYPE_UINT,
  STATE_TYPE_FLOAT,
  STATE_TYPE_DOUBLE,
  STATE_TYPE_INT64,
  STATE_TYPE_INT32,
  STATE_TYPE_INT16,
  STATE_TYPE_INT8,
  STATE_TYPE_UINT64,
  STATE_TYPE_UINT32,
  STATE_TYPE_UINT16,
  STATE_TYPE_UINT8,
  STATE_TYPE_TIMETICK,
  STATE_TYPE_ADDR,
  STATE_TYPE_ANALOG,
};

struct hien_state_t {
  hien_state_mgr_t* mgr;

  uint32_t type;
  char* path;

  void* data;
  size_t size;
  size_t count;
};

hien_state_t* state_new(int type, char* path, void* data,  size_t count);
void state_delete(hien_state_t* sta);
int state_save(hien_vm_t* vm, char* name, hien_state_t* sta);
int state_load(hien_vm_t* vm, char* name, hien_state_t* sta);

struct hien_state_mgr_t {
  hien_vm_t* vm;

  kuso_pvec_t* states; /* hien_state_t* */
};

hien_state_mgr_t* state_mgr_new(hien_vm_t* vm);
void state_mgr_delete(hien_state_mgr_t* mgr);
void state_mgr_add(hien_state_mgr_t* mgr, hien_state_t* sta);
void state_mgr_remove(hien_state_mgr_t* mgr, hien_state_t* sta);
int state_mgr_save(hien_vm_t* vm, hien_state_mgr_t* mgr, char* name);
int state_mgr_load(hien_vm_t* vm, hien_state_mgr_t* mgr, char* name);

char* state_build_path(char* path, ...);
char* state_build_path_va(char* pathf, va_list path);

void state_add_int_arr_va(hien_state_mgr_t* mgr, size_t cnt, int* val, char* pathf, va_list path);
void state_add_uint_arr_va(hien_state_mgr_t* mgr, size_t cnt, unsigned int* val, char* pathf, va_list path);
void state_add_float_arr_va(hien_state_mgr_t* mgr, size_t cnt, float* val, char* pathf, va_list path);
void state_add_double_arr_va(hien_state_mgr_t* mgr, size_t cnt, double* val, char* pathf, va_list path);
void state_add_int64_arr_va(hien_state_mgr_t* mgr, size_t cnt, int64_t* val, char* pathf, va_list path);
void state_add_int32_arr_va(hien_state_mgr_t* mgr, size_t cnt, int32_t* val, char* pathf, va_list path);
void state_add_int16_arr_va(hien_state_mgr_t* mgr, size_t cnt, int16_t* val, char* pathf, va_list path);
void state_add_int8_arr_va(hien_state_mgr_t* mgr, size_t cnt, int8_t* val, char* pathf, va_list path);
void state_add_uint64_arr_va(hien_state_mgr_t* mgr, size_t cnt, uint64_t* val, char* pathf, va_list path);
void state_add_uint32_arr_va(hien_state_mgr_t* mgr, size_t cnt, uint32_t* val, char* pathf, va_list path);
void state_add_uint16_arr_va(hien_state_mgr_t* mgr, size_t cnt, uint16_t* val, char* pathf, va_list path);
void state_add_uint8_arr_va(hien_state_mgr_t* mgr, size_t cnt, uint8_t* val, char* pathf, va_list path);
void state_add_timetick_arr_va(hien_state_mgr_t* mgr, size_t cnt, timetick_t* val, char* pathf, va_list path);
void state_add_addr_arr_va(hien_state_mgr_t* mgr, size_t cnt, addr_t* val, char* pathf, va_list path);
void state_add_analog_arr_va(hien_state_mgr_t* mgr, size_t cnt, analog_t* val, char* pathf, va_list path);

#endif

