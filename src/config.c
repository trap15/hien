/**********************************************************
*
* config.c
*
* (C)2013-2014 Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
**********************************************************/

#include "hien.h"

#if MACOSX
#include <asl.h>
#include <unistd.h>

static int did_mac_init = FALSE;

/* FUCKING HURR DURR DURR DAMN IT */
void _handle_mac_bundle(char* app)
{
  if(did_mac_init)
    return;
  did_mac_init = TRUE;

  char* ncwd;
  char* ptr;
  char* fcwd;
  size_t len = strlen(app);
  ncwd = mem_alloc(len+1);
  strncpy(ncwd, app, len);
  ncwd[len] = '\0';

  /* Go to bundle.app/Contents/MacOS */
  ptr = strrchr(ncwd, '/');
  if(ptr != NULL) *ptr = '\0';

  /* Go to bundle.app/Contents */
  ptr = strrchr(ncwd, '/');
  if(ptr != NULL) *ptr = '\0';

  /* Go to bundle.app/Contents/Resources/ */
  kuso_asprintf(&fcwd, "%s/Resources/", ncwd);
  mem_free(ncwd);
  chdir(fcwd);
  mem_free(fcwd);

  aslmsg msg;
  aslclient c = asl_open("hien", "com.apple.console", 0);

  msg = asl_new(ASL_TYPE_MSG);
  asl_set(msg, ASL_KEY_FACILITY, "com.apple.console");
  asl_set(msg, ASL_KEY_LEVEL, ASL_STRING_NOTICE);
  asl_set(msg, ASL_KEY_READ_UID, "-1");

  int fd = dup(2);
  //asl_set_filter(c, ASL_FILTER_MASK_UPTO(ASL_LEVEL_DEBUG));
  asl_add_log_file(c, fd);
  asl_log_descriptor(c, msg, ASL_LEVEL_INFO, 1, ASL_LOG_DESCRIPTOR_WRITE);
  asl_log_descriptor(c, msg, ASL_LEVEL_INFO, 2, ASL_LOG_DESCRIPTOR_WRITE);
}
#endif

static int _cfg_change_loglevel(hien_config_t* conf, hien_cfg_entry_t* ent, char* val_s, int val_i, double val_d, void* data)
{
  (void)ent;
  (void)data;
  (void)val_i;
  (void)val_d;
  if(strcasecmp(val_s, "DEBUG") == 0) {
    conf->loglvl = LOG_DEBUG;
  }else if(strcasecmp(val_s, "WARN") == 0) {
    conf->loglvl = LOG_WARN;
  }else if(strcasecmp(val_s, "ERR") == 0) {
    conf->loglvl = LOG_ERR;
  }else
    return FALSE;
  return TRUE;
}

static int _cfg_change_path(hien_config_t* conf, hien_cfg_entry_t* ent, char* val_s, int val_i, double val_d, void* data)
{
  (void)data;
  (void)val_i;
  (void)val_d;
  if(strcmp(ent->path, "path.boards") == 0) {
    config_set_rom_path(conf, val_s);
  }else if(strcmp(ent->path, "path.states") == 0) {
    config_set_state_path(conf, val_s);
  }else
    return FALSE;
  return TRUE;
}

#if CPU_PROFILER
static int _cfg_change_proflist(hien_config_t* conf, hien_cfg_entry_t* ent, char* val_s, int val_i, double val_d, void* data)
{
  (void)data;
  (void)val_i;
  (void)val_d;
  if(strcmp(ent->path, "vm.profile") == 0) {
    config_set_profile_list(conf, val_s);
  }else
    return FALSE;
  return TRUE;
}
#endif

static int _cfg_change_int(hien_config_t* conf, hien_cfg_entry_t* ent, char* val_s, int val_i, double val_d, void* data)
{
  (void)data;
  (void)val_s;
  (void)val_d;
  if(strcmp(ent->path, "vm.sync-rate") == 0) {
    conf->sync_rate = val_i;
  }else if(strcmp(ent->path, "sound.sample-rate") == 0) {
    config_set_snd_samp_rate(conf, val_i);
  }else if(strcmp(ent->path, "sound.buffer-length") == 0) {
    config_set_snd_buflen(conf, val_i);
  }else
    return FALSE;
  return TRUE;
}

static int _cfg_change_dec(hien_config_t* conf, hien_cfg_entry_t* ent, char* val_s, int val_i, double val_d, void* data)
{
  (void)data;
  (void)val_s;
  (void)val_i;
  if(strcmp(ent->path, "sound.volume") == 0) {
    config_set_snd_volume(conf, val_d);
  }else if(strcmp(ent->path, "vm.speed") == 0) {
    conf->speed = val_d;
  }else
    return FALSE;
  return TRUE;
}

hien_cfg_entry_t _defaultcfgs[] = {
  { "log-level", CFG_TYPE_STR,
#if DEBUG
    "DEBUG",
#else
    "WARN",
#endif
    _cfg_change_loglevel, CFG_NOBOUND,CFG_NOBOUND, NULL },

  { "path.boards", CFG_TYPE_STR, "~/hien/roms",  _cfg_change_path, CFG_NOBOUND,CFG_NOBOUND, NULL },
  { "path.states", CFG_TYPE_STR, "~/hien/state", _cfg_change_path, CFG_NOBOUND,CFG_NOBOUND, NULL },

  { "vm.sync-rate", CFG_TYPE_INT, "4000000", _cfg_change_int, CFG_NOBOUND,0, NULL },
  { "vm.speed",     CFG_TYPE_DEC,     "1.0", _cfg_change_dec, CFG_NOBOUND,0, NULL },
#if CPU_PROFILER
  { "vm.profile",   CFG_TYPE_STR,        "", _cfg_change_proflist, CFG_NOBOUND,CFG_NOBOUND, NULL },
#endif

  { "sound.sample-rate",   CFG_TYPE_INT, "44100", _cfg_change_int,384000,4000, NULL },
  { "sound.buffer-length", CFG_TYPE_INT,   "512", _cfg_change_int,  8192,  32, NULL },
  { "sound.volume",        CFG_TYPE_DEC,   "1.0", _cfg_change_dec,    64,   0, NULL },

  { NULL, CFG_TYPE_NONE, NULL,  NULL, CFG_NOBOUND,CFG_NOBOUND, NULL },
};

hien_config_t* config_new(hien_vm_t* vm, char* file)
{
  hien_config_t* conf;

  conf = mem_zalloc_type(hien_config_t);

  conf->vm = vm;
  conf->rompath = NULL;
  conf->stapath = NULL;
  conf->board = NULL;

  conf->sync_rate = 4000000;

  conf->snd.rate = 32000;
  conf->snd.buflen = 512;

#if DEBUG
  conf->loglvl = LOG_DEBUG;
#else
  conf->loglvl = LOG_WARN;
#endif

  conf->cfglist = kuso_pvec_new(0);
  config_add_entries(conf, NULL, _defaultcfgs, NULL);

  if(file != NULL) {
    if(!config_load(conf, file)) {
      config_delete(conf);
      conf = config_new(vm, NULL);
    }
  }

  return conf;
}

void config_delete(hien_config_t* conf)
{
  hien_cfg_entry_t** iter = kuso_pvec_iter(conf->cfglist, hien_cfg_entry_t*);
  for(; *iter != NULL; iter++) {
    mem_free((*iter)->path);
    mem_free(*iter);
  }
  kuso_pvec_delete(conf->cfglist);
  if(conf->board != NULL)
    mem_free(conf->board);
  mem_free(conf->rompath);
  mem_free(conf->stapath);
  mem_free(conf);
}

void config_add_entry(hien_config_t* conf, char* pfx, hien_cfg_entry_t ent, void* data)
{
  hien_cfg_entry_t* rent;

  rent = mem_zalloc_type(hien_cfg_entry_t);
  if(pfx != NULL)
    kuso_asprintf(&rent->path, "%s.%s", pfx, ent.path);
  else
    kuso_asprintf(&rent->path, "%s", ent.path);

  rent->type = ent.type;
  rent->deflt = ent.deflt;
  rent->change = ent.change;
  rent->data = data;
  rent->max = ent.max;
  rent->min = ent.min;
  kuso_pvec_append(conf->cfglist, rent);
}

void config_add_entries(hien_config_t* conf, char* pfx, hien_cfg_entry_t* ents, void* data)
{
  hien_cfg_entry_t* ent;
  for(ent = ents; ent->type != CFG_TYPE_NONE; ent++) {
    config_add_entry(conf, pfx, *ent, data);
  }
}

void config_set_default(hien_config_t* conf)
{
  hien_cfg_entry_t** iter = kuso_pvec_iter(conf->cfglist, hien_cfg_entry_t*);
  for(; *iter != NULL; iter++) {
    config_handle_option(conf, -1, (*iter)->path, (*iter)->deflt);
  }
}

int config_load(hien_config_t* conf, char* file)
{
  (void)conf;
  (void)file;
  return 0;
}

int config_save(hien_config_t* conf, char* file)
{
  (void)conf;
  (void)file;
  return 0;
}

void config_set_rom_path(hien_config_t* conf, char* path)
{
  if(conf->rompath != NULL)
    mem_free(conf->rompath);
  conf->rompath = file_expand_path(path);
}
void config_set_state_path(hien_config_t* conf, char* path)
{
  if(conf->stapath != NULL)
    mem_free(conf->stapath);
  conf->stapath = file_expand_path(path);
}

void config_set_snd_samp_rate(hien_config_t* conf, int rate)
{
  conf->snd.rate = rate;
  osd_snd_change_settings(conf->vm);
}
void config_set_snd_buflen(hien_config_t* conf, int len)
{
  conf->snd.buflen = len;
  osd_snd_change_settings(conf->vm);
}

void config_set_snd_volume(hien_config_t* conf, double vol)
{
  conf->snd.volume = pow(vol, 2);
  osd_snd_change_settings(conf->vm);
}

static void _config_split_option(char* str, char** optname, char** optval)
{
  char* ptr;
  int done;

  for(done = FALSE, ptr = str; !done && *ptr != '\0'; ptr++) {
    switch(*ptr) {
      case '=': case ',':
        done = TRUE;
        break;
    }
  }
  if(*ptr == '\0') {
    *optval = NULL;
    *optname = mem_strdup(str);
  }else {
    *optval = mem_strdup(ptr);
    *optname = mem_strdup(str);
    (*optname)[ptr - str - 1] = '\0';
  }
}

int config_handle_option(hien_config_t* conf, int no, char* optname, char* optval)
{
  hien_cfg_entry_t* ent = NULL;
  hien_cfg_entry_t** iter = kuso_pvec_iter(conf->cfglist, hien_cfg_entry_t*);

  for(; *iter != NULL; iter++) {
    if(strcmp(optname, (*iter)->path) == 0) {
      ent = *iter;
      break;
    }
  }

  if(ent == NULL) {
    hienlog(conf->vm,LOG_ERR, "Unknown option %s\n", optname);
    return FALSE;
  }

  if(ent->type != CFG_TYPE_BOOL && no != -1) {
    if(no) {
      hienlog(conf->vm,LOG_ERR, "Prefix 'no-' only allowed on boolean configuration options.\n");
      return FALSE;
    }
    if(optval == NULL) {
      hienlog(conf->vm,LOG_ERR, "Only boolean configuration options may have no value.\n");
      return FALSE;
    }
  }

  switch(ent->type) {
    case CFG_TYPE_BOOL: {
      int res;
      if(no == 2)
        res = ent->deflt[0] == 'Y' ? 1 : 0;
      else
        res = no ? 0 : 1;

      if(!ent->change(conf, ent, res ? "Y" : "N", res, 0, ent->data)) {
        hienlog(conf->vm,LOG_ERR, "Error handling option %s with value %s\n", optname, no ? "N" : "Y");
        return FALSE;
      }
      break;
    }
    case CFG_TYPE_INT: {
      char* epos;
      int tmp = xtoi_full(optval, -1, &epos);
      if(*epos != '\0') {
        hienlog(conf->vm,LOG_ERR, "Option %s requires an integer parameter.\n", optname);
        return FALSE;
      }
      if(ent->max != CFG_NOBOUND && tmp > ent->max) {
        hienlog(conf->vm,LOG_ERR, "Option %s's parameter %d is too high (max: %d).\n", optname, tmp, ent->max);
        return FALSE;
      }
      if(ent->min != CFG_NOBOUND && tmp < ent->min) {
        hienlog(conf->vm,LOG_ERR, "Option %s's parameter %d is too low (min: %d).\n", optname, tmp, ent->min);
        return FALSE;
      }

      if(!ent->change(conf, ent, optval, tmp, 0, ent->data)) {
        hienlog(conf->vm,LOG_ERR, "Error handling option %s with value %s\n", optname, optval);
        return FALSE;
      }
      break;
    }
    case CFG_TYPE_DEC: {
      char* epos;
      char* xpos;
      double v;
      int tmp = xtoi_full(optval, -1, &epos);
      if(*epos != '\0' && *epos != '.') {
        hienlog(conf->vm,LOG_ERR, "Option %s requires a decimal parameter.\n", optname);
        return FALSE;
      }
      if(ent->max != CFG_NOBOUND && tmp > ent->max) {
        hienlog(conf->vm,LOG_ERR, "Option %s's parameter %d is too high (max: %d).\n", optname, tmp, ent->max);
        return FALSE;
      }
      if(ent->min != CFG_NOBOUND && tmp < ent->min) {
        hienlog(conf->vm,LOG_ERR, "Option %s's parameter %d is too low (min: %d).\n", optname, tmp, ent->min);
        return FALSE;
      }
      v = tmp;
      if(*epos != '\0') {
        int dist;
        tmp = xtoi_full(epos+1, -1, &xpos);
        if(*xpos != '\0') {
          hienlog(conf->vm,LOG_ERR, "Option %s requires a decimal parameter.\n", optname);
          return FALSE;
        }
        dist = xpos-(epos+1);
        v += (double)tmp / (double)pow(10, dist);
      }

      if(!ent->change(conf, ent, optval, v, v, ent->data)) {
        hienlog(conf->vm,LOG_ERR, "Error handling option %s with value %s\n", optname, optval);
        return FALSE;
      }
      break;
    }
    case CFG_TYPE_STR:
      if(!ent->change(conf, ent, optval, 0, 0, ent->data)) {
        hienlog(conf->vm,LOG_ERR, "Error handling option %s with value %s\n", optname, optval);
        return FALSE;
      }
      break;
    case CFG_TYPE_OTHER:
      if(!ent->change(conf, ent, optval, 0, 0, ent->data)) {
        hienlog(conf->vm,LOG_ERR, "Error handling option %s with value %s\n", optname, optval);
        return FALSE;
      }
      break;
  }
  return TRUE;
}

void config_handle_cmdline(hien_config_t* conf, int argc, char* argv[])
{
  char* str;
  int i;

  for(i = 1; i < argc; i++) {
    str = argv[i];
#if MACOSX
    if(strncmp(str, "-psn", 4) == 0) {
      _handle_mac_bundle(argv[0]);
      continue;
    }
#endif
    if(str[0] != '-' || str[1] != '-') {
      if(conf->board != NULL)
        mem_free(conf->board);
      conf->board = mem_strdup(str);
    }
  }
}

void config_handle_options(hien_config_t* conf, int argc, char* argv[])
{
  char* optname;
  char* optval;
  char* str;
  int no = 0;
  int i;

  for(i = 1; i < argc; i++) {
    str = argv[i];
#if MACOSX
    if(strncmp(str, "-psn", 4) == 0) {
      _handle_mac_bundle(argv[0]);
      continue;
    }
#endif
    if(str[0] == '-' && str[1] == '-') {
      str += 2;
      if(strncmp(str, "no-", 3) == 0) {
        str += 3;
        no = 1;
      }else{
        no = 0;
      }
      _config_split_option(str, &optname, &optval);
      hienlog(conf->vm,LOG_DEBUG, "Option %s[%s]=[%s]\n", no ? "NO" : "", optname, optval);
      if(!config_handle_option(conf, no, optname, optval)) {
        /* throw an error */
      }
      mem_free(optname); optname = NULL;
      mem_free(optval);  optval  = NULL;
    }
  }
}

char* config_make_rom_path(hien_config_t* conf, char* loc, char* label)
{
  char* str;
  str = NULL;
  kuso_asprintf(&str, "%s/%s/rom/%s.%s", conf->rompath, conf->board, loc, label);

  return str;
}

char* config_make_def_path(hien_config_t* conf, char* name, char* ext)
{
  char* str;
  str = NULL;
  kuso_asprintf(&str, "%s/%s/def/%s%s%s", conf->rompath, conf->board, name, ext ? "." : "", ext ?: "");

  return str;
}

char* config_make_state_path(hien_config_t* conf, char* name, char* path)
{
  char* str;
  str = NULL;
  kuso_asprintf(&str, "%s/%s/%s/%s", conf->stapath, conf->board, name, path);

  return str;
}

void config_set_profile_list(hien_config_t* conf, char* list)
{
#if CPU_PROFILER
  char* p;
  char* np;
  char* f;

  if(conf->vm == NULL || conf->vm->board == NULL || conf->vm->board->devs == NULL)
    return;

  char* mlist = mem_strdup(list);
  hien_dev_t** iter = kuso_pvec_iter(conf->vm->board->devs, hien_dev_t*);
  for(; *iter != NULL; iter++) {
    cpuprof_disable((*iter)->prof);
  }
  p = strtok(mlist, ",");
  while(p != NULL) {
    np = p+strlen(p);

    f = strtok(p, ":");
    iter = kuso_pvec_iter(conf->vm->board->devs, hien_dev_t*);
    for(; *iter != NULL; iter++) {
      if(strcmp((*iter)->name, f) == 0) {
        hienlog(conf->vm,LOG_DEBUG, "Enable profiling for %s\n", f);
        f = strtok(NULL, ":");
        hienlog(conf->vm,LOG_DEBUG, "File name is %s\n", f);
        cpuprof_enable((*iter)->prof, f);
      }
    }

    p = strtok(np, ",");
  }
#else
  (void)conf;
  (void)list;
#endif
}
