# Libs
addModuleNoHdr("lib", "kuso")

# Kiban
$libs += " -ltcc"
addModuleNoHdr("lib", "kiban")

# Header files
addHdr("analog.h")
addHdr("board.h")
addHdr("board_desc.h")
addHdr("config.h")
addHdr("cpuprof.h")
addHdr("dev.h")
addHdr("devinfo.h")
addHdr("file.h")
addHdr("hash.h")
addHdr("helper.h")
addHdr("hien.h")
addHdr("log.h")
addHdr("osd.h")
addHdr("rom.h")
addHdr("sndplay.h")
addHdr("sound.h")
addHdr("state.h")
addHdr("timer.h")
addHdr("timeunit.h")
addHdr("ui.h")
addHdr("vm.h")

# Util
addObj("file.o")
addObj("timeunit.o")
addObj("hash.o")
addObj("config.o")

# Core
addObj("timer.o")
addObj("analog.o")
addObj("dev.o")
addObj("devinfo.o")
addObj("state.o")
addObj("sndplay.o") # Sound player core
addObj("board.o")
addObj("board_desc.o")
addObj("sound.o")
addObj("log.o")
addObj("rom.o")
addObj("cpuprof.o")
addObj("vm.o")
addMainObj("vm_main.o")

# APU
addModule("apu", "adpcm")

addModule("apu", "ym2149")
addModule("apu", "opn")
addModule("apu", "oki6295")
addModule("apu", "sn76496")
addModule("apu", "x1_010")

# CPU
#addModule("cpu", "z80")
addModule("cpu", "z80_old")
addModule("cpu", "m6502")

# Other Device
addModule("dev", "nmk112")
addModule("dev", "s98core")

addModule("dev", "m68000")
addModule("dev", "nmk005")
addModule("dev", "nmk008")
addModule("dev", "nmk009")
addModule("dev", "nmk901")
addModule("dev", "nmk902")
addModule("dev", "nmk903")
addModule("dev", "nmk111")

addModule("dev", "clkport")
addModule("dev", "printer")

# Targets
addMainTarget("hien")

# Tools
addAltObj("tools/dummyosd.o")

addAltTarget("prof_view", "obj/tools/prof_view.o")
addBuildObj("tools/prof_view.o")

addDir("tools")
