.PHONY: all rsrc clean release

all: Makescript
	$(MAKE) -f Makescript all
test: all
	./test_mem
	./test_timer
	./test_sound
rsrc: Makescript
	$(MAKE) -f Makescript rsrc
clean: Makescript
	$(MAKE) -f Makescript clean
release: Makescript
	$(MAKE) -f Makescript release
	rm Makescript

Makescript: config.rb
	./config.rb

